import loramac
import binascii
# Joining with Over The Air Activation
# Set your device EUI, application EUI, application key:
deveui = binascii.unhexlify('AAAAAAAAAAAAAAAA')
appeui = binascii.unhexlify('BBBBBBBBBBBBBBBB')
appkey = binascii.unhexlify('CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC')

loramac.setDeveui(deveui)
loramac.setAppeui(appeui)
loramac.setAppkey(appkey)
loramac.setAdr(True)

# Join a network using the OTAA procedure:
err = loramac.joinOtaa()
if err == 0
    print('Join procedure succeeded!')
else 
    print('Join procedure failed!')

# Send confirmable data on port 2 (cnf and port are optional):
loramac.tx('This is RIOT',True,2)
