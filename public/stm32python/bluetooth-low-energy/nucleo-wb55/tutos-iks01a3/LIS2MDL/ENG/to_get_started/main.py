# Purpose of the script:
# Measures the magnetic field along 3 orthogonal axes (in µT) every quarter of a second.
# This example requires an X-NUCLEO IKS01A3 shield for the MEMS LIS2MDL magnetometer
# Earth field intensity: 30µT to 60µT (in theory)

from machine import I2C
import lis2mdl
from time import sleep_ms
from math import sqrt

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the magnetometer
magnetometer = lis2mdl.LIS2MDL(i2c)

while True:

	# Read the magnetic field on the three orthogonal axes
	magnetometer.get()
	bx = magnetometer.x()
	by = magnetometer.y()
	bz = magnetometer.z()

	# Display the components of the magnetic field (signed integers with 3 digits)
	print("Components of the magnetic field :")
	print(" - According to x : %6d µT" % bx)
	print(" - According to y : %6d µT" % by)
	print(" - According to z : %6d µT" % bz)

	# Modulus of the magnetic field
	modulus = sqrt(bx*bx + by*by + bz*bz)
	
	# Displays the modulus of the magnetic field with one decimal after the decimal point
	print("Modulus of the magnetic field : %.1f µT" % modulus)
	print("")
	sleep_ms(250)
