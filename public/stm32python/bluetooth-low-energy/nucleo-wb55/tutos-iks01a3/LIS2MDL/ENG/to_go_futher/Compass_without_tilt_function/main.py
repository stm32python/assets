# Purpose of the script:
# Simulation of a compass without tilt compensation using the LIS2MDL magnetometer.
# Before its implementation, you will have to calibrate the LIS2MDL with the appropriate script.
# Source explaining the calculation of the corrections on the magnetometer:
# https://appelsiini.net/2018/calibrate-magnetometer/

# This example requires an X-NUCLEO IKS01A3 shield for the LIS2MDL MEMS magnetometer
# Caution, it does not take into account the declination which must be added to the angle
# with the magnetic North Pole so that the compass points to the North Pole
# pole. The declination value depends on your geographical location.

from machine import I2C
import lis2mdl # Magnetometer driver
from time import sleep_ms
from math import atan2, pi

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the magnetometer
magnetometer = lis2mdl.LIS2MDL(i2c)

# Conversion factor between radians and degrees for angles
RadToDeg = 180 / ft

# Extreme values obtained by the calibration script

min_bx = const(-99)
max_bx = const(-12)
min_by = const(-28)
max_by = const(53)
min_bz = const(-45)
max_bz = const(47)

# Calculation of the "Hard Iron" offsets for each axis:

offset_x = (max_bx + min_bx) / 2
offset_y = (max_by + min_by) / 2
offset_z = (max_bz + min_bz) / 2

# Calculation of coefficients for the approximate correction of
# soft iron distortions for each axis:

avg_delta_x = (max_bx - min_bx) / 2
avg_delta_y = (max_by - min_by) / 2
avg_delta_z = (max_bz - min_bz) / 2

avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

scale_x = avg_delta / avg_delta_x
scale_y = avg_delta / avg_delta_y
scale_z = avg_delta / avg_delta_z

while True:

	# Read the magnetic field on the three orthogonal axes
	magnetometer.get()
	bx = magnetometer.x()
	by = magnetometer.y()
	bz = magnetometer.z()

	# We apply the hard iron and soft iron corrections.
	# This allows us to obtain the physical values of the magnetometer measurements in
	# micro teslas of the magnetometer measurements.
	
	bx = (bx - offset_x) * scale_x
	by = (by - offset_y) * scale_y
	bz = (bz - offset_z) * scale_z
		
	# Calculation of the orientation of the magnetic field vector with respect to
	# to the axis (Ox) (angle between the magnetic north and the axis (Ox)).
	
	angle_deg = atan2(by, bx) * RadToDeg

	# We do not want negative angles. When this happens we calculate the
	# the complement to 360° so that the reported angle varies between 0° and 360°.

	if angle_deg < 0:
		angle_deg += 360

	# Display the angular deviation from magnetic north
	print("Angular deviation from magnetic north: %.1f°" % angle_deg)
	print("")

	sleep_ms(250)
