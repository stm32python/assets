# Purpose of the script:
# Simulation of a compass with tilt compensation using the LIS2MDL magnetometer
# and the LIS2DW12 accelerometer.

# This example requires an X-NUCLEO IKS01A3 shield for the LIS2MDL
# and the LIS2DW12 accelerometer.

# The tilt compensation is calculated using the instructions in document AN3192
# of STMicroelectronics available for download on the STM32python website.
# The formulas had to be adapted (inversions and permutations of some axes) according to the explanations given on pages 19 and 20.
# the explanations given on pages 19 and 20 of AN3192 because the orientations
# of the MEMS axes are not the same on the LSM303DLH and on the LIS2MDL
# and LIS2DW12 components of the X-NUCLEO IKS01A3.

from machine import I2C
import lis2mdl # Magnetometer driver
import lis2dw12 # Compass driver
from time import sleep_ms # For time delays
from math import atan2, asin, cos, sin, pi, sqrt # Trigonometric functions

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the magnetometer
magnetometer = lis2mdl.LIS2MDL(i2c)

# Instantiation of the accelerometer
accelerometer = lis2dw12.LIS2DW12(i2c)

# Conversion factor between radians and degrees for angles
RadToDeg = 180 / ft

# Extreme values obtained by the calibration script

min_bx = const(-99)
max_bx = const(-12)
min_by = const(-28)
max_by = const(53)
min_bz = const(-45)
max_bz = const(47)

# Calculation of the "Hard Iron" offsets for each axis:

offset_x = (max_bx + min_bx) / 2
offset_y = (max_by + min_by) / 2
offset_z = (max_bz + min_bz) / 2

# Calculation of coefficients for the approximate correction of
# soft iron distortions for each axis:

avg_delta_x = (max_bx - min_bx) / 2
avg_delta_y = (max_by - min_by) / 2
avg_delta_z = (max_bz - min_bz) / 2

avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

scale_x = avg_delta / avg_delta_x
scale_y = avg_delta / avg_delta_y
scale_z = avg_delta / avg_delta_z

while True:

	# Read the magnetic field on the three orthogonal axes
	magnetometer.get()

	# Apply the hard iron and soft iron corrections and correct the
	# definitions of the magnetometer axes.
	bx = (magnetometer.x() - offset_x) * scale_x
	by = -1 * (magnetometer.y() - offset_y) * scale_y
	bz = (magnetometer.z() - offset_z) * scale_z

	# Norm of the magnetic field vector
	mag = sqrt(bx*bx + by*by + bz*bz)

	# We correct the definitions of the axes of the accelerometer.
	ax = accelerometer.y()
	ay = -1 * accelerometer.x()
	az = accelerometer.z()

	# Norm of the acceleration vector
	acc = sqrt(ax*ax + ay*ay + az*az)
	
	# If the norms are not zero
	if mag > 0 and acc > 0:

		# We normalize the components of the acceleration and magnetic field vectors
		#(essential for the calculation of arcsinus and arccosinus which follow)
		
		inv_acc = 1 / acc
		
		ax *= inv_acc
		ay *= inv_acc
		az *= inv_acc

		inv_mag = 1 / mag

		bx *= inv_mag
		by *= inv_mag
		bz *= inv_mag

		# Calculation of the Euler angles.
		# The three Euler angles (pitch, roll and yaw / heading) give the orientation of the magnetometer
		# in space. They must vary according to the rule given on page 19 of AN3192.
		
		pitch = asin(-ax)
		roll = asin(ay/cos(pitch))
	
		xh = bx * cos(pitch) + bz * sin(pitch)
		yh = bx * sin(roll) * sin(pitch) + by * cos(roll) - bz * sin(roll) * cos(pitch)
		#zh = -bx * cos(roll) * sin(pitch) + by * sin(roll) + bz * cos(roll) * cos(pitch)

		# Heading (or Yaw) : the heading (or yaw) we are looking for, the rotation around (Oz)
		heading = atan2(yh, xh)
		
		# Conversions in degrees
		pitch_deg = pitch * RadToDeg
		roll_deg = roll * RadToDeg
		heading_deg = heading * RadToDeg
	
		# We don't want negative headings. When this happens we calculate the
		# the complement to 360° so that the reported angle varies between 0° and 360°.
		if heading_deg < 0:
			heading_deg += 360

		# Display of Euler angles
		print("Pitch  = %.1f°" % pitch_deg)
		print("Roll  = %.1f°" % roll_deg)
		print("Heading  = %.1f°" % heading_deg)
		print("")

		sleep_ms(250)
