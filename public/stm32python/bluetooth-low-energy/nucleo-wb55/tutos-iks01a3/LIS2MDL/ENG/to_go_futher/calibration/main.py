# Purpose of the script:

# Obtaining the extreme values of the magnetic field measured by the LIS2MDL magnetometer
# along its x, y, z axes. These values are necessary to calibrate the LIS2MDL, in
# to remove the systematic shifts of its measurements along each axis.
# (Hard Iron and Soft Iron effects)

# You will need to run the script and, as long as the LED is blinking, move the
# card in large "8" movements. The six values that will be used for
# calibration of the LIS2MDL will finally be displayed on the serial port of the USB-USER.

# IMPORTANT :
# To calibrate the LIS2MDL for compass and heading tracking, you will need to
# Rotate the LIS2MDL in space with "figure 8" movements while the LED is blinking.
# The calibration process must be performed for each IKS01A3, each LIS2MDL
# has different offsets.
# The response of the LIS2MDL is strongly modified by all magnetic fields present.
# When using this script, take care to stay away from loudspeakers, magnets,
# metal masses containing iron or nickel and other electrical devices in operation that might
# electrical devices that could interfere with the process.

from machine import I2C
import lis2mdl # Library for the magnetometer
from time import sleep_ms
from math import sqrt
from pyb import Timer # Library to manage timers

# Timer 1 interrupt service routine (ISR), to blink LED 1.
def blink_LED1(timer):
	pyb.LED(1).toggle()

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the magnetometer
magnetometer = lis2mdl.LIS2MDL(i2c)

DUMMY_VALUE = const(1000) # Default value of the magnetic field amplitudes along each axis.
MAX_ITER = const(100) # Number of iterations for the calibration

# Initialization of the extreme values for the calibration

min_bx = DUMMY_VALUE
min_by = DUMMY_VALUE
min_bz = DUMMY_VALUE

max_bx = -DUMMY_VALUE
max_by = -DUMMY_VALUE
max_bz = -DUMMY_VALUE

print("Start of calibration")
	
tim1 = Timer(1, freq= 10) # Timer 1 frequency set to 10 Hz
tim1.callback(blink_LED1) # Call the ISR of the timer 1 overflow interrupt

for index in range (1, MAX_ITER):

	# Reads the magnetic field on the three orthogonal axes
	magnetometer.get()
	bx = magnetometer.x()
	by = magnetometer.y()
	bz = magnetometer.z()
	
	# At each iteration, we determine the min and max values of the magnetic field according to x,y and z
	
	min_bx = min(min_bx, bx)
	min_by = min(min_by, by)
	min_bz = min(min_bz, bz)

	max_bx = max(max_bx, bx)
	max_by = max(max_by, by)
	max_bz = max(max_bz, bz)

	# Modulus of the magnetic field
	modulus = sqrt(bx*bx + by*by + bz*bz)
	# Display the modulus of the magnetic field with a single decimal
	print("%3d" %index + " - Modulus of the magnetic field: " + "%.1f" % modulus + " µT")
	
	sleep_ms(250)

# Stop the timer which makes LED 1 blink
tim1.deinit()

# Display on the serial port of the extreme values for the calibration
print("End of calibration (µT)")
print("Values for calibration :")
print("min_bx : %3d, max_bx : %3d" % (min_bx, max_bx))
print("min_by : %3d, max_by : %3d" % (min_by, max_by))
print("min_bz : %3d, max_bz : %3d" % (min_bz, max_bz))
