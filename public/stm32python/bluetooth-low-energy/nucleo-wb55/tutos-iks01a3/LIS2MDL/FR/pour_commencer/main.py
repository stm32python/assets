# Objet du script :
# Mesure le champ magnétique selon 3 axes orthogonaux (en µT) tous les quarts de seconde. 
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le magnétomètre MEMS LIS2MDL 
# Intensité du champ terreste : 30µT à 60µT (en théorie)

from machine import I2C
import lis2mdl
from time import sleep_ms
from math import sqrt

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000) 

# Instanciation du magnétomètre
magnetometre = lis2mdl.LIS2MDL(i2c)

while True:

	# Lecture du champ magnétique sur les trois axes orthogonaux
	magnetometre.get()
	bx = magnetometre.x()
	by = magnetometre.y()
	bz = magnetometre.z()

	# Affiche les composantes du champ magnétique (entiers signés avec 3 digits)
	print("Composantes du champ magnétique :")
	print(" - Selon x : %6d µT" % bx)
	print(" - Selon y : %6d µT" % by)
	print(" - Selon z : %6d µT" % bz)

	# Module du champ magnétique
	module = sqrt(bx*bx + by*by + bz*bz)
	
	# Affiche le module du champ magnétique avec une décimale après la virgule
	print("Module du champ magnétique : %.1f µT" % module)
	print("")
	sleep_ms(250)