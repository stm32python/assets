# Objet du script :
# Simulation d'une boussole avec compensation d'inclinaison à l'aide du magnétomètre LIS2MDL
# et de l'accéléromètre LIS2DW12.

# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le magnétomètre LIS2MDL
# et l'accéléromètre LIS2DW12.

# La compensation d'inclinaison est calculée à l'aide des instructions du document AN3192 
# de STMicroelectronics disponible en téléchargement sur le site Web STM32python. 
# Les formules ont du être adaptées (inversions et permutations de certains axes) conformémet 
# aux explications données aux pages 19 et 20 de AN3192 car les orientations 
# des axes des MEMS ne sont pas les mêmes sur le LSM303DLH et sur les composants LIS2MDL 
# et LIS2DW12 de la X-NUCLEO IKS01A3.

from machine import I2C
import lis2mdl # Pilote du magnétomètre
import lis2dw12 # Pilote de la boussole
from time import sleep_ms # Pour les temporisations
from math import atan2, asin, cos, sin, pi, sqrt # Fonctions trigonométriques

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000) 

# Instanciation du magnétomètre
magnetometre = lis2mdl.LIS2MDL(i2c)

# Instanciation de l'accéléromètre
accelerometre = lis2dw12.LIS2DW12(i2c)

# Facteur de conversion entre les radians et les degrés pour les angles
RadToDeg = 180 / pi

# Valeurs extrêmes obtenues par le script de calibration

min_bx = const(-99)
max_bx = const(-12)
min_by = const(-28)
max_by = const(53)
min_bz = const(-45)
max_bz = const(47)

# Calcul des décalages (offsets) "Hard Iron" pour chaque axe :

offset_x = (max_bx + min_bx) / 2
offset_y = (max_by + min_by) / 2
offset_z = (max_bz + min_bz) / 2

# Calcul des coefficients pour la correction approximative des 
# distorsions "Soft Iron" pour chaque axe :

avg_delta_x = (max_bx - min_bx) / 2
avg_delta_y = (max_by - min_by) / 2
avg_delta_z = (max_bz - min_bz) / 2

avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

scale_x = avg_delta / avg_delta_x
scale_y = avg_delta / avg_delta_y
scale_z = avg_delta / avg_delta_z

while True:

	# Lecture du champ magnétique sur les trois axes orthogonaux
	magnetometre.get()

	# On applique les corrections hard iron et soft iron et on corrige les
	# définitions des axes du magnétomètre.
	bx = (magnetometre.x() - offset_x) * scale_x
	by = -1 * (magnetometre.y() - offset_y) * scale_y
	bz = (magnetometre.z() - offset_z) * scale_z

	# Norme du vecteur champ magnétique
	mag = sqrt(bx*bx + by*by + bz*bz)

	# On corrige les définitions des axes de l'accéléromètre.
	ax = accelerometre.y()
	ay = -1 * accelerometre.x()
	az = accelerometre.z()

	# Norme du vecteur accélération
	acc = sqrt(ax*ax + ay*ay + az*az)
	
	# Si les normes ne sont pas nulles
	if mag > 0 and acc > 0:

		# On normalise les composantes des vecteurs accélération et champ magnétique
		#(indispensable pour les calculs des arcsinus et arccosinus qui suivent)
		
		inv_acc = 1 / acc
		
		ax *= inv_acc
		ay *= inv_acc
		az *= inv_acc

		inv_mag = 1 / mag

		bx *= inv_mag
		by *= inv_mag
		bz *= inv_mag

		# Calcul des angles d'Euler.
		# Les trois angles d'Euler (pitch, roll et yaw / heading) donnent l'orientation du magnétomètre
		# dans l'espace. Ils doivent varier selon la règle donnée à la page 19 de AN3192.
		
		pitch = asin(-ax)
		roll = asin(ay/cos(pitch))
	
		xh = bx * cos(pitch) + bz * sin(pitch)
		yh = bx * sin(roll) * sin(pitch) + by * cos(roll) - bz * sin(roll) * cos(pitch)
		#zh = -bx * cos(roll) * sin(pitch) + by * sin(roll) + bz * cos(roll) * cos(pitch)

		# Heading (ou Yaw) : cap (ou lacet) que l'on recherche, la rotation autour de (Oz)
		heading = atan2(yh, xh)
		
		# Conversions en degrés 
		pitch_deg = pitch * RadToDeg
		roll_deg = roll * RadToDeg
		heading_deg = heading * RadToDeg
	
		# On ne veut pas de caps négatifs. Lorsque cela se produit on calcule le
		# le complément à 360° pour que l'angle reporté varie entre 0° et 360°.
		if heading_deg < 0:
			heading_deg += 360

		# Affichage des angles d'Euler
		print("Pitch (tangage) = %.1f°" % pitch_deg)
		print("Roll (roulis) = %.1f°" % roll_deg)
		print("Heading (cap) = %.1f°" % heading_deg)
		print("")

		sleep_ms(250)