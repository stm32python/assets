# Objet du script :

# Obtention des valeurs extrêmes du champ magnétique mesuré par le magnétomètre LIS2MDL 
# selon ses axes x, y, z. Ces valeurs sont nécessaires pour calibrer le LIS2MDL, en 
# l'occurence supprimer les décalages systématiques de ses mesures selon chaque axe.
# (effets "Hard Iron" et "Soft Iron")

# Vous devrez lancer le script et, aussi longtemps que la LED clignote déplacer la
# carte selon des mouvements amples en forme de "8". Les six valeurs qui serviront pour
# le calibrage du LIS2MDL seront finalement affichées sur le port série du USB-USER.

# IMPORTANT : 
# - Pour calibrer le LIS2MDL en vue d'en faire une boussole et suivre un cap, il faudra 
#  faire tourner le LIS2MDL dans l'espace avec des mouvements "en 8" pendant que la LED clignote. 
# - Le processus de calibration doit être réalisé pour chaque IKS01A3, chaque LIS2MDL
# ayant des décalages propres différents.
# - La réponse du LIS2MDL est fortement modifiée par tous les champs magnétiques présents.
# Lorsque vous utilisez ce script, prenez soin de vous éloigner des hauts parleurs, aimants, 
# masses métalliques contenant du fer ou du nikel et des autres appareils électriques en 
# fonctionnement qui pourraient perturber le processus.

from machine import I2C
import lis2mdl # Bibliothèque pour le magnétomètre
from time import sleep_ms
from math import sqrt
from pyb import Timer # Bibliothèque pour gérer les timers

# Routine de service de l'interruption (ISR) du Timer 1, pour faire clignoter la LED 1.
def blink_LED1(timer):
	pyb.LED(1).toggle()

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000) 

# Instanciation du magnétomètre
magnetometre = lis2mdl.LIS2MDL(i2c)

DUMMY_VALUE = const(1000) # Valeur par défaut des amplitudes du champ magnétique selon chaque axe.
MAX_ITER = const(100) # Nombre d'itérations pour le calibrage

# Initialisation des valeurs extêmes pour le calibrage

min_bx = DUMMY_VALUE
min_by = DUMMY_VALUE
min_bz = DUMMY_VALUE

max_bx = -DUMMY_VALUE
max_by = -DUMMY_VALUE
max_bz = -DUMMY_VALUE

print("Démarrage du calibrage")
	
tim1 = Timer(1, freq= 10) # Fréquence du timer 1 fixée à 10 Hz
tim1.callback(blink_LED1) # Appelle l'ISR de l'interruption de dépassement du timer 1

for index in range (1, MAX_ITER):

	# Lecture du champ magnétique sur les trois axes orthogonaux
	magnetometre.get()
	bx = magnetometre.x()
	by = magnetometre.y()
	bz = magnetometre.z()
	
	# A chaque itération, on détermine les valeurs min et max du champ magnétique selon x,y et z
	
	min_bx = min(min_bx, bx)
	min_by = min(min_by, by)
	min_bz = min(min_bz, bz)

	max_bx = max(max_bx, bx)
	max_by = max(max_by, by)
	max_bz = max(max_bz, bz)

	# Module du champ magnétique
	module = sqrt(bx*bx + by*by + bz*bz)
	# Affiche le module du champ magnétique avec une décimale après la virgule
	print("%3d" %index + " - Module du champ magnétique : " + "%.1f" % module + " µT")
	
	sleep_ms(250)

# Arrêt du timer qui fait clignoter la LED 1
tim1.deinit() 

# Affichage sur le port série des valeurs extêmes pour le calibrage
print("Fin calibrage (µT)")
print("Valeurs pour le calibrage :")
print("min_bx : %3d, max_bx : %3d" % (min_bx, max_bx))
print("min_by : %3d, max_by : %3d" % (min_by, max_by))
print("min_bz : %3d, max_bz : %3d" % (min_bz, max_bz))
