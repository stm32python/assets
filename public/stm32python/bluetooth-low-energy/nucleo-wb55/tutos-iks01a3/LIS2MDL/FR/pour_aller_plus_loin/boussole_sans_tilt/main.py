# Objet du script :
# Simulation d'une boussole sans compensation d'inclinaison à l'aide du magnétomètre LIS2MDL.
# Avant sa mise en oeuvre, vous devrez calibrer le LIS2MDL avec le script adéquat.
# Source expliquant le calcul des corrections sur le magnétomètre : 
# https://appelsiini.net/2018/calibrate-magnetometer/

# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le magnétomètre MEMS LIS2MDL
# Attention, il ne tient pas compte de la déclinaison qui doit être ajoutée à l'angle
# avec le Pôle Nord magnétique pour que la boussole pointe vers le Pôle Nord
# géographique. La valeur de la déclinaison dépend de votre localisation géographique.

from machine import I2C
import lis2mdl # Pilote du magnétomètre
from time import sleep_ms
from math import atan2, pi

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000) 

# Instanciation du magnétomètre
magnetometre = lis2mdl.LIS2MDL(i2c)

# Facteur de conversion entre les radians et les degrés pour les angles
RadToDeg = 180 / pi

# Valeurs extrêmes obtenues par le script de calibration

min_bx = const(-99)
max_bx = const(-12)
min_by = const(-28)
max_by = const(53)
min_bz = const(-45)
max_bz = const(47)

# Calcul des décalages (offsets) "Hard Iron" pour chaque axe :

offset_x = (max_bx + min_bx) / 2
offset_y = (max_by + min_by) / 2
offset_z = (max_bz + min_bz) / 2

# Calcul des coefficients pour la correction approximative des 
# distorsions "Soft Iron" pour chaque axe :

avg_delta_x = (max_bx - min_bx) / 2
avg_delta_y = (max_by - min_by) / 2
avg_delta_z = (max_bz - min_bz) / 2

avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

scale_x = avg_delta / avg_delta_x
scale_y = avg_delta / avg_delta_y
scale_z = avg_delta / avg_delta_z

while True:

	# Lecture du champ magnétique sur les trois axes orthogonaux
	magnetometre.get()
	bx = magnetometre.x()
	by = magnetometre.y()
	bz = magnetometre.z()

	# On applique les corrections hard iron et soft iron.
	# Ceci permet d'obtenir les valeurs physiques dimensionnées en
	# micro teslas des mesures du magnétomètre.
	
	bx = (bx - offset_x) * scale_x
	by = (by - offset_y) * scale_y
	bz = (bz - offset_z) * scale_z
		
	# Calcul de l'orientation du vecteur champ magnétique par rapport 
	# à l'axe (Ox) (angle entre le nord magnétique et l'axe (Ox)).
	
	angle_deg = atan2(by, bx) * RadToDeg

	# On ne veut pas d'angles négatifs. Lorsque cela se produit on calcule le
	# le complément à 360° pour que l'angle reporté varie entre 0° et 360°.

	if angle_deg < 0:
		angle_deg += 360

	# Affichage de l'écart angulaire avec le nord magnétique
	print("Ecart angulaire avec le nord magnétique : %.1f°" % angle_deg)
	print("")

	sleep_ms(250)