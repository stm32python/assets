# Objet du script :
# Mesure l'altitude toutes les cinq secondes.
# Cet exemple utilise comme paramètre la valeur de la pression atmosphérique rapportée au niveau de la
# mer pour le lieu où vous vous trouvez au cours de la dernière heure (variable LocalSeaLevelPress).
# Cette valeur n'est pas constante et il sera nécessaire de l'ajuster d'heure en heure pour intégrer
# l'évolution des conditions météorologiques locales et les compenser dans l'estimation de l'altitude.
# URL d'un site Internet qui donne les valeurs de LocalSeaLevelPress toutes les 10 minutes en France :
# https://www.infoclimat.fr/cartes/observations-meteo/temps-reel/pression-au-niveau-de-la-mer/france.html
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le baromètre MEMS LPS22.

from machine import I2C
import lps22
from time import sleep_ms

# Pression au niveau de la mer dans la région "en ce moment"
LocalSeaLevelPress = 1014.1 # en hPa

# Calcul de l'altitude
def getAltitude(pressure, sea_lvl_press ):
	return 44330.0*(1-pow((pressure)/(sea_lvl_press),1.0/5.255));
	
# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation du capteur
sensor = lps22.LPS22(i2c)

while True:

	# Temporisation de 5 secondes
	sleep_ms(5000)
	
	# Calcul de l'altitude intégrant la correction de pression
	altitude = getAltitude(sensor.pressure(), LocalSeaLevelPress)
	
	# Affichage sur le port série de l'USB USER
	print("Altitude : %.1f m" % altitude)


