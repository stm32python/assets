# Objet du script :
# Mesure la température et la pression toutes les cinq secondes.
# Rapporte la pression au niveau de la mer sur la base de l'altitude du lieu.
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le baromètre MEMS LPS22.

from machine import I2C
import lps22 # Pilote du baromètre
import pyb # Pour piloter les LED
from time import sleep_ms

altitude_locale = 485 # en mètres

# Pression corrigée au niveau de la mer
def SeaLevelPressure(pressure, altitude):
	return pressure * pow(1.0 - (altitude * 2.255808707E-5), -5.255)
	
# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation du capteur
sensor = lps22.LPS22(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	temp = round(sensor.temperature())
	press = round(SeaLevelPressure(sensor.pressure(),altitude_locale))

	print("Température : " + str(temp) + "°C, Pression : " + str(press) + " hPa : ", end = '')

	if temp > 25 :
		led_rouge.on()
		print("chaud")
	elif temp > 18 and temp <= 25 :
		led_vert.on()
		print("confortable")
	else:
		led_bleu.on()
		print("froid")

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
	
	sleep_ms(5000)

