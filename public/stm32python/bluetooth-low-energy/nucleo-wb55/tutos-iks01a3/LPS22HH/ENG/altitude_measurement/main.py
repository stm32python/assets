# Purpose of the script:
# Measures altitude every five seconds.
# This example uses as a parameter the value of the air pressure referred to the sea level for the
# This example uses as a parameter the value of the air pressure referred to sea level for the place where you are located during the last hour (LocalSeaLevelPress variable).
# This value is not constant and it will be necessary to adjust it from hour to hour to incorporate 
# the evolution of local weather conditions and compensate for them in the altitude estimate.
# URL of a website that gives LocalSeaLevelPress values every 10 minutes in France:
# https://www.infoclimat.fr/cartes/observations-meteo/temps-reel/pression-au-niveau-de-la-mer/france.html
# This example requires an X-NUCLEO IKS01A3 shield for the MEMS LPS22 barometer.

from machine import I2C
import lps22
from time import sleep_ms

# Sea level pressure in the region "right now
LocalSeaLevelPress = 1014.1 # in hPa

# Calculation of the altitude
def getAltitude(pressure, sea_lvl_press ):
	return 44330.0*(1-pow((pressure)/(sea_lvl_press),1.0/5.255));
	
# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the sensor 
sensor = lps22.LPS22(i2c)

while True:

	# Delay of 5 seconds
	sleep_ms(5000)
	
	# Compute altitude with pressure correction
	altitude = getAltitude(sensor.pressure(), LocalSeaLevelPress)
	
	# Display on the USB USER serial port
	print("Altitude : %.1f m" % altitude)


