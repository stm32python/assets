# Purpose of the script:
# Measures temperature and pressure every five seconds.
# Reports the pressure at sea level based on the altitude of the location.
# This example requires an X-NUCLEO IKS01A3 shield for the MEMS LPS22 barometer.

from machine import I2C
import lps22 # Driver of the barometer
import pyb # To drive the LEDs
from time import sleep_ms

altitude_local = 485 # in meters

# Corrected pressure at sea level
def SeaLevelPressure(pressure, altitude):
	return pressure * pow(1.0 - (altitude * 2.255808707E-5), -5.255)
	
# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the sensor
sensor = lps22.LPS22(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	temp = round(sensor.temperature())
	press = round(SeaLevelPressure(sensor.pressure(),altitude_local))

	print("Temperature : " + str(temp) + "°C, Pressure : " + str(press) + "hPa : ", end = '')

	if temp > 25 :
		led_red.on()
		print("hot")
	elif temp > 18 and temp <= 25 :
		led_green.on()
		print("comfortable")
	else:
		led_blue.on()
		print("cold")

	led_red.off()
	led_green.off()
	led_blue.off()
	
	sleep_ms(5000)
