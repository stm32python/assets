# Objet du script :
# Mesure les accélérations suivant 3 axes orthogonaux (en mg) toutes les demi secondes. 
# Allume ou éteint les LED de la carte selon les valeurs des accélérations
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour l'accéléromètre MEMS LIS2DW12 

from machine import I2C
import pyb # Pour gérer les LED
import lis2dw12 # Pilote de l'accéléromètre
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation de l'accéléromètre
accelerometre = lis2dw12.LIS2DW12(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:
	sleep_ms(500)
	if abs(accelerometre.x()) > 700 : # Si la valeur absolue de l'accélération sur l'axe x est supérieure à 700 mg alors
		led_vert.on()
		print("led_vert.on")
	else:
		led_vert.off()
	if abs(accelerometre.y()) > 700 : # Si la valeur absolue de l'accélération sur l'axe y est supérieure à 700 mg alors
		led_bleu.on()
		print("led_bleu.on")
	else:
		led_bleu.off()
	if abs(accelerometre.z()) > 700 : # Si la valeur absolue de l'accélération sur l'axe z est supérieure à 700 mg alors
		led_rouge.on()
		print("led_rouge.on")
	else:
		led_rouge.off()