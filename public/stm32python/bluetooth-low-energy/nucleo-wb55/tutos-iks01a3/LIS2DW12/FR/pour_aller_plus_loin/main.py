# Objet du script :
# Mesure les accélérations suivant 3 axes orthogonaux (en mg) toutes les quarts de seconde. 
# Utilise cette mesure pour évaluer l'inclinaison du plan de l'accéléromètre par rapport au plan horizontal (tilt)
# Source : Freescale AN3461 rev 2 (page 4)
# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour l'accéléromètre MEMS LIS2DW12.

from machine import I2C
import lis2dw12 # Pilote de l'accéléromètre
from time import sleep_ms # Pour temporiser
from math import sqrt, atan, pi # Fonctions et constantes trigonométriques

# Facteur de conversion entre les radians et les degrés pour les angles
RadToDeg = 180 / pi

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation de l'accéléromètre
accelerometre = lis2dw12.LIS2DW12(i2c)

while True:

	# Relevé des accélération
	ax = accelerometre.x()
	ay = accelerometre.y()
	az = accelerometre.z()
	
	# Angle entre l'axe des x et le sol
	denom = sqrt(ay*ay + az*az)
	if denom != 0:
		rho = atan(ax/denom) * RadToDeg
		print("Angle entre l'axe des x et le plan horizontal : %.1f°" % rho)

	# Angle entre l'axe des y et le sol
	denom = sqrt(ax*ax + az*az)
	if denom != 0:
		phi = atan(ay/denom) * RadToDeg
		print("Angle entre l'axe des y et le plan horizontal : %.1f°" % phi)
	
	# Angle entre l'axe des z et la verticale
	if az != 0:
		rho = atan(sqrt(ax*ax + ay*ay)/az) * RadToDeg
		print("Angle entre l'axe des z et la direction verticale : %.1f°" % rho)

	print("")
	
	sleep_ms(250)