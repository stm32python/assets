# Purpose of the script:
# Measures accelerations along 3 orthogonal axes (in mg) every quarter of a second. 
# Use this measurement to evaluate the tilt of the accelerometer plane with respect to the horizontal plane (tilt)
# Source : Freescale AN3461 rev 2 (page 4)
# This example requires an X-NUCLEO IKS01A3 shield for the LIS2DW12 MEMS accelerometer.

from machine import I2C
import lis2dw12 # Accelerometer driver
from time import sleep_ms # To delay
from math import sqrt, atan, pi # Trigonometric functions and constants

# Conversion factor between radians and degrees for angles
RadToDeg = 180 / pi

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1) 

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the accelerometer
accelerometer = lis2dw12.LIS2DW12(i2c)

while True:

	# Acceleration reading
	ax = accelerometer.x()
	ay = accelerometer.y()
	az = accelerometer.z()
	
	# Angle between the x axis and the ground
	denom = sqrt(ay*ay + az*az)
	if denom != 0:
		rho = atan(ax/denom) * RadToDeg
		print("Angle between the x-axis and the horizontal plane: %.1f°" % rho)

	# Angle between the y axis and the ground
	denom = sqrt(ax*ax + az*az)
	if denom != 0:
		phi = atan(ay/denom) * RadToDeg
		print("Angle between the y-axis and the horizontal plane: %.1f°" % phi)
	
	# Angle between the z axis and the vertical plane
	if az != 0:
		rho = atan(sqrt(ax*ax + ay*ay)/az) * RadToDeg
		print("Angle between the z-axis and the vertical direction: %.1f°" % rho)

	print("")
	
	sleep_ms(250)