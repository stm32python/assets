# Purpose of the script :
# Measures accelerations along 3 orthogonal axes (in mg) every half second.
# Turns on or off the LEDs of the board according to the acceleration values
# This example requires an X-NUCLEO IKS01A3 shield for the LIS2DW12 MEMS accelerometer

from machine import I2C
import pyb # To manage the LEDs
import lis2dw12 # Driver of the accelerometer
from time import sleep_ms # To delay

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Instantiate the accelerometer
accelerometer = lis2dw12.LIS2DW12(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:
	sleep_ms(500)
	if abs(accelerometer.x()) > 700 : # If the absolute value of the acceleration on the x axis is greater than 700 mg then
		led_green.on()
		print("led_green.on")
	else:
		led_green.off()
	if abs(accelerometer.y()) > 700 : # If the absolute value of the acceleration on the y axis is greater than 700 mg then
		led_blue.on()
		print("led_blue.on")
	else:
		led_blue.off()
	if abs(accelerometer.z()) > 700 : # If the absolute value of the acceleration on the z axis is higher than 700 mg then
		led_red.on()
		print("led_red.on")
	else:
		led_red.off()
