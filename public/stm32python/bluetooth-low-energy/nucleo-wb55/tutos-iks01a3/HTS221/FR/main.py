# Objet du script :
# Mesure la température (en degrés celsius) et l'humidité relative (en %) de l'air ambiant toutes les secondes
# Affiche la température et l'humidité sur le terminal de l'USB USER
# Allume ou éteint les LED de la carte selon la température

# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le capteur MEMS HTS221 de température et d'humidité relative 

from machine import I2C
import hts221
import pyb
from time import sleep_ms

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Initialisation du capteur de température et d'humidité relative
sensor = hts221.HTS221(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	sleep_ms(1000)
	
	temp = round(sensor.temperature(),1)
	humi = round(sensor.humidity(),1)

	print("Température : " + str(temp) + "°C, Humidité relative : " + str(humi) + "% : ", end ='')

	if temp > 25 :
		led_rouge.on()
		print("chaud")
	elif temp > 18 and temp <= 25 :
		led_vert.on()
		print("confortable")
	else:
		led_bleu.on()
		print("froid")

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
