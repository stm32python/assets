# Purpose of the script:
# Measures the temperature (in degrees Celsius) and relative humidity (in %) of the ambient air every second
# Displays the temperature and humidity on the USB USER terminal
# Turns on or off the LEDs of the board according to the temperature

# This example requires an X-NUCLEO IKS01A3 shield for the MEMS HTS221 temperature and relative humidity sensor 

from machine import I2C
import hts221
import pyb
from time import sleep_ms

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1) 

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Initialization of the temperature and relative humidity sensor
sensor = hts221.HTS221(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	sleep_ms(1000)

	temp = round(sensor.temperature(),1)
	humi = round(sensor.humidity(),1)

	print("Temperature : " + str(temp) + "°C, Relative humidity : " + str(humi) + "% : ", end ='')

	if temp > 25 :
		led_red.on()
		print("hot")
	elif temp > 18 and temp <= 25 :
		led_green.on()
		print("comfortable")
	else:
		led_blue.on()
		print("cold")

	led_red.off()
	led_green.off()
	led_blue.off()
