# Objet du script :
# Mesure de température toutes les secondes.
# Allume ou éteint les LED de la carte selon les valeurs des températures, les affiche sur le port série

# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le capteur MEMS de température STTS751.

from machine import I2C # Pilote du contrôleur I2C
import stts751 # Pilote du STTS751
import pyb # Pour gérer les GPIO
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

sensor = stts751.STTS751(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	sleep_ms(1000)
	temp = round(sensor.temperature(),1)
 
	print("Température : " + str(temp) + "°C (", end='')

	if temp > 25 :
		led_rouge.on()
		print("chaud)")
	elif temp > 18 and temp <= 25 :
		led_vert.on()
		print("confortable)")
	else:
		led_bleu.on()
		print("froid)")

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
