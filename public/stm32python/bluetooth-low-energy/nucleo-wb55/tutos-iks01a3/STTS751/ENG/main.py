# Purpose of the script :
# Temperature measurement every second.
# Turns on or off the LEDs of the board according to the temperature values, displays them on the serial port

# This example requires an X-NUCLEO IKS01A3 shield for the STTS751 MEMS temperature sensor.

from machine import I2C # I2C controller driver
import stts751 # STTS751 driver
import pyb # To manage the GPIO
from time import sleep_ms # To delay

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

sensor = stts751.STTS751(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	sleep_ms(1000)
	temp = round(sensor.temperature(),1)

	print("Temperature : " + str(temp) + "°C (", end='')

	if temp > 25 :
		led_red.on()
		print("hot)")
	elif temp > 18 and temp <= 25 :
		led_green.on()
		print("comfortable)")
	else:
		led_blue.on()
		print("cold)")

	led_red.off()
	led_green.off()
	led_blue.off()
