# Objet du script :
# Mesures d'accélérations et de rotations selon trois axes orthogonaux tous les dixièmes de seconde.
# Allume ou éteint les LED de la carte selon les valeurs des accélérations
# Affiche les vitesses de rotation angulaires sur le port série

# Cet exemple nécessite un shield X-NUCLEO IKS01A3 pour le capteur MEMS - centrale à inertie LSM6DSO 

from machine import I2C
import lsm6dso
import pyb
from time import sleep_ms

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
intertial_sensor = lsm6dso.LSM6DSO(i2c)
intertial_sensor.scale_g('2000') # Moindre sensibilité pour les mesures angulaires
intertial_sensor.scale_a('2g') # Sensibilité maximum pour les mesures d'accélérations

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	sleep_ms(100)

	# Accéléromètre
	ax = intertial_sensor.ax()
	ay = intertial_sensor.ay()
	az = intertial_sensor.az()

	# Gyroscope
	gx = intertial_sensor.gx()
	gy = intertial_sensor.gy()
	gz = intertial_sensor.gz()

	if abs(ax) > 700 : # Si la valeur absolue de l'accélération sur l'axe X est supérieur à 700 mG alors
		led_vert.on()
		print("ax : " + str(ax) + " mg")
	else:
		led_vert.off()
	if abs(ay) > 700 : # Si la valeur absolue de l'accélération sur l'axe Y est supérieur à 700 mG alors
		led_bleu.on()
		print("ay : " + str(ay) + " mg")
	else:
		led_bleu.off()
	if abs(az) > 700 : # Si la valeur absolue de l'accélération sur l'axe Z est supérieur à 700 mG alors
		led_rouge.on()
		print("az : " + str(az) + " mg")
	else:
		led_rouge.off()

	print("Gx = " + str(gx/1000) + " Rad/s")
	print("Gy = " + str(gy/1000) + " Rad/s")
	print("Gz = " + str(gz/1000) + " Rad/s")
