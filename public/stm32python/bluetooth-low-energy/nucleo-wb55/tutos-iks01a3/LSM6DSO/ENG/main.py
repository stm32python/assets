# Purpose of the script:
# Measures accelerations and rotations along three orthogonal axes every tenth of a second.
# Turns on or off the LEDs of the board according to the acceleration values
# Displays the angular rotation speeds on the serial port

# This example requires an X-NUCLEO IKS01A3 shield for the MEMS sensor - inertial unit LSM6DSO

from machine import I2C
import lsm6dso
import pyb
from time import sleep_ms

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
intertial_sensor = lsm6dso.LSM6DSO(i2c)
intertial_sensor.scale_g('2000') # Lower sensitivity for angular measurements
intertial_sensor.scale_a('2g') # Maximum sensitivity for acceleration measurements

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	sleep_ms(100)

	# Accelerometer
	ax = intertial_sensor.ax()
	ay = intertial_sensor.ay()
	az = intertial_sensor.az()

	# Gyroscope
	gx = intertial_sensor.gx()
	gy = intertial_sensor.gy()
	gz = intertial_sensor.gz()

	if abs(ax) > 700 : # If the absolute value of the acceleration on the X axis is higher than 700 mG then
		led_green.on()
		print("ax : " + str(ax) + " mg")
	else:
		led_green.off()
	if abs(ay) > 700 : # If the absolute value of the acceleration on the Y axis is higher than 700 mG then
		led_blue.on()
		print("ay : " + str(ay) + " mg")
	else:
		led_blue.off()
	if abs(az) > 700 : # If the absolute value of the acceleration on the Z axis is higher than 700 mG then
		led_red.on()
		print("az : " + str(az) + " mg")
	else:
		led_red.off()

	print("Gx = " + str(gx/1000) + " Rad/s")
	print("Gy = " + str(gy/1000) + " Rad/s")
	print("Gz = " + str(gz/1000) + " Rad/s")
