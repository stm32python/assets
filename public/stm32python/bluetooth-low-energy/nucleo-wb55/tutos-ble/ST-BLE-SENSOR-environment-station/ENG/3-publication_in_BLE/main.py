# Reading of two sensors of the IKS01A3 shield and display on the serial port of the USB USER
# Display of temperature, humidity and pressure values on the Grove RGB LCD
# Adaptation of the LCD backlight color according to the temperature
# RF Casting of the measurements with the BLE radio of the STM32WB55
# Pressure correction by relating it to the sea level
# Data are processed by the ST BLE Sensor smartphone application
# Hardware:
# - A NUCLEO-WB55 board
# - A Grove Base Shield For Arduino
# - A shield X-NUCLEO IKS01A3
# - A Grove RGB LCD (I2C)

# Corrected pressure at sea level (requires local altitude value)
alti = 485
def SeaLevelPressure(pressure, altitude):
	return pressure * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

from machine import I2C

import hts221
import lps22
from time import sleep_ms, time # To manage timings and time stamps
import i2c_lcd
import ble_sensor # To implement the GATT protocol for Blue-ST
import bluetooth # "BLE primitives" classes
# (see https://docs.micropython.org/en/latest/library/ubluetooth.html)

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensors
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instances of the sensors
sensor_hts221 = hts221.HTS221(i2c)
sensor_lps22 = lps22.LPS22(i2c)

#Instance of the Display class
lcd = i2c_lcd.Display(i2c)
lcd.color(255,255,255) #Back light of the LCD : white

# Reposition the cursor of the LCD display to the top left
lcd.home()

# Instance of the BLE class
ble = bluetooth.BLE()
ble_device = ble_sensor.BLESensor(ble)

while True:

	# Read the sensors
	temp = sensor_hts221.temperature()
	humi = sensor_hts221.humidity()
	pres = PressureNivMer(sensor_lps22.pressure(), alti)

	# Convert the values returned by the sensors into text
	stemp = str(round(temp,1))
	shumi = str(int(humi))
	spres = str(int(pres))

	# Display on the serial port of the USB USER
	print("Temperature : " + stemp + " °C, Relative humidity : " + shumi + " %, Pressure : " + spres + " hPa")

	# Preparation of data for sending in BLE.
	# The Blue-ST protocol codes temperatures, pressures and humidities as integers.
	# So we multiply the different measurements by 10 or 100 to keep decimals before
	# rounding to the nearest integer.
	# For example if temp = 18.45°C => we send ble_temp = 184.
	ble_pres = int(pres*100)
	ble_humi = int(humi*10)
	ble_temp = int(temp*10)
	timestamp = time()

	# Send data in BLE
	ble_device.set_data_env(timestamp, ble_pres, ble_humi, ble_temp, True)

	# Adapts the color of the LCD backlight according to the temperature read
	if temp > 25 :
		lcd.color(255,0,0)
	elif temp > 15 and temp <= 25 :
		lcd.color(255,255,255)
	else:
		lcd.color(0,0,255)

	# Writing on the LCD
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Temperature (C)')
	lcd.move(0,1)
	lcd.write(stemp)
	sleep_ms(1000)

	lcd.clear()
	lcd.move(0,0)
	lcd.write('Humidity (%)')
	lcd.move(0,1)
	lcd.write(shumi)
	sleep_ms(1000)

	lcd.clear()
	lcd.move(0,0)
	lcd.write('Pressure (hPa)')
	lcd.move(0,1)
	lcd.write(spres)
	sleep_ms(1000)
