# Purpose of the script: Implementation of the Blue-ST protocol for a peripheral
# Definition of a _ST_APP_SERVICE with four features:
# 1 - SWITCH : to turn off and on a LED of the peripheral from a central
# 2 - PRESSURE : to send an absolute pressure measurement from the peripheral to a central
# 3 - HUMIDITY : to send a relative humidity measurement from the peripheral to a central
# 4 - TEMPERATURE : to send a temperature measurement from the peripheral to a central

import bluetooth # Low level library for BLE management
from ble_advertising import advertising_payload # To manage GAP advertising
from struct import pack # To aggregate bytes sent by BLE frames
from micropython import const # To define integer constants
import pyb # To manage the LEDs of the NUCLEO-WB55

# Constants to build the GATT Blue-ST service of the peripheral

_IRQ_CENTRAL_CONNECT                 = const(1)
_IRQ_CENTRAL_DISCONNECT              = const(2)
_IRQ_GATTS_WRITE                     = const(3)

# For UUIDs and codes, we refer to the Blue-ST SDK documentation available here:
# https://www.st.com/resource/en/user_manual/dm00550659-getting-started-with-the-bluest-protocol-and-sdk-stmicroelectronics.pdf.

# 1 - Definition of the custom service according to the Blue-ST SDK

# Indicates that we will communicate with an application that conforms to the Blue-ST protocol
_ST_APP_UUID = bluetooth.UUID('00000000-0001-11E1-AC36-0002A5D5C51B')

# UUID of a temperature characteristic
_ENV_UUID = (bluetooth.UUID('001C0000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY)

# UUID of a switch feature
_SWITCH_UUID = (bluetooth.UUID('20000000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY|bluetooth.FLAG_WRITE)

_ST_APP_SERVICE = (_ST_APP_UUID, (_ENV_UUID, _SWITCH_UUID ))

# 2 - Construction of the GAP advertising frame (message content)

_PROTOCOL_VERSION = const(0x01)
_DEVICE_ID = const(0x80) # Generic NUCLEO card
_FEATURE_MASK = const(0x201C0000) # Switch (2^29), pressure (2^20), humidity (2^19), temperature (2^18)

# Calculation of masks
# SWITCH characteristic: 2^29 = 1000000000000000000000000000000000 (in binary) = 20000000 (in hexadecimal)
# Characteristic PRESSURE: 2^20 = 0000000001000000000000000000000000 (in binary) = 100000 (in hexadecimal)
# Characteristic HUMIDITY: 2^19 = 0000000000000010000000000000000000 (in binary) = 80000 (in hexadecimal)
# TEMPERATURE characteristic: 2^18 = 000000000001000000000000000000 (in binary) = 40000 (in hexadecimal)
# We do the sum bit by bit :
# _FEATURE_MASK : 1000000001110000000000000000000000 (in binary) = 201C0000 (in hexadecimal)

_DEVICE_MAC = [0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC] # Dummy MAC hardware address

# Advertising frame: concatenation of information with the Micropython function "pack
# The string '>BBI6B' designates the format of the arguments, see the pack documentation here: https://docs.python.org/3/library/struct.html
_MANUFACTURER = pack('>BBI6B', _PROTOCOL_VERSION, _DEVICE_ID, _FEATURE_MASK, *_DEVICE_MAC)

# Initialization of the LEDs
led_blue = pyb.LED(3)
led_red = pyb.LED(1)

class BLESensor:

	# Initialization, GAP start and radio publication of advertising frames
	def __init__(self, ble, name='WB55-MPY'):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		((self._env_handle,self._switch_handle),) = self._ble.gatts_register_services((_ST_APP_SERVICE, ))
		self._connections = set()
		self._payload = advertising_payload(name=name, manufacturer=_MANUFACTURER)
		self._advertise()
		self._handler = None

	# BLE event management...
	def _irq(self, event, data):
		# If a central has sent a connection request
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _, = data
			self._connections.add(conn_handle)
			print("Connected")
			led_blue.on()

		# If the central has sent a disconnection request
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _, = data
			self._connections.remove(conn_handle)
			# Relaunch advertising to allow new connections
			self._advertise()
			print("Disconnected")

		# If a write is detected in the SWITCH characteristic of the LED
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle, = data
			if conn_handle in self._connections and value_handle == self._switch_handle:
				# Read the value of the characteristic
				data_received = self._ble.gatts_read(self._switch_handle)
				self._ble.gatts_write(self._switch_handle, pack('<HB', 1000, data_received[0]))
				self._ble.gatts_notify(conn_handle, self._switch_handle)
				# Depending on the value written, the red LED is turned on or off
				if data_received[0] == 1:
					led_red.on()
				else:
					led_red.off()

	# We write in the environmental characteristic
	# Points of CAUTION :
	# - The values must be transmitted in the order of the characteristic IDs (pressure: 20 th bit, humidity: 19 th bit, temperature 18 th bit)
	# - Be careful with the formatting string of the Python function "pack", equal here to '<HiHh', see the documentation of "pack" in Python.
	def set_data_env(self, timestamp, pressure, humidity, temperature, notify):
		self._ble.gatts_write(self._env_handle, pack('<HiHh', timestamp, pressure, humidity, temperature))
		if notify:
			for conn_handle in self._connections:
				# Notifies the central that the feature values have just been refreshed and, therefore, can be read
				self._ble.gatts_notify(conn_handle, self._env_handle)

	# To start advertising with a 5 second period, specify that a central can connect to the peripheral
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable=True)
		led_blue.off()
