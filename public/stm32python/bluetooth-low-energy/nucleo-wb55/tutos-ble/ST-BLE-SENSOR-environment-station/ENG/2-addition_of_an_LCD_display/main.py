# Reading of two sensors of the IKS01A3 shield and display on the serial port of the USB USER
# Display of temperature, humidity and pressure values on the Grove RGB LCD.
# Adaptation of the color of the LCD backlight according to the temperature.
# Hardware :
# - A Nucleo WB55 board
# - A Grove Base Shield For Arduino
# - A X-Nucleo IKS01A3 shield
# - A Grove RGB LCD (I2C)

from machine import I2C # To manage the I2C bus
import hts221 # To manage the MEMS sensor HTS221
import lps22 # To manage the MEMS sensor LPS22
from time import sleep_ms # To manage the time delays
import i2c_lcd # To manage the display on the LCD

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensors
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instances of the sensors
sensor_hts221 = hts221.HTS221(i2c)
sensor_lps22 = lps22.LPS22(i2c)

# First reading of the sensors
sensor_hts221.temperature()
sensor_hts221.humidity()
sensor_lps22.pressure()

# Instance of the Display class
lcd = i2c_lcd.Display(i2c)
lcd.color(255,255,255) #Back light of the LCD : white
lcd.home() # We replace the cursor in the top left corner

while True:

	# Reading the sensors
	temp = round(sensor_hts221.temperature(),1) # keep one decimal after the decimal point
	humi = int(sensor_hts221.humidity()) # No decimal point is kept (rounded to the nearest integer)
	pres = int(sensor_lps22.pressure()) # No decimal point is kept (rounded to the nearest integer)

	# Recording of the values read in strings
	stemp = str(temp)
	shumi = str(humi)
	spres = str(pres)

	# Display on the serial port of the USB USER
	print("Temperature : " + stemp + "°C, Relative humidity : " + shumi + "%, Pressure : " + spres + "hPa")

	# Adapts the color of the LCD backlight according to the temperature read
	if temp > 25 :
		lcd.color(255,0,0)
	elif temp > 15 and temp <= 25 :
		lcd.color(255,255,255)
	else:
		lcd.color(0,0,255)

	# Display of temperature, humidity and pressure on the LCD in turn

	# Display of the temperature
	lcd.clear() # Clear the LCD
	lcd.move(0,0) # We move to the first column (index 0), first row (index 0)
	lcd.write('Temperature (C)') # The string "Temperature (C)" is written
	lcd.move(0,1) # Move to the first column (index 0), second line (index 1)
	lcd.write(stemp) # We write the displayable representation of the temperature
	sleep_ms(1000) # One second delay

	# Display of the humidity
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Humidity (%)')
	lcd.move(0,1)
	lcd.write(shumi)
	sleep_ms(1000) # One second delay

	# Display of the pressure
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Pressure (hPa)')
	lcd.move(0,1)
	lcd.write(spres)
	sleep_ms(1000) # One second delay

