# Reading and displaying temperature, humidity and pressure
# on the serial port of the USB USER.
# Hardware : a NUCLEO-W55 board and an X-NUCLEO-IKS01A3 extension board

from machine import I2C # To manage the I2C bus
import hts221 # To manage the MEMS sensor HTS221
import lps22 # To manage the MEMS sensor LPS22
from time import sleep_ms # To manage the time delays

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensors
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instances of the sensors
sensor_hts221 = hts221.HTS221(i2c)
sensor_lps22 = lps22.LPS22(i2c)

# First reading of the sensors
sensor_hts221.temperature()
sensor_hts221.humidity()
sensor_lps22.pressure()

while True:

	# Reading the sensors and rounding
	temp = round(sensor_hts221.temperature(),1)
	humi = int(sensor_hts221.humidity())
	pres = int(sensor_lps22.pressure())

	# Display on the serial port of the USB USER
	print("Temperature : " + str(temp) + " °C, Relative humidity : " + str(humi) + " %, Pressure : " + str(pres) + " hPa")

	# Time delay: one measurement per second
	sleep_ms(1000)
