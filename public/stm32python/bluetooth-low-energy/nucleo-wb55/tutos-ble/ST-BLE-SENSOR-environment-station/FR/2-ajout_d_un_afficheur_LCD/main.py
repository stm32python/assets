# Lecture de deux capteurs du shield IKS01A3 et affichage sur le port série de l'USB USER
# Affichage des valeurs de température, humidité, pression sur le LCD RGB Grove.
# Adaptation de la couleur du rétro-éclairage du LCD selon la température.
# Matériel : 
#  - Une carte Nucleo WB55
#  - Un Grove Base Shield For Arduino
#  - Un shield X-Nucleo IKS01A3
#  - Un LCD RGB Grove (I2C)

from machine import I2C # Pour gérer le bus I2C
import hts221 # Pour gérer le capteur MEMS HTS221
import lps22 # Pour gérer le capteur MEMS LPS22
from time import sleep_ms # Pour gérer les temporisations
import i2c_lcd # Pour gérer l'affichage sur le LCD

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec les capteurs
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instances des capteurs
capteur1 = hts221.HTS221(i2c)
capteur2 = lps22.LPS22(i2c)

# Première lecture des capteurs
capteur1.temperature()
capteur1.humidity()
capteur2.pressure()

# Instance de la classe Display
lcd = i2c_lcd.Display(i2c)
lcd.color(255,255,255) #Back light du LCD : blanche
lcd.home() # On replace le curseur en haut à gauche

while True:

	# Lecture des capteurs
	temp = round(capteur1.temperature(),1) # on garde une décimale après la virgule
	humi = int(capteur1.humidity()) # On ne conserve aucune décimale (arrondi à l'entier le plus proche)
	pres = int(capteur2.pressure()) # On ne conserve aucune décimale (arrondi à l'entier le plus proche)

	# Enregistrement des valeurs lues dans des chaînes de caractères 
	stemp = str(temp)
	shumi = str(humi)
	spres = str(pres)

	# Affichage sur le port série de l'USB USER
	print("Température : " + stemp + "°C, Humidité relative : " + shumi + "%, Pression : " + spres + "hPa")

	# Adapte la couleur du rétro-éclairage du LCD selon la température lue
	if temp > 25 :
		lcd.color(255,0,0)
	elif temp > 15 and temp <= 25 :
		lcd.color(255,255,255)
	else:
		lcd.color(0,0,255)

	# Affichage tour à tour sur le LCD de la température, de l'humidité et de la pression

	# Affichage de la température
	lcd.clear() # On efface le LCD
	lcd.move(0,0) # On se place en première colonne (indice 0), première ligne (indice 0)
	lcd.write('Temperature (C)') # On écrit la chaîne "Temperature (C)"
	lcd.move(0,1) # On se place en première colonne (indice 0), deuxième ligne (indice 1)
	lcd.write(stemp) # On écrit la réprésentation affichable de la température
	sleep_ms(1000) # Temporisation d'une seconde

	# Affichage de l'humidité
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Humidite (%)')
	lcd.move(0,1)
	lcd.write(shumi)
	sleep_ms(1000) # Temporisation d'une seconde

	# Affichage de la pression
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Pression (hPa)')
	lcd.move(0,1)
	lcd.write(spres)
	sleep_ms(1000) # Temporisation d'une seconde

