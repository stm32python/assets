# Lecture et affichage, sur le port série de l'USB USER
# - de la température, 
# - de l'humidité,
# - de la pression.
# Matériel : une carte NUCLEO-W55 et une carte d'extension X-NUCLEO-IKS01A3

from machine import I2C # Pour gérer le bus I2C
import hts221 # Pour gérer le capteur MEMS HTS221
import lps22 # Pour gérer le capteur MEMS LPS22
from time import sleep_ms # Pour gérer les temporisations

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec les capteurs
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instances des capteurs
capteur1 = hts221.HTS221(i2c)
capteur2 = lps22.LPS22(i2c)

# Première lecture des capteurs
capteur1.temperature()
capteur1.humidity()
capteur2.pressure()

while True:

	# Lecture des capteurs et arrondis
	temp = round(capteur1.temperature(),1)
	humi = int(capteur1.humidity())
	pres = int(capteur2.pressure())

	# Affichage sur le port série de l'USB USER
	print("Température : " + str(temp) + " °C, Humidité relative : " + str(humi) + " %, Pression : " + str(pres) + " hPa")

	# Temporisation : une mesure par seconde
	sleep_ms(1000)
