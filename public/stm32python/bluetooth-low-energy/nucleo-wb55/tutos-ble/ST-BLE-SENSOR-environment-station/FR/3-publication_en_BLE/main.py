# Lecture de deux capteurs du shield IKS01A3 et affichage sur le port série de l'USB USER
# Affichage des valeurs de température, humidité, pression sur le LCD RGB Grove
# Adaptation de la couleur du rétro-éclairage du LCD selon la température
# "Casting" RF des mesures avec la radio BLE du STM32WB55 
# Correction de la pression en la rapportant au niveau de la mer
# Les données sont traitées par l'application smartphone ST BLE Sensor
# Matériel : 
#  - Une carte NUCLEO-WB55
#  - Un Grove Base Shield For Arduino
#  - Un shield X-NUCLEO IKS01A3
#  - Un LCD RGB Grove (I2C)

# Pression corrigée au niveau de la mer (nécessite la valeur de l'altitude locale)
alti = 485
def PressionNivMer(pression, altitude):
	return pression * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

from machine import I2C

import hts221
import lps22
from time import sleep_ms, time # Pour gérér les temporisations et l'horodatage
import i2c_lcd
import ble_sensor # Pour implémenter le protocole GATT pour Blue-ST
import bluetooth # Classes "primitives du BLE" 
# (voir https://docs.micropython.org/en/latest/library/ubluetooth.html)

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec les capteurs
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instances des capteurs
sensor1 = hts221.HTS221(i2c)
sensor2 = lps22.LPS22(i2c)

#Instance de la classe Display
lcd = i2c_lcd.Display(i2c)
lcd.color(255,255,255) #Back light du LCD : blanche

# Repositionne le curseur de l'afficheur LCD en haut à gauche
lcd.home()

# Instance de la classe BLE
ble = bluetooth.BLE()
ble_device = ble_sensor.BLESensor(ble)

while True:

	# Lecture des capteurs
	temp = sensor1.temperature()
	humi = sensor1.humidity()
	pres = PressionNivMer(sensor2.pressure(), alti)

	# Conversion en texte des valeurs renvoyées par les capteurs 
	stemp = str(round(temp,1))
	shumi = str(int(humi))
	spres = str(int(pres))

	# Affichage sur le port série de l'USB USER
	print("Température : " + stemp + " °C, Humidité relative : " + shumi + " %, Pression : " + spres + " hPa")

	# Préparation des données pour envoi en BLE.
	# Le protocole Blue-ST code les températures, pressions et humidités sous forme de nombres entiers.
	# Donc on multiplie les différentes mesures par 10 ou par 100 pour conserver des décimales avant
	# d'arrondir à l'entier le plus proche.
	# Par exemple si temp = 18.45°C => on envoie ble_temp = 184. 
	ble_pres = int(pres*100)
	ble_humi = int(humi*10)
	ble_temp = int(temp*10)
	timestamp = time()

	# Envoie des données en BLE 
	ble_device.set_data_env(timestamp, ble_pres, ble_humi, ble_temp, True) 

	# Adapte la couleur du rétro-éclairage du LCD selon la température lue
	if temp > 25 :
		lcd.color(255,0,0)
	elif temp > 15 and temp <= 25 :
		lcd.color(255,255,255)
	else:
		lcd.color(0,0,255)

	# Ecriture sur le LCD
	lcd.clear()
	lcd.move(0,0)
	lcd.write('Temperature (C)')
	lcd.move(0,1)
	lcd.write(stemp)
	sleep_ms(1000)

	lcd.clear()
	lcd.move(0,0)
	lcd.write('Humidite (%)')
	lcd.move(0,1)
	lcd.write(shumi)
	sleep_ms(1000)

	lcd.clear()
	lcd.move(0,0)
	lcd.write('Pression (hPa)')
	lcd.move(0,1)
	lcd.write(spres)
	sleep_ms(1000)
