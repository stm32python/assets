# This script shows how to create a UART Central which means how :
# 1 - Detect a Peripheral running the UART service and exposing two characteristics: TX and RX.
# 2 - Connect to this Peripheral to receive, in the form of UTF-8 encoded characters, a message notified by TX.
# 3 - Reply to the Peripheral by writing in RX
# In this example:
#   - the Peripheral sends a string containing the displayable representation of
#     temperature, pressure and humidity values that it has measured.
#   - the Central receives this string, cuts it out and displays the measurements on the USB USER serial terminal
#   - the Central sends a simple acknowledgement of receipt to the Peripheral.
# Source : https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_simple_central.py

import bluetooth # "BLE primitive" classes
from ble_advertising import decode_services, decode_name # To decode received messages
from binascii import hexlify # Convert a binary data to its hexadecimal representation
+
# Constants required to build the GATT BLE UART service
# See : https://docs.micropython.org/en/latest/library/ubluetooth.html
_IRQ_SCAN_RESULT = const(5)
_IRQ_SCAN_DONE = const(6)
_IRQ_PERIPHERAL_CONNECT = const(7)
_IRQ_PERIPHERAL_DISCONNECT = const(8)
_IRQ_GATTC_SERVICE_RESULT = const(9)
_IRQ_GATTC_SERVICE_DONE = const(10)
_IRQ_GATTC_CHARACTERISTIC_RESULT = const(11)
_IRQ_GATTC_CHARACTERISTIC_DONE = const(12)
_IRQ_GATTC_WRITE_DONE = const(17)
_IRQ_GATTC_NOTIFY = const(18)
_IRQ_MTU_EXCHANGED = const(21)

# Connectable objects with scannable advertising
_ADV_IND = const(0x00)
_ADV_DIRECT_IND = const(0x01)

# Parameters for setting the duty cycle of the GAP scan
_SCAN_DURATION_MS = const(2000)
_SCAN_INTERVAL_US = const(30000)
_SCAN_WINDOW_US = const(30000)

# Definition of the UART service with its two characteristics RX and TX
_UART_SERVICE_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_RX_CHAR_UUID = bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_TX_CHAR_UUID = bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")

# Global variables shared by asynchronous functions that respond to events (callback) 
MAC_address = 0 # Hardware address of the BLE radio of the Central
Central_ACK_required = 0 # Does the Central have to send an acknowledgement to the Peripheral?

# Maximum number of bytes that can be exchanged by the RX & TX characteristics
_MAX_NB_BYTES = const(128)

# Class to manage the BLE Central
class BLECentral:

	# Initialization 
	def __init__(self, ble):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		self._ble.config(mtu=_MAX_NB_BYTES)
		self._reset()
		
		# Displays the MAC address of the object
		dummy, byte_mac = self._ble.config('mac')
		hex_mac = hexlify(byte_mac) 
		print("MAC address : %s" %hex_mac.decode("ascii"))

	# Reset (called on disconnections)
	def _reset(self):
		# Delete the cache of addresses and names of scans
		self._name = None
		self._addr_type = None
		self._addr = None
		
		# Response functions (callback) to the completion of various events
		self._scan_callback = None
		self._conn_callback = None
		self._read_callback = None

		# Function to respond to peripheral notifications 
		self._notify_callback = None

		# Addresses and characteristics of the connected peripheral
		self._conn_handle = None
		self._start_handle = None
		self._end_handle = None
		self._tx_handle = None
		self._rx_handle = None

	# Event management interruptions
	def _irq(self, event, data):
		# "Scan result" event
		if event == _IRQ_SCAN_RESULT:
			# Reading the content of the advertising frame
			addr_type, addr, adv_type, rssi, adv_data = data
			# If advertising signals a UART service
			if adv_type in (_ADV_IND, _ADV_DIRECT_IND) and _UART_SERVICE_UUID in decode_services(adv_data):
				# A potential peripheral is identified, references it and stops the scan.
				self._addr_type = addr_type
				self._addr = bytes(addr) # Note: the addr buffer is owned by the caller, so it must be copied.
				self._name = decode_name(adv_data) or "?"
				self._ble.gap_scan(None)

		# "Scan completed" event
		elif event == _IRQ_SCAN_DONE:
			if self._scan_callback:
				if self._addr:
					# A Peripheral was detected (and the scan was explicitly interrupted accordingly)
					self._scan_callback(self._addr_type, self._addr, self._name)
					self._scan_callback = None
				else:
					# The scan has exceeded its time-out period.
					self._scan_callback(None, None, None)

		# "Successful connection" event
		elif event == _IRQ_PERIPHERAL_CONNECT:
			conn_handle, addr_type, addr = data
			if addr_type == self._addr_type and addr == self._addr:
				self._conn_handle = conn_handle
				self._ble.gattc_exchange_mtu(self._conn_handle)
				self._ble.gattc_discover_services(self._conn_handle)

		# Disconnection" event (initiated by the central or by the advertising_payload)
		elif event == _IRQ_PERIPHERAL_DISCONNECT:
			conn_handle, _, _ = data
			if conn_handle == self._conn_handle:
				# If disconnection is initiated by the central , the reset has already been done
				self._reset()

		# Event "The connected Peripheral has notified a service to the Central "
		elif event == _IRQ_GATTC_SERVICE_RESULT:
			conn_handle, start_handle, end_handle, uuid = data
			if conn_handle == self._conn_handle and uuid == _UART_SERVICE_UUID:
				self._start_handle, self._end_handle = start_handle, end_handle

		# Event "Search for services completed".
		elif event == _IRQ_GATTC_SERVICE_DONE:
			if self._start_handle and self._end_handle:
				self._ble.gattc_discover_characteristics(
					self._conn_handle, self._start_handle, self._end_handle
				)
			else:
				print("The UART service cannot be found.")

		# Event "The connected Peripheral has notified a feature to the Central".
		elif event == _IRQ_GATTC_CHARACTERISTIC_RESULT:
			conn_handle, def_handle, value_handle, properties, uuid = data
			if conn_handle == self._conn_handle and uuid == _UART_RX_CHAR_UUID:
				self._rx_handle = value_handle
			if conn_handle == self._conn_handle and uuid == _UART_TX_CHAR_UUID:
				self._tx_handle = value_handle

		# Event "Feature search completed".
		elif event == _IRQ_GATTC_CHARACTERISTIC_DONE:
			if self._tx_handle is not None and self._rx_handle is not None:
				# We have completed the connection and Peripheral discovery, 
				# generates the connection callback.
				if self._conn_callback:
					self._conn_callback()
			else:
				print("UART RX characteristic not found.")

		# "Peripheral Acknowledgement" event, 
		# which occurs when the central sends a message, if an AR has been explicitly requested
		elif event == _IRQ_GATTC_WRITE_DONE:
			conn_handle, value_handle, status = data
			print("Writing in RX done")

		# Event "Response to peripheral notifications on the TX feature"
		elif event == _IRQ_GATTC_NOTIFY:
			conn_handle, value_handle, notify_data = data
			if conn_handle == self._conn_handle and value_handle == self._tx_handle:
				if self._notify_callback:
					self._notify_callback(notify_data)

		# Event "ATT MTU exchange complete (either initiated by us or the remote device)"
		elif event == _IRQ_MTU_EXCHANGED:
			print("Characteristics payload length set to " + str(_MAX_NB_BYTES) + " bytes")

	# Returns True if we are connected to the UART service.
	def is_connected(self):
		return (
			self._conn_handle is not None
			and self._tx_handle is not None
			and self._rx_handle is not None
		)

	# Search for a Peripheral that offers the UART service
	def scan(self, callback=None):
		self._addr_type = None
		self._addr = None
		self._scan_callback = callback
		# Scans during _SCAN_DURATION_MS, during _SCAN_WINDOWS_US durations spaced by _SCAN_INTERVAL_US
		self._ble.gap_scan(_SCAN_DURATION_MS, _SCAN_INTERVAL_US, _SCAN_WINDOW_US)

	# Connects to the specified Peripheral
	# If no Peripheral specified, uses cached addresses after a scan
	def connect(self, addr_type=None, addr=None, callback=None):
		self._addr_type = addr_type or self._addr_type
		self._addr = addr or self._addr
		self._conn_callback = callback
		if self._addr_type is None or self._addr is None:
			return False
		self._ble.gap_connect(self._addr_type, self._addr)
		return True

	# Disconnects from the peripheral
	def disconnect(self):
		if not self._conn_handle:
			return
		self._ble.gap_disconnect(self._conn_handle)
		self._reset()

	# Sends data to the UART (writes to the RX characteristic)
	# This method allows the Central  to send a message to the connected Peripheral.

	def write(self, v, response = False):
		
		if not self.is_connected():
			return

		self._ble.gattc_write(self._conn_handle, self._rx_handle, v, 1 if response else 0)
		
		# Confirms that the acknowledgement of receipt has been sent
		global Central_ACK_required
		Central_ACK_required = 0

	# Activates the reception event manager on the UART
	def on_notify(self, callback):
		self._notify_callback = callback

# Handler of the receive event that responds to a notification when the TX feature
# is changed.
def on_receipt(v):
	
	# Conversion to bytes of the payload the TX characteristic
	b = bytes(v)

	# We convert the received bytes into characters coded in UTF-8 format
	payload = b.decode('utf-8')

	print("Message received from " + str(MAC_address) + " : ", payload)
	
	# We separate the measures thanks to the split instruction
	temp, humi, press = payload.split("|")

	# Temperature, pressure and relative humidity values are displayed
	print("Temperature = " + temp + " °C")
	print("Relative humidity = " + humi + " %")
	print("Pressure = " + press + " hPa")

	# The central has received a message from the peripheral, so it must send it an acknowledgement of receipt
	global Central_ACK_required
	Central_ACK_required = 1
+
# Creation of an instance of the central class
ble = bluetooth.BLE()
central = BLECentral(ble)

no_peripheral = 0 # Will be 1 if a peripheral is detected

# Scan Event Manager
def on_scan(addr_type, addr, name):

	import ubinascii # To convert binary information to text
	from ubinascii import hexlify # To convert a hexadecimal number to its displayable binary representation

	if addr_type is not None:
		global MAC_address
		b = bytes(addr)
		print("advertising_payload found : ", name)
		MAC_address = hexlify(b).decode("ascii")
		central.connect()
	else:
		global no_peripheral
		no_peripheral = 1
		print("No advertising_payloads found.")

# Main program
def demo():
	print("BLE Central")

	import time # To manage time and timeouts

	no_peripheral = 0

	#Captures scan events
	central.scan(callback = on_scan)

	# Waiting for connection...
	while not central.is_connected():
		time.sleep_ms(100)
		if no_peripheral == 1:
			return
	print("Connected")

	# Captures reception events. The notification comes from the TX feature.
	central.on_notify(on_receipt)

	# Sending an acknowledgement message from the central to the peripheral
	while central.is_connected():
		if Central_ACK_required == 1:
			try: # Try to send a message
				v = "Acknowledgement to device " + MAC_address
				central.write(v)
			except: # In case of failure...
				print("Failed to send a response from the Central")

	# In the case that the central is disconnected
	print("Disconnected")

# If the script name is "main", execute the "demo()" function
if __name__ == "__main__":
	demo()
