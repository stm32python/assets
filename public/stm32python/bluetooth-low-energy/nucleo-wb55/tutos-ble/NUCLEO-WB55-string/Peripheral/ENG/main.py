# This script shows how to create a UART Peripheral running the Nordic UART Service.
# It will:
# 1 - Expose two features TX and RX to exchange data.
# 2 - Connect to a central and notify it of messages in TX.
# 3 - Read the data written back by the central in RX.
# The Peripheral sends to the central a string containing the temperature, humidity and
# pressure measured with an X-NUCLEO IKS01A3 expansion card.
# Sources:
#	https://docs.micropython.org/en/latest/library/ubluetooth.html
#	https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_uart_peripheral.py

import bluetooth # "BLE primitive" classes
from ble_advertising import advertising_payload # To build the advertising framework
from binascii import hexlify # Convert a binary data to its hexadecimal representation

# Constants required to build the BLE UART service
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_IRQ_MTU_EXCHANGED = const(21)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)

# Definition of the UART service with its two characteristics RX and TX

_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_TX = (
	bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_NOTIFY, # This feature will notify the exchange of any changes made to it by the device
)
_UART_RX = (
	bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_WRITE, # The central can write in this characteristic
)
_UART_SERVICE = (
	_UART_UUID,
	(_UART_TX, _UART_RX),
)

# Maximum number of bytes that can be exchanged by the RX  & TX characteristic
_MAX_NB_BYTES = const(128)

class BLEUART:

	# Initializations
	def __init__(self, ble, name="mpy-uart", charbuf=_MAX_NB_BYTES):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		self._ble.config(mtu=_MAX_NB_BYTES)

		# Registration of the service
		((self._tx_handle, self._rx_handle),) = self._ble.gatts_register_services((_UART_SERVICE,))
		# Increase the size of the rx & tx buffer and activate the append mode
		self._ble.gatts_set_buffer(self._rx_handle, charbuf, True)
		self._ble.gatts_set_buffer(self._rx_handle, charbuf, True)
+		self._ble.gatts_write(self._tx_handle, bytes(charbuf))
+		self._ble.gatts_write(self._rx_handle, bytes(charbuf))

		self._connections = set()
		self._rx_buffer = bytearray()
		self._handler = None
		# Advertising of the service (services=[_UART_UUID] is required for the exchange to identify the service)
		self._payload = advertising_payload(name=name, services=[_UART_UUID])
		self._advertise()

		# Displays the MAC address of the object
		dummy, byte_mac = self._ble.config('mac')
		hex_mac = hexlify(byte_mac)
		print("Address MAC : %s" %hex_mac.decode("ascii"))


	# Interruption to manage receptions
	def irq(self, handler):
		self._handler = handler

	# Monitor connections to send notifications
	def _irq(self, event, data):

		# If a central connects
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
			print("New connection", conn_handle)

		# If a central disconnects
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			print("Disconnected", conn_handle)
			if conn_handle in self._connections:
				self._connections.remove(conn_handle)
			# Restarts advertising to allow new connections
			self._advertise()

		# When a client writes to a feature exposed by the server
		# (handling of reception events from the central)
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle = data
			if conn_handle in self._connections and value_handle == self._rx_handle:
				self._rx_buffer += self._ble.gatts_read(self._rx_handle)
				if self._handler:
					self._handler()

		# Event "ATT MTU exchange complete (either initiated by us or the remote device)"
		elif event == _IRQ_MTU_EXCHANGED:
			print("Characteristics payload length set to " + str(_MAX_NB_BYTES) + " bytes")

	# Called to check if there are messages waiting to be read in RX
	def any(self):
		return len(self._rx_buffer)

	# Returns the characters received in RX
	def read(self, sz=None):
		if not sz:
			sz = len(self._rx_buffer)
		result = self._rx_buffer[0:sz]
		self._rx_buffer = self._rx_buffer[sz:]
		return result

	# Writes in TX a message to the attention of the central
	def write(self, data):
		for conn_handle in self._connections:
			self._ble.gatts_notify(conn_handle, self._tx_handle, data)

	# Terminate the connection to the simulated serial port
	def close(self):
		for conn_handle in self._connections:
			self._ble.gap_disconnect(conn_handle)
		self._connections.clear()

	# To start advertising, specify that a central can connect to the device
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = True)

	# Is the device connected to a central ?
	def is_connected(self):
		return len(self._connections) > 0


# Value of the local altitude (in meters)
local_altitude = const(485)

# Calculation of the corrected pressure at sea level (requires the local altitude value)
def SeeLevelPressure(pression, altitude):
	return pression * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

# Main program
def demo():

	print("BLE peripheral")

	from machine import I2C # To manage the I2C bus
	import HTS221 # To manage the HTS221 MEMS sensor
	import LPS22 # To manage the MEMS sensor LPS22
	import time # To manage the timers and the measurement of the elapsed time

	# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
	i2c = I2C(1)

	# Instances of sensors
	sensor_hts221 = HTS221.HTS221(i2c)
	sensor_lps22 = LPS22.LPS22(i2c)

	# Pause for one second to give the I2C time to initialize
	time.sleep_ms(1000)

	# Instantiation of the BLE
	ble = bluetooth.BLE()
	uart = BLEUART(ble)

	# Reception Event Manager
	def on_rx():
		print("Data received from the central : ", uart.read().decode().strip())

	# Receive (asynchronous) data (i.e. reaction to the writings of the central in RX).
	uart.irq(handler=on_rx)

	# Error management structure to handle keyboard interruptions
	try:
		while True:
		
			# Reading of the sensors
			temp = sensor_hts221.temperature()
			humi = sensor_hts221.humidity()
			pres = sensor_lps22.pressure()

			# Conversion to text of the values returned by the sensors
			stemp = str(round(temp,1))
			shumi = str(int(humi))
			spres = str(int(SeeLevelPressure(pres, local_altitude)))

			# Display on the serial port of the USB USER
			print("Temperature : " + stemp + " °C, Relative humidity : " + shumi + " %, Pressure : " + spres + " hPa")

			if uart.is_connected():

				# We concatenate the data:
				data = stemp + "|" + shumi + "|" + spres

				# We send them to the central (i.e. we notify them in TX):
				uart.write(data)

				print("Data sent to the central : " + data)

			# 5 second delay
			time.sleep_ms(5000)

	# In case of keyboard interruption (user presses CTRL+C)
	except KeyboardInterrupt:
		pass # does not leave the application and goes to the next step

	# Closes the active UART
	uart.close()

# If the script name is "main", execute the "demo()" function
if __name__ == "__main__":
	demo()
