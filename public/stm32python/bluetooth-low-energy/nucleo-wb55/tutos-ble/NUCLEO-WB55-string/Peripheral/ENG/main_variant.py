# This script shows how to create a UART device, i.e. how to :
# 1 - Expose two features TX and RX to exchange data.
# 2 - Connect to a central and notify it of messages in TX.
# 3 - Read the data written back by the central in RX.
# The device sends the central a string containing the temperature, humidity and
# pressure measured with an X-NUCLEO IKS01A3 extension card.

# Source : https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_simple_peripheral.py

import bluetooth # Classes "primitives du BLE"
# (See https://docs.micropython.org/en/latest/library/ubluetooth.html)

from ble_advertising import advertising_payload # To build the advertising framework

# Constants required to build the BLE UART service
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_FLAG_READ = const(0x0002)
_IRQ_MTU_EXCHANGED = const(21)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)

# Definition of the UART service with its two characteristics RX and TX

_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")

_UART_TX = (
	bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_READ | _FLAG_NOTIFY, # This feature will notify the central of any changes made to it by the device
)

_UART_RX = (
	bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_WRITE, # The central can write in this characteristic
)

_UART_SERVICE = (
    _UART_UUID,
    (_UART_TX, _UART_RX),
)

# Maximum number of bytes that can be exchanged by the RX & TX characteristic
_MAX_NB_BYTES = const(128)

class BLEperipheral:

	# Initializations
	def __init__(self, ble, name="mpy-uart", charbuf=_MAX_NB_BYTES):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		self._ble.config(mtu=_MAX_NB_BYTES)

		# Service Registration:
		((self._handle_tx, self._handle_rx),) = self._ble.gatts_register_services((_UART_SERVICE,))
		# Increase the size of the rx & tx buffer and activate the append mode
		self._ble.gatts_set_buffer(self._rx_handle, charbuf, True)
		self._ble.gatts_set_buffer(self._rx_handle, charbuf, True)
+		self._ble.gatts_write(self._tx_handle, bytes(charbuf))
+		self._ble.gatts_write(self._rx_handle, bytes(charbuf))

		self._connections = set()
		self._write_callback = None
		# Advertising of the service:
		self._payload = advertising_payload(name=name, services=[_UART_UUID])
		self._advertise()

	# Event management interruptions
	def _irq(self, event, data):
		# In case of connection to a central
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			print("New connection", conn_handle)
			self._connections.add(conn_handle)

		# In case of disconnection of an central
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			print("Disconnected", conn_handle)
			self._connections.remove(conn_handle)
			# Relaunch advertising to enable new connections
			self._advertise()

		# In case of writing by the central in the RX characteristic
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle = data
			value = self._ble.gatts_read(value_handle)
			if value_handle == self._handle_rx and self._write_callback:
				self._write_callback(value)

		# Event "ATT MTU exchange complete (either initiated by us or the remote device)"
		elif event == _IRQ_MTU_EXCHANGED:
			print("Characteristics payload length set to " + str(_MAX_NB_BYTES) + " bytes")

	# Sending data to the central, by notification on the TX characteristic
	def send(self, data):
		for conn_handle in self._connections:
			self._ble.gatts_notify(conn_handle, self._handle_tx, data)

	# Is the device connected to a central ?
	def is_connected(self):
		return len(self._connections) > 0

	# To start advertising, specify that a central can connect to the device
	def _advertise(self, interval_us=500000):
		print("Start advertising")
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable=True)

	# Call back that reacts to entries in RX by a central
	def on_write(self, callback):
		self._write_callback = callback

# Corrected pressure at sea level (requires local altitude value)
alti = 485
def SeeLevelPressure(pression, altitude):
	return pression * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

# Reception Event Manager
def on_receipt(v):
	# Conversion to bytes of the payload of the RX characteristic
	b = bytes(v)
	# The received bytes are converted into characters coded in ASCII format
	payload = b.decode("ascii")
	print("Message received : ", payload)


# Main program
def demo():

	print("Peripheral BLE")

	from machine import I2C # To manage the I2C bus
	import HTS221 # To manage the HTS221 MEMS sensor
	import LPS22 # To manage the MEMS sensor LPS22
	import time # To manage the timers and the measurement of the elapsed time

	# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
	i2c = I2C(1)

	# Sensor instances
	sensor_hts221 = HTS221.HTS221(i2c)
	sensor_lps22 = LPS22.LPS22(i2c)

	# Pause for one second to give the I2C time to initialize
	time.sleep_ms(1000)

	# Instantiation of the BLE
	ble = bluetooth.BLE()
	peripheral = BLEperipheral(ble)

	# Receive (asynchronous) data (i.e. reaction to the writings of the exchange in RX).
	peripheral.on_write(on_receipt)

	while True:

		# Reading of the sensors
		temp = sensor_hts221.temperature()
		humi = sensor_hts221.humidity()
		pres = sensor_lps22.pressure()

		# Conversion to text of the values returned by the sensors
		stemp = str(round(temp,1))
		shumi = str(int(humi))
		spres = str(int(SeeLevelPressure(pres, alti)))

		# Display on the serial port of the USB USER
		print("Temperature : " + stemp + " °C, Relative humidity : " + shumi + " %, Pressure : " + spres + " hPa")

		if peripheral.is_connected():
			# We concatenate the data:
			data = stemp + "|" + shumi + "|" + spres

			# We send them to the central (i.e. we notify them in TX):
			peripheral.send(data)

			print("Data sent to the central : " + data)

		time.sleep_ms(5000) # 5 second delay

# If the script name is "main", execute the "demo()" function
if __name__ == "__main__":
	demo()

