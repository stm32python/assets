# Ce script montre comment créer un periphérique UART, c'est à dire comment :
# 1 - Exposer deux caractéristiques TX et RX pour échanger des données.
# 2 - Se connecter à un central et lui notifier des messages dans TX.
# 3 - Lire les données écrites en retour par le central dans RX.
# Le périphérique envoie au central une chaîne de caractères contenant la température, l'humidité et
# la pression mesurées avec une carte d'extension X-NUCLEO IKS01A3.

# Source : https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_simple_peripheral.py

import bluetooth # Classes "primitives du BLE"
# (voir https://docs.micropython.org/en/latest/library/ubluetooth.html)

from ble_advertising import advertising_payload # Pour construire la trame d'advertising

# Constantes requises pour construire le service BLE UART
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_FLAG_READ = const(0x0002)
_FLAG_WRITE_NO_RESPONSE = const(0x0004)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)

# Définition du service UART avec ses deux caractéristiques RX et TX

_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")

_UART_TX = (
	bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_READ | _FLAG_NOTIFY, # Cette caractéristique notifiera le central des modifications que lui apportera le périphérique
)

_UART_RX = (
	bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_WRITE | _FLAG_WRITE_NO_RESPONSE, # Le central pourra écrire dans cette caractéristique
)

_UART_SERVICE = (_UART_UUID,(_UART_TX, _UART_RX),)

# Nombre maximum d'octets qui peuvent être échangés par la caractéristique RX
_MAX_NB_BYTES = const(100)

class BLEPeripheral:

	# Initialisations
	def __init__(self, ble, name="mpy-uart", rxbuf=_MAX_NB_BYTES):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		# Enregistrement du service :
		((self._handle_tx, self._handle_rx),) = self._ble.gatts_register_services((_UART_SERVICE,))
		# Augmente la taille du tampon rx et active le mode "append"
		self._ble.gatts_set_buffer(self._handle_rx, rxbuf, True)
		self._connections = set()
		self._write_callback = None
		# Advertising du service :
		self._payload = advertising_payload(name=name, services=[_UART_UUID])
		self._advertise()

	# Interruptions de gestion des évènements
	def _irq(self, event, data):
		# En cas de connexion à un central
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			print("Nouvelle connexion", conn_handle)
			self._connections.add(conn_handle)

		# En cas de déconnexion d'un central
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			print("Déconnecté", conn_handle)
			self._connections.remove(conn_handle)
			# Relance l'advertising pour permettre de nouvelles connexions
			self._advertise()
		
		# En cas d'écriture par le central dans la caractéristique RX
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle = data
			value = self._ble.gatts_read(value_handle)
			if value_handle == self._handle_rx and self._write_callback:
				self._write_callback(value)
	
	# Envoi de données au central, par notification sur la caractéristique TX
	def send(self, data):
		for conn_handle in self._connections:
			self._ble.gatts_notify(conn_handle, self._handle_tx, data)
	
	# Est-ce que le périphérique est connecté à un central ?
	def is_connected(self):
		return len(self._connections) > 0
	
	# Pour démarrer l'advertising, précise qu'un central pourra se connecter au périphérique
	def _advertise(self, interval_us=500000):
		print("Démarre l'advertising")
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable=True)

	# Call back qui réagit aux écritures dans RX par un central
	def on_write(self, callback):
		self._write_callback = callback

# Pression corrigée au niveau de la mer (nécessite la valeur de l'altitude locale)
alti = 485
def PressionNivMer(pression, altitude):
	return pression * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

# Gestionnaire de l'évènement de réception
def on_receipt(v):
	# Conversion en octets de la charge utile de la caractéristique RX
	b = bytes(v)
	# On convertit les octets reçus en caractères codés au format ASCII
	payload = b.decode("ascii")
	print("Message reçu : ", payload)


# Programme principal
def demo():

	print("Périphérique BLE")

	from machine import I2C # Pour gérer le bus I2C
	import HTS221 # Pour gérer le capteur MEMS HTS221
	import LPS22 # Pour gérer le capteur MEMS LPS22
	import time # Pour gérer les temporisations et la mesure du temps écoulé

	# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
	i2c = I2C(1) 

	# Instances des capteurs
	sensor1 = HTS221.HTS221(i2c)
	sensor2 = LPS22.LPS22(i2c)

	# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
	time.sleep_ms(1000)
	
	# Instanciation du BLE
	ble = bluetooth.BLE()
	peripheral = BLEPeripheral(ble)

	# Réception (asynchrone) des données (ie réaction aux écritures du central dans RX).
	peripheral.on_write(on_receipt)

	while True:

		# Lecture des capteurs
		temp = sensor1.temperature()
		humi = sensor1.humidity()
		pres = sensor2.pressure()

		# Conversion en texte des valeurs renvoyées par les capteurs
		stemp = str(round(temp,1))
		shumi = str(int(humi))
		spres = str(int(PressionNivMer(pres, alti)))

		# Affichage sur le port série de l'USB USER
		print("Température : " + stemp + " °C, Humidité relative : " + shumi + " %, Pression : " + spres + " hPa")

		if peripheral.is_connected():
			
			# On concatène les données :
			data = stemp + "|" + shumi + "|" + spres
			
			# On les envoie au central (ie on les notifie dans TX):
			peripheral.send(data)
			
			print("Données envoyées au central : " + data)

		time.sleep_ms(5000) # Temporisation de 5 secondes

# Si le nom du script est "main", exécute la fonction "demo()"
if __name__ == "__main__":
	demo()

