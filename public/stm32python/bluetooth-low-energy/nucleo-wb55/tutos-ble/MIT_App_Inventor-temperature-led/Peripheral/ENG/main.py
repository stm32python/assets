# This example shows how to program a BLE device with the Blue-ST standard to send
# temperature measurements and control an LED using a service containing a characteristic
# of temperature.
# If you don't have a temperature sensor:
# The measurements are simulated with a random number generator
# If you have an I2C BME280 temperature sensor:
# The temperature is sent back by this one
# The measurements (simulated or not) are updated every five seconds
# and notified at the same frequency to a possible BLE central which would be connected.

import ble_sensor # BLE GATT implementation according to Blue-ST
import bluetooth # For the management of the BLE
from time import sleep_ms #  For time management and timeouts

# Is a BME280 I2C temperature sensor module connected to the NUCLEO-WB55?
SENSOR = True

# Si vous ne disposez pas d'un capteur de température
if not SENSOR:
	from random import randint # If you do not have a temperature sensor
else:
# If you have a BME280 I2C temperature sensor

	from machine import I2C # I2C bus controller driver
	import bme280 # Sensor driver

	# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
	i2c = I2C(1)

	# Pause for one second to give the I2C time to initialize
	sleep_ms(1000)

	# List of I2C addresses of the present peripherals
	print("I2C Adresses used : " + str(i2c.scan()))

	# Instantiation of the sensor
	bme = bme280.BME280(i2c=i2c)

# Installation of the BLE
ble = bluetooth.BLE()
ble_device = ble_sensor.BLESensor(ble)

while True:

	if not SENSOR:
		# Random value between -20 and 90 °C
		temperature = randint(-20, 90)
	else:
		# Reading the values measured by the BME280
		bme280 = bme.values
		temperature = round(bme280[0],1)

	# Sending in BLE of the temperature by choosing to notify the application
	ble_device.set_data_temperature(temperature, notify=1)
	
	# Five second delay
	sleep_ms(5000)
