# Cet exemple montre comment programmer un périphérique BLE avec le standard Blue-ST pour envoyer
# des mesures de température et contrôler une LED à l'aide d'un service contenant une caractéristique
# de température.
# Si vous n'avez pas de capteur de température :
#    Les mesures sont simulées avec un générateur de nombres aléatoires 
# Si vous disposez d'un capteur de température I2C BME280 :
#    La température est remontée par celui-ci
# Les mesures (simulées ou pas) sont mises à jour toutes les cinq secondes 
# et notifiées à la même fréquence à un éventuel central BLE qui serait connecté.

import ble_sensor # Implémentation du BLE GATT selon Blue-ST
import bluetooth # Pour la gestion du BLE
from time import sleep_ms # Pour la gestion du temps et des temporisations

# Est-ce qu'un module I2C capteur de température BME280 est connecté ) la NUCLEO-WB55 ?
SENSOR = True

# Si vous ne disposez pas d'un capteur de température
if not SENSOR:
	from random import randint # Pour la génération de valeurs aléatoires
else:
# Si vous disposez d'un capteur de température I2C  BME280

	from machine import I2C # Pilote du contrôleur de bus I2C
	import bme280 # Pilote du capteur

	# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
	i2c = I2C(1)

	# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
	sleep_ms(1000)

	# Liste des adresses I2C des périphériques présents
	print("Adresses I2C utilisées : " + str(i2c.scan()))

	# Instanciation du capteur
	bme = bme280.BME280(i2c=i2c)

# Instantiation du BLE
ble = bluetooth.BLE()
ble_device = ble_sensor.BLESensor(ble)

while True:

	if not SENSOR:
		# Valeur aléatoire entre -20 et 90 °C
		temperature = randint(-20, 90)
	else:
		# Lecture des valeurs mesurées par le BME280
		bme280 = bme.values
		temperature = round(bme280[0],1)

	# Envoi en BLE de la température en choisissant de notifier l'application
	ble_device.set_data_temperature(temperature, notify=1) 
	
	# Temporisation de cinq secondes
	sleep_ms(5000)
