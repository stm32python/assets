# This example shows how to program a BLE peripheral with the Bluetooth SIG standard
# standard to send temperature and humidity measurements using a service containing two
# characteristics.
# The measurements are simulated with a random number generator and then updated every
# Every five seconds by the peripheral, and notified at the same frequency to a central.

import bluetooth # To manage the BLE
import random # To generate random values
from struct import pack # To build the BLE features payloads, by aggregating bytes
from time import sleep_ms # To manage timeouts in milliseconds
from ble_advertising import advertising_payload # To build advertising frames

# Constants to build BLE services
# See: https://docs.micropython.org/en/latest/library/ubluetooth.html

_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_INDICATE_DONE = const(20)

_FLAG_READ = const(0x0002)
_FLAG_NOTIFY = const(0x0010)
_FLAG_INDICATE = const(0x0020)

# SIG identifier of the environmental data service.

# See org.bluetooth.service.environmental_sensing
_ENV_SENSE_UUID = bluetooth.UUID(0x181A)

# SIG identifier of the temperature feature.
# See org.bluetooth.characteristic.temperature

_TEMP_CHAR = (
	bluetooth.UUID(0x2A6E),
	# The characteristic can be read, notified and "indicated
	_FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# SIG identifier of the moisture feature.
# See org.bluetooth.characteristic.temperature
_HUMI_CHAR = (
	bluetooth.UUID(0x2A6F),
	# The characteristic can be read, notified and "indicated
	_FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# Build a two-feature service.
_ENV_SENSE_SERVICE = (
	_ENV_SENSE_UUID,
	(_TEMP_CHAR,_HUMI_CHAR,),
)

# Icon associated with an environmental data advertiser (GAP)
# See org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_ENVSENSOR = const(5696)

# Class to manage environmental data sharing
class BLEenvironment:

	# Initializations
	def __init__(self, ble, name="Nucleo-WB55"):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		 # Predicts two characteristics (temperature and humidity)
		((self._temp_handle,self._humi_handle,),) = self._ble.gatts_register_services((_ENV_SENSE_SERVICE,))
		self._connections = set()
		self._payload = advertising_payload(
			name=name, services=[_ENV_SENSE_UUID], appearance=_ADV_APPEARANCE_GENERIC_ENVSENSOR
		)
		self._advertise()
		self._handler = None

	# BLE event management
	def _irq(self, event, data):
		# When a central connects ...
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
			print("Connected")

		# When a central disconnects ...
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			self._connections.remove(conn_handle)
			# Relaunch advertising for future connections
			self._advertise()
			print("Disconnected")
		# When an indicate event is validated, acknowledge its receipt
		elif event == _IRQ_GATTS_INDICATE_DONE:
			conn_handle, value_handle, status = data

	def set_temp(self, temp_deg_c, notify=False, indicate=False):
		# Writes the temperature in float format "<f" and leaves it for a possible central to read.
		self._ble.gatts_write(self._temp_handle, pack("<f", temp_deg_c))
		if notify or indicate:
			for conn_handle in self._connections:
				if notify:
					# Notify connected central
					self._ble.gatts_notify(conn_handle, self._temp_handle)
				if indicate:
					# "Indicate" the connected central  (like Notify, but with an acknowledgement)
					self._ble.gatts_indicate(conn_handle, self._temp_handle)

	def set_humi(self, humi_percent, notify=False, indicate=False):
		
		# Writes the humidity in float format "<f" and leaves it for a possible central to read.
		self._ble.gatts_write(self._humi_handle, pack("<f", humi_percent))
		if notify or indicate:
			for conn_handle in self._connections:
				if notify:
					# Notify connected central
					self._ble.gatts_notify(conn_handle, self._humi_handle)
				if indicate:
					# "Indicate" the connected central (like Notify, but with an acknowledgement)
					self._ble.gatts_indicate(conn_handle, self._humi_handle)

	# Sends advertising frames every 5 seconds, specifies that we can connect to the object
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = True)


# Main program

print("BLE peripheral")

ble = bluetooth.BLE()
ble_device = BLEenvironment(ble)

while True:

	# simulated environmental values
	temperature = random.randint(-20, 90) # random value between -20 and 90 °C
	humidity = random.randint(0, 100) # Random value between 0 and 100 %.

	print("The peripheral sends:")
	print(" - Temperature (°C) : " + str(temperature))
	print(" - Relative humidity (%) : " + str(humidity))

	# Send in BLE the temperature by choosing to notify the application
	ble_device.set_temp(temperature, notify=True, indicate = False)
	
	# Send in BLE the humidity by choosing to notify the application
	ble_device.set_humi(humidity, notify=True, indicate = False)

	# Delay of five seconds
	sleep_ms(5000)
