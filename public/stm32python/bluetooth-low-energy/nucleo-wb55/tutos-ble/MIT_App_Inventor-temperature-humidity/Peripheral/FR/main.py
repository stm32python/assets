# Cet exemple montre comment programmer un périphérique BLE GATT avec le standard Bluetooth SIG
# pour envoyer des mesures de température et d'humidité à l'aide d'un service contenant deux
# caractéristiques.
# Les mesures sont simulées avec un générateur de nombres aléatoires puis mises à jour toutes 
# les cinq secondes par le périphérique, et notifiées à la même fréquence à un central éventuellement connecté.

import bluetooth # Pour la gestion du BLE
import random # Pour la génération de valeurs aléatoires
from struct import pack # Pour construire les "payloads" des caractéristiques BLE, en aggrégeant des octets
from time import sleep_ms # Pour la gestion du temps et des temporisations
from ble_advertising import advertising_payload # Pour construire des trames d'advertising
from binascii import hexlify # Convertit une donnée binaire en sa représentation hexadécimale

# Constantes pour construire les services BLE
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_INDICATE_DONE = const(20)

_FLAG_READ = const(0x0002)
_FLAG_NOTIFY = const(0x0010)
_FLAG_INDICATE = const(0x0020)

# Identifiant SIG du service de données environnementales.
# Voir org.bluetooth.service.environmental_sensing
_ENV_SENSE_UUID = bluetooth.UUID(0x181A)

# Identifiant SIG de la caractéristique de température.
# Voir org.bluetooth.characteristic.temperature
_TEMP_CHAR = (
	bluetooth.UUID(0x2A6E),
	# La caratéristique peut être lue, se notifier et "s'indiquer"
	_FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# Identifiant SIG de la caractéristique d'humidité.
# Voir org.bluetooth.characteristic.temperature
_HUMI_CHAR = (
	bluetooth.UUID(0x2A6F),
	# La caratéristique peut être lue, se notifier et "s'indiquer"
	_FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# Construction d'un service à deux caractéristiques.
_ENV_SENSE_SERVICE = (
	_ENV_SENSE_UUID,
	(_TEMP_CHAR,_HUMI_CHAR,),
)

# Icône associée à un advertiser (GAP) de données environnementales.
# Voir org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_ENVSENSOR = const(5696)

# Classe pour gérer le partage de données environnementales
class BLEenvironment:

	# Initialisations
	def __init__(self, ble, name="Nucleo-WB55"):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		 # Prévoit deux caractéristiques (température et humidité)
		((self._temp_handle,self._humi_handle,),) = self._ble.gatts_register_services((_ENV_SENSE_SERVICE,))
		self._connections = set()
		self._payload = advertising_payload(
			name=name, services=[_ENV_SENSE_UUID], appearance=_ADV_APPEARANCE_GENERIC_ENVSENSOR
		)
		self._advertise()
		self._handler = None
		
		# Affiche l'adresse MAC de l'objet
		dummy, byte_mac = self._ble.config('mac')
		hex_mac = hexlify(byte_mac) 
		print("Adresse MAC : %s" %hex_mac.decode("ascii"))

	# Gestion des évènements BLE
	def _irq(self, event, data):
		# Lorsqu'un central se connecte...
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
			print("Connecté")
			
		# Lorsqu'un central se déconnecte...
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			self._connections.remove(conn_handle)
			# Relance l'advertising pour de futures connexions
			self._advertise()
			print("Déconnecté")
			
		# Lorsqu'un évènement "indicate" est validé, renvoie un accusé de réception
		elif event == _IRQ_GATTS_INDICATE_DONE:
			conn_handle, value_handle, status = data

	# Pour envoyer la température ...
	def set_temp(self, temp_deg_c, notify=False, indicate=False):
		
		# Ecrit la température au format float "<f" et la laisse en lecture à un éventuel central.
		self._ble.gatts_write(self._temp_handle, pack("<f", temp_deg_c))
		if notify or indicate:
			for conn_handle in self._connections:
				if notify:
					# Notifie les centraux connectés du rafraichissement de la valeur de la température
					self._ble.gatts_notify(conn_handle, self._temp_handle)
				if indicate:
					# "Indicate" les centraux connectés (comme Notify, mais requiert un accusé de réception)
					self._ble.gatts_indicate(conn_handle, self._temp_handle)

	# Pour envoyer l'humidité ...
	def set_humi(self, humi_percent, notify=False, indicate=False):
		
		# Ecrit l'humidité au format float "<f" et la laisse en lecture à un éventuel central.
		self._ble.gatts_write(self._humi_handle, pack("<f", humi_percent))
		if notify or indicate:
			for conn_handle in self._connections:
				if notify:
					# Notifie les centraux connectés du rafraichissement de la valeur de l'humidité
					self._ble.gatts_notify(conn_handle, self._humi_handle)
				if indicate:
					# "Indicate" les centraux connectés (comme Notify, mais requiert un accusé de réception)
					self._ble.gatts_indicate(conn_handle, self._humi_handle)

	# Envoie des trames d'advertising toutes les 5 secondes, précise que l'on pourra se connecter au device
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = True)


# Programme "principal"
def demo():

	print("Périphérique BLE")

	# Objet BLE
	ble = bluetooth.BLE()
	
	# Instance de la classe environnementale
	ble_device = BLEenvironment(ble)

	while True:

		temperature = random.randint(-20, 90) # Valeur aléatoire entre -20 et 90 °C
		humidite = random.randint(0, 100) # Valeur aléatoire entre 0 et 100 %

		# Envoi en BLE de la température en choisissant de notifier l'application
		ble_device.set_temp(temperature, notify=True, indicate = False) 

		# Envoi en BLE de l'humidité en choisissant de notifier l'application
		ble_device.set_humi(humidite, notify=True, indicate = False)
		
		# Temporisation de cinq secondes
		sleep_ms(5000)

if __name__ == "__main__":
	demo()
