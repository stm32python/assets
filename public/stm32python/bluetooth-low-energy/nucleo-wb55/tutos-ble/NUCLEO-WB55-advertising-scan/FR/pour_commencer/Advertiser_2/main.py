# Cet exemple montre comment programmer un advertiser BLE avec le standard Bluetooth SIG
# pour plublier des mesures de température et d'humidité en mode advertising (GAP).
# Les mesures sont simulées avec un générateur de nombres aléatoires puis mises à jour toutes 
# les cinq secondes par l'advertiser, et notifiées à la même fréquence à tout scanner à proximité.

import bluetooth # Bibliothèque pour la gestion du BLE
import random # Bibliothèque pour la génération de valeurs aléatoires
from struct import pack # Méthode pour construire les "payloads" des caractéristiques BLE, en aggrégeant des octets
from time import sleep_ms # Méthode pour la gestion des temporisations en millisecondes
from ble_advertising import advertising_payload # Méthode pour construire des trames d'advertising

# Identifiant de l'advertiser
mon_nom = "Adv2"

# Icône pour une trame GAP environnementale.
# Voir org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_ENVSENSOR = const(5696)

# Classe pour gérer l'advertising de données environnementales
class BLE_Adv_Env:

	# Initialisations
	def __init__(self, ble):
		self._ble = ble
		self._ble.active(True)
		self._connections = set()
		self._handler = None

	# Envoie des trames d'advertising toutes les 5 secondes, précise que l'on ne pourra pas se connecter à l'advertiser
	def advertise(self, interval_us=500000, message = None):
		self._payload = advertising_payload(name=message, services=None, appearance=_ADV_APPEARANCE_GENERIC_ENVSENSOR)
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = False)

# Programme principal

print("Hello, je suis " + mon_nom)

# Initialisations du BLE et du protocole GAP
ble = bluetooth.BLE()
ble_device = BLE_Adv_Env(ble)

while True:

	# Mesures (simulées)
	temperature = random.randint(-20, 90) # Valeur aléatoire entre -20 et 90 °C
	humidite = random.randint(0, 100) # Valeur aléatoire entre 0 et 100 %
	
	stemperature = str(temperature)
	shumidite = str(humidite)
	
	print(mon_nom + " publie  :")
	print(" - Température (°C) : " + stemperature)
	print(" - Humidité relative (%) : " + shumidite)
	
	# Publication en BLE de la température et de l'humidité
	ble_device.advertise(message = mon_nom + "|" + stemperature + "|" + shumidite)

	# Temporisation de cinq secondes
	sleep_ms(5000)