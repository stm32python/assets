# This example shows how to program a BLE advertiser with the Bluetooth SIG
# To publish temperature and humidity measurements in advertising mode (GAP).
# The measurements are simulated with a random number generator and then updated
# and published every five seconds by the advertiser.
# Advertisers report their identity by sending their MAC address.

import bluetooth # Library to manage BLE
import random # Library for generating random values
from time import sleep_ms # Method to manage timeouts in milliseconds
from ble_advertising import advertising_payload # Method to build advertising frames
from binascii import hexlify # Convert a binary data to its hexadecimal representation

# Advertiser's Identifier
hex_mac = None

# Icon for an environmental GAP frame.
# See org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_ENVSENSOR = const(5696)

# Class to manage the advertising of environmental data
class BLE_Adv_Env:

	# Initializations
	def __init__(self, ble):
		self._ble = ble
		self._ble.active(True)
		self._connections = set()
		self._handler = None

		# Obtains & records the MAC address of the object
		dummy, byte_mac = self._ble.config('mac')
		global hex_mac
		hex_mac = str(hexlify(byte_mac).decode("ascii"))

	# Sends advertising frames every 5 seconds, specifies that we will not be able to connect to the advertiser
	def advertise(self, interval_us=500000, message = None):
		self._payload = advertising_payload(name=message, services=None, appearance=_ADV_APPEARANCE_GENERIC_ENVSENSOR)
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = False)

# Main program

# Initializations of the BLE and the GAP protocol
ble = bluetooth.BLE()
ble_device = BLE_Adv_Env(ble)

while True:

	# Measurements (simulated)
	temperature = random.randint(-20, 90) # Random value between -20 and 90 °C
	humidity = random.randint(0, 100) # Random value between 0 and 100
	
	stemperature = str(temperature)
	shumidity = str(humidity)
	
	print("Publication of " + hex_mac)
	print(" - Temperature (°C) : " + stemperature)
	print(" - Relative humidity (%) : " + shumidity)
	
	# Publication in BLE of temperature and humidity
	ble_device.advertise(message = hex_mac + "|" + stemperature + "|" + shumidity)

	# Five second delay
	sleep_ms(5000)
