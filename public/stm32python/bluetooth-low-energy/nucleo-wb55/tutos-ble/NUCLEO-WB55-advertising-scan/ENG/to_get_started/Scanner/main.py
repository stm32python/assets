# Example of a NUCLEO-WB55 in BLE scanner mode listening to frames coming from
# other (two in our example) NUCLEO-WB55 cards in advertising mode.
# The scanner captures the advertising frames whose name starts with "Adv". It also displays the
# temperature and humidity data sent as text in the name of the advertising frames.

import bluetooth # Library to manage the BLE
from time import sleep_ms # Method for generating timings in milliseconds
from ble_advertising import decode_services, decode_name # Methods to decode the content of advertising frames

# Constants used for GAP (voir https://docs.micropython.org/en/latest/library/ubluetooth.html)
_IRQ_SCAN_RESULT = const(5) # The scan signals an advertising frame
_IRQ_SCAN_DONE = const(6) # The scan has ended

# Advertising notifications of objects that are not connectable
_ADV_SCAN_IND = const(0x02)
_ADV_NONCONN_IND = const(0x03)

# Settings for setting the duty cycle of the GAP scan
_SCAN_DURATION_MS = const(1000)
_SCAN_INTERVAL_US = const(10000)
_SCAN_WINDOW_US = const(10000)

# Class to create an environmental BLE scanner
class BLE_Scan_Env:

	# Initializations
	def __init__(self, ble):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		self._reset()

	# Deletion of cached data
	def _reset(self):
		# Cached names and addresses after a device scan step
		self._message = set()
		# Advertising scan callback
		self._scan_callback = None

	# Event management
	def _irq(self, event, data):
		# The device scan identified at least one advertiser
		if event == _IRQ_SCAN_RESULT:
			# Reading the content of the advertising frame
			addr_type, addr, adv_type, rssi, adv_data = data

			# If the advertising frame specifies that its transmitter is not connectable
			if adv_type in (_ADV_SCAN_IND, _ADV_NONCONN_IND):
				# The message is contained in the "name" field of the advertising frame
				smessage = decode_name(adv_data)
				# If the message starts with "Adv", save it in a "set".
				# (to avoid recording the same message several times during the scan)
				if smessage[0:3] == "Adv":
					self._message.add(smessage)

		# When the scan has ended (after _SCAN_DURATION_MS)
		elif event == _IRQ_SCAN_DONE:
			if self._scan_callback: # If a callback related to this event has been assigned
				if len(self._message) > 0:
					# At least one message was recorded during the scan, call the callback to display
					self._scan_callback(self._message)
					# Disable the callback
					self._scan_callback = None

	# Proceed with the scan
	def scan(self, callback = None):
		# Initialize (empty) the set that will contain the messages
		self._message = set()
		# Assign the callback that will be called at the end of the scan
		self._scan_callback = callback
		# Scans during _SCAN_DURATION_MS, during _SCAN_WINDOWS_US durations spaced by _SCAN_INTERVAL_US
		self._ble.gap_scan(_SCAN_DURATION_MS, _SCAN_INTERVAL_US, _SCAN_WINDOW_US)

# Main program

print("Hello, I'm Scanny")

# Instantiation of the BLE
ble = bluetooth.BLE()
scanner = BLE_Scan_Env(ble)

# Callback function called at the end of the scan
def on_scan(message):
	# For each advertising message recorded
	for payload in message:
		# The measurements are separated by the split instruction
		objet, temp, humi = payload.split("|")
		print("Message from " + objet + " :")
		print(" - Temperature : " + temp + "°C")
		print(" - Humidity : " + humi + "%")

while True:
	# Starts the scan of the advertising frames
	scanner.scan(callback=on_scan)

	sleep_ms(5000)
