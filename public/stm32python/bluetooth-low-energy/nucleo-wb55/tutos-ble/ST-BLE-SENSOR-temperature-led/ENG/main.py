# Purpose of the script:
# Communication in BLE of a temperature value read from an X-NUCLEO IKS01A3 expansion board.
# Display on the smartphone application ST BLE Sensor.
# Possibility to control the blue LED on the NUCLEO-WB55 board..
# Hardware:
# - A NUCLEO-WB55 board
# - An X-NUCLEO IKS01A3 extension board

from time import sleep_ms, time # To manage the timings and the time stamp
import bluetooth # To manage the BLE protocol
import ble_sensor # To implement the GATT Blue-ST protocol
import hts221 # To manage the MEMS sensor HTS221

from machine import I2C

# We use the I2C n°1 of the NUCLEO-W55 board to communicate with the sensors
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instance of HTS221
sensor = hts221.HTS221(i2c)

ble = bluetooth.BLE() # Instance of the BLE class
ble_device = ble_sensor.BLESensor(ble) # Instance of the Blue-ST class

while True: # Loop without exit clause

	# timestamp
	timestamp = time()

	# temperature value
	temp = sensor.temperature()

	stime = str(timestamp)
	stemp = str(round(temp,1))
	# Multiply by 10 the temperature to encode it as an integer while keeping a decimal
	# (following Blue-ST standard).
	ble_temp = int(temp*10)

	# Display on the serial port of the USB USER
	print("Time stamp : " + stime + " Temperature : " + stemp + " °C")

	# Send in BLE the time stamp and the temperature by choosing to notify the application
	ble_device.set_data_temperature(timestamp, ble_temp, notify=1)

	# One second delay
	sleep_ms(1000)
