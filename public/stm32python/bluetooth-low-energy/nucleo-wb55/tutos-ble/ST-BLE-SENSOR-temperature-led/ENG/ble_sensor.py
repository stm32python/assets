# Purpose of the script: Implementation of the GATT Blue-ST protocol for a peripheral
# Definition of a _ST_APP_SERVICE with two characteristics:
# 1 - SWITCH : to turn off and on a LED of the peripheral from a central
# 2 - TEMPERATURE : to send a temperature measurement from the peripheral to a central

import bluetooth # To manage the BLE
from ble_advertising import advertising_payload # To manage GAP advertising
from struct import pack # To aggregate the bytes sent by the BLE frames
from micropython import const # To define integer constants
import pyb # To manage the blue LED of the NUCLEO-WB55

# Constants defined for the Blue-ST protocol
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)

# For UUIDs and codes, we refer to the Blue-ST SDK documentation available here:
# https://www.st.com/resource/en/user_manual/dm00550659-getting-started-with-the-bluest-protocol-and-sdk-stmicroelectronics.pdf.

# 1 - Definition of the custom service according to the Blue-ST SDK

# Indicates that we will communicate with an application compliant with the Blue-ST protocol:
_ST_APP_UUID = bluetooth.UUID('00000000-0001-11E1-AC36-0002A5D5C51B')

# UUID of a temperature characteristic
_TEMPERATURE_UUID = (bluetooth.UUID('00040000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY)

# UUID of a switch feature
_SWITCH_UUID = (bluetooth.UUID('20000000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_WRITE|bluetooth.FLAG_NOTIFY)

# The service will contain these two features
_ST_APP_SERVICE = (_ST_APP_UUID, (_TEMPERATURE_UUID, _SWITCH_UUID))

# 2 - Construction of the GAP advertising frame (message content)
_PROTOCOL_VERSION = const(0x01) # Protocol version
_DEVICE_ID = const(0x80) # Generic NUCLEO card
_DEVICE_MAC = [0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC] # Dummy MAC hardware address
_FEATURE_MASK = const(0x20040000) # Selected services: temperature (2^18) and switch (2^29)

# Explanation of the calculation of the mask determining the characteristics of the active service (_FEATURE_MASK)
# Each characteristic is associated with a binary code. We simply have to sum the codes of all the characteristics
# that we want to expose with GATT:

# Characteristic SWITCH: code = 2^29 = 1000000000000000000000000000000000 (in binary) = 20000000 (in hexadecimal)
# TEMPERATURE characteristic: code = 2^18 = 0000000000010000000000000000000000 (in binary) = 40000 (in hexadecimal)
# _FEATURE_MASK = SWITCH + TEMPERATURE = 100000000001000000000000000000 (in binary) = 20040000 (in hexadecimal)

# Advertising frame: concatenation of information with the Micropython function "pack"
# The string '>BBI6B' indicates the format of the arguments, see the pack documentation here: https://docs.python.org/3/library/struct.html
_MANUFACTURER = pack('>BBI6B', _PROTOCOL_VERSION, _DEVICE_ID, _FEATURE_MASK, *_DEVICE_MAC)

# Initialization of LEDs
led_red = pyb.LED(1)
led_blue = pyb.LED(3)

class BLESensor:

	# Initialization, GAP start and radio publication of advertising frames
	def __init__(self, ble, name='WB55-MPY'):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		((self._temperature_handle,self._switch_handle),) = self._ble.gatts_register_services((_ST_APP_SERVICE, ))
		self._connections = set()
		self._payload = advertising_payload(name=name, manufacturer=_MANUFACTURER)
		self._advertise()
		self._handler = None

	# BLE event management...
	def _irq(self, event, data):
		# If a central has sent a connection request
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _, = data
			# Connects to the central (and stops advertising automatically)
			self._connections.add(conn_handle)
			print("Connected")
			led_blue.on() # Turns on the blue LED

		# If the central has sent a disconnection request
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _, = data
			self._connections.remove(conn_handle)
			# Relaunch advertising to allow new connections
			self._advertise()
			print("Disconnected")

		# If a write is detected in the SWITCH characteristic of the LED
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle, = data
			if conn_handle in self._connections and value_handle == self._switch_handle:
				# Read the value of the characteristic
				data_received = self._ble.gatts_read(self._switch_handle)
				self._ble.gatts_write(self._switch_handle, pack('<HB', 1000, data_received[0]))
				self._ble.gatts_notify(conn_handle, self._switch_handle)
				# Depending on the value written, the red LED is turned on or off
				if data_received[0] == 1:
					led_red.on() # Turns on the red LED
				else:
					led_red.off() # Turns off the red LED

	# We write the timestamp and the temperature value in the "temperature" characteristic
	def set_data_temperature(self, timestamp, temperature, notify):
		self._ble.gatts_write(self._temperature_handle, pack('<Hh', timestamp, temperature))
		if notify:
			for conn_handle in self._connections:
				# Notifies Central (the smartphone) that the feature has just been written and can be read
				self._ble.gatts_notify(conn_handle, self._temperature_handle)

	# Starts advertising with a period of 5 seconds, specifies that a central can connect to the peripheral
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable=True)
		led_blue.off() # Turns off the blue LED
