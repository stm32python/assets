# Objet du script :
# Communication en BLE d'une valeur de température lue sur une carte d'extension X-NUCLEO IKS01A3.
# Affichage sur l'application smartphone ST BLE Sensor.
# Possibilité de commander une LED de la carte NUCLEO-WB55.
# Matériel : 
#  - Une carte NUCLEO-WB55
#  - Une carte d'extension  X-NUCLEO IKS01A3

from time import sleep_ms, time # Pour gérer les temporisations et l'horodatage
import bluetooth # Pour gérer le protocole BLE 
import ble_sensor # Pour l'implémentation du protocole GATT Blue-ST
import hts221 # Pour gérer le capteur MEMS HTS221 

from machine import I2C

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec les capteurs
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instance du HTS221
sensor = hts221.HTS221(i2c)

ble = bluetooth.BLE() # Instance de la classe BLE
ble_device = ble_sensor.BLESensor(ble) # Instance de la classe Blue-ST

while True: # Boucle sans clause de sortie

	# Horodatage
	timestamp = time()

	# valeur de température
	temp = sensor.temperature()

	stime = str(timestamp)
	stemp = str(round(temp,1))
	# Multiplie par 10 la température pour la coder sous forme d'entier en conservant une décimale 
	# (norme adoptée par Blue-ST).
	ble_temp = int(temp*10)

	# Affichage sur le port série de l'USB USER
	print("Horodatage : " + stime + " Température : " + stemp + " °C")

	# Envoi en BLE de l'horodatage et de la température en choisissant de notifier l'application
	ble_device.set_data_temperature(timestamp, ble_temp, notify=1) 

	# Temporisation d'une seconde
	sleep_ms(1000)
