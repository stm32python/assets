# Purpose of the script: implementation of the UART BLE service from Nordic Semiconductors (NUS for
# "Nordic UART Service").
# Sources :
#	https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_uart_peripheral.py

import bluetooth # Classes "primitives du BLE"
from ble_advertising import advertising_payload # Pour construire la trame d'advertising
from binascii import hexlify # Convertit une donnée binaire en sa représentation hexadécimale

# Constants required to build the BLE UART service
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)

# Definition of the UART service with its two characteristics RX and TX

_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_TX = (
	bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_NOTIFY, # This feature will notify the exchange of any changes made to it by the device
)
_UART_RX = (
	bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_WRITE, # The central can write in this characteristic
)
_UART_SERVICE = (
	_UART_UUID,
	(_UART_TX, _UART_RX),
)

# org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_COMPUTER = const(128)

# Maximum number of bytes that can be exchanged by the RX characteristic
_MAX_NB_BYTES = const(100)

ascii_mac = None

class BLEUART:

	# Initializations
	def __init__(self, ble, name="mpy-uart", rxbuf=_MAX_NB_BYTES):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		# Service registration
		((self._tx_handle, self._rx_handle),) = self._ble.gatts_register_services((_UART_SERVICE,))
		# Increase the size of the rx buffer and activate the append mode
		self._ble.gatts_set_buffer(self._rx_handle, rxbuf, True)
		self._connections = set()
		self._rx_buffer = bytearray()
		self._handler = None
		# Advertising of the service:
		# You can add services=[_UART_UUID] as an option, but this may make the payload of the feature too long
		self._payload = advertising_payload(name=name, appearance=_ADV_APPEARANCE_GENERIC_COMPUTER)
		self._advertise()

		# Display the MAC address of the object
		dummy, byte_mac = self._ble.config('mac')
		hex_mac = hexlify(byte_mac)
		global ascii_mac
		ascii_mac = hex_mac.decode("ascii")
		print("Adresse MAC : %s" %ascii_mac)

	# Interruption to manage receptions
	def irq(self, handler):
		self._handler = handler

	# Monitor the connections in order to send notifications
	def _irq(self, event, data):
		# If a central connects
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
		# If a central disconnects
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			if conn_handle in self._connections:
				self._connections.remove(conn_handle)
			# Restarts the advertising to allow new connections
			self._advertise()
		# When a client writes to a feature exposed by the server
		#(management of reception events from the central)
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle = data
			if conn_handle in self._connections and value_handle == self._rx_handle:
				self._rx_buffer += self._ble.gatts_read(self._rx_handle)
				if self._handler:
					self._handler()

	# Called to check if there are messages waiting to be read in RX
	def any(self):
		return len(self._rx_buffer)

	# Returns the characters received in RX
	def read(self, sz=None):
		if not sz:
			sz = len(self._rx_buffer)
		result = self._rx_buffer[0:sz]
		self._rx_buffer = self._rx_buffer[sz:]
		return result

	# Writes in TX a message to the attention of the central
	def write(self, data):
		for conn_handle in self._connections:
			self._ble.gatts_notify(conn_handle, self._tx_handle, data)

	# Terminate the connection to the simulated serial port
	def close(self):
		for conn_handle in self._connections:
			self._ble.gap_disconnect(conn_handle)
		self._connections.clear()

	# To start the advertising, specify that a central will be able to connect to the device
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = True)


def demo():

	import time # To manage timeouts

	# Instance of the BLE and the UART service
	ble = bluetooth.BLE()
	uart = BLEUART(ble)

	# Interruptions reception management
	def on_rx():
		print("rx: ", uart.read().decode().strip())

	# We activate the reception by interruptions
	uart.irq(handler=on_rx)
	nums = [4, 8, 15, 16, 23, 42]
	i = 0

	# Management of the keyboard interruption (CTRL+C)
	try:
		while True:
			# We send the information to the central
			uart.write(str(ascii_mac) + "_" + str(nums[i]) + "\r\n")
			i = (i + 1) % len(nums)
			# One second delay
			time.sleep_ms(1000)
	except KeyboardInterrupt:
		pass # If a keyboard interrupt is intercepted, continue the execution of the program

	# Stop the UART service
	uart.close()

# If the script name is "main", execute the "demo()" function
if __name__ == "__main__":
    demo()
