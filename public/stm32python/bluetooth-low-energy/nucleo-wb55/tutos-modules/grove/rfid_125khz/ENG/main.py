# Purpose of the script: implementation of a 125 kHz RFID reader from Grove
# This script is adapted from https://gcworks.fr/tutoriel/esp/LecteurRFID125kHz.html

from pyb import UART # Class to manage the UART

uart = UART(2) # Instance of UART number 2

# Initialization at 9600 bauds, 8 bits frames, no parity check with a stop bit at 1
uart.init(9600, bits=8, parity = None, stop = 1)

# Number of bytes in the TAG identifier
_RFID_TAG_SIZE = const(10)

while True:
	
	# Reading of a RFID tag. 
	# The data arrive on the serial port and are stored in an array of bytes

	# When all bytes of the RFID tag identifier are in the receive queue
	if uart.any()> _RFID_TAG_SIZE: 
		
		# Loads the bytes in an array
		donnee_tag = uart.read()
		print("RFID tag data : ", data_tag.decode("ascii"))

		# We isolate and decode the 5 low weight bytes of the "10 ASCII Data Characters" field
		rfid_hexa = ""
		for i in range(5, 11):
			rfid_hexa += chr(data_tag[i])
		print("RFID identifier in hexa: ", rfid_hexa)

		# The RFID identifier indicated on the tag is obtained by conversion hexadecimal -> decimal
		rfid = str(int(rfid_hexa, 16))
		while len(rfid)< _RFID_TAG_SIZE:
			rfid = "0" + rfid
		print("RFID identifier in decimal : %s" %rfid)