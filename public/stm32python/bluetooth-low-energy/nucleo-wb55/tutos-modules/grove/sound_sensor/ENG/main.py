# Purpose of the script: Implementation of the Grove sound sensor

from time import sleep_ms # To delay
from pyb import Pin, ADC # Pin and co-converter management 
							# analog-to-digital (ADC)

adc = ADC(Pin('A1')) # We "connect" the ADC to pin A1

while True :
	sleep_ms(500) # Delay of 500 milliseconds
	print("Value of the ADC (prop. sound volume): " + str(adc.read())) 