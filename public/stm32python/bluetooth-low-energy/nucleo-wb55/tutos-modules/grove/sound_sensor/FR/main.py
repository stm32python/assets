# Objet du script : Mise en oeuvre du capteur de son Grove

from time import sleep_ms	# Pour temporiser
from pyb import Pin, ADC	# Gestion des broches et du coonvertisseur 
							# analogique-numérique (ADC)

adc = ADC(Pin('A1')) # On "connecte" l'ADC à la broche A1

while True :
	sleep_ms(500) # Temporisation de 500 millisecondes
	print("Valeur de l'ADC (prop. volume sonore) : " + str(adc.read())) 