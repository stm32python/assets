# Purpose of the script: Grove Joystick implementation.

from time import time_sleep_ms # To time out
from pyb import Pin

vertical = ADC(PIN('A0')) # by connecting the joystick to A0, the Y axis will be read by A0 and the X axis by A1
horizontal= ADC(PIN('A1')) #on A1, Y will be read by A1 and X by A2; on A2, Y will be read by A2 and X by A3.

while True :

	sleep_ms(500)
	
	x = vertical.read()
	y = horizontal.read()

	if x <= 780 and x >= 750 :
		print("Top")
	if x <= 280 and x >= 240 :
		print("Bottom")
	if y <= 780 and y >= 750 :
		print("Left")
	if y <= 280 and y >= 240 :
		print("Right")
	if x >= 1000: # By pressing the joystick, the output of the X axis is set to 1024, the maximum.
		print("Pressed")
