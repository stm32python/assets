# Purpose of the script
# Shows how to drive a DC motor with an H-bridge
# based on the L293D or L298N from STMicroelectronics

import pyb
from pyb import Pin, Timer
import time

#Global variables
BP1 = 0
BP2 = 0
BP3 = 1

#BP (input + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3')
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#LED (LED of the NUCLEO board)
led_blue = pyb.LED(3)
led_green = pyb.LED(2)
led_red = pyb.LED(1)

#Motor driver ("enable" + "input 1" and "input 2" as outputs)
motor_enable = pyb.Pin('D3')
motor_pin1 = pyb.Pin('D4', Pin.OUT_PP)
motor_pin2 = pyb.Pin('D5', Pin.OUT_PP)

#PWM for the motor (Timer 1 channel 3, frequency = 1kHz and on "enable")
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=motor_enable)

#Interrupt SW1 (advance the motor)
def ITbutton1(line):
	#Global variables
	global BP1
	global BP2
	global BP3
	#Incremente BP1, the rest at 0
	BP1 = BP1 + 1
	BP2 = 0
	BP3 = 0

#Interruption of SW2 (move back the motor)
def ITbutton2(line):
	#Global variable
	global BP1
	global BP2
	global BP3
	#Incremente BP2, the rest at 0
	BP1 = 0
	BP2 = BP2 + 1
	BP3 = 0

#Interrupt SW3 (stop the motor)
def ITbutton3(line):
	#Global variables
	global BP1
	global BP2
	global BP3
	#Incremental BP3, the rest is 0
	BP1 = 0
	BP2 = 0
	BP3 = BP3 + 1

#Initialization of interrupt vectors
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton2)
irq_3 = pyb.ExtInt(sw3, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton3)

#Function that manages the motor
def etat_moteur(speed):
	#Global variables
	global BP1
	global BP2
	global BP3

	#If BP1 supported
	if((BP1 != 0) and (BP2 == 0) and (BP3 == 0)):
		#Turns on the blue LED
		led_blue.on()
		#Turns on enable 1 / Turns off enable 2
		motor_pin1.high()
		motor_pin2.low()
		#PWM to set the motor voltage
		ch3.pulse_width_percent(speed)
	#If BP2 pressed
	if((BP1 == 0) and (BP2 != 0) and (BP3 == 0)):
		#Lights the green LED
		led_green.on()
		#Turns on enable 2 / Turns off enable 1
		motor_pin1.low()
		motor_pin2.high()
		#PWM to set the motor voltage
		ch3.pulse_width_percent(speed)
	#If BP3 pressed
	if((BP1 == 0) and (BP2 == 0) and (BP3 != 0)):
		#Lights the red LED
		led_red.on()
		#Turns off enable 1 and 2
		motor_pin1.low()
		motor_pin2.low()
		#PWM to set motor voltage
		ch3.pulse_width_percent(0)

#Variable to manage the motor rotation speed
speed = 100

#Infinite loop
while True:
	#Turns off all LEDs by default
	led_blue.off()
	led_green.off()
	led_red.off()
	#Puts in parameter the desired speed
	etat_moteur(vitesse)
