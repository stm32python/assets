# Purpose of the code:
# Receiving a MORSE coded message with a photodiode.
# Step 2: A threshold is applied to the analog signal. Reception controlled by a timer.
# Source adapted from :
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/blob/master/micropython/

from micropython import alloc_emergency_exception_buf # See explanations in the script
from pyb import ADC, Pin, Timer # Access to ADC, GPIO and timer

# Detection threshold for the analog signal.
# Above this value, the IR pulse received is considered as "high" ('1')
# Below this value, the received IR pulse is considered as "low" ('0')
THRESHOLD_LUM = const(2000)

# Instantiation and start of the analog-to-digital converter
adc = ADC(Pin('A1'))

# Buffer table to ensure correct feedback of error messages when
# these occur in the service routine of an interrupt.
alloc_emergency_exception_buf(100)

# Timer 1 overflow interrupt service routine (ISR).
# This ISR receives the infrared pulses and translates them into '0' and '1' symbols according to their intensity

@micropython.native # Directive to optimize the bytecode
def listen(timer):

	# Read the received IR pulse and digitize it with the ADC then
	# comparison of the value with the set LUM_THRESHOLD.

	if adc.read() < THRESHOLD_LUM: # If the digitized value is lower than the threshold ...
		print('0', end='') # Display a '0' (without line feed)

	else: # If the scanned value is higher than the threshold ...
		print('1', end='') # Display a '1' (without line feed)

# Timer frequency when listening to IR pulses: 1 Hz
FREQ = const(1)

# Start timer 1 at the frequency of FREQ Hz.
tim1 = Timer(1, freq = FREQ)

# Assign the "listen" function to the timer 1 overflow interrupt.
tim1.callback(listen)

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code receiver")
print("-" * 32 + "\n")
