# Purpose of the code :
# Emission of a Morse code message with an infrared (IR) LED.
# Step 1: Transmission of the 'EN NT' message
# The Grove Base Shield switch is set to 3.3V
# Source adapted from:
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/blob/master/micropython/

# Quantum time for Morse code; minimum time (in milliseconds) that separates
# two symbols.
TICK = const(1000) # A second one

from pyb import Pin # Class to manage the pins (GPIO)

# The IR LED is connected to pin D4
led = Pin('D4', Pin.OUT_PP)

# Installation of the Morse code transmitter encoder
from morsecode import MorseEncode
morse = MorseEncode(led, tick = TICK)

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code transmitter")
print("-" * 32 + "\n")

# Sends the message "EN NT" encoded in Morse code in a loop
# The corresponding Morse code is: '. -. [space]-. - '
# Where:
# [space] denotes 6 ticks during which no signal is transmitted.
# '. ' corresponds to 'E'
# '-. ' corresponds to 'N
# '-' corresponds to 'T
while True:
	morse.message('EN NT')
