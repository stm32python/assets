# Purpose of the code :
# Emission of a MORSE coded message with an infrared (IR) LED.
# Step 2: Transmission of the 'EN NT' message
# The Grove Base Shield switch is set to 3.3V
# Source adapted from :
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/blob/master/micropython/

# Quantum of time for Morse code; minimum duration (in milliseconds) that separates
# two symbols.
TICK = const(1000) # One second

from pyb import Pin # Class to manage the pins (GPIO)
# The IR LED is connected to pin D4
led = Pin('D4', Pin.OUT_PP)

# Installation of the Morse code transmitter encoder
from morsecode import MorseEncode
morse = MorseEncode(led, tick = TICK)

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code transmitter")
print("-" * 32 + "\n")

# Sends the message "EN NT" encoded in Morse code in a loop
while True:
	morse.message('EN NT')
