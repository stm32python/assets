# Purpose of the code:
# Receiving a MORSE-encoded message with a photodiode.
# Source adapted from:
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/blob/master/micropython/

from micropython import schedule, alloc_emergency_exception_buf # See explanations in the script
from pyb import ADC, Pin, Timer # Access to ADC, GPIO and timers

# Time quantum for Morse code: minimum duration (in milliseconds) that separates
# two symbols. Attention, this value must be the same in the transmitter script !
TICK = const(1000)

# Frequency of the timer listening to the IR pulses : 1 Hz
FREQ = const(1)

# Detection threshold for the analog signal.
# Above this value, the IR pulse received is considered "high" (1)
# Below this value, the received IR pulse is considered "low". (0)
THRESHOLD_LUM = const(2000)

# Instantiation and start of the analog-to-digital converter
adc = ADC(Pin('A1'))

# Buffer table to ensure correct feedback of error messages when
# these occur in the service routine of an interrupt.
alloc_emergency_exception_buf(100)

# Global variables for "on-the-fly" decoding of the Morse code message
nb_low = 0 # Count the number of "low" IR pulses received consecutively
nb_high = 0 # Count of consecutive "high" IR pulses received
symbol= '' # Last symbol of the Morse message received (none)
append = False # Should a received symbol be appended to the currently received Morse message?
decode = False # Do we have a complete word ready to be translated?
message_morse = [] # List of symbols that make up a complete word, in the order they were received

# Interrupt Service Routine (ISR) for timer 1 overflow.
# This ISR receives the infrared pulses and translates them into symbols '.', '-' and ' '.

@micropython.native # Directive to optimize the bytecode
def listen(timer):

	# Access to global variables
	global nb_low, nb_high
	global symbol, decode, append

	# Read the received IR pulse and digitize it with the ADC then
	# comparison of the value with the set LUM_THRESHOLD.
	
	# Suspends the reading of the pulses until the "decode_task" coroutine
	# has finished its work
	while append or decode:
		pass

	if adc.read() < THRESHOLD_LUM: # If the digitized value is below the threshold ...

		nb_low += 1 # An additional low pulse is counted
		
		# If we had counted 1 consecutive high pulse(s) until now...
		if nb_high == 1:
			symbol = '.' # Then the last Morse symbol transmitted was a '.'
			append = True # We can add this '.' to the list of received symbols
# print('.', end='')

		# If we had counted 3 consecutive high pulses so far...
		elif nb_high == 3:
			symbol = '-' # Then the last Morse symbol transmitted was a '-'.
			append = True # We can add this '-' to the list of received symbols
# print('-', end='')

		 # We have just received a low pulse, so the count of consecutive high pulses
		 # consecutive and reset to zero
		nb_high = 0
	
	else: # If the digitized value is higher than the threshold ...
	
		nb_high += 1 # One more high pulse is counted

		# If we had counted 3 consecutive low pulses so far...
		if nb_low == 3:
			symbol = ' ' # Then the last Morse symbol transmitted was a ' ' (character separator)
			append = True # We can add this ' ' to the list of received symbols
# print(' ', end='')

		# If we had counted 9 consecutive low pulses so far...
		elif nb_low == 9:
			# Then the last symbol received is a long space; we have received a complete word!
			decode = True # We can start the translation of the received word.

		 # We have just received a high pulse, so the count of consecutive low pulses is
		 # and reset to zero
		nb_low = 0

# Starts timer 1 at FREQ Hz.
tim1 = Timer(1, freq = FREQ)

# Assigns the "listen" function to the timer 1 overflow interrupt.
tim1.callback(listen)

# Installing the Morse code decoder
from morsecode import MorseDecode
morse = MorseDecode()

import uasyncio # For asynchronous execution

# Decodes msg written in Morse code to Latin alphabet
@micropython.native # Directive to optimize the bytecode
def morse_to_latin(msg):
	print(morse.decode(msg))

# Coroutine / task (asynchronous) to decode words received from Morse code to
# the Latin alphabet.

@micropython.native # Directive to optimize the bytecode
async def decode_task():

	DELAY = TICK // 10 # To time out ...

	# Access to global variables
	global symbol, decode, append

	while True:

		if append: # If a new symbol has been received ...

			message_morse.append(symbol) # Adds the symbol to those already received
			append = False # Tells the ISR "listen" that the job is done

		elif decode: # If all symbols of a complete word have been received ...

			msg = ''.join(message_morse) # Save the word
			message_morse.clear() # Clears the list of received symbols
			decode = False # Tells the ISR "listen" that the job is done

			# Proceed to decode the word in Latin alphabet "as soon as possible
			schedule(morse_to_latin, msg)

		await uasyncio.sleep_ms(DELAY)

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code receiver")
print("-" * 32 + "\n")

# Call to the scheduler which launches the asynchronous execution of the decode_task function
uasyncio.run(decode_task())
