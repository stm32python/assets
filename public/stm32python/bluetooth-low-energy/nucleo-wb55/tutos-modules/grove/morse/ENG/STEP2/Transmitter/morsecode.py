# Code adapted from :
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/tree/master/
# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-03-24
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython V1.14
# Project: Morse Code
# Link: https://morsecode.world/international/morse.html

from utime import sleep_ms

# The international Morse code
# 1 - The duration of a dot (.) is 1 tick.
# 2 - The duration of a dash (-) is 3 ticks.
# 3 - The duration separating the . or - in a character is 1 tick.
# 4 - The length of time between two consecutive characters in a word is 3 ticks
# 5 - The duration separating two words is 9 ticks

# Dictionary for encoding
_MORSE_CODE = {
	'A': '.-',
	'B': '-...',
	'C': '-.-.',
	'D': '-..',
	'E': '.',
	'F': '..-.',
	'G': '--.',
	'H': '....',
	'I': '..',
	'J': '.---',
	'K': '-.-',
	'L': '.-..',
	'M': '--',
	'N': '-.',
	'O': '---',
	'P': '.--.',
	'Q': '--.-',
	'R': '.-.',
	'S': '...',
	'T': '-',
	'U': '..-',
	'V': '...-',
	'W': '.--',
	'X': '-..-',
	'Y': '-.--',
	'Z': '--..',
	'1': '.----',
	'2': '..---',
	'3': '...--',
	'4': '....-',
	'5': '.....',
	'6': '-....',
	'7': '--...',
	'8': '---..',
	'9': '----.',
	'0': '-----',
	'&': '.-...',
	"'": '.----.',
	'@': '.--.-.',
	'(': '-.--.',
	')': '-.--.-',
	':': '---...',
	',': '--..--',
	'=': '-...-',
	'!': '-.-.--',
	'.': '.-.-.-',
	'-': '-....-',
	'+': '.-.-.',
	'_': '...-.',
	'"': '.-..-.',
	'?': '..--..',
	'/': '-..-.',
	}


# Default time quatum (tick) (one second)
_TICK = const(1000)

@micropython.native
# Decode a message written in Morse code 
class MorseDecode(object):

	# Initializations
	def __init__(self):
		# Builds the dictionary for decoding
		self.morse_decode = {v: k for k, v in _MORSE_CODE.items()}

	# Decodes a Morse message (a sequence of '.', '-' and spaces)
	def decode(self, morse_code):
		msg = ''
		for dd in morse_code.split():
			msg += self.morse_decode.get(dd, '#')
		return msg

@micropython.native
# Encoding of a Morse code message to an alphabetical message
class MorseEncode(object):

	# Initializations
	def __init__(self, signal_pin, tick = _TICK):
		self.signal = signal_pin # Pin of the sending device
		self.signal(0) # Turns off the sending device
		self.tick = tick # time quantum ('tick')

	# Sends a message in Morse code
	def message(self, msg):
		# We pass the letters in upper case, we separate the words, we go through them one by one :
		for word in msg.upper().split():
			self._word(word) # Encodes the word in Morse & emits the corresponding signal
			self.space() # Adds a space at the end of the word and the message

	# Encodes a word in Morse code
	def _word(self, word):
		# For each of its characters ...
		for char in word:
			print(char, end=' ')
			# Find the Morse code of the character, proceed to its transmission 
			self._char(_MORSE_CODE.get(char, '#'))

	# Encode and send a character in Morse code
	def _char(self, morse_code):
		# Browse the dashes (-) and dots (.) that make up the character
		for dd in morse_code:
			if dd == '.': # If it is a dot ...
				self.dot() # Send a dot (.)
			elif dd == '-': # If it is a dash ...
				self.dash() # Emit a dash (-)
			else: # If it is neither a '.' nor a '-', do nothing
				pass
		print()
		sleep_ms(self.tick * 2) # Wait for 2 ticks in the "low" state


	# Emit a dot (.)
	def dot(self):
		print('.', end='') # Display a '.' in the terminal
		self.signal(1) # Turn on the transmitting device ("high" state)
		sleep_ms(self.tick * 1) # A dot corresponds to a "high" state for 1 tick
		self.signal(0) # Turns off the transmitting device ("low" state)
		sleep_ms(self.tick) # Wait for 1 tick in the "low" state

	# Emit a dash (-)
	def dash(self):
		print('-', end='') # Display a '-' in the terminal
		self.signal(1) # Turn on the transmitting device (state "high")
		sleep_ms(self.tick * 3) # A dash corresponds to a "high" state for 3 ticks
		self.signal(0) # Turn off the transmitting device (low state)
		sleep_ms(self.tick) # Wait for 1 tick in the "low" state

	# Emit a space (between two words)
	def space(self):
		print() # Displays an empty line in the terminal
		sleep_ms(self.tick * 6) # A space is coded with a "down" state for 6 ticks