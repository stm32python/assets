# Purpose of the code:
# Receipt of a message coded in MORSE with an infrared LED (IR)
# Step 1: Reception in "polling", capture of the analogical signal of the IR LED
# Reading and digitizing the signal with a Grove light sensor (LS06-S phototransistor)
# The Grove Base Shield switch is set to 3.3V

from pyb import ADC, Pin # Analog to digital converter and GPIO
from time import sleep_ms # For time delays

# Quantum of time for Morse code; minimum time (in milliseconds) that separates
# two symbols.
TICK = const(1000) # One second

# Instantiation and start of the analog-to-digital converter
adc = ADC(Pin('A1'))

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code receiver")
print("-" * 32 + "\n")

while True:
	# Digitize the value read on the photodiode
	Measure = adc.read()
	print("Brightness %d" %Measure)
	sleep_ms(TICK) # Half second delay
