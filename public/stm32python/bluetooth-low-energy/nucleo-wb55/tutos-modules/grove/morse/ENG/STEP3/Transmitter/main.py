# Purpose of the code : 
# Emission of a message coded in MORSE with an infrared (IR) LED.
# Step 2 : Emission of the message 'Hello World
# The Grove Base Shield switch is set to 3.3V
# Source adapted from:
# https://gitlab.com/olivierlenoir/MicroPython-MorseCode/-/blob/master/micropython/

# Quantum of time for Morse code; minimum time (in milliseconds) that separates
# two symbols. 
TICK = const(1000) # One second

from pyb import Pin # Class to manage the pins (GPIO)
# The IR LED is connected to pin D4
led = Pin('D4', Pin.OUT_PP)

# Installation of the Morse code transmitter encoder
from morsecode import MorseEncode
morse = MorseEncode(led, tick = TICK)

# Display a header in the serial terminal
print("\n" + "-" * 32)
print("Morse code transmitter")
print("-" * 32 + "\n")

# Send the Morse code "Hello World" message in a loop
while True:
	morse.message('Hello World')
