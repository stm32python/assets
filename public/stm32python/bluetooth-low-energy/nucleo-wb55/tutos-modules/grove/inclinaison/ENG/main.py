# Object: implementation of a ball switch / tilt sensor

from time import sleep_ms # For the timer
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :

	sleep_ms(500) # Delay of 500 milliseconds

	state = p_in.value() # Reading of the sensor, 0 if horizontal and 1 if inclined

	if state:
		print("Tilted")
	else:
		print("Horizontal")
