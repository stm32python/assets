#Source: https://github.com/micropython/micropython/tree/master/drivers
#Script Purpose: To present the read and write functions offered by sdcard.py.

#Arduino pins used for the SPI
# MOSI : D11
# MISO : D12
# SCK : D13
# CS : D9
# SD Module used : MH-SD Card Module powered in 3.3V
# Warning : Catalex µSD modules do not seem to work with sdcard.py !

import sdcard, os
from pyb import SPI

spi = SPI(1, SPI.MASTER, baudrate=100000, polarity=1, phase=0) # SPI bus instance
sd = sdcard.SDCard(spi, machine.Pin('D9')) # SD card module selection pin

vfs = os.VfsFat(sd) # Declaration of a FAT file system
os.mount(vfs, "/fc") # Mount the logical volume associated with the SD card module

print("List of files on the SD card (file system test)")
print(os.listdir("/fc"))

line_alphabet = "abcdefghijklmnopqrstuvwxyz\n"
two_center_line_alphabet = line_alphabet * 200 # 5400 characters
line_numbers = "1234567890"

fn = "/fc/file1.txt"
print()

print("Read / write several blocks")
with open(fn, "w") as f:
	n = f.write(two_center_line_alphabet)
	print(n, "written bytes")
	n = f.write(line_numbers)
	print(n, "written bytes")
	n = f.write(two_center_line_alphabet)
	print(n, "written bytes")

with open(fn, "r") as f:
	result1 = f.read()
	print(len(result1), "bytes read")

fn = "/fc/file2.txt"
print()
print("Read/write a single block")
with open(fn, "w") as f:
	n = f.write(line_numbers) # only one block
	print(n, "written bytes")

with open(fn, "r") as f:
	result2 = f.read()
	print(len(result2), "bytes read")

os.umount("/fc")

print()
print("Check written data")
success = True

if result1 == "".join((two_cent_line_alphabet, line_numbers, two_cent_line_alphabet)):
	print("Large file: OK")
else:
	print("Large file: Failed")
	success = False
	
if result2 == line_numbers:
	print("Small file: OK")
else:
	print("Small file: Failure")
	success = False
print()
print("Tests", "passed" if success else "failed")
