# Purpose of the script: Implementation of grove I2C pressure sensor module,
# temperature and humidity based on the BME280 sensor
# Save the measurements in a file on a SD card

import os,pyb,time,sdcard,bme280 # Drivers to read-write files, to time,
									# of the SD card module and the bme280 sensor.
from machine import I2C, Pin # Drivers for the I2C bus controller and I/O
from pyb import SPI # Driver of the SPI bus controller

# We use the I2C n°1 of the NUCLEO-W55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
time.sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
bme = bme280.BME280(i2c=i2c)

spi = SPI(1, SPI.MASTER, baudrate=100000, polarity=1, phase=0) # SPI bus instance
sd = sdcard.SDCard(spi, machine.Pin('D9')) # SD card module selection pin

vfs = os.VfsFat(sd) # Declaration of a FAT file system
os.mount(vfs, "/fc") # Mount the logical volume associated with the SD card module

fname = "/fc/log.csv"

with open(fname, "w") as log:# Open the file "log.csv" in writing
	
	n = log.write("Time,Temp,Pres,Humi" + '\n')
	
	while True:

		# Delay of one second
		time.sleep_ms(1000)
	
		# Read the measured values
		bme280data = bme.values
	
		# Separation and formatting (rounding) of measurements
		temp = round(bme280data[0],1)
		press = int(bme280data[1])
		humi = int(bme280data[2])

		# Display of measurements
		print('=' * 40) # Print a separating line
		print("Temperature : " + str(temp) + " °C")
		print("Pressure : " + str(press) + " hPa")
		print("Relative humidity: " + str(humi) + " %")
	
		# Writing in a "log.csv" file on the SD card
		t = time.ticks_ms() # Time label
		# write in the file
		#n = log.write(str(t) + "," + str(temp) + "," + str(press) + "," + str(humi) + '\n')
		n = log.write("{},{},{},{}".format(t, temp, press, humi))
		print(n, "written bytes")
