# Purpose of the script: to wire together three Grove RGB LED modules
# and make them display "blue" (first LED), "white" (second LED) and
# (second LED) and "red" (third LED)

from machine import Pin # To manage the inputs / outputs
import p9813 # To manage chainable LEDs
from time import sleep # To time out

# Corresponds to the D7 connector of the Grove base shield
pin_clk = Pin('D7', Pin.OUT) # Clock pin
pin_data = Pin('D8', Pin.OUT) # Data pin

# Number of RGB LED modules
num_led = 3

# Instantiation of a chain of 3 RGB LED modules
chain = p9813.P9813(pin_clk, pin_data, num_led)

# Turns off all LEDs
chain.reset()

# Delay for one second
sleep(1)

# First LED in blue
chain[0] = (0, 0, 255)

# Second LED in white
chain[1] = (255, 255, 255)

# Third LED in red
chain[2] = (255, 0, 0)

# Apply parameters, turn on LEDs
chain.write()

# Change the color of all LEDs to red
#chain.fill((0,0,255))
#chain.write()

