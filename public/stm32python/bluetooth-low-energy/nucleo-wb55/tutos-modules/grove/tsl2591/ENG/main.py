# Script Purpose:
# Adafruit I2C Digital Light Sensor Module Implementation (TLS2591)
# Source : https://github.com/mchobby/esp8266-upy/tree/master/tsl2591

from tsl2591 import * # Module driver
from machine import I2C # I2C bus driver
from time import sleep # to delay

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1) 

# Pause for one second to give the I2C time to initialize
sleep(1)

# Instantiate the sensor
tsl = TSL2591( i2c )

# Caution: manipulating the gain and integration time in an inappropriate way
# inappropriate way can lead to totally wrong results.

# GAIN_LOW (1x gain)
# GAIN_MED (25x gain, default)
# GAIN_HIGH (428x gain)
# GAIN_MAX (9876x gain)
# tsl.gain = GAIN_LOW # x25

# INTEGRATIONTIME_100MS (100ms, default)
# INTEGRATIONTIME_200MS
# INTEGRATIONTIME_300MS
# INTEGRATIONTIME_400MS
# INTEGRATIONTIME_500MS
# INTEGRATIONTIME_600MS
# tsl.integration_time = INTEGRATIONTIME_400MS

while True:

	# Reading a value

	print( "Illuminance : %.1f lx" % tsl.lux )

	# Integer value proportional to the infrared illumination
	ir = tsl.infrared

	# Integer value proportional to the visible illumination
	vi = tsl.visible
	
	# Sum of the two ...
	total = ir + vi
	
	if total !=0:
		inv_total = 100 / total

		print("Infrared : %.1f %%" %(ir*inv_total))
		print("Visible light : %1.f %%" %(vi*inv_total))

	print("")

	# 5 second delay
	sleep(5)