# Purpose of the script: implementation of a Grove MU-Z16 CO2 sensor
# This script is adapted from the https://forum.pycom.io/topic/4821/solved-uart-and-mh-z16-co2-sensor/13 forum
# Sensor data sheet: https://www.winsen-sensor.com/d/files/MH-Z16.pdf

from pyb import UART # Class to manage the UART
from time import sleep_ms

uart = UART(2) # Instance of the UART number 2

# Initialization at 9600 baud, 8-bit frames, no parity check with a stop bit at 1
uart.init(9600, bits=8, parity=None, stop = 1)

# Do we need to recalibrate the sensor ?
calibrate = False

# Should the sensor be preheated?
preheat = False

# Thermometer offset correction (depends on module/sensor)
TEMP_OFFSET = const(5)

# CO2 sensor offset correction (determined with another calibrated CO2 sensor)
CO2_OFFSET = const(0)

# Sequence of bytes to start a measurement
CMD_MEASURE = b'\xFF\x01\x86\x00\x00\x00\x00\x00\x79'

# Byte sequence to calibrate
CMD_CALIBRATE = b'\xFF\x87\x87\x00\x00\x00\x00\x00\xF2'

# Number of sensor heating iterations
HEATING_ROUNDS = const(300)

if preheat:
	print("Preheating (%d minutes)\n" %(HEATING_ROUNDS // 60))
	p = 0
	for i in range(HEATING_ROUNDS):
		#Launch a measurement
		uart.write(CMD_MEASURE)
		# Delay 1 second
		sleep_ms(1000)
		p += 1
		if p == 60:
			p = 0
			print(str((i+1) // 60) + "minute(s) elapsed".)

# Possible calibration (in a ventilated room or outside)
if calibrate:
	print("Starting the calibration\n")
	uart.write(CMD_CALIBRATE)
	print("End of calibration\n")

# Sensor Precision (ppm)
#ACCURACY = const(200)

# Sensor high detection threshold (ppm)
THRESHOLD = const(2000)

while True:

	#Launch a measurement
	uart.write(CMD_MEASURE)
	sleep_ms(10)
	
	# Wait until you receive the answer (9 characters)
	while uart.any() < 9:
		sleep_ms(1)

	# Reading the answer
	resp = bytearray(uart.read(9))

	# Extract temperature and CO2 concentration from the answer
	co2_ppm = int(resp[2]) * 256 + int(resp[3]) + CO2_OFFSET
	temp_celsius = (int(resp[4]) - 40) + TEMP_OFFSET

	# Displays temperature and CO2 concentration
	print("Temperature : %d °C" % temp_celsius)
	if co2_ppm < THRESHOLD:
		print("CO2 concentration : %d ppm\n" % co2_ppm)
	else:
		print("CO2 concentration > %d ppm\n" % THRESHOLD)

	# Five-second delay
	sleep_ms(5000)

