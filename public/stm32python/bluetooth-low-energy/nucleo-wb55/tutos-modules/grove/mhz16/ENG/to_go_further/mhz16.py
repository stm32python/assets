# Grove CO2 Sensor Module Driver MH-Z16
# This script is adapted from the forum:
# https://forum.pycom.io/topic/4821/solved-uart-and-mh-z16-co2-sensor/13
# Sensor datasheet:
# https://www.winsen-sensor.com/d/files/MH-Z16.pdf

from time import sleep_ms

#Sensor response size, in bytes 
_NB_BYTES = const(9)

# Sequence of bytes to start a measurement
_CMD_MEASURE = b'\xFF\x01\x86\x00\x00\x00\x00\x00\x79'

# Byte sequence to calibrate
_CMD_CALIBRATE =  b'\xFF\x87\x87\x00\x00\x00\x00\x00\xF2'

class MHZ16:

	def __init__(self, uart, temp_offset = 0, co2_offset = 0):
		self._uart = uart
		self._temp_offset = temp_offset
		self._co2_offset = co2_offset

	def preheat(self):
		p = 0
		for i in range(300):
			# Start a measurement
			self._uart.write(_CMD_MEASURE)
			# Delay 1 second
			sleep_ms(1000)
			p += 1
			if p == 60:
				p = 0
 
	def calibrate(self):
		self._uart.write(_CMD_CALIBRATE)

	def measure(self):
		
		try:
			# Start a measurement
			self._uart.write(_CMD_MEASURE)
			sleep_ms(10)

			# Wait until you receive the answer (9 characters)
			while self._uart.any() < _NB_BYTES:
				sleep_ms(1)

			# Read the answer
			resp = bytearray(self._uart.read(_NB_BYTES))

			# Extract temperature and CO2 concentration from the answer 
			co2_ppm = (resp[2] * 256 + resp[3]) + self._co2_offset
			temp_celsius = (resp[4] - 40) + self._temp_offset
			return(temp_celsius, co2_ppm)
		except:
			return(-1, -1)