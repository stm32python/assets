# Purpose of the script: implementation of a MU-Z16 CO2 sensor from Grove
# This time we use a class library (mhz16.py).

import mhz16
from pyb import UART# Class for managing the UART
from time import sleep_ms

uart = UART(2) # Instance of the UART number 2

# Initialization of the UART
uart.init(9600, bits=8, parity=None, stop = 1)

# Thermometer offset correction (depends on module/sensor)
TEMP_OFFSET = const(5)

# Sensor Instance
sensor = mhz16.MHZ16(uart, temp_offset = TEMP_OFFSET)

# Do we need to recalibrate the sensor?
calibrate = False

# Should the sensor be preheated?
preheat = False

# Start calibration (in a ventilated room or outdoors)

if preheat:
	print("Preheating (5 minutes)")
	sensor.preheat()

if calibrate:
	print("Calibration\n")
	sensor.calibrate()

# Sensor accuracy (ppm)
#ACCURACY = const(200)

# Sensor high detection threshold (ppm)
THRESHOLD = const(2000)

while True:

	# Start a measurement
	(temp_celsius, co2_ppm) = sensor.measure()
	
	# If the values returned are positive
	if temp_celsius != -1 and co2_ppm != -1:
	
		# Displays temperature and CO2 concentration
		print("Temperature : %d °C" %temp_celsius)

		if co2_ppm < THRESHOLD:
			print(" CO2 Concentration : %d ppm\n" % co2_ppm)
		else:
			print(" CO2 Concentration > %d ppm\n" % THRESHOLD)

	else:
		print("Measurement problem")

	# Five-second delay
	sleep_ms(5000)
