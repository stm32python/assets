# Purpose of the script: Implement a PIR motion sensor with a non-blocking delay.
# The Grove PIR Motion sensor module we selected returns a non-zero signal for one second after it has
# detects a motion. If other motions occur during this second, it will not distinguish them from the one that first
# activated it first.
# The method "pyb.wfi()" is used to put the microcontroller in power saving mode between two interrupts.
# This function must be used in scripts that also use interrupts because they are necessary to
# wake up the microcontroller from its sleep on command.

from machine import Pin
import time # Library to manage timeouts

# Interrupt service routine
# This function does only one thing: it sets the global variable "motion" to "True".

def handle_interrupt(pin):
	global motion
	motion = 1 # We signal the motion

led = Pin('D8', Pin.OUT) # Pin of the LED indicating the alarm

pir = Pin('D4', Pin.IN) # PIR sensor pin

# The interrupt is "attached" to the pin of the PIR sensor.
# This means that the interrupt manager contained in the STM32WB55 will "monitor"
# the voltage on pin D4. If this voltage increases and goes from 0 to 3.3V (IRQ_RISING)
# then the interrupt manager will force the STM32WB55 to execute the "handle_interrupt" function.

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

motion = 0 # Global variable that will be modified by the interrupt service routine
start = 0 # Global variable to measure the elapsed time

while True:
	
	# The time in milliseconds since the start of MicroPython is measured
	now = time.ticks_ms()

	if led.value() == 0 and motion == 1 : # If the LED is off and the interruption has taken place...
		print('Motion detected!')
		# The time in milliseconds since the start of MicroPython is stored
		start = time.ticks_ms()
		led.value(1) # Turn on the LED
	
	# If the LED is on and 1 second has passed since the last motion detection...
	elif led.value() == 1 and time.ticks_diff(now, start) > 1000:
		print('Waiting...')
		led.value(0) # Turn off the LED
		motion = 0 # Reset the interrupt occurrence indicator
		
	# Put the microcontroller in sleep mode waiting for the next interrupt
	pyb.wfi()
