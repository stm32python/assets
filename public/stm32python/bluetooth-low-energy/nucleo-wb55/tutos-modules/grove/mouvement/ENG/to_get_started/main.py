# Purpose of the script: Implement a PIR motion sensor
# The Grove PIR Motion sensor module we selected returns a non-zero signal for one second after it has
# detects a motion. If other movements occur during this second, it will not distinguish them from the one that
# activated it first.

from machine import Pin
from time import sleep

# Global variable that will be modified by the interrupt service routine
motion = False

# Interrupt service routine
# This function does only one thing: it sets the global variable "motion" to True.
@micropython.viper
def handle_interrupt(pin):
	global motion
	motion = True


led = Pin('D8', Pin.OUT) # LED pin

pir = Pin('D4', Pin.IN) # PIR sensor pin

# The interrupt is "attached" to the PIR sensor pin.
# This means that the interrupt handler contained in the STM32WB55 will "monitor
# the voltage on pin D4. If this voltage increases and goes from 0 to 3.3V (IRQ_RISING)
# then the interrupt manager will force the STM32WB55 to execute the "handle_interrupt" function.

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
	if motion: # if motion = True then it means that the interrupt has occurred.
		print('Motion detected!')
		led.value(1) # Turn on the LED
		sleep(1) # Delay for 1 second
		led.value(0) # Turns off the LED
		motion = False
		print('Waiting...')
	# Places the microcontroller in sleep mode waiting for the next interrupt
	pyb.wfi()

