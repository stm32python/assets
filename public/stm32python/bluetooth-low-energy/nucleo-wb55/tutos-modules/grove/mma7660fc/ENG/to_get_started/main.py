# Purpose of the script: MMA7660FC 3-axis accelerometer implementation (+/- 1.5g)
# Datasheet : https://www.nxp.com/docs/en/data-sheet/MMA7660FC.pdf
# This example is adapted from :
# https://github.com/ControlEverythingCommunity/MMA7660FC/blob/master/Python/MMA7660FC.py

from machine import I2C # To control the I2C bus
from time import sleep_ms # To manage the timers

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the present devices
print("I2C Adresses I2C used : " + str(i2c.scan()))

# Sensor address on the I2C bus: 0x4C (76)
MMA7660FC_ADR = const(76)

# Address of the mode selection register: 0x07 (7)
MMA7660FC_SMR = const(7)

# Standby mode : 0x00 (0)
MMA7660FC_STANDBY = b'\x00'

# "Active" mode: 0x01 (1)
MMA7660FC_ACTIVE = b'\x01'

# Address of the register used to set the sampling frequency: 0x08(8)
MMA7660FC_SRR = const(8)

# Address of the output register exposing the 3 bytes containing the accelerations : 0x00(0)
MMA7660FC_ODR = const(0)

# The sampling rate will be two measurements per second: 0x06(6)
# This value can be changed for more frequent measurements, up to 120 per second
# (see https://www.nxp.com/docs/en/data-sheet/MMA7660FC.pdf)


MMA7660FC_SRATE1 = b'\x07'
MMA7660FC_SRATE2 = b'\x06'
MMA7660FC_SRATE4 = b'\x05'
MMA7660FC_SRATE8 = b'\x04'
MMA7660FC_SRATE16 = b'\x03'
MMA7660FC_SRATE32 = b'\x02'
MMA7660FC_SRATE64 = b'\x01'
MMA7660FC_SRATE120 = b'\x00'

# Switches to "standby" mode
# - writes to the memory of the I2C device located at address MMA7660FC_ADR
# - writes from address MMA7660FC_SMR
# - writes the bytes contained in MMA7660FC_STANDBY, which must be placed in an array
i2c.writeto_mem(MMA7660FC_ADR, MMA7660FC_SMR, MMA7660FC_STANDBY)

# Program the sampling frequency (measurements per second)
i2c.writeto_mem(MMA7660FC_ADR, MMA7660FC_SRR, MMA7660FC_SRATE16)

# Switches to "active" mode
i2c.writeto_mem(MMA7660FC_ADR, MMA7660FC_SMR, MMA7660FC_ACTIVE)

# Pause for 500 milliseconds to ensure that writing is complete
sleep_ms(500)

# Conversion factor between the values read in the registers and the physical acceleration
# expressed in g.
RAW_TO_G = 0.047

while True: # Loop without exit clause

	# Reading the acceleration vector: 3 bytes from the address of the output register
	# MMA7660FC_ODR
	data = i2c.readfrom_mem(MMA7660FC_ADR, MMA7660FC_ODR, 3)

	sleep_ms(500)

	# The acceleration values are coded on the first 6 bits (from right to left) of
	# each byte.
	# We must therefore apply a binary mask to the bytes read, with the logical operation "&" in order to:
	# to put at zero the two leftmost bits.
	# The mask which is appropriate is 00111111 (in binary) = 0x3F (in hexadecimal).

	xAccl = data[0] & 0x3F

	# We recenter the unsigned result coded on 6 bits of the interval [0, 64] in the interval
	# [-32, 31] in order to restore the sign of the acceleration along each axis (two's complement).
	
	if xAccl > 31 :
		xAccl -= 64

	yAccl = data[1] & 0x3F
	if yAccl > 31 :
		yAccl -= 64

	zAccl = data[2] & 0x3F
	if zAccl > 31 :
		zAccl -= 64

	# Accelerations are displayed in g, using the RAW_TO_G conversion factor
	# Data are displayed as decimal numbers with 1 digit precision (%.1f)

	print("Acceleration axe X : %.1f g" %(xAccl * RAW_TO_G))
	print("Acceleration axe Y : %.1f g" %(yAccl * RAW_TO_G))
	print("Acceleration axe Z : %.1f g" %(zAccl * RAW_TO_G))

	# Quarter-second delay
	sleep_ms(250)
