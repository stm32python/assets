# Purpose of the script: MMA7660FC 3-axis accelerometer implementation (+/- 1.5g)
# Datasheet : https://www.nxp.com/docs/en/data-sheet/MMA7660FC.pdf

from machine import I2C
import mma7660 # To manage the accelerometer
import pyb # To manage the inputs and outputs (LEDs)
from time import sleep_ms # For time delays

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for 1s to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the present devices
print("I2C Adresses used : " + str(i2c.scan()))

# Accelerometer Instantiation
accelerometer = mma7660.MMA7660(i2c)

# "start()" method to start the accelerometer
accelerometer.start()

#  LED Instantiation
led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

THRESHOLD = 0.75 # Acceleration threshold to turn LEDs on or off

last_face = -1
last_portrait_landscape = -1

while True:

	#"get()" method to get the accelerometer measurements
	ax, ay, az = accelerometer.get()
	
	# If the absolute value of the acceleration on the X axis is greater than THRESHOLD 'mg' then
	if abs(ax) > THRESHOLD :
		led_green.on()
	else:
		led_green.off()
		
	# If the absolute value of the acceleration on the Y axis is greater than THRESHOLD 'mg' then
	if abs(ay) > THRESHOLD :
		led_blue.on()
	else:
		led_blue.off()

	# If the absolute value of the acceleration on the Z axis is greater than THRESHOLD 'mg' then
	if abs(az) > THRESHOLD :
		led_red.on()
	else:
		led_red.off()

	# Report taps
	if accelerometer.tap():
		print("Tap !")

	# Report the shakes
	if accelerometer.shake():
		print("Shake !")

	# Reports the orientation (coin flip) of the module. For this function to respond consistently, you must position the Grove
	# function to be consistent, you must position the Grove module so that
	# Z axis is close to vertical.

	if abs(az) > 0.7: # If the module is held almost horizontally
		face = accelerometer.facing()
		if face != last_face:
			last_face = face
			if face == 0:
				print("Plan du module (côté connecteur Grove) orienté vers le haut")
			elif face == 1 :
				print("Plan du module (côté connecteur Grove) orienté vers le bas")
	else:
		last_face = -1

	# Test the portrait - landscape orientation. For this function to be consistent, you must position the Grove module so that
	# Z axis points toward you and is close to horizontal.
	
	if abs(az) < 0.3: # If the module is held almost vertically
		portrait_landscape = accelerometer.portrait_landscape()
		if portrait_landscape != last_portrait_landscape:
			last_portrait_landscape = portrait_landscape
			if portrait_landscape == 6:
				print("Portrait - paysage : axe Y vers la droite, axe X vers le bas")
			elif portrait_landscape == 5:
				print("Portrait - paysage : axe Y vers la gauche, axe X vers le haut")
			elif portrait_landscape == 2:
				print("Portrait - paysage : axe Y vers le bas, axe X vers la gauche")
			elif portrait_landscape == 1:
				print("Portrait - paysage : axe Y vers le haut, axe X vers la droite")
	else:
		last_portrait_landscape = -1

	sleep_ms(250) # Quarter-second delay
