# Test of the TAP function
# Creation of a pedometer based on an example provided by Frédéric Boulanger
# CentraleSupélec - Computer Science Department


from machine import I2C # To control the I2C bus
from time import sleep_ms # To manage the timers

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
MMA_I2C = I2C(1) 

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the present devices
print("Adresses I2C utilisées : " + str(MMA_I2C.scan()))

MMA_address =  const(0x4C) 
 
# Address of the special condition detection register ( Tilt register) 
_TILT_REG = const(3)

# Address of the accelerometer interrupt setup register
_INTSU_REG = const(6)

# Address of the mode selection register
_SM_REG = const(7)

# Address of the register used to set the sampling rates
_SR_REG = const(8)

# Address of the taps parameters detection register
_PDET_REG = const(9)

# Address of the taps detection filter register
_PD_REG = const(10)
 
# Sets the accelerometer to on mode
def MMA_on():
	MMA_I2C.writeto_mem(MMA_address, _SM_REG, b'\x01')
 
# Sets the accelerometer to standby mode
def MMA_off():
	MMA_I2C.writeto_mem(MMA_address, _SM_REG, b'\x00')
 
# Mini pedometer

# Configuration 
MMA_off() # Disabling the accelerometer

MMA_I2C.writeto_mem(MMA_address, _INTSU_REG, b'\xFF') # enable all interrupts for taps
MMA_I2C.writeto_mem(MMA_address, _SR_REG, b'\x00') # configure sampling rates for taps detection

# Detection of taps on the 3 axes (bits [7-5] at 0)
# Anti-bounce : four oscillations are filtered : i.e. 0x04 (possible value from 1 to 31)
MMA_I2C.writeto_mem(MMA_address, _PDET_REG, b'\x04') 

MMA_I2C.writeto_mem(MMA_address, _PD_REG, b'\x03') # taps detection delay, aggregates 3 adjacent taps

MMA_on() # Activating the accelerometer

tap_count = 0 # taps counter

while True:
	
	# reading the TILT register
	tilt = MMA_I2C.readfrom_mem(MMA_address, _TILT_REG, 1)
	val = tilt[0]

	if val & (1<<5):
		print("TAP")
		tap_count += 1
		print("Nb de taps : %d" %tap_count)


