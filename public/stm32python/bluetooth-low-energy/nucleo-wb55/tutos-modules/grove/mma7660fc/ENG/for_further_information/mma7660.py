# Class that implements the MMA7660FC 3-axis accelerometer driver (+/- 1.5g)
# TILT register functions added from the explanations :
# - From the book "MicroPython and Pyboard - Python on microcontroller : 
# from getting started to advanced use".
# Author: Dominique Meurisse, ISBN-10: 2409022901, ISBN-13: 978-2409022906
# - Examples provided by Frédéric Boulanger, CentraleSupélec - Computer Science Department
# https://github.com/Frederic-soft/pyboard/blob/master/MMA7660/MMA7660.py

_DEVICE_ADDRESS = const(0x4C) # I2C bus address of the accelerometer 

# Address of the output register exposing the 3 bytes containing the accelerations
# Acceleration according to x : address = 0
# Acceleration according to y : address = 1
# Acceleration according to z : address = 2
_OUTPUT_REG = const(0)

# Address of the special condition detection register
_TILT_REG = const(3)

# Address of the accelerometer interrupt setup register
_INTSU_REG = const(6)

# Settings for the INTSU register
# Enable all interrupts
_INT_SET = b'\xFF'

# Address of the register used to set the sampling frequency
_SR_REG = const(8)

# Parameters for the SR register
# Measurement / sampling frequency (in nb per second)
#_SRATE1 = b'\x07'
#_SRATE2 = b'\x06'
#_SRATE4 = b'\x05'
#_SRATE8 = b'\x04'
#_SRATE16 = b'\x03'
#_SRATE32 = b'\x02'
#_SRATE64 = b'\x01'
_SRATE120 = b'\x00'

# Address of the mode selection register
_SM_REG = const(7)

# Settings for the SM register
_STANDBY = b'\x00' # "standby" Mode
_ACTIVE = b'\x01' # "actif" Mode

# Address of the taps parameters detection register
_PDET_REG = const(9)

# Parameters of the PDET register
# Detection of taps on the 3 axes (bits [7-5] at 0)
# Anti-bounce: 20 oscillations are filtered: 0x14 (bits [4-0]) (possible value from 1 to 31)
_TAPS_BEBOUNCE = b'\x14'

# Address of the taps detection filter register
_PD_REG = const(10)

# Parameter for the PD register
# Delay of detection of taps, aggregates up to 31 consecutive taps
_TAPS_FUSE = b'\x1F'

# Conversion factor between the values read in the registers and the physical acceleration
# expressed in g.
_RAW_TO_G = 0.047

class MMA7660():

	def __init__(self, i2c, addr = _DEVICE_ADDRESS, srate = _SRATE120):
		self.i2c = i2c
		self.i2c.scan()
		self.address = addr 
		# Byte buffer array to retrieve values from the output register of the  the accelerometer
		self.databuf = bytearray(3)
		# Table containing the dimensioned values of the accelerations in the 3 axes
		self.data = [0,0,0]
		# The accelerometer is placed in "standby" mode
		self.stop()

		# Interrupts are enabled
		self.i2c.writeto_mem(self.address, _INTSU_REG, _INT_SET)
		# We set the frequency of measurements
		self.i2c.writeto_mem(self.address, _SR_REG, srate)
		# We set the parameters of the "taps" detection
		self.i2c.writeto_mem(self.address, _PDET_REG, _TAPS_BEBOUNCE)
		self.i2c.writeto_mem(self.address, _PD_REG, _TAPS_FUSE)

	# Start method to start the accelerometer
	def start(self):
		self.i2c.writeto_mem(self.address, _SM_REG, _ACTIVE)

	# method to stop the accelerometer
	def stop(self):
		self.i2c.writeto_mem(self.address, _SM_REG, _STANDBY)

	# Method for changing the measurement frequency
	def setSamplingRate(self, rate):
		# Switches to "standby" mode
		self.stop()
		# Program the sampling frequency (measurements per second)
		self.i2c.writeto_mem(self.address, _SR_REG, rate)
		# Switches to "active" mode
		self.start()

	# Method for obtaining acceleration measurements
	def get(self):
		self.databuf = self.i2c.readfrom_mem(self.address, _OUTPUT_REG, 3) # Lecture des données

		# Acceleration along the x axis
		ax = self.databuf[0] & 0x3F # Two's complement
		if ax > 31:
			ax = ax - 64
		self.data[0] = ax * _RAW_TO_G # Conversion to g

		# Acceleration along the Y axis
		ay = self.databuf[1] & 0x3F
		if ay > 31:
			ay = ay - 64
		self.data[1] = ay * _RAW_TO_G

		# Acceleration along the Z axis
		az = self.databuf[2] & 0x3F
		if az > 31:
			az = az - 64
		self.data[2] = az * _RAW_TO_G

		return tuple(self.data)

	# Method to delay until the TILT register is updated
	def _read_tilt_reg(self):
		# Read the register (one byte)
		reg_content = self.i2c.readfrom_mem(self.address, _TILT_REG, 1)

		# If the register was not being updated (bit number 6 equal to "1")
		if not (reg_content[0] & (1<<6)):
			return reg_content[0]
		else:
			return 0

	# Method to determine if the accelerometer is shaken
	# Returns :
	# 1 if the accelerometer is shaken 
	# 0 otherwise
	def shake(self):
		val = self._read_tilt_reg() & (1<<7)
		if val:
			return 1
		else:
			return 0

	# Method to determine if the accelerometer is tapped
	# Returns :
	# 1 if the accelerometer is tapped 
	# 0 otherwise
	def tap(self):
		val = self._read_tilt_reg() & (1<<5)
		if val:
			return 1
		else:
			return 0

	# Method to determine if the accelerometer is on the front or the back: Returns :
	# 0 if the accelerometer is facing the "FACE" side
	# 1 if the accelerometer is turned to the "BATTERY" side
	# -1 if undetermined state
	def facing(self):
		val = self._read_tilt_reg() & 0b11 
		if val == 1:
			return 0
		elif val == 2:
			return 1
		else:
			return -1 

	# Method to detemmin portrait/landscape mode
	# Returns :
	# 1 if landscape mode, to the left
	# 2 if landscape mode, to the right
	# 5 if inverted vertical position
	# 6 if normal vertical position
	def portrait_landscape(self):
		return ( self._read_tilt_reg() & 0b11100 ) >> 2
