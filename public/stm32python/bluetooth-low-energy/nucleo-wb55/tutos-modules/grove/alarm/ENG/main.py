# Purpose of the script:
# Design of an alarm system based on a motion sensor.
# A PIR motion sensor is configured as an interrupt on pin D2.
# A buzzer is connected to pin D3 and driven by a PWM.
# Hardware required:
# - A PIR sensor (preferably operating on 3.3V)
# - A buzzer (preferably 3.3V)
# An interrupt is used to capture the signal from the motion detector.

from time import sleep_ms
from pyb import Pin, Timer

# LED configuration
led_blue = pyb.LED(1)
led_red = pyb.LED(3)

# Buzzer configuration
frequency = 440
buz = Pin('D3')
# D3 generates a PWM with TIM1, CH3
timer1 = Timer(1, freq=frequency)
channel3 = timer1.channel(3, Timer.PWM, pin=buz)

# PIR sensor configuration
PIR_Pin = Pin('D2', Pin.IN)
motion = False

# PIR sensor interruption management function
def handle_interrupt(pin):
	global motion
	motion = True
	global interrupt_pin
	interrupt_pin = PIR_Pin

# Activation of the PIR sensor interruption
PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=handle_interrupt)

# Main
while True:

	if motion:
		print('Movement detected!')
		led_red.on()
		led_blue.off()
		# Cyclic ratio set at 5% (the buzzer is powered 5% of the time of a period)
		channel3.pulse_width_percent(5)
		sleep_ms(1000)
		print('Motion detector activated')
		led_blue.on()
		led_red.off()
		# Cyclic ratio set to 0% (the buzzer is no longer powered)
		channel3.pulse_width_percent(0)
		motion = False
	
	# Places the microcontroller in sleep mode until the next interrupt
	pyb.wfi()
