# Objet du script :
# Conception d'un système d'alarme basé sur un détecteur de mouvements.
# Un capteur de mouvement PIR est configuré en interruption sur la broche D2.
# Un buzzer est connecté à la broche D3 et piloté par une PWM.
# Matériel requis :
#   - Un capteur PIR (de préférence fonctionnant en 3.3V)
#   - Un buzzer (de préférence fonctionnant en 3.3V)
# On utilise une interruption pour capturer le signal du détecteur de mouvements.

from time import sleep_ms
from pyb import Pin, Timer

# Configuration des LED
led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)

# Configuration du buzzer
frequency = 440
buz = Pin('D3') 
# D3 génère une PWM avec TIM1, CH3
timer1 = Timer(1, freq=frequency)
channel3 = timer1.channel(3, Timer.PWM, pin=buz)

# Configuration du capteur PIR
PIR_Pin = Pin('D2', Pin.IN)
motion = False

# Fonction de gestion de l'interruption du capteur PIR
def handle_interrupt(pin):
	global motion
	motion = True
	global interrupt_pin
	interrupt_pin = PIR_Pin

# Activation de l'interruption du capteur PIR
PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=handle_interrupt)

# Boucle principale
while True:

	if motion:
		print('Mouvement détecté !')
		led_rouge.on()
		led_bleu.off()
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		channel3.pulse_width_percent(5)
		sleep_ms(1000)
		print('Détecteur de mouvement activé')
		led_bleu.on()
		led_rouge.off()
		# Rapport cyclique paramétré à 0% (le buzzer n'est plus alimenté)
		channel3.pulse_width_percent(0)
		motion = False
	
	# Place le microcontrôleur en sommeil en attendant la prochaine interruption
	pyb.wfi() 
