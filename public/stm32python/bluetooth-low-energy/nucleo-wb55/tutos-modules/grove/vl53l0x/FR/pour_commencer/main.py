# Source : https://github.com/uceeatz/VL53L0X
# Objets du Script : 
# - Mise en oeuvre d'une PWM pour réaliser un variateur de lumière.
# - Contrôle de l'intensité lumineuse au moyen d'un capteur de distance VL53L0x.
# L'intensité de la LED varie en fonction de la distance mesurée par le capteur
# (plus la distance est courte, plus la lumière est intense).
# Matériel (en plus de la carte NUCLEO-WB55) :
# - un Grove Base Shield
# - une LED connectée sur D6 du Grove Base Shield.
# - un capteur Time of Flight Grove connecté sur une prise I2C du Grove Base Shield.

from pyb import Pin, Timer # Pilotes des entrées-sorties et des timers
from time import sleep_ms # Pour temporiser

# initialisation de la PWM 
p = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i = 0

# Déclage du capteur (en mm, différent pour chaque module)
SENSOR_OFFSET = const(30)

from machine import I2C
import VL53L0X # Bibliothèque pour le VL53L0X

#Initialisation du bus I2C numéro 1 du STM32WB55 
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

tof = VL53L0X.VL53L0X(i2c) # Instance du capteur

tof.start() # démarrage du capteur

while True:

	try:
		distance = max(tof.read() - SENSOR_OFFSET, 0) # mesure de distance
		print("Distance : %1d mm" %distance)
		# Pause de 5 millisecondes
		sleep_ms(5)
	except:
		print("Erreur de mesure")
		break
		
	# Convertit la distance en un nombre entre 0 et 100
	rapport_cyclique = 100 - min( distance // 10 , 100)
	
	# Applique ce rapport cyclique à la PWM de la LED 
	ch.pulse_width_percent(rapport_cyclique)
	
	# Pause de 5 millisecondes
	sleep_ms(5)

tof.stop() # arrêt du capteur