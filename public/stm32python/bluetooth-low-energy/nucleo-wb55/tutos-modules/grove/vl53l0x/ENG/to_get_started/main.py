# Source: https://github.com/uceeatz/VL53L0X
# Script Objects:
# - Implementation of a PWM to realize a light dimmer.
# - Control of the light intensity with a distance sensor VL53L0x.
# The intensity of the LED varies according to the distance measured by the sensor
# (the shorter the distance, the more intense the light).
# Hardware (in addition to the NUCLEO-WB55 board):
# - a Grove Base Shield
# - a LED connected to D6 of the Grove Base Shield.
# - a Grove Time of Flight sensor connected to an I2C socket on the Grove Base Shield.

from pyb import Pin, Timer # I/O and timer drivers
from time import sleep_ms # To time out

# initialization of the PWM
p = Pin('D6')
ti = 1 # Timer
ch = 1 # Channel
tim = Timer(ti, freq=1000) # PWM frequency set to 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i = 0

# Sensor offset (in mm, different for each module)
SENSOR_OFFSET = const(30)

from machine import I2C
import VL53L0X # Library for the VL53L0X

#Initialization of the I2C bus number 1 of the STM32WB55
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

tof = VL53L0X.VL53L0X(i2c) # Instance of the sensor

tof.start() # start of the sensor

while True:

	try:
		distance = max(tof.read() - SENSOR_OFFSET, 0) # distance measurement
		print("Distance: %1d mm" %distance)
		# Pause for 5 milliseconds
		sleep_ms(5)
	except:
		print("Measurement error")
		break
		
	# Convert the distance into a number between 0 and 100
	duty cycle = 100 - min( distance // 10 , 100)
	
	# Apply this duty cycle to the PWM of the LED
	ch.pulse_width_percent(duty cycle)
	
	# Pause for 5 milliseconds
	sleep_ms(5)

tof.stop() # stop the sensor
