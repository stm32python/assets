# Reading and digitizing the signal of a Grove luminosity sensor (LS06-S phototransistor)
# Caution: the sensor must be supplied with 5V to give a response between 0 and 4095.

from pyb import ADC, Pin # Analog to digital converter and GPIO
from time import sleep # For time delays

# Instantiation and start of the analog-to-digital converter
adc = ADC(Pin('A1'))

while True:
	# Digitizes the value read, produces a time-varying result in the range [0; 4095]
	Measure = adc.read()
	print("Brightness %d (arbitrary units)" %Measurement)
	sleep(1) # One second delay