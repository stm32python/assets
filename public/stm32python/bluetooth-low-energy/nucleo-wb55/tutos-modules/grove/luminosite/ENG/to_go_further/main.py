# Purpose of the script:
# Designing a night light ...
# - Reads the ambient light intensity with a Grove light sensor (LS06-S phototransistor)
# - Turns on an LED (Grove module) with an intensity inversely proportional to the light
# ambient light.

from time import sleep_ms
from pyb import ADC, Timer, Pin

# Photoresistor on A1 (analog)
adc = ADC(Pin('A1'))

#LED on D3 (PWM output)
led = Pin('D3')

# Reference voltage / range of the ADC : +3.3V
varef = 3.3

# Resolution of the ADC 12 bits = 2^12 = 4096 (min = 0, max = 4095)
RESOLUTION = const(4096)

# Quantum of the ADC
quantum = varef / (RESOLUTION - 1)

#Timer configuration : timer 1 channel 3
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=led)

while True:

	#Recovering the value of the photoresistor
	value = adc.read()
	
	#Changes the brightness of the LED connected to D3
	
	ch3.pulse_width_percent(1 - value * quantum)
	
	# Delay of one tenth of a second
	sleep_ms(100)
