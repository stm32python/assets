# Objet du script :
# Conception d'une veilleuse d'obscurité ... 
# - Lit l'intensité lumineuse ambiante avec un capteur Grove de luminosité (LS06-S phototransistor)
# - Allume une LED (module Grove) avec une intensité inversement proportionnelle à la lumière
#   ambiante.

from time import sleep_ms
from pyb import ADC, Timer, Pin

# Photorésistance sur A1 (analogique)
adc = ADC(Pin('A1'))

#LED sur D3 (sortie PWM)
led = Pin('D3')

# Tension de référence / étendue de mesure de l'ADC : +3.3V
varef = 3.3

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = varef / (RESOLUTION - 1)

#Configuration du timer : timer 1 channel 3
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=led)

while True:

	#Récupération de la valeur de la photorésistance
	valeur = adc.read()
	
	#Change l'intensité de luminosité de la LED connectée sur D3
	
	ch3.pulse_width_percent(1 - valeur * quantum)
	
	# Temporisation d'un dixième de seconde
	sleep_ms(100)