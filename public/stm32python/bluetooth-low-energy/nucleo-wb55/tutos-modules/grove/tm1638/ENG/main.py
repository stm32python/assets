# Purpose of the script: Implementation of an 8x7 segment display driven by a TM1638 controller

# Importing libraries
import tm1638 # Display drivers
from machine import Pin # NUCLEO-WB55 I/O drivers

# Display declaration
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# LED management
tm.leds(0b01010101) # lights one out of 2 LEDs
tm.leds(0b00000001) # turns on LED 1 and turns off the others
tm.led(2, 0) # turns off led 3
tm.led(0,1) #turns on led 0

# We get the information on the buttons
boutons = tm.keys()

# Segments
tm.show('  *--*  ')
tm.show('a.b.c.d.e.f.g.h.')

tm.number(-1234567)
tm.number(1234)

tm.scroll('Hello World')
tm.scroll('Hello World', 100)

# Change the brightness of the leds and segments
tm.brightness(0)

# Turns off LEDs and segments
tm.clear()

