# Purpose of the script:
# Read messages from the Nintendo SNES controller with Micropython

from pyb import Pin # Pin management
from time import sleep_ms # To delay

# Constants to associate buttons with values
BTN_A = 256
BTN_B = 1
BTN_X = 512
BTN_Y = 2
BTN_SELECT = 4
BTN_START = 8
BTN_UP = 16
BTN_DOWN = 32
BTN_LEFT = 64
BTN_RIGHT = 128
BTN_L = 1024
BTN_R = 2048

#Port input/output
PIN_LATCH = Pin('D2', Pin.OUT)
PIN_CLOCK = Pin('D3', Pin.OUT)
PIN_DATA = Pin('D4', Pin.IN)

print("SNES controller in MicroPython:")

# Data acquisition function
def getSnesButtons():
	value = 0
	PIN_LATCH.high()
	PIN_LATCH.low()
	# Read the value of the 16 data bits
	for i in range(0, 16, 1):
		value |= PIN_DATA.value() << i
		PIN_CLOCK.high()
		PIN_CLOCK.low()
	# Return value
	return ~value

# Data display function
def loop():
	oldBtns = -65536

	# Call the acquisition function
	btns = getSnesButtons()

	# As long as the values are the same we do nothing
	while(oldBtns == btns):
		btns = getSnesButtons()
	oldBtns = btns

	# Display according to the retrieved data
	if(btns & BTN_A):
		print("A ", end = '')
	else:
		print("- ", end = '')

	if(btns & BTN_B):
		print("B ", end = '')
	else:
		print("- ", end = '')

	if(btns & BTN_X):
		print("X ", end = '')
	else:
		print("- ", end = '')

	if(btns & BTN_Y):
		print("Y ", end = '')
	else:
		print("- ", end = '')

	if(btns & BTN_SELECT):
		print("SELECT ", end = '')
	else:
		print("------ ", end = '')

	if(btns & BTN_START):
		print("START ", end = '')
	else:
		print("----- ", end = '')

	if(btns & BTN_UP):
		print("UP ", end = '')
	else:
		print("-- ", end = '')

	if(btns & BTN_DOWN):
		print("DOWN ", end = '')
	else:
		print("---- ", end = '')

	if(btns & BTN_LEFT):
		print("LEFT ", end = '')
	else:
		print("---- ", end = '')

	if(btns & BTN_RIGHT):
		print("RIGHT ", end = '')
	else:
		print("----- ", end = '')

	if(btns & BTN_L):
		print("L ", end = '')
	else:
		print("- ", end = '')

	if(btns & BTN_R):
		print("R ")
	else:
		print("- ")

	sleep_ms(500) # Half-second delay

# Infinite loop of acquisition and display
while True:
	boucle()
