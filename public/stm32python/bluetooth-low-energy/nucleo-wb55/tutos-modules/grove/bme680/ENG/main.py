# Code Purpose:
# Grove BME680 temperature, humidity, pressure and air quality sensor module implementation.
# Resource adapted from the site: https://RandomNerdTutorials.com/micropython-bme680-esp32-esp8266/

from machine import Pin, I2C # Management of pins and I2C
from time import sleep # Management of timings	
import bme680 # Grove BME680 module drivers

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep(1)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c.scan()) + "\n")

# Instantiation of the sensor
bme = bme680.BME680_I2C(i2c=i2c)

# Counting the number of series of measurements
nbmes = 0

while True:
	
	# Displays the series of measurements
	nbmes = nbmes + 1
	print('Series of measurements number: ', nbmes)

	# Structure to catch possible errors
	try: # Try to do all the following...

		# Rounding and string conversions of the measures
		temp = str(round(bme.temperature, 1)) + ' C'
		hum = str(round(bme.humidity, 1)) + ' %'
		pres = str(round(bme.pressure, 1)) + ' hPa'
		gas = str(round(bme.gas/1000, 1)) + ' KOhms'

		# Display of the measurements on the serial terminal of the USB User
		print('Temperature:', temp)
		print('Relative humidity :', hum)
		print('Pressure :', pres)
		print('VOC sensitive resistance :', gas)
		print('-------------------')

	except OSError as e: # If an error has occurred in the "try" block...
		
		print('Sensor reading error!')

	sleep(30) # Timeout; next measurement in 30 seconds.
