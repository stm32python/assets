# Objet du script :
# Mise en oeuvre d'un afficheur à matrices de LED 8x8 Max7219 sur bus SPI

import max7219 # Pilote de l'afficheur
from machine import Pin, SPI # Pour piloter le bus SPI
from time import sleep # Pour temporiser

# Nome de colonnes de LED sur l'afficheur
NB_COL = const(32)

# Nombre de lignes de LED sur l'afficheur
NB_LIG = const(8)

# Paramètre d'affichage (voir plus bas)
DECAL_DTE = const(0)
DECAL_BAS = const(1)
negatif = False

# La broche "Chip Select" du bus SPI sera "D9" 
CHIP_SELECT = 'D9'

# Initialisation du bus SPI 1
spi = SPI(1)
sleep(1)

# Instanciation du pilote de la matrice de LED
display = max7219.Max7219(NB_COL, NB_LIG, spi, Pin(CHIP_SELECT))

# La luminosité des LED sera 8 (valeur comprise entre 0 et 15)
display.brightness(8)

# Le fond du texte sera constitué de LED éteintes
display.fill(negatif)

# Le texte affiché sera "1234"
# Il sera décalé vers la droite de DECAL_DTE colonnes de LED
# Il sera décalé vers le bas de DECAL_BAS lignes de LED
# Le texte sera constitué de LED allumées
display.text('1234', DECAL_DTE, DECAL_BAS, not negatif)

# Affiche le texte
display.show()
