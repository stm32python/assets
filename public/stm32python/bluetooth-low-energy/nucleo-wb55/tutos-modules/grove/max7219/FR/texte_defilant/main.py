# Objet du script 
# Code avec défilement de texte

import max7219 # Pilotes de l'afficheur
from machine import Pin, SPI # Pilotes des entrées-sorties et du bus SPI
from time import sleep_ms # Pour temporiser

# Nome de colonnes de LED sur l'afficheur
NB_COL = const(32)

# Initialisation du bus SPI1
spi = SPI(1)
sleep_ms(1000)

# Instanciation de l'afficheur
display = max7219.Max7219(32, 8, spi, Pin('D9'))

negatif = False

def display_text_scroll(text, inv):
	# pour 4 blocs de LED

	# Calcul du décalage vers la gauche : len(text) * -8 - 1
	for p in range(NB_COL, len(text) * -8 - 1, -1):
		# Eteint toutes les LED
		display.fill(inv)
		# Affiche text décalé de p colonnes
		display.text(text, p, 0, not inv)
		display.show()
		# Temporisation de 50 millisecondes entre deux décalages
		sleep_ms(50)

# Programme principal
while True:
	display_text_scroll("STMicroelectronics", negatif)
