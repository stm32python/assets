# Object of the script : scrolling text

import max7219 # Display drivers
from machine import Pin, SPI # I/O and SPI bus drivers
from time import sleep_ms # To temporize

# Name of LED columns on the display
NB_COL = const(32)

# Initialization of the SPI1 bus
spi = SPI(1)
sleep_ms(1000)

# Instantiation of the display
display = max7219.Max7219(32, 8, spi, Pin('D9'))

negatif = False

def display_text_scroll(text, inv):
	# for 4 LED blocks

	# Calculation of the left shift : len(text) * -8 - 1
	for p in range(NB_COL, len(text) * -8 - 1, -1):
		# Turns off all LEDs
		display.fill(inv)
		# Display text shifted by p columns
		display.text(text, p, 0, not inv)
		display.show()
		# 50 millisecond delay between two shifts
		sleep_ms(50)

# Main 
while True:
	display_text_scroll("STMicroelectronics", negatif)
