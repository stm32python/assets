# Purpose of the script: Implementation of a Max7219 8x8 LED array display on SPI bus

import max7219 # Display driver
from machine import Pin, SPI #To control the SPI bus
from time import sleep #To temporize

# Name of LED columns on the display
NB_COL = const(32)

# Number of LED lines on the display
NB_LINE = const(8)

# Display settings (see below)
DECAL_DTE = const(0)
DECAL_BAS = const(1)
negatif = False

# The "Chip Select" pin of the SPI bus will be "D9".
CHIP_SELECT = 'D9'

# Initialization of the SPI bus 1
spi = SPI(1)
sleep(1)

# Instantiation of the LED array driver
display = max7219.Max7219(NB_COL, NB_LINE, spi, Pin(CHIP_SELECT))

# LED brightness will be 8 (value between 0 and 15)
display.brightness(8)

# The background of the text will consist of unlit LEDs
display.fill(negatif)

# The text displayed will be "1234"
# It will be shifted to the right of DECAL_DTE LED columns
# It will be shifted down from DECAL_BAS LED lines
# The text will consist of lit LEDs
display.text('1234', DECAL_DTE, DECAL_BAS, not negatif)

# Displays the text
display.show()
