# Purpose of the script: implementation of the GROVE MY9221 LED bar with MicroPython
# This example is a simple copy of the resource found here:
# https://github.com/mcauser/micropython-my9221
# Hardware: a NUCLEO-WB55 board, a Grove Base Shield and a Grove LED Bar V2.0 module.
# The module is plugged into the D4 connector of the Grove Base Shield.

from machine import Pin # To drive the pins
from my9221 import MY9221 #To control the LED bar

# Intanciation of the LED bar
# Be sure to change the pin references if you connect it
# elsewhere on the Grove Base Shield!
ledbar = MY9221(Pin('D4'), Pin('D5'))

# All LEDs are on, with maximum brightness
ledbar.level(10)

# Four LEDs on, half bright
ledbar.level(4, 0x0F)

# Reverse orientation, first LED is green
ledbar.reverse(True)
ledbar.level(1)

# Normal orientation, the first LED is red
ledbar.reverse(False)
ledbar.level(1)

# Turns on only certain LEDs
ledbar.bits(0b1111100000) # binary mask, the last 5 LEDs are ON
ledbar.bits(0b0000011111) # binary mask, the first 5 LEDs are ON
ledbar.bits(1) # Either 0b0000000001 => only the first LED is ON
ledbar.bits(3) # So 0b0000000011 => only the first 2 LEDs are ON
ledbar.bits(7) # So 0b0000000111 => only the first 3 LEDs are ON

# The first and last LEDs are ON, very dimly
ledbar.bits(513, 7) # 513 = 0b1000000001

# Turns on the odd-numbered LEDs, then the even-numbered LEDs
for i in range(50):
	ledbar.bits(0b0101010101)
	ledbar.bits(0b1010101010)
	buf = b'\x00\xff\x00\xff\x00\xff\x00\xff\x00\xff'
	ledbar.bytes(buf)

# Simulates a shading effect
for i in range(50):
	buf = bytearray([0,1,3,7,15,31,63,127,255,255])
	ledbar.reverse(True)
	ledbar.bytes(buf)
	ledbar.reverse(False)
	ledbar.bytes(buf)

# Different luminosities
from time import sleep_ms
buf = [0,0,0,0,0,255,127,63,15,7]
ledbar.bytes(buf)
sleep_ms(1000)

# Cyclic LED strip with variable brightness
buf = [0,1,3,7,15,31,63,127,255,255]
for i in range(0):
    buf.insert(0,buf.pop())
    ledbar.bytes(buf)
    sleep_ms(100)

# Lights the LEDs in a random sequence
import urandom
for i in range(100):
    ledbar.bits(urandom.getrandbits(10))

# Examines all possible lighting combinations
for i in range(1024):
    ledbar.bits(i)

# Display in grayscale, coded on 8 bits (default)
# LuminositY 0x00-0xFF
ledbar._write16(0x00) # command
ledbar._write16(0xFF) # led 1
ledbar._write16(0xFF) # led 2
ledbar._write16(0x00) # led 3
ledbar._write16(0x00) # led 4
ledbar._write16(0x00) # led 5
ledbar._write16(0xFF) # led 6
ledbar._write16(0xFF) # led 7
ledbar._write16(0x00) # led 8
ledbar._write16(0x00) # led 9
ledbar._write16(0x00) # led 10
ledbar._write16(0x00) # unused channel, required
ledbar._write16(0x00) # unused channel, required
ledbar._latch()

# Display in grayscale, coded on 12 bits
# Luminosity 0x000-0xFFF
ledbar._write16(0x0100) # command
ledbar._write16(0x0FFF) # led 1
ledbar._write16(0x0000) # led 2
ledbar._write16(0x00FF) # led 3
ledbar._write16(0x0000) # led 4
ledbar._write16(0x000F) # led 5
ledbar._write16(0x000F) # led 6
ledbar._write16(0x0000) # led 7
ledbar._write16(0x00FF) # led 8
ledbar._write16(0x0000) # led 9
ledbar._write16(0x0FFF) # led 10
ledbar._write16(0x0000) # unused channel, required
ledbar._write16(0x0000) # unused channel, required
ledbar._latch()

# Display in grayscale, coded on 14 bits
# Luminosity 0x000-0x3FFF
ledbar._write16(0x0200) # command
ledbar._write16(0x3FFF) # led 1
ledbar._write16(0x03FF) # led 2
ledbar._write16(0x0000) # led 3
ledbar._write16(0x0000) # led 4
ledbar._write16(0x0000) # led 5
ledbar._write16(0x003F) # led 6
ledbar._write16(0x0003) # led 7
ledbar._write16(0x0000) # led 8
ledbar._write16(0x0000) # led 9
ledbar._write16(0x0000) # led 10
ledbar._write16(0x0000) # unused channel, required
ledbar._write16(0x0000) # unused channel, required
ledbar._latch()

# Display in grayscale, coded on 16 bits
# Luminosity 0x0000-0xFFFF
ledbar._write16(0x0300) # command
ledbar._write16(0xFFFF) # led 1
ledbar._write16(0x0FFF) # led 2
ledbar._write16(0x00FF) # led 3
ledbar._write16(0x000F) # led 4
ledbar._write16(0x0007) # led 5
ledbar._write16(0x0003) # led 6
ledbar._write16(0x0001) # led 7
ledbar._write16(0x0000) # led 8
ledbar._write16(0x0000) # led 9
ledbar._write16(0x0000) # led 10
ledbar._write16(0x0000) # unused channel, required
ledbar._write16(0x0000) # unused channel, required
ledbar._latch()
