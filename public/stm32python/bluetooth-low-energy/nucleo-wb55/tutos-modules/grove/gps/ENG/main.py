# Example of decoding GPS frames using the adafruit_gps lib https://github.com/alexmrqt/micropython-gps

# This code is adapted from the example provided by Adafruit.
# It has been tested and works correctly as is:
# - with the Grove GPS module (SIM28) from Seeed Studio: https://wiki.seeedstudio.com/Grove-GPS/
# - with the Ultimate GPS Breakout module from Adafruit: https://www.adafruit.com/product/746
# In principle, it should work with all GPS UART modules except, perhaps the
# initialization commands that are specific to the Adafruit GPS PMTK314 module:
# https://cdn-shop.adafruit.com/datasheets/PMTK_A11.pdf.

# Caution: since the antennas built into these modules are not active, you will most likely be
# forced to test the modules outside, with an open sky overhead, to
# acquire the satellites.

# See Wikipedia for an explanation of NMEA frames: https://fr.wikipedia.org/wiki/NMEA_0183

from pyb import UART # Class to manage the UART
import time
import adafruit_gps # Class to decode NMEA frames

# Opening the LPUART on pins D0(RX) D1(TX)
# Attention, the Time-Out must be greater than the GPS module polling frequency !
uart = UART(2, 9600, timeout = 5000)

# Create an instance of the GPS module
gps = adafruit_gps.GPS(uart)

# Initialize the PMTK314 module by specifying which frames it returns and at what frequency.

# Returns the GGA and RMC frames:
gps.send_command('PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Returns the minimum info (RMC frames only, position):
#gps.send_command('PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Tells the GPS not to send any more frames (to save energy)
#gps.send_command('PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Asks the GPS to transmit ALL NMEA frames (but the adafruit_gps lib
# lib doesn't know how to decode them all):
#gps.send_command('PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Tells the GPS to send back the frames once a second:
gps.send_command('PMTK220,1000') # 1000 => 1000 ms <=> 1s
# If you increase this delay beyond 1000 ms, make sure the TimeOut of the
# the UART is larger than the new value!
# You can also reduce this delay. For example, for two measurements per second:
# gps.send_command('PMTK220,500')
# But be aware that if the frequency becomes too high, the adafruit_gps lib may not be able to
# may not be able to decode the frames fast enough and you may lose
# information.

last_print = time.ticks_ms() # Initial time measurement

print("Satellite acquisition in progress...")

# Loop without exit clause with a 1000 ms delay.
# Displays the location every second if the "fix" of the satellites is validated.

while True:
	# Make sure to call gps.update() at each iteration and at least twice
	# more often than the frame rate returned by the GPS module.
	# This method returns a bool that takes the value 'True' if it has managed to decode a new frame.
	# We don't check its return and test the 'has_fix' property instead.
	gps.update()
	
	current = time.ticks_ms() # Measure the current time
	
	if current-last_print >= 1000: # If the elapsed time is at least 1 second
	
		last_print = current # Stores the time for the next iteration

		if gps.has_fix: # Acquisition of satellites is done!
		
			# Displays the frame information: location, date, etc.
			print('=' * 40) # prints a separator line
			
			# Decodes and formats the timestamp data (fix time)
			print('Timestamp acquisition: {}/{}/{} {:02}:{:02}:{:02}'.format(
				gps.timestamp_utc[1],
				gps.timestamp_utc[2],
				gps.timestamp_utc[0],
				gps.timestamp_utc[3],
				gps.timestamp_utc[4],
				gps.timestamp_utc[5]))
			print('Latitude: {} degrees'.format(gps.latitude)) # Latitude of the module
			print('Longitude: {} degrees'.format(gps.longitude)) # Longitude of the module
			print('Acquisition quality: {}'.format(gps.fix_quality)) # Quality of the location provided

			# Some information beyond latitude, longitude and timestamp label are optional
			# depending on the configuration you have applied to the module.
			if gps.satellites is not None:
				print('# satellites: {}'.format(gps.satellites)) # Number of satellites acquired by the module
			if gps.altitude_m is not None:
				print('Altitude: {} meters'.format(gps.altitude_m)) # Estimated altitude of the module above a theoretical "mean" ellipsoid that encompasses the Earth
			if gps.track_angle_deg is not None:
				print('Speed: {} knots'.format(gps.speed_knots)) # Estimated velocity of the lander in knots (1 knot = 1.852 km/h)
			if gps.track_angle_deg is not None:
				print('Heading: {} degrees'.format(gps.track_angle_deg)) # Angle between the direction of the geographic north and the direction followed by the module
			if gps.horizontal_dilution is not None:
				print('Horizontal precision dilution: {}'.format(gps.horizontal_dilution)) # Quality of the module location in latitude and longitude
			if gps.height_geoid is not None:
				print('Elevation above geoid: {} meters'.format(gps.height_geoid)) # Deviation between the altitude of the earth's geoid and that of the mean ellipsoid (actual reported altitude) at the module location
