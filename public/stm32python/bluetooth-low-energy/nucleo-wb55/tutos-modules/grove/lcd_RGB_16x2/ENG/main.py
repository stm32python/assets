# Libraries for the LCD RGB copied from this site: https://github.com/Bucknalla/micropython-i2c-lcd
# Purpose of the script: # Measures the temperature (in degrees Celsius) of the ambient air every second
# Displays the temperature on the Grove RGB LCD and on the USB USER terminal
# Adjust the background color of the LCD and turn on the LEDs according to the temperature read
# This example requires an X-NUCLEO IKS01A3 shield, a Grove Base Shield for Arduino and a Grove RGB LCD.
# Attention, the Grove RGB LCD must be powered in 5V, remember to place the switch of the Grove Base Shield
# for Arduino on the right position!
# NB: It is the shield X-NUCLEO IKS01A3 that brings the PULL-UP resistors on the pins SCL and SDA of the I2C, essential for the proper functioning of the LCD RGB Grove.

from machine import I2C # Library to manage the I2C
import stts751 # Library to manage the MEMS temperature sensor
import pyb # Library  to manage the LEDs
from time import sleep_ms # Library to manage timeouts
import i2c_lcd # Library for the Grove RGB LCD display

# using I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to initialize the I2C time
sleep_ms(1000)

sensor = stts751.STTS751(i2c)

#Display class instance
lcd = i2c_lcd.Display(i2c)
lcd.home()

# Initialization of the 3 LEDs
led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	# The measured temperature is rounded to 1 decimal
	temp = round(sensor.temperature(),1)
	
	# Transforms the numerical value of the temperature into its representation in the form of a character string.
	stemp = str(temp)
	
	# Display on the serial port of the USB USER
	print("temperature : " + stemp + "°C (", end='')

	lcd.move(0,0) # column 0, line 0
	lcd.write('Temperature (C)') # The string "Temperature (C)" is written from the cursor position
	lcd.move(0,1) #column 0, line 1
	lcd.write(stemp) # We write the displayable representation of the temperature
	
	if temp > 25 :
		led_red.on()
		print("chaud)")
		lcd.color(255,0,0) # LCD backlight: red
	elif temp > 18 and temp <= 25 :
		led_green.on()
		print("confortable)")
		lcd.color(0,255,0) # LCD backlight: green
	else:
		led_blue.on()
		print("froid)")
		lcd.color(0,0,255) # LCD backlight: blue

	# LEDs are turned off
	led_red.off()
	led_green.off()
	led_blue.off()
	
	# 1 second delay
	sleep_ms(1000)
