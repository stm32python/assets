# Purpose of the script: Implementation of a Grove RGB LED module (WS2813 Mini).
# We randomly vary the color of the LED.

import neopixel # Driver for the Neopixel LED
from random import seed, randint # To generate random integers
from pyb import Pin # To manage the pins
from time import sleep_ms, ticks_ms # To time and measure the elapsed time

# We initialize the Neopixel LED on pin D2
_NB_LED = const(1)
np = neopixel.NeoPixel(Pin('D2'), _NB_LED)

# Initial values of the intensity on the three channels
# (useless in practice, but makes the code more readable).

red = 0
green = 0
blue = 0

# Initializes the random integer generator with a number
# of processor ticks
seed(ticks_ms())

# Maximum intensity of the colors (255 at most)
_INTENSITE_MAX = const(128)

# Loop without exit clause
while True:

	# Randomly determine intensity values on the three channels
	# (integer between 0 and intensite_max)

	red = randint(0, _INTENSITE_MAX)
	green = randint(0, _INTENSITE_MAX)
	blue = randint(0, _INTENSITE_MAX)

	# Intensity values on the three channels for all LEDs
	for i in range(_NB_LED):
		np[i] = (red, green, blue)

	# We display
	np.write()

	# Delay for a quarter of a second
	sleep_ms(250)


