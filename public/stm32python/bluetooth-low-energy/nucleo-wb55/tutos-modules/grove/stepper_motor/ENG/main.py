# Purpose of the script: implementation of a stepper motor control

import pyb
from pyb import Pin, Timer
from time import sleep_ms

#Global variables
motor_status = 0
direction_motor = 0

#BP (input + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#LED (LED of the NUCLEO board)
led_blue = pyb.LED(3)
led_green = pyb.LED(2)
led_red = pyb.LED(1)

#GPIO that controls the relay/transistor
A = pyb.Pin('D0', Pin.OUT_PP)
B = pyb.Pin('D1', Pin.OUT_PP)
C = pyb.Pin('D2', Pin.OUT_PP)
D = pyb.Pin('D3', Pin.OUT_PP)

motor = (A, B, C, D)

#Interrupt SW1
def ITbutton1(line):
	#Global variables
	global motor_status
	#Motor status at 0 or 1
	if(motor_status == 1):
		motor_status = 0
	else:
		motor_status = 1

def ITbutton2(line):
	#global variables
	global direction_motor
	#Motor direction at 0 (clockwise) or 1 (counterclockwise)
	if(direction_motor == 1):
		direction_motor = 0
	else:
		direction_motor = 1
	#Reset the motor
	motor_stop()

#Initialization of interrupt vectors
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton2)

def moteur_stop():
	#LED management
	led_blue.off()
	led_green.off()
	led_red.on()
	#Turns off all outputs
	for i in range(0, 4, 1):
		motor[i].low()

def motor_running(direction, TempoSpeed):
	i = 0

	#Turn clockwise
	if(direction == 0):
		#LED management
		led_blue.off()
		led_green.on()
		led_red.off()
		#In the clockwise direction
		for i in range(0, 4, 1):
			#Lights segment 1
			motor[i].on()
			sleep_ms(TempoSpeed)
			#Turns on segment 2
			if(i == 3):
				motor[0].on()
			else:
				motor[i+1].on()
			sleep_ms(TempoSpeed)
			#Turns off segment 1
			motor[i].off()
			sleep_ms(TempoSpeed)
	
	#Turn counterclockwise
	if(direction == 1):
		#Management of the LEDs
		led_blue.on()
		led_green.off()
		led_red.off()
		#In counter-clockwise direction
		for i in range(3, -1, -1):
			#Turns on segment 2
			motor[i].on()
			sleep_ms(TempoSpeed)
			#Turns on segment 1
			if(i == 0):
				motor[3].on()
			else:
				motor[i-1].on()
			sleep_ms(TempoSpeed)
			#Turns off segment 2
			motor[i].off()
			sleep_ms(TempoSpeed)

#Temporization of the motor rotation speed. /!\ cannot be less than 3ms /!\
TempoSpeed = 3

#Infinite loop
while True:
	if(motor_status == 0):
		motor_stop()
		
	if(motor_status == 1):
		motor_rotate(direction_motor, TempoSpeed)
		
