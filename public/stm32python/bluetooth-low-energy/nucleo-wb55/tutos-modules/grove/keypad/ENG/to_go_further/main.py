# Purpose of the script: implementation of a digicode with the keypad.

from pyb import Pin, ExtInt
import time
print("start")

#*** Definition of the input, output and interrupt vectors
IN = [0]*4
OUT = [0]*4
IRQ = [0]*4

#*** Definition of physical connections ***
ROW = ['A15', 'C10', 'A10', 'C6']
COLUMN = ['A9', 'C12', 'C13', 'A8']

#*** Definition of the representative matrix of the keyboard ***
MATRIX = [[1,2,3,'A'],
     [4,5,6,'B'],
     [7,8,9,'C'],
     ['*',0,'#','D']]

#*** Output allocation ***
for a in range(4):
	OUT[a] = Pin(ROW[a], Pin.OUT)
	OUT[a].value(1)

led_blue = pyb.LED(3)
led_green = pyb.LED(2)
led_red = pyb.LED(1)

#*** Flags and interrupt functions ***
flag = 0
flag_BP = 0

def inter(line):	#Press keypad
	global flag
	time.sleep(0.2)
	flag = 1

def BP(line): #Press SW1
	global flag_BP
	global nb_seizure
	print("Enter new code: ")
	led_blue.on()
	flag_BP = 1
	nb_seizure = 0

#*** Allocation of inputs in interrupt ***
for a in range(4):
	IN[a] = pyb.Pin( COLUMN[a] , pyb.Pin.IN)
	IN[a].init(pyb.Pin.IN, pyb.Pin.PULL_DOWN, af=-1)
	IRQ[a] = ExtInt(IN[a], ExtInt.IRQ_RISING, Pin.PULL_DOWN, inter)

sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
irq_BP = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, BP)

#*** Definition of the variables allowing the management of the digicode ***
old_carac = -1
nb_seizure = 0
code = [1,2,3,'A']
code_seized = [0]*4

#*** Function that manages the pressing of a keypad button ***
def appuie():
	global flag
	global old_carac
	global nb_saisie
	global code_saisi
	global code

	flag = 0

	for i in range(4):
		if(IN[i].value()==1): #Tests which column is concerned by the press
			for j in range(4): #Turns off the different rows
				OUT[j].value(0);
				if(IN[i].value()==0):
					if (MATRIX[i][j]!=old_carac): #Find the pressed button and display it by managing bounces and removing long presses
						print(MATRIX[i][j])
						old_carac = MATRIX[i][j]
						code_seized[nb_seized] = MATRIX[i][j]

						if(flag_BP == 1):
							code[nb_seizure] = MATRIX[i][j] #Change the code

						nb_seizure = nb_seizure + 1

					for k in range(4):
						OUT[k].value(1) #Recall all rows
					break;

#*** Function that tests if the code entered is the right one ***
def test_code():
	global nb_saisie

	for i in range(4):
		if(code[i] != code_saisi[i]):
			print("Code false")
			led_red.on()
			nb_seizure = 0
			time.sleep(2)
			led_red.off()
			print("Enter the 4 digit code: ")
			return -1
	print("Right code")
	led_green.on()
	nb_seizure = 0
	time.sleep(2)
	led_green.off()
	print("Enter the 4 digit code: ")


#*** Start of main ***
print("Enter the 4-digit code: ")

while (1):
	if((flag == 1) and (nb_saisie < 4)):	#If a key has been pressed
		press()

	if (nb_seizure >= 4): #If the code is entered in full

		if(flag_BP==1): #Management of the new code
			flag_BP=0
			print("The new code is: ", code[0], code[1], code[2], code[3])
			nb_seizure = 0
			time.sleep(2)
			led_blue.off()
			print("Enter the 4-digit code: ")

		else:
			test_code()
