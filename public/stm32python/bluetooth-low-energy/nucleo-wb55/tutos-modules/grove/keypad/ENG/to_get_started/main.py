# Purpose of the script: implementation of a 4x4 matrix keyboard.
# Required equipment (in addition to the NUCLEO-WB55): a 4x4 matrix keyboard and, of course
# cables to connect it to the NUCLEO-WB55...

from keypad import Keypad4x4 # Library to manage the keyboard

keyboard = Keypad4x4() # Instantiate the keyboard

while True:
	key = keyboard.read_key() # Read the pressed key
	print(key) # Display the pressed key