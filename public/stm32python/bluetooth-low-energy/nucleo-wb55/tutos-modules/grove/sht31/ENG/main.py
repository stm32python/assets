# Example adapted from https://github.com/kfricke/micropython-sht31/blob/master/
# Purpose of the script: Implementation of the grove I2C temperature sensor module 
# and humidity sensor based on the SHT31 sensor

from time import sleep_ms
from machine import I2C, Pin
from sht31 import SHT31

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
sht = SHT31(i2c=i2c)

while True:

	# One second delay
	sleep_ms(1000)
	
	# Read the measured values
	shtdata = sht.get_temp_humi()

	# Formatted display of the measurements
	print('=' * 40) # Print a separating line
	
	# Display the temperature in degrees Celsius.
	print("Temperature: %.1f °C" %shtdata[0])
	
	# Displays the humidity in percent.
	# Be careful, the '%' character at the end is split so as not to be interpreted
	# as a formatting instruction!
	print("Relative humidity : %.1f %%" %shtdata[1])
