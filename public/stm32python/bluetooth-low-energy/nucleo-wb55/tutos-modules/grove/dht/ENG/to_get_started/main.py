# Purpose of the script: to use a humidity and temperature sensor
# of the DHT family (DHT11 or DHT22).

import dht # To manage DHT11 and DHT22
from time import sleep_ms # To delay

# Instantiate DHT22
sensor = dht.DHT22('D2')

# For a DHT11, simply use this syntax:
# sensor = dht.DHT11('D2')

while True:

	# Exception handling structure
	try:
		
		# We measure and read the results
		sensor.measure()
		temp = sensor.temperature()
		humi = sensor.humidity()
		
		# If both measurements return 0 simultaneously
		if humi == 0 and temp == 0:
			raise ValueError("Sensor error")

		# Formatting (rounding) of measurements
		temperature = round(temp,1)
		humidity = int(humi)

		# Display of the measurements
		print('=' * 40) # Print a separating line
		print("Temperature : " + str(temperature) + " °C")
		print("Relative humidity: " + str(humidity) + " %")

	# If an exception is caught
	except Exception as e:
		print(str(e) + '\n')

	# Two seconds delay
	sleep_ms(2000)