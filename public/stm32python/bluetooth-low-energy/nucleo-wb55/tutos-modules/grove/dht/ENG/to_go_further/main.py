# Example adapted from https://github.com/kurik/uPython-DHT22/blob/master/main.py
# Purpose of the script: Implementation of the temperature and humidity sensor DHT22
# Second approach : use of a class exploiting the interrupts of a timer

import dht22 # To manage the DHT22
from time import sleep_ms # To delay

# Initialization of the DHT22 driver.
# We need timer 2 for this one
dht22.init(timer_id = 2, data_pin = 'D2')

while True:

	# To handle exceptions
	try:
		# Recovering measures (2-uple)
		(hum, tem) = dht22.measure()
		
		# In case of erroneous return on both measures simultaneously
		if hum == 0 and tem == 0:
			raise ValueError("Sensor error")

		# Formatting (rounding) of measurements
		temp = round(tem,1)
		humi = int(hum)

		# Display of the measurements
		print('=' * 40) # Print a separation line
		print("Temperature : " + str(temp) + " °C")
		print("Relative humidity: " + str(humi) + " %")

	# If an exception has occurred
	except Exception as e:
		print(str(e) + '\n')

	# Two seconds delay
	sleep_ms(2000)

