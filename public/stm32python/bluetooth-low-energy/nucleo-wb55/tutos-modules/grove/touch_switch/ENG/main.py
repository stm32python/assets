# Purpose of the script: Implementation of a touch sensor/switch

from pyb import Pin
from time import sleep_ms # To delay

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :
	time.sleep_ms(500) # Delay for 500 milliseconds

	if p_in.value() == 1: # If the sensor is touched
		print("ON")
	else: # Otherwise
		print("OFF")