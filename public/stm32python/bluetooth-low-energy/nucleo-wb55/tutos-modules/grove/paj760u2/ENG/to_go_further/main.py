# Purpose of the script:
# Driving a Grove RGB LED ring module with a Grove gesture sensor module

import neopixel, paj7620 # Drivers for RGB LED ring and gesture sensor
from time import sleep_ms # To time and measure the elapsed time
from machine import Pin, I2C # To manage the pins and the I2C bus
import uasyncio # For asynchronous management 

# We initialize the LED ring on pin D2
_NB_LED = const(24) # 24 LEDs on the ring
ring = neopixel.NeoPixel(Pin('D2'), _NB_LED)

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the gesture sensor
i2c = I2C(1) 

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c.scan()))

# Sensor address on the I2C bus : 0x73 
PAJ7620U2_ADR = const(0x73)

# Instantiation of the gesture sensor
g = paj7620.PAJ7620(i2c = i2c)

ROTATION = 1 # Direction of rotation (clockwise at startup)
_NB_LED_M1 = const(_NB_LED - 1)

# Asynchronous coroutine to manage the RGB LED ring
@micropython.native # Request the bytecode compiler to produce code for the STM32WB55
async def wheel():

	global ROTATION

	led = 0

	while True:
		
		# Index of the current LED
		if ROTATION: # Hourly rotation
			led += 1
			if led > _NB_LED_M1:
				led = 0
		else: # Counter-clockwise rotation
			led -= 1
			if led < 0:
				led = _NB_LED_M1
		
		# Turn off all LEDs
		for i in range(_NB_LED):
			ring[i] = (0, 0, 0)

		# Turn on the current LED
		ring[led] = (128, 128, 128)
		ring.write()
		
		# Non-blocking timeout
		await uasyncio.sleep_ms(10)

# Asynchronous coroutine to manage the gesture sensor
@micropython.native # Request the bytecode compiler to produce a code for the STM32WB55
async def control():
	
	global ROTATION

	while True:

		gesture = g.gesture()

		if gesture == 7:
			print("Clockwise")
			ROTATION = 1
		elif gesture == 8:
			print("Counterclockwise")
			ROTATION = 0

		# Non-blocking timeout
		await uasyncio.sleep_ms(100)

# Asynchronous coroutine to launch the two other coroutines
@micropython.native 
async def main():
	# Create one task per coroutine
	task1 = uasyncio.create_task(wheel()) # Task for the ring management coroutine
	task2 = uasyncio.create_task(control()) # Task for the sensor management coroutine
	await task1, task2 # Pauses until both tasks are finished

# Starts the scheduler (and thus, the two tasks)
uasyncio.run(main())