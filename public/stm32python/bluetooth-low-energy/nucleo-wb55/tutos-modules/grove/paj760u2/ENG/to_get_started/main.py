# Purpose of the script:
# Testing a Grove gesture sensor module
# Code adapted from :
# https://github.com/itechnofrance/micropython/blob/master/librairies/paj7620/use_paj7620.py

import paj7620 # Drivers for RGB LED ring and gesture sensor
from time import sleep_ms # To time and measure the elapsed time
from machine import I2C # To manage the I2C bus

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the gesture sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c.scan()))

# Sensor address on the I2C bus : 0x73
PAJ7620U2_ADR = const(0x73)

# Instantiation of the gesture sensor
g = paj7620.PAJ7620(i2c = i2c)

# List of gestures
gestures = [ "No gesture", "Forward", "Backward", "Right", "Left", "Up", "Down", "Clockwise", "Counterclockwise", "Wave"]

while True:

	# Read the gesture
	index = g.gesture()

	# Extract the gesture from the list
	if index > 0 and index < 10:
		print(gestures[index])
	
	# Delay of 10 milliseconds
	sleep_ms(10)
