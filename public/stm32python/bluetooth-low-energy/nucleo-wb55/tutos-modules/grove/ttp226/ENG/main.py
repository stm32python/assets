#Script Purpose: Implementation of a touch sensor/switch

from pyb import Pin
from time import sleep_ms # To delay

i1 = Pin('D4', Pin.IN, Pin.PULL_UP)
i2 = Pin('D5', Pin.IN, Pin.PULL_UP)

while True :
	time.sleep_ms(500) # Delay of 500 milliseconds

	if i1.value() == 1: # If we touch the first switch
		print("Switch 1 : ON")
	else: # Otherwise
		print("Switch 1 : OFF")

	if i2.value() == 1: # If the second switch is touched
		print("Switch 2 : ON")
	else: # Otherwise
		print("Switch 2 : OFF")
