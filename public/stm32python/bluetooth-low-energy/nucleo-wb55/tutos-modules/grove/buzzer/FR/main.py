# Objet du script : Jouer un jingle sur un buzzer (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D3 sur laquelle est 
# branché le buzzer.

from pyb import Pin, Timer

# Liste des notes qui seront jouées par le buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 génère une PWM avec TIM1, CH3
BUZZER = Pin('D3') 

while True :
	# Itération entre 0 et 7
	for i in range (0,7) :
		# On ajuste la fréquence pendant l'itération
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		ch3.pulse_width_percent(5)