# Purpose of the script: Play a jingle on a buzzer (Grove or other).
# This example demonstrates the use of PWM for pin D3 on which the buzzer is 
# the buzzer is connected.

from pyb import Pin, Timer

# List of notes that will be played by the buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 generates a PWM with TIM1, CH3
BUZZER = Pin('D3') 

while True :
	# Iteration between 0 and 7
	for i in range (0,7) :
		# We adjust the frequency during the iteration
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Cyclic ratio set at 5% (the buzzer is powered 5% of the time of a period)
		ch3.pulse_width_percent(5)