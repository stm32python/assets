# Purpose of the script: Implementation of grove I2C pressure sensor module,
# and temperature on the BMP280

from time import sleep_ms # To time out
from machine import I2C # Driver of the I2C bus controller
import bmp280 # Driver of the sensor

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used: " + str(i2c.scan())

# Instantiation of the sensor
bmp = bmp280.BMP280(i2c=i2c)

while True:

	# One second delay
	sleep_ms(1000)
	
	# Read the temperature
	temperature = bmp.temperature
	
	# Reading of the pressure
	pressure = bmp.pressure
	
	# Reads the altitude estimate
	altitude = bmp.altitude

	# Display the measurements
	print('=' * 40) # Print a separating line
	print("Temperature : %.1f °C" %temperature)
	print("Pressure : %.1f hPa" %pressure)
	print("Altitude : %.1f m" %altitude)
