# Objet du script : Mise en oeuvre du module grove I2C capteur de pression,
# et température sur le BMP280

from time import sleep_ms # Pour temporiser
from machine import I2C # Pilote du contrôleur de bus I2C
import bmp280 # Pilote du capteur

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
bmp = bmp280.BMP280(i2c=i2c)

while True:

	# Temporisation d'une seconde
	sleep_ms(1000)
	
	# Lecture de la température
	temperature = bmp.temperature
	
	# Lecture de la pression
	pression = bmp.pressure
	
	# Lecture de l'estimation d'altitude
	altitude = bmp.altitude

	# Affichage des mesures
	print('=' * 40)  # Imprime une ligne de séparation
	print("Température : %.1f °C" %temperature)
	print("Pression : %.1f hPa" %pression)
	print("Altitude : %.1f m" %altitude) 