# Purpose of the script: Implementation of the Grove - Laser PM2.5 Sensor (HM3301) module
# Data sheet: https://files.seeedstudio.com/wiki/Grove-Laser_PM2.5_Sensor-HM3301/res/HM-3300%263600_V2.1.pdf
# This module gives an estimate of the average mass of particles present in a cubic meter of air
# according to their approximate diameter: 1 µm, 2.5 µm or 10 µm.

from time import sleep_ms # To manage time delays
from machine import I2C # To manage the I2C
import hm3301 # To manage the sensor

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c1 = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c1.scan()))

# Instantiation of the sensor
sensor = hm3301.HM3301(i2c=i2c1)

while True :
	
	# Mass concentration of the particles of size 1 µm
	std_PM1 = sensor.getData(0)
	# Mass concentration of particles of size 2.5 µm
	std_PM2_5 = sensor.getData(1)
	# Mass concentration of the particles of size 10 µm
	std_PM10 = sensor.getData(2)
	
	# Display
	print("Particle concentration size 1 µm : %d µg/m^3" % std_PM1)
	print("Particle concentration size 2.5 µm: %d µg/m^3" % std_PM2_5)
	print("Particle size concentration 10 µm: %d µg/m^3" % std_PM10)
	
	# 5 second delay
	sleep_ms(5000)
