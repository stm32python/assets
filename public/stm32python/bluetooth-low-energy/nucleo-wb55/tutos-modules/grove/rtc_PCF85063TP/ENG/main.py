# Purpose of the script:
# Demonstration of the implementation of a High Precision RTC module using a component
# PCF85063TP from NXP (data sheet: https://www.nxp.com/docs/en/data-sheet/PCF85063TP.pdf)

from pcf85063tp import RTC_HP # PCF85063TP driver
from machine import I2C # Driver of the I2C bus
from time import sleep # To temporize

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep(1)

# We create an instance of the RTC
rtc = RTC_HP(i2c)

# We set the date to August 8, 2021
rtc.fillByYMD(2021,8,10)

# We set the time to 8:15 minutes and 22 seconds
rtc.fillByHMS(8,15,22)

# Set the day of the week to "TUESDAY
rtc.fillDayOfWeek('MAR')

# Start the clock of the RTC module
rtc.startClock()

# Display for one minute, every second, the time and date
for i in range(60):
	print(rtc.readTime())
	sleep(1)

# Stop the clock of the RTC module
#rtc.stopClock()
