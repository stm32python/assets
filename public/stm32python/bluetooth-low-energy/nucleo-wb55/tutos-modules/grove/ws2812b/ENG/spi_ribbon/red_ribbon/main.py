# This code is adapted from 'https://github.com/JanBednarik/micropython-ws2812'.
# Purpose of this script :
# Program a strip led drived by a WS2812 controlor
# Turn on all the led on red.
# This example used a SPI controlor configured with a baudrate of 4000000 bauds/s

from machine import SPI
import ws2812bstm32
from time import sleep_ms

# WS2812 is connected to MOSI of SPI bus
spi = SPI(1)
led_number = const(30)

sleep_ms(1000)
# Initialisation of strip led
# For STM32WB55 the baudrate MUST be 4000000 bauds
strip = ws2812bstm32.WS2812B(spi_bus=1, ledNumber=led_number, intensity=0.5, baudrate=4000000)

# clear strip led
strip.clear()
strip.show()

# tunr on all the led on red
for index in range(0, led_number):
	strip.put_pixel(index, 255, 0, 0)
strip.show()

sleep_ms(2000)

# Tun on / off the led on sequence
for index in range(0, led_number):
	if index != 0:
		leds.clean(i-1)
	leds.put_pixel(i, 0, 0, 255)
	leds.show()
	sleep_ms(500)
leds.clean(i)
leds.show()
sleep_ms(500)


