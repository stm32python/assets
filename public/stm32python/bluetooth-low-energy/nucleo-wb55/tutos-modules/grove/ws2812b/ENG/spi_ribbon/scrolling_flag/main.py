# This code is adapted from 'https://github.com/JanBednarik/micropython-ws2812'.
# Purpose of this script :
# Program a strip led drived by a WS2812 controlor
# Display flag and move flag on strip led.
# This example used a SPI controlor configured with a baudrate of 4000000 bauds/s

from machine import SPI
import ws2812bstm32
from time import sleep_ms

french_flag = [
	(0,0,250), (0,0,250), (0,0,250),
	(250,250,250), (250,250,250), (250,250,250),
	(250,0,0), (250,0,0), (250,0,0)]

italien_flag = [
	(0,250,0), (0,250,0), (0,250,0),
	(250,250,250), (250,250,250), (250,250,250),
	(250,0,0), (250,0,0), (250,0,0)]

def dual_flag(leds, data0, data1, num_led, t, display):
	i = 0
	while i < num_led:
		if (i + len(data0)) < num_led:
			for d in data0:
				leds.put_pixel(i, d[0], d[1], d[2])
				if display:
					leds.show()
				sleep_ms(t)
				i+=1
			if not display:
				leds.show()
			i+=1
		else:
			break
		if (i + len(data1)) < num_led:
			for d in data1:
				leds.put_pixel(i, d[0], d[1], d[2])
				if display:
					leds.show()
					sleep_ms(t)
					i+=1
			if not display:
				leds.show()
			i+=1
		else:
			break
			

def flag_move(leds, data, num_led, t):
	i = 0
	while i < num_led:
		if (i + len(data)) < num_led:
			if i != 0:
				leds.clean(i-1)
			j = i
			for d in data:
				leds.put_pixel(j, d[0], d[1], d[2])
				j+=1
			leds.show()
			sleep_ms(t)
			i+=1
		else:
			break

led_number = const(16)

# Init strip LED
# WS2812 is connected on MOSI of SPI bus
# For STM32WB55 the baudrate MUST be 4000000
strip = ws2812bstm32.WS2812B(spi_bus=1, ledNumber=led_number, intensity=0.5, baudrate=4000000)

# erase strip led
strip.clear()
strip.show()

# Display 2 flags
dual_flag(strip, french_flag, italien_flag, led_number, 500, True)

while True:

	# erase strip led
	strip.clear()
	strip.show()

	# move flag
	flag_move(strip, french_flag, led_number, 500)

	# Erase strip led
	strip.clear()
	strip.show()

	# Move flag
	flag_move(strip, italien_flag, led_number, 500)

