# Ce code a été adapté à partir de 'https://github.com/JanBednarik/micropython-ws2812'.
# Objet du script :
# Programmer un ruban de LED piloté par un contrôleur WS2812.
# Il s'agit d'allumer toute les LED du ruban en rouge.
# Cet exemple utilise un contrôleur SPI paramétré avec un débit (baudrate) de 4000000 bauds/s

from machine import SPI
import ws2812bstm32  # Bibliothèque pour gérer le ruban de LED
from time import sleep_ms

# Le WS2812 est connecté au MOSI du bus SPI
spi = SPI(1)
led_number = const(30)

sleep_ms(1000)
# Initialisation dyu ruban de LED
# Pour le STM32WB55 le débit DOIT être de 4000000 bauds
strip = ws2812bstm32.WS2812B(spi_bus=1, ledNumber=led_number, intensity=0.5, baudrate=4000000)

# Efface le ruban
strip.clear()
strip.show()

# Allume toutes les LED en rouge
for index in range(0, led_number):
	strip.put_pixel(index, 255, 0, 0)
strip.show()

sleep_ms(2000)

# Allume / éteint les LED en séquence
for index in range(0, led_number):
	if index != 0:
		leds.clean(i-1)
	leds.put_pixel(i, 0, 0, 255)
	leds.show()
	sleep_ms(500)
leds.clean(i)
leds.show()
sleep_ms(500)


