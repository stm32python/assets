# Example adapted from https://github.com/blaa/th02-sensor/blob/master/
# Purpose of the script: Implementation of the grove I2C temperature and humidity sensor module 
# and humidity sensor based on the TH02 sensor

from time import sleep_ms
from machine import I2C, Pin
from th02 import TH02

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
th = TH02(i2c=i2c)

while True:

	# One second delay
	sleep_ms(1000)
	
	# Read the measured values
	humi = th.get_humidity()
	temp = th.get_temperature()

	# Formatted display of the measurements
	print('=' * 40) # Print a separator line
	
	# Display the temperature in degrees Celsius.
	print("Temperature : %.1f °C" %temp)
	
	# Displays the humidity in percent. 
	# Be careful, the '%' character at the end is split so as not to be interpreted
	# as a formatting instruction!
	print("Relative humidity : %.1f %%" %humi)
