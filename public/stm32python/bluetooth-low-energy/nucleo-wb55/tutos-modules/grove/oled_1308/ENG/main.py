# Purpose of the script: Displays text on an OLED display controlled by an SSD1308.

from time import sleep_ms # To temporize
from machine import Pin, I2C # I/O and I2C bus drivers
import ssd1308 # Display driver

# Initialization of the I2C device
i2c = I2C(1)

# Pause for one second to initialize the I2C
sleep_ms(1000)

# List of I2C addresses of the present devices
print("USED I2C adresses : " + str(i2c.scan()))

# Setting the screen characteristics
screen_width_oled = 128
screen_height_oled = 32
oled = ssd1308.SSD1308_I2C(screen_width_oled, screen_height_oled, i2c)

# Sending the text to be displayed on the OLED screen
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('So easy !!!', 0, 20)
oled.show() # Display !
