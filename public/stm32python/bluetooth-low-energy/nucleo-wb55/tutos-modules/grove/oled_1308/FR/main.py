# Objet du script :
# Affiche un texte sur un afficheur OLED contrôlé par un SSD1308.

from time import sleep_ms # Pour temporiser
from machine import Pin, I2C # Pilotes des entrées-sorties et du bus I2C
import ssd1308 # Pilote de l'afficheur

# Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1308.SSD1308_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

# Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show() # Affichage !
