# Objet du script : Mise en oeuvre d'un contrôle moteur avec un relais ou avec un transistor
# Matériel requis :
#  - La carte NUCLEO-WB55
#  - Une breadboard
#  - Un moteur à courant continu
#  - Une alimentation externe (batterie ou pile)
#  - Un relais HK4100F-DC5V-SHG ou un transistor NPN 2N2222A + une résistance de 220 Ohm, selon le montage choisi

from pyb import Pin, Timer

# Variables globales
etat_moteur = 0

# Bouton poussoir (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# LED (LED de la carte NUCLEO)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(1)

# GPIO qui contrôle le relais/transistor
moteur = pyb.Pin('D4', Pin.OUT_PP)

# Interruption de SW1
def ITbutton1(line):
	# Variables globales
	global etat_moteur
	# Etat moteur à 0 ou 1
	if(etat_moteur == 1):
		etat_moteur = 0
	else:
		etat_moteur = 1

# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)

# Boucle infinie
while True:
	if(etat_moteur == 0):
		led_vert.off()
		led_rouge.on()
		moteur.low()
	if(etat_moteur == 1):
		led_vert.on()
		led_rouge.off()
		moteur.high()
