# Purpose of the script: Implementation of a motor control with a relay or with a transistor
# Required equipment:
# - The NUCLEO-WB55 board
# - A breadboard
# - A DC motor
# - An external power supply (battery or cell)
# - A HK4100F-DC5V-SHG relay or a 2N2222A NPN transistor + a 220 Ohm resistor, depending on the chosen assembly

from pyb import Pin, Timer

# Global variables
motor_status = 0

# Push button (input + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# LED (LED on the NUCLEO board)
led_green = pyb.LED(2)
led_red = pyb.LED(1)

# GPIO that controls the relay/transistor
motor = pyb.Pin('D4', Pin.OUT_PP)

# Interrupt SW1
def ITbutton1(line):
	# Global variables
	global motor_status
	# Motor status at 0 or 1
	if(motor_status == 1):
		motor_status = 0
	else:
		motor_status = 1

# Initialization of the interrupt vectors
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)

# Infinite loop
while True:
	if(etat_moteur == 0):
		led_green.off()
		led_red.on()
		moteur.low()
	if(etat_moteur == 1):
		led_green.on()
		led_red.off()
		moteur.high()
