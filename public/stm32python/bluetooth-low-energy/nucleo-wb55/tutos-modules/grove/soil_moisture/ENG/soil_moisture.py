# Driver for analog soil moisture sensors of the type :
# - Capacitive, e.g.: https://wiki.seeedstudio.com/Grove-Capacitive_Moisture_Sensor-Corrosion-Resistant/
# Resistive, e.g. : https://wiki.seeedstudio.com/Grove-Moisture_Sensor/
# After an initial measurement in air (result: sig_air ) and in water (result: sig_water )
# it is possible to calibrate the sensor by passing these two results in the initialization method.

class SOILMOIST:

	# Initialization
	# sig_air : analog return of the sensor when it is in the air
	# sig_water : analog return of the sensor when it is immersed in water
	def __init__(self, adc, sig_air = 0, sig_water = 4095):
		self._adc = adc
		self._min = sig_air
		self._max = sig_water

	# "Raw" analog measurement
	def raw(self):
		return self._adc.read()

	# Measurement refocused in the calibrated range, in %.
	# 0% : sensor in air
	# 100% : sensor in water
	def measure(self):
		return self._map(self._adc.read(), self._min, self._max, 0, 100 )

	# Arduino API map function
	# https://www.arduino.cc/reference/en/language/functions/math/map/
	def _map(self, x, in_min, in_max, out_min, out_max):
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min