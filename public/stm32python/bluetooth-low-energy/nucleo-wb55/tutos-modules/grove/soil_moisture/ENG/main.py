# Purpose of the script:
# Implementation of resistive and capacitive type soil moisture sensors.
# If you are using a resistive sensor, choose CAP = False.
# If you use a capacitive sensor, choose CAP = True.
#
# Calibration procedure:
# Set CALIB = False
# 2. Leave the sensor in the air and bring up the raw values of the ADC.
# Store the result : CAL_AIR = ...
# 3. Immerse the sensor 2/3 in water (without wetting its electronic components!) and
# electronic components!) and reassemble the raw values of the ADC.
# Store the result: CAL_WATER = ...
# 3. Set CALIB = True
# 4. Relaunch the measurements to obtain the humidity values in percent.

from time import sleep # To delay
from pyb import Pin, ADC # To manage pins and ADC
import soil_moisture # driver of the soil moisture sensor

# Have we already calibrated?
CALIB = True

# Is the sensor capacitive?
# (if not, it is taken as resistive)
CAP = True

NB_MES = 255
INV_NB_MES = 1 / NB_MES

# Sensor on A0 (analog)
adc = ADC(Pin('A0'))

if CALIB: # If the calibration constants have already been determined
	
	if CAP: # If it is a capacitive sensor
		
		# Return of the raw measurement for ...
		CAL_AIR = 2535 # My capacitive sensor in air
		CAL_WATER = 1535 # My capacitive sensor is immersed in water
	
	else: # If it is a resistive sensor
		
		# Return of the raw measurement for ...
		CAL_AIR = 0 # My resistive sensor immersed in air
		CAL_WATER = 2365 # My resistive sensor immersed in water

	# Instance of the sensor class with the specified calibration constants
	sensor = soil_moisture.SOILMOIST(adc, sig_air = CAL_AIR, sig_water = CAL_WATER)

else: # If we have not yet calibrated

	#instance of the sensor class
	sensor = soil_moisture.SOILMOIST(adc)

while True:

	if not CALIB: # If we are in the calibration phase

		# Return the "raw" measurements in order to get the constants
		# get the constants CAL_AIR and CAL_WATER
		sum = 0
		for i in range(NB_MES):
			# Raw measurement (in quanta of the ADC)
			sum = sum + sensor.raw()

		# Average of NB_MES measurements
		avg = sum * INV_NB_MES
				
		print("Raw value of the ADC: " % avg)

	else: # If the sensor has already been calibrated

		print("Humidity %1d %%" % sensor.measure())
	
	sleep(10) # Ten seconds delay
