# Source: https://pypi.org/project/micropython-scd30/
# Purpose of the script: Grove SCD30 I2C module implementation.

from time import sleep_ms
from machine import I2C, Pin
from scd30 import SCD30

# We use the I2C n°1 of the NUCLEO-W55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the present devices
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Sensor Instantiation
I2C_ADDRESS = const(0x61)
scd30 = SCD30(i2c, addr = I2C_ADDRESS)

while True:

	# Waits for the sensor to return values (default is every 2 seconds)
	while scd30.get_status_ready() != 1:
		sleep_ms(200)

	# Reading measured values
	scd30data = scd30.read_measurement()

	# Separation and formatting (rounding) of measurements
	conco2 = int(scd30data[0])
	temp = round(scd30data[1],1)
	humi = int(scd30data[2])

	# Measurement display
	print('=' * 40) # Print a separation line
	print("CO2 Concentration: " + str(conco2) + " ppm")
	print("Temperature : " + str(temp) + " °C")
	print("Relative Humidity : " + str(humi) + " %")
