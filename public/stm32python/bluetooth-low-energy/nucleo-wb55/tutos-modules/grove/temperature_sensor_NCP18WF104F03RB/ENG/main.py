"""
Temperature measurement from a Grove Temperature sensor V1.2 module connected to input A0
Since the internal ADC operates on 3.3V, the sensor must be supplied with 3.3V on the Grove adapter card.

Reminder: The value of a NTC thermistor obeys the relation R=R0 x exponential (B( 1/T -1/298))
with B=4275°Kelvin R0=100KOhm and Ten °Kelvin= T in°C +273

The sensor voltage U is related to A0 = 3.3V x R1 /( R1+R) with R1=100K=R0
Deduce U as a function of R0 then T in °C:

  U=3.3/1+exponential (B( 1/T -1/298))
  T= 1/((ln(3.3/U -1) /B)+1/298)

See https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/#reference
Source: F HERGNIOT, Lycée Algoud-Laffemas, Valence
"""

from time import sleep_ms # To delay
from pyb import Pin, ADC # Pin and ADC drivers
from math import log # Logarithm function (neperian)

adc_A0 = ADC('A0') # Initialize the ADC on input A0 
print("Temperature measurement from a Groove sencor V1.2 thermistor") 

# Rule of 3 to deduce the voltage across the thermistor from the 
# converted value of the ADC
ratio = 3.3/4096 

def mesure_temperature_CTN_v12():

	B = const(4275) # coefficient B of the thermistor
	R0 = const(100000) # R0 = 100k x corrective accuracy with true measurement

	N = adc_A0.read() # Conversion of 12 bits - 4096 values from 0 to 3.3V
	U= N * ratio # We deduce the voltage from the converted value of the ADC

	temp = 1.0/(log((3.3/U) -1)/B+1/298)-273 # Calculates the temperature from the voltage

	return temp


def demo():

	while True:
		temperature = mesure_temperature_CTN_v12()
		
		# Displays the temperature as a float xx,x
		print("The temperature value is : %.1f °C" %temperature)
		sleep_ms(1000)

if __name__ == '__main__':
	demo()