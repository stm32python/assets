# Purpose of the Script: Implementation of a PWM to realize a light dimmer.
# The intensity of the LED stops varying when the sw1 button is released.
# Management of the button by "polling".
# Hardware (in addition to the NUCLEO-WB55 card): an LED connected to D6 and GND.

# Reminder : Pins and PWM timers-channels wired on it for the NUCLEO-WB55
#  D0 : TIM2_CH4
#  D1 : TIM2_CH3
#  D3 : TIM1_CH3
#  D5 : TIM2_CH1
#  D6 : TIM1_CH1
#  D9 : TIM1_CH2
#  D11 : TIM17_CH1
#  D12 : TIM16_CH1
#  D13 : TIM2_CH1
#  D14 : TIM17_CH1
#  D15 : TIM16_CH1
#  A3 : TIM2_CH1
#  A2 : TIM2_CH2

from pyb import Pin, Timer
from time import sleep_ms

# Initialization of the button SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

button_pressed = False

# initialization of the PWM
p = Pin('D6')
ti = 1 # Timer
ch = 1 # Channel
tim = Timer(ti, freq=1000) # PWM frequency set to 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i=0

while True:

	if not sw1.value(): # If the button is pressed

		while i < 101: # increase the intensity of the LED by 1% steps
			ch.pulse_width_percent(i)
			i=i+1
			sleep_ms(10) # pause for 10 ms

		while i > 0: # reduce the intensity of the LED by 1% steps
			ch.pulse_width_percent(i)
			i=i-1
			sleep_ms(10) # pause for 10 ms

	else:
		ch.pulse_width_percent(0) # if the button is not pressed
