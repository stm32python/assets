# Purpose of the Script: Implementation of a PWM to realize a light dimmer.
# The intensity of the LED stops dimming after a first press on the sw1 button.
# It starts to vary again after a second press on sw1.
# Use of an external interrupt to manage the button.
# Hardware (in addition to the NUCLEO-WB55 card): an LED connected to D6 and GND.

from pyb import Pin, Timer, ExtInt
from time import sleep_ms

# Initialization of the SW1 button
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Has the button been pressed?
button_pressed = False

# Routine to manage the interruption of the button
def ISR_button(line):
# print("line =", line)
	global button_pressed # keyword "global" very important here
	button_pressed = not button_pressed

ext = ExtInt(Pin('SW1'), ExtInt.IRQ_RISING, Pin.PULL_UP, ISR_button)

# initialization of the PWM
p = Pin('D6')
ti = 1 # Timer
ch = 1 # Channel
tim = Timer(ti, freq=1000) # PWM frequency set to 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i=0

while True:

	if button_pressed :

		while i < 101: # increase the intensity of the LED by 1% steps
			ch.pulse_width_percent(i)
			i=i+1
			sleep_ms(10) # pause for 10 ms

		while i > 0: # reduce the intensity of the LED by 1% steps
			ch.pulse_width_percent(i)
			i=i-1
			sleep_ms(10) # pause for 10 ms
