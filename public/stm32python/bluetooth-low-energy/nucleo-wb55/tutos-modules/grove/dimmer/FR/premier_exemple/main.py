# Objet du Script : Mise en oeuvre d'une PWM pour réaliser un variateur de lumière.
# L'intensité de la LED cesse de varier lorsque le bouton sw1 est relâché.
# Gestion du bouton par "scrutation" ("polling" en anglais).
# Matériel (en plus de la carte NUCLEO-WB55) : une LED connectée sur D6 et GND.

# Rappel : Broches et timers-canaux PWM câblés dessus pour la NUCLEO-WB55
#  D0 : TIM2_CH4
#  D1 : TIM2_CH3
#  D3 : TIM1_CH3
#  D5 : TIM2_CH1
#  D6 : TIM1_CH1
#  D9 : TIM1_CH2
#  D11 : TIM17_CH1
#  D12 : TIM16_CH1
#  D13 : TIM2_CH1
#  D14 : TIM17_CH1
#  D15 : TIM16_CH1
#  A3 : TIM2_CH1
#  A2 : TIM2_CH2

from pyb import Pin, Timer
from time import sleep_ms

# Initialisation du bouton SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

button_pressed = False

# initialisation de la PWM 
p = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i=0

while True:

	if not sw1.value(): # Si le bouton est appuyé

		while i < 101: # augmente l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i+1
			sleep_ms(10) # pause de 10 ms

		while i > 0: # réduit l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i-1
			sleep_ms(10) # pause de 10 ms

	else:
		ch.pulse_width_percent(0) # Si le bouton n'est pas appuyé
