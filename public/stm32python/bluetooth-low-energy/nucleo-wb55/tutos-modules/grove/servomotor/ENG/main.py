# Purpose of the script:
# To drive a servo motor

from time import sleep # To timer
import pyb # To access the pins and counters (timers)

servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)

while True:
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5)	# 90 degree actuator
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)		#  0 degree actuator
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=3)		#  -90 degree actuator
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)		#  0 degree actuator
	sleep(2)

tim_servo.deinit() # Stop the timer of the actuator