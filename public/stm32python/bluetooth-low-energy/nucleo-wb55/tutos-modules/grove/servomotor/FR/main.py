# Objet du script :
# Piloter un servomoteur

from time import sleep # Pour temporiser
import pyb # Pour accéder  aux broches et aux compteurs (timers)

servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)

while True:
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5)	# Servomoteur à 90 degrés
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)		# Servomoteur à 0 degré
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=3)		# Servomoteur à -90 degrés
	sleep(2)
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)		# Servomoteur à 0 degré
	sleep(2)

tim_servo.deinit() # Arrêt du timer du servomoteur