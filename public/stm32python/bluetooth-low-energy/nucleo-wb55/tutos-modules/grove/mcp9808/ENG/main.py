# Purpose of the script:
# Measures temperature every second with a Grove I2C MCP9808 temperature sensor module
# Turns on or off the LEDs of the board according to the temperature values, displays them on the serial port
# Source : https://github.com/mchobby/esp8266-upy/blob/master/mcp9808/examples/

from machine import I2C # I2C controller driver
from mcp9808 import MCP9808 # MCP9808 driver
import pyb # To manage the GPIOs
from time import sleep_ms # To time out

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to initialize the I2C
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the MCP9808
sensor = MCP9808(i2c)

# Instantiation of the LEDs
led_red = pyb.LED(1)
led_green = pyb.LED(2)
led_blue = pyb.LED(3)

while True:

	# Read the temperature
	temp = sensor.get_temp

	# LED and attribute management
	if temp > 25:
		led_red.on()
		attribute = "Hot!"
	elif temp > 18 and temp <= 25:
		led_green.on()
		attribute = "Comfortable"
	else:
		led_blue.on()
		attribute = "Cold!"

	led_red.off()
	led_green.off()
	led_blue.off()

	print("Temperature : %.1f °C (%s)" %(temp,attribute))

	# Delay of 5 seconds
	sleep_ms(5000)
