# Purpose of the script:  Implementation of the Grove LCD 16x2 I2C module
# Libraries for the LCD copied and adapted from this site: https://github.com/Bucknalla/micropython-i2c-lcd

from time import sleep
from machine import I2C # I2C bus driver
from i2c_lcd import lcd # 16x2 LCD module driver

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)
# Pause for one second to initialize I2C
sleep(1)

# List of I2C addresses of the present devices
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instantiation of the display
display = lcd(i2c)

while True:

	# Position the cursor in column 1, line 1
	column = 1
	line = 1
	display.setCursor(column - 1, line -1)

	# Displays "Hello World"
	display.write('Hello World')

	# Position the cursor in column 1, line 2
	column = 1
	line = 2
	display.setCursor(column - 1, line -1)

	#display  "Hello World"
	display.write('Bonjour Monde')

	# Wait for 5 seconds
	sleep(5)

	# Clears the display
	display.clear()

	# Wait for 5 seconds
	sleep(5)
