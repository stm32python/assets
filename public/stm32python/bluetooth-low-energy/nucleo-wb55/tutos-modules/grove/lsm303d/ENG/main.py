# Purpose of the script: implementation of the Grove module "6 Axis Accelerometer And Compass_V2.0".
# It uses an I2C bus.
# This example shows how to program a compass with tilt compensation
# using this module.
# The tilt compensation is calculated using the instructions in document AN3192
# available for download on the STM32python website.
# Source : https://github.com/Seeed-Studio/Grove_6Axis_Accelerometer_And_Compass_v2/blob/master/LSM303D.cpp

from machine import I2C
from lsm303 import LSM303D # IMU driver
from time import sleep_ms
from math import sqrt, atan2, pi, asin, cos, sin

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Parameters to calibrate the magnetometer (by default, "neutral" because the module is already perfectly
# calibrated in factory).

OFFSET_X = 0
OFFSET_Y = 0
OFFSET_Z = 0

SCALE_X = 1
SCALE_Y = 1
SCALE_Z = 1

# Initialization of the IMU instance
imu = LSM303D(i2c, ox = OFFSET_X, oy = OFFSET_Y, oz = OFFSET_Z, sx = SCALE_X, sy = SCALE_Y, sz = SCALE_Z)

# Conversion factor between radians and degrees for angles
RadToDeg = 180 / pi

# Should we start the data collection procedure to calibrate the magnetometer?
# WARNING: this module being already calibrated at the factory, recalibrating it will probably result in degrading
# its accuracy, so we do not recommend this operation. However, it could be useful if the module is
# attached to a larger system that introduces additional Hard Iron and Soft Iron distortions.

CALIBRATE_COMP = False

if CALIBRATE_COMP:
	# Starts the calibration routine
	imu.calibrate_mag()

else:
	# Simulates a compass with tilt compensation
	while True:

		# Measure acceleration and magnetic field vectors
		acc = imu.get_acc()
		mag = imu.get_mag()
		
		# Computes the norm of the vectors
		norm_acc = sqrt(acc[0]*acc[0] + acc[1]*acc[1] + acc[2]*acc[2])
		norm_mag = sqrt(mag[0]*mag[0] + mag[1]*mag[1] + mag[2]*mag[2])

		# If both norms are non-zero
		if norm_acc > 0 and norm_mag > 0:

			# Normalize the components of the vectors so that you can compute
			# the following arcsinus and arccosinus.

			inv_acc = 1 / norm_acc

			ax = acc[0] * inv_acc
			ay = acc[1] * inv_acc
			az = acc[2] * inv_acc
		
			inv_mag = 1 / norm_mag

			bx = mag[0] * inv_mag
			by = mag[1] * inv_mag
			bz = mag[2] * inv_mag
			
			# Calculates the Euler angles
			
			pitch = asin(-ax)
			roll = asin(ay/cos(pitch))
	
			xh = bx * cos(pitch) + bz * sin(pitch)
			yh = bx * sin(roll) * sin(pitch) + by * cos(roll) - bz * sin(roll) * cos(pitch)
			#zh = -bx * cos(roll) * sin(pitch) + by * sin(roll) + bz * cos(roll) * cos(pitch)

			heading = atan2(yh, xh)

			# Expression of angles in degrees for display
			pitch_deg = pitch * RadToDeg
			roll_deg = roll * RadToDeg
			heading_deg = heading * RadToDeg
			
			# If the obtained heading is negative, compute its complement at 360
			if heading_deg < 0:
				heading_deg += 360

			# Display of angles
			print("Pitch = %.1f°" % pitch_deg)
			print("Roll = %.1f°" % roll_deg)
			print("Heading = %.1f°" % heading_deg)
			print("")

		sleep_ms(250)
