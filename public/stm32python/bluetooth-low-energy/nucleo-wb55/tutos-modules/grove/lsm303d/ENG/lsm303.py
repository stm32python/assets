# STMicroelectronics LSM303D MEMS driver
# Component data sheet: https://www.st.com/en/mems-and-sensors/lsm303d.html
# Sources for the driver:
# https://github.com/Seeed-Studio/Grove_6Axis_Accelerometer_And_Compass_v2/blob/master/LSM303D.cpp
# https://github.com/kamikaze/pyboard-examples/blob/master/imu/lsm303.py (for the accelerometer only!)

from machine import I2C
from time import sleep_ms
import struct

LSM303D_ADDR =const(0x1E)

STATUS_M = const(0x07)

OUT_X_L_A = const(0x28)
OUT_X_H_A = const(0x29)
OUT_Y_L_A = const(0x2A)
OUT_Y_H_A = const(0x2B)
OUT_Z_L_A = const(0x2C)
OUT_Z_H_A = const(0x2D)

OUT_X_L_M = const(0x08)
OUT_X_H_M = const(0x09)
OUT_Y_L_M = const(0x0A)
OUT_Y_H_M = const(0x0B)
OUT_Z_L_M = const(0x0C)
OUT_Z_H_M = const(0x0D)

CTRL1 = const(0x20)
CTRL2 = const(0x21)
CTRL5 = const(0x24)
CTRL6 = const(0x25)
CTRL7 = const(0x26)

# Conversion factors between the numerical value of the acceleration and
# acceleration in g (according to page 10 of the data sheet).
ACC_02G = 0.000061
ACC_04G = 0.000122
ACC_06G = 0.000183
ACC_08G = 0.000244
ACC_16G = 0.000732

ACC_RNG = [2 ,4, 6, 8, 16]

# Possible frequencies of the accelerometer (in Hz)
ACC_FRQ = [3.125, 6.25, 12.5, 25, 50, 100, 200, 400, 800, 1600]

# conversion factors between the numerical value of the magnetic field and
# the magnetic field in µT (from page 10 of the data sheet).
# For information: 1 Milligauss = 0.1 Microtesla
MAG_02G = 0.008
MAG_04G = 0.016
MAG_08G = 0.032
MAG_12G = 0.0479

MAG_RNG = [2, 4 ,8, 12]

# Possible frequencies of the magnetometer (in Hz)
MAG_FRQ = [3.125, 6.25, 12.5, 25, 50, 100]

# Required parameters for magnetometer calibration.
# These values must be determined for each Grove module using
# the calibrate_mag function using the calibration procedure.

OFFSET_X = const(0)
OFFSET_Y = const(0)
OFFSET_Z = const(0)

SCALE_X = const(1)
SCALE_Y = const(1)
SCALE_Z = const(1)

class LSM303D():

	# Initializations
	def __init__(self, i2c, address = LSM303D_ADDR, ox = OFFSET_X, oy = OFFSET_Y, oz = OFFSET_Z, sx = SCALE_X, sy = SCALE_Y, sz = SCALE_Z):
		
		self.address = address
		self.i2c = i2c
		self.i2c.scan()
		
		# Buffer lists to read registers
		self.one_byte = bytearray(1)
		self.six_bytes = bytearray(6)
		
		# IMU status
		self.mag_range = 2 # Magnetometer sensitivity +/- 2g
		self.acc_range = 2 # Accelerometer sensitivity +/- 2 Gauss
		self.acc_freq = 50 # Frequency of accelerometer measurements
		self.mag_freq = 50 # Frequency of magnetometer measurements
		
		# Vector for magnetometer calibration
		self.mag_offset = [ox, oy, oz]
		self.mag_scale = [sx, sy, sz]

		# Stop the accelerometer
		self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x00]))

		# Default setting of the IMU
		# Value written in the CTRL1 register: 0x57 = 0101 0111
		# - Measurements on the 3 axes are active: 111
		# - Continuous update of acceleration measurements : 0
		# - Accelerometer frequency set to 50 Hz : 0101
		
		self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x57]))

		# Value written to the CTRL2 register: 0000 0000
		# - Accelerometer anti-alias filter bandwidth. : 773 Hz
		# - Accelerometer sensitivity : +/- 2g
		# - Acceleration self-test disabled
		# - SPI interface : 4 wires (not used by our module)
		self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x00]))

		# Value written to register CTRL5: 0x70 = 0111 0000
		# - Temperature sensor disabled: 0
		# Resolution of the magnetic sensor high: 11
		# Frequency of the magnetic sensor set at 50 Hz: 100
		# - Interruptions not activated: 00
		self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x70]))

		# Value written to the CTRL6 register: 0000
		# - Sensitivity of the magnetometer +/- 2 gauss : 00
		self.i2c.writeto_mem(self.address, CTRL6, bytearray([0x00]))

		# Value written in the register CTRL7 : 0000 
		# - High-pass filter mode selection for acceleration data : 00
		# - Filtered acceleration data selection bypassed : 0
		# - Temperature sensor is on while magnetic sensor is on : 0
		# - Continuous update of magnetic measurements : 00
		self.i2c.writeto_mem(self.address, CTRL7, bytearray([0x00]))

	# Stop the accelerometer
	def stop_acc(self):
		self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x00]))
		self.acc_freq = 0
		print("Accelerometer disabled")

	# Stop the magnetometer
	def stop_mag(self):
		self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x78]))
		self.mag_freq = 0
		print("Magnetometer disabled")

	# Returns the "raw" (non-dimensional) components of the acceleration vector
	def get_raw_acc(self):

		# Gets the acceleration value (encoded on 16 bits in two's complement)
		self.i2c.readfrom_mem_into(self.address,OUT_X_L_A | 0x80, self.six_bytes)
		raw_accel_data = struct.unpack('<hhh', self.six_bytes)

		acc_x = raw_accel_data[0]
		acc_y = raw_accel_data[1] 
		acc_z = raw_accel_data[2] 

		return (acc_x, acc_y, acc_z)

	# Returns the dimensioned components of the acceleration vector (in g)
	def get_acc(self):
		
		raw_accel_data = self.get_raw_acc()
		
		if self.acc_range == 2: # amplitude +/- 2g
			acc_x = raw_accel_data[0] * ACC_02G
			acc_y = raw_accel_data[1] * ACC_02G
			acc_z = raw_accel_data[2] * ACC_02G
		elif self.acc_range == 4: # amplitude +/- 4g
			acc_x = raw_accel_data[0] * ACC_04G
			acc_y = raw_accel_data[1] * ACC_04G
			acc_z = raw_accel_data[2] * ACC_04G
		elif self.acc_range == 6: # amplitude +/- 6g
			acc_x = raw_accel_data[0] * ACC_06G
			acc_y = raw_accel_data[1] * ACC_06G
			acc_z = raw_accel_data[2] * ACC_06G
		elif self.acc_range == 8: # amplitude +/- 8g
			acc_x = raw_accel_data[0] * ACC_08G
			acc_y = raw_accel_data[1] * ACC_08G
			acc_z = raw_accel_data[2] * ACC_08G
		elif self.acc_range == 16: # amplitude +/- 16g
			acc_x = raw_accel_data[0] * ACC_16G
			acc_y = raw_accel_data[1] * ACC_16G
			acc_z = raw_accel_data[2] * ACC_16G

		return (acc_x, acc_y, acc_z)

	# To change the frequency of the accelerometer
	def set_acc_freq(self, value):
		if value in ACC_FRQ:
			# Stop the accelerometer
			self.stop_acc()

			# Restarts the accelerometer at the required frequency
			if value == 3.125:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x17]))
			elif value == 6.25:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x27]))
			elif value == 12.5:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x37]))
			elif value == 25:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x47]))
			elif value == 50:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x57]))
			elif value == 100:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x67]))
			elif value == 200:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x77]))
			elif value == 400:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x87]))
			elif value == 800:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0x97]))
			elif value == 1600:
				self.i2c.writeto_mem(self.address, CTRL1, bytearray([0xA7]))
			self.acc_freq = value
			print("Fréquence de l'accéléromètre : +/- %.3f Hz" %value)
		else:
			print("You have required +/- %.3f Hz for the accelerometer." %value)
			print("The allowed values are: ")
			for itm in ACC_FRQ:
				print(" +/- %.3f Hz" %itm)
			raise AttributeError("Error set_acc_freq")

	# To change the accelerometer amplitude
	def set_acc_range(self, value):
		if value in ACC_RNG:
			if value == 2:
				self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x00]))
			elif value == 4:
				self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x08]))
			elif value == 6:
				self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x10]))
			elif value == 8:
				self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x18]))
			elif value == 16:
				self.i2c.writeto_mem(self.address, CTRL2, bytearray([0x20]))
			self.acc_range = value
			print("Accelerometer amplitude: +/- %1d g" %value)
		else:
			print("You have required +/- %1d g for the accelerometer." %value)
			print("The allowed values are: ")
			for itm in ACC_RNG:
				print(" +/- %1d g" %itm)
			raise AttributeError("Error set_acc_range")

	# Returns the "raw" (non-dimensional) components of the magnetic field vector
	def get_raw_mag(self):

		self.i2c.readfrom_mem_into(self.address, STATUS_M ,self.one_byte)

		# Mask on the least significant bit of the status register (updated value?)
		if int(self.one_byte[0] & 0b00000001):
			# Gets the value of the magnetic field (coded on 16 bits in two's complement)
			self.i2c.readfrom_mem_into(self.address, OUT_X_L_M | 0x80, self.six_bytes)
			raw_mag_data = struct.unpack_from('<hhh', self.six_bytes)
			
			# Apply calibration corrections
			mag_x = (raw_mag_data[0] - self.mag_offset[0]) * self.mag_scale[0]
			mag_y = (raw_mag_data[1] - self.mag_offset[1]) * self.mag_scale[1]
			mag_z = (raw_mag_data[2] - self.mag_offset[2]) * self.mag_scale[2]
		else:
			mag_x = float("NaN")
			mag_y = float("NaN")
			mag_z = float("NaN")

		return (mag_x, mag_y, mag_z)

	# Returns the dimensioned components of the magnetic field vector (in µT)
	def get_mag(self):

		raw_mag_data = self.get_raw_mag()

		if self.mag_range == 2: # amplitude +/- 2 gauss
			mag_x = raw_mag_data[0] * MAG_02G
			mag_y = raw_mag_data[1] * MAG_02G
			mag_z = raw_mag_data[2] * MAG_02G
		elif self.mag_range == 4: # amplitude +/- 4 gauss
			mag_x = raw_mag_data[0] * MAG_04G
			mag_y = raw_mag_data[1] * MAG_04G
			mag_z = raw_mag_data[2] * MAG_04G
		elif self.mag_range == 8: # amplitude +/- 8 gauss
			mag_x = raw_mag_data[0] * MAG_08G
			mag_y = raw_mag_data[1] * MAG_08G
			mag_z = raw_mag_data[2] * MAG_08G
		elif self.mag_range == 12: # amplitude +/- 12 gauss
			mag_x = raw_mag_data[0] * MAG_12G
			mag_y = raw_mag_data[1] * MAG_12G
			mag_z = raw_mag_data[2] * MAG_12G

		return (mag_x, mag_y, mag_z)

	# To change the frequency of the magnetometer
	def set_mag_freq(self, value):
		if value in MAG_FRQ:
			
			# Stop the magnetometer
			self.stop_mag()
			
			# Restarts the magnetometer at the required frequency
			if value == 3.125:
				self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x60]))
			elif value == 6.25:
				self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x64]))
			elif value == 12.5:
				self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x68]))
			elif value == 25:
				self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x6C]))
			elif value == 50:
				self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x70]))
			elif value == 100:
				if sef.acc_freq == 0 or sef.acc_freq > 50:
					self.i2c.writeto_mem(self.address, CTRL5, bytearray([0x74]))
				else:
					print("The magnetometer frequency can only be set to 100 Hz if the accelerometer is stopped or has a frequency greater than 50 Hz.")
					raise AttributeError("Error set_mag_freq")
			self.mag_freq = value
			print("Magnetometer frequency: +/- %.3f Hz" %value)
		else:
			print("You have required +/- %.3f Hz for the magnetometer." %value)
			print("The allowed values are: ")
			for itm in MAG_FRQ:
				print(" +/- %.3f Hz" %itm)
			raise AttributeError("Error set_mag_freq")

	# To change the magnetometer amplitude
	def set_mag_range(self, value):
		if value in MAG_RNG:
			if value == 2:
				self.i2c.writeto_mem(self.address, CTRL6, bytearray([0x00]))
			elif value == 4:
				self.i2c.writeto_mem(self.address, CTRL6, bytearray([0x20]))
			elif value == 8:
				self.i2c.writeto_mem(self.address, CTRL6, bytearray([0x40]))
			elif value == 12:
				self.i2c.writeto_mem(self.address, CTRL6, bytearray([0x60]))
			self.mag_range = value
			print("Magnetometer amplitude: +/- %1d gauss" %value)
		else:
			print("You have required +/- %1d Gauss for the magnetometer." %value)
			print("The allowed values are: ")
			for itm in MAG_RNG:
				print(" +/- %1d gauss" %itm)
			raise AttributeError("Error set_mag_range")
			
	# Calibrates the magnetometer
	# This function determines the minimum and maximum values of magnetic field measurements 
	# and calculates the corrections to be made to the values returned
	# to the values returned by the magnetometer.
	# The procedure requires the magnetometer to make movements in
	# in space, in the shape of a "8" for two minutes.
	
	def calibrate_mag(self):

		# Initialize the extreme values for the calibration

		MAX_ITER = const(100) # Number of iterations for calibration
		DUMMY = const(32767)

		min_x = DUMMY  
		min_y = DUMMY
		min_z = DUMMY

		max_x = -DUMMY
		max_y = -DUMMY
		max_z = -DUMMY

		print("Start calibration")

		for index in range (1, MAX_ITER):

			# Read the magnetic field on the three orthogonal axes
			raw_mag_data = self.get_raw_mag()
			
			x = raw_mag_data[0]
			y = raw_mag_data[1]
			z = raw_mag_data[2]

			# At each iteration, we determine the min and max values of the magnetic field according to x,y and z

			min_x = min(min_x, x)
			min_y = min(min_y, y)
			min_z = min(min_z, z)

			max_x = max(max_x, x)
			max_y = max(max_y, y)
			max_z = max(max_z, z)

			sleep_ms(250)

		# Display on the serial port the extreme values for calibration
		print("End of calibration")
		print("\n")
		print("Amplitudes taken for calibration :")
		print("\n")
		print("min_x : %5d, max_x : %5d" % (min_x, max_x))
		print("min_y : %5d, max_y : %5d" % (min_y, max_y))
		print("min_z : %5d, max_z : %5d" % (min_z, max_z))

		# Calculation of the "Hard Iron" offsets for each axis:

		self.mag_offset = [(max_x + min_x)/2, (max_y + min_y)/2, (max_z + min_z)/2]

		# Calculation of coefficients for the approximate correction of 
		# "Soft Iron" distortions for each axis:

		avg_delta_x = (max_x - min_x) / 2
		avg_delta_y = (max_y - min_y) / 2
		avg_delta_z = (max_z - min_z) / 2

		avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

		self.mag_scale = [avg_delta/avg_delta_x, avg_delta/avg_delta_y, avg_delta/avg_delta_z]

		print("The calibration constants are:")
		print("\n")
		print("  OFFSET_X = %.1f" % self.mag_offset[0])
		print("  OFFSET_Y = %.1f" % self.mag_offset[1])
		print("  OFFSET_Z = %.1f" % self.mag_offset[2])
		print("\n")
		print("  SCALE_X = %.1f" % self.mag_scale[0])
		print("  SCALE_Y = %.1f" % self.mag_scale[1])
		print("  SCALE_Z = %.1f" % self.mag_scale[2])
