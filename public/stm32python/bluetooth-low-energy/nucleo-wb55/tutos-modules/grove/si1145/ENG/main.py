# Example adapted from https://github.com/neliogodoi/MicroPython-SI1145
# Purpose of the script: Implementation of grove I2C sunlight sensor module,
# based on the SI1145 sensor to measure the UV index.

from time import sleep_ms # To manage the timings
from machine import I2C # To manage the I2C
import si1145 # To manage the sensor

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c1 = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c1.scan()))

# Instantiation of the sensor
sensor = si1145.SI1145(i2c=i2c1)

while True :
	# UV index
	uv_index = sensor.read_uv
	
	# Infrared radiation intensity characteristic value
	ir_analog = sensor.read_ir
	
	# Characteristic value of the intensity of visible radiation
	visible_analog = sensor.read_visible
	
	# Display
	print("UV index: %d IR: %d (AU)\n Visible: %d (AU)\n" % (uv_index, ir_analog, visible_analog))
	
	# 5 second delay
	sleep_ms(5000)
