# Caution: the sensor must be supplied with 5V to give a response between 0 and 4095.

from pyb import ADC, Pin # Analog to digital converter and GPIO
from time import sleep # For time delays

# Instantiation and start of the analog-to-digital converter
adc = ADC(Pin('A0'))

while True:
	# Digitizes the value read, produces a time-varying result in the range [0; 4095]
	Measure = adc.read()

	# If a drop falls on the sensor then the user is warned. To change the detection level you have to change the value of the if condition
	if(Measure <= 3500):
		print("Alert: rain detection")

	# One second delay
	sleep(1)
