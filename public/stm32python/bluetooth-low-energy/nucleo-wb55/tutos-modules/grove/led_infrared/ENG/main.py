# Code object: MicroPyhton version of the "Blink" program.
# Blinks an LED (possibly infrared) at a programmable frequency.
# Requires an external LED to the board (Grove module for example).

from pyb import Pin # Class to manage GPIO pins
from time import sleep_ms # Class for timing

# The LED is assigned to pin D4
led = Pin('D4', Pin.OUT_PP)

while True :
	sleep_ms(500) # Set 0.5 seconds
	led.off()
	print("LED off")
	sleep_ms(1000) # Pause 1 second
	led.on()
	print("LED on")
