# Example adapted from https://github.com/mcauser/micropython-tm1637'
# Purpose of the script:
# Measures the temperature (in degrees Celsius) of the ambient air every second
# Displays the temperature on the Grove 7-segment display
# This example requires an X-NUCLEO IKS01A3 shield, a Grove Base Shield for Arduino and a Grove 7-segment display.
# The 7-segment display must be connected to the "A0" connector of the Grove Base Shield for Arduino.

from machine import I2C # I2C bus drivers
import stts751 # Temperature sensor drivers

# GPIO drivers and Micropython timing
import pyb
from time import  sleep_ms
from pyb import Pin
import tm1637 # 7-segment display drivers

# Initialization of the 7-segment display
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate the temperature sensor
i2c = I2C(1)

# Pause for 1s to give the I2C time to initialize
sleep_ms(1000)

sensor = stts751.STTS751(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:
	
	temp = round(sensor.temperature()) # Rounded to the nearest integer
	
	print("Température : " + str(temp) + "°C (", end='')
	
	tm.number(temp) # Displays the temperature
	
	if temp > 24:
		led_red.on()
		print("Chaud)")
	elif temp > 18 and temp <= 24:
		led_green.on()
		print("Confortable)")
	else:
		led_blue.on()
		print("Froid)")

	led_red.off()
	led_green.off()
	led_blue.off()
	
	sleep_ms(1000) # 1 second delay
