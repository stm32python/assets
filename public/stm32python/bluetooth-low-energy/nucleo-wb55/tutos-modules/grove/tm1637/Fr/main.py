# Exemple adapté de https://github.com/mcauser/micropython-tm1637'
# Objet du script :
# Mesure la température (en degré celsius) de l'air ambiant toutes les secondes
# Affiche la température sur l'afficheur Grove 7-segments
# Cet exemple nécessite un shield X-NUCLEO IKS01A3, un Grove Base Shield pour Arduino et un Afficheur Grove 7-segments.
# L'afficheur 7-segments doit être connecté sur la fiche "A0" du Grove Base Shield pour Arduino.

from machine import I2C # Pilotes du bus I2C
import stts751 # Pilotes du capteur de température

# Pilotes GPIO et temporisation de Micropython
import pyb
from time import  sleep_ms
from pyb import Pin
import tm1637 # Pilotes de l'afficheur 7-segments

# Initialisation de l'afficheur 7-segments
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer le capteur de température
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

sensor = stts751.STTS751(i2c)

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:
	
	temp = round(sensor.temperature()) # Arrondi à l'entier le plus proche
	
	print("Température : " + str(temp) + "°C (", end='')
	
	tm.number(temp) # Affiche la température
	
	if temp > 24:
		led_rouge.on()
		print("Chaud)")
	elif temp > 18 and temp <= 24:
		led_vert.on()
		print("Confortable)")
	else:
		led_bleu.on()
		print("Froid)")

	led_rouge.off()
	led_vert.off()
	led_bleu.off()
	
	sleep_ms(1000) # Temporisation d'une seconde