# Purpose of the script:
# Demonstration of the implementation of an RTC module using a component
# DS1307 from Maxim Integrated (data sheet: https://datasheets.maximintegrated.com/en/ds/DS1307.pdf)
# Driver adapted from the source at
# https://github.com/mcauser/micropython-tinyrtc-i2c/blob/master/ds1307.py
# WARNING, to work properly this module must be supplied with 5V


from ds1307 import DS1307 # Driver from ds1307
from machine import I2C # I2C bus driver
from time import sleep # To time

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep(1)

# We create an instance of the RTC
ds = DS1307(i2c)

# We set the date to August 26, 2021 (4th day of the week)
# Set the time to 23h 59min 0sec
ds.setDateTime([2021, 8, 26, 4, 23, 59, 58, 0])

# The time and date are displayed for one minute, every second
for i in range(60):
	print(ds.readDateTime())
	sleep(1)

# Stop the clock
ds.halt(True)
