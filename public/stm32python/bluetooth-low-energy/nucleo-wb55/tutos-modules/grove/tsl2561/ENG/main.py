# Script Purpose:
# Grove I2C Digital Light Sensor Module Implementation (TLS2561)
# Sources: https://github.com/mchobby/esp8266-upy/tree/master/tsl2561

from tsl2561 import * # Module driver
from machine import I2C # I2C bus driver
from time import sleep # to delay

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep(1)

# Instantiate the sensor
tsl = TSL2561( i2c )

while True:
	# Read a value
	
	# This will automatically activate the sensor (which takes time)
	# then performs the reading then deactivates the sensor.
	# Returns a value in lux (ex: 6.815804 Lux)
	#print( tsl.read() )

	# Note: you can manually activate/deactivate the sensor with
	# active(True/False).

	# You can manually change the gain and integration time
	# * The gain can be 1 or 16
	# * Integration time: 0 or 13 or 101 or 402 (0=manual)
	#tsl.gain( 16 )
	#tsl.integration_time( 402 )
	#print( tsl.read() )

	# You can also use an automatic gain selection (AutoGain)
	# (only if you do not use manual integration)
	tsl.integration_time( 402 )
	print( "Illumination : %.1f lx" % tsl.read(autogain=True))

	# One second delay
