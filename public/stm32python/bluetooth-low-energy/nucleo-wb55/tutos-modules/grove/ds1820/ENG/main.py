# Source: https://github.com/micropython/micropython
# Purpose of the script: Temperature measurement every second.
# Displays the measured values on the serial port of the USB user
# This example requires a Grove Base Shield and a DS18X20 temperature sensor connected on A0

from time import sleep_ms
import machine
import onewire # Library for the OneWire protocol
import ds18x20 # Library for the DS18X20 temperature sensor

# We have connected our sensor to pin A0
dat = machine.Pin('A0')

# Create a "OneWire" object (instance of the OneWire protocol)
onew = onewire.OneWire(dat)

# Connect the probe to this instance
ds = ds18x20.DS18X20(onew)

# Search for devices on the OneWire bus
roms = ds.scan()
print('Devices found:', roms)

# Performs 20 consecutive temperature measurements
for i in range(20):
	print('Temperature (degrees Celsius):', end=' ')
	ds.convert_temp()
	sleep_ms(1000)
	# For all objects connected to the OneWire
	for rom in roms:
		temp = round(ds.read_temp(rom),1)
		print(temp, end=' ')
	# Print a blank line
	print("\n")
