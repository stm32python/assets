# Example adapted from https://github.com/safuya/micropython-sgp30/blob/master/
# Purpose of the script: Implementation of the grove I2C VOC and CO2 gas sensor module
# based on the SGP30 sensor

from time import sleep_ms
from machine import I2C, Pin
from sgp30 import SGP30

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of the I2C addresses of the peripherals present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
sgp = SGP30(i2c)

while True:

	# Delay of one second
	sleep_ms(1000)

	# Read the measured values
	co2eq, tvoc = sgp.indoor_air_quality

	# Formatted display of the measured values
	print("CO2eq = %d ppm TVOC = %d ppb" % (co2eq, tvoc))
