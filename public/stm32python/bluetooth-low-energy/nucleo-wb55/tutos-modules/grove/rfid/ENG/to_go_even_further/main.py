# Purpose of the script: to use RFID technology to create a secure lock (bis)

import pyb
import time
import mfrc522

# Initialization of the badge reader
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') #SCK, MOSI, MISO, RST, SDA

# Initialization of LEDs (LED_1, LED_2, LED_3)
led_blue = pyb.LED(3)
led_green = pyb.LED(2)
led_red = pyb.LED(1)
led_blue.on()
led_red.off()
led_green.off()

# Initialization of the actuator
servo = pyb.Pin('D6')

# Initialization of the buzzer
buzzer = pyb.Pin('D3')

# Button initializations
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Variables
compteur = 0
MASTERKEY = [None]*12
e = 0
nb_max_badge = 8

print("\nRegister your first badge")
#infinie loop
while True:
	# Reading a badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)
	# Recording the UID of a badge
	if stat == rdr.OK and compteur == 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			MASTERKEY = raw_uid[0:4] #Retrieves the UID values
			print("-"*36)
			print("| Registered UID: %03d.%03d.%03d.%03d |" %(MASTERKEY[0], MASTERKEY[1], MASTERKEY[2], MASTERKEY[3]))
			print("-"*36)
			compteur = compteur + 1 #Increment the counter to not come back in this loop
			led_blue.off()

	# Registration of a new badge by pressing SW1
	elif sw1.value() == 0:
		if e < nb_max_badge:
			led_blue.on()
			print("\nRegistration of another badge")
			(stat, tag_type) = rdr.request(rdr.REQIDL)
			(stat, raw_uid) = rdr.anticoll()
			while(stat == rdr.ERR): # On hold until a badge is detected
				(stat, tag_type) = rdr.request(rdr.REQIDL)
				(stat, raw_uid) = rdr.anticoll()
			if stat == rdr.OK: #If badge detected then we save
				e = e + 4 # Point to an empty MASTERKEY slot
				MASTERKEY = MASTERKEY + raw_uid[0:4] # MASTERKEY stores the old UID + the new UID
				print("-"*36)
				print("| Registered UID : %03d.%03d.%03d.%03d |" %(MASTERKEY[e], MASTERKEY[e+1], MASTERKEY[e+2], MASTERKEY[e+3]))
				print("-"*36)
				led_blue.off()
		else:
			print("\n|!| Error : already 3 badges registered !")

	# Display of registered UIDs if SW2 is pressed
	elif sw2.value() == 0:
		print("\nList of registered UIDs")
		print("-"*23)
		for i in range(0, e+1, 4):
			print("| --> %03d.%03d.%03d.%03d |"%(MASTERKEY[i+0], MASTERKEY[i+1], MASTERKEY[i+2], MASTERKEY[i+3]))
		print("-"*23)

	# Deleting a badge by pressing SW3
	elif sw3.value() == 0:
		print("\nDeleting a badge")
		led_red.on()
		while(stat == rdr.ERR): # On hold until a badge is detected
			(stat, tag_type) = rdr.request(rdr.REQIDL)
			(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			j = 0
			while(MASTERKEY[j:j+4] != raw_uid[0:4]): # Search for the UID of the badge presented in those stored
				j = j + 4
			del MASTERKEY[j:j+4] # Deleting the badge
			e = e - 4
			print("Badge n°%d deleted !" %(j/4+1))
			led_red.off()

	#Display of the UID in the serial monitor
	elif stat == rdr.OK and compteur != 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			del raw_uid[4]
			print("\nUID lu : %03d.%03d.%03d.%03d" %(raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))

		# Checking the badge
		if (raw_uid[0:4] == MASTERKEY[0:4]) or (raw_uid[0:4] == MASTERKEY[4:8]):
			print("--> Badge: valid".)
			led_green.on() # Lights up the green LED
			tim_servo = pyb.Timer(1, freq=50)
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5) # Rotates the actuator 90 degrees
			time.sleep(3) # 3 second delay
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)
			time.sleep_ms(500) # Time for the actuator to reset
			tim_servo.deinit() # Timer stop for the actuator
			led_green.off()
		else:
			print("--> Badge : not valid")
			led_red.on() # Lights up the red LED
			for j in range(0, 5): # Buzzer sequence
				tim_buzzer = pyb.Timer(1, freq=1000)
				tim_buzzer.channel(3, pyb.Timer.PWM, pin=buzzer, pulse_width_percent=5)
				time.sleep_ms(200)
				tim_buzzer = pyb.Timer(1, freq=3000)
				tim_buzzer.channel(3, pyb.Timer.PWM, pin=buzzer, pulse_width_percent=5)
				time.sleep_ms(200)
			tim_buzzer.deinit() # Stop the timer for the buzzer
			led_red.off() # Turns off the red LED
