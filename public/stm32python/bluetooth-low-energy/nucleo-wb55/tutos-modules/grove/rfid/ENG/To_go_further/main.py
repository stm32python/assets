# Purpose of the script: to use RFID technology to create a secure lock

import pyb
import time
import mfrc522

#Initialization of the badge reader
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10')		#SCK, MOSI, MISO, RST, SDA

# Initialization of LEDs (LED_1, LED_2, LED_3)
led_blue = pyb.LED(3)
led_green = pyb.LED(2)
led_red = pyb.LED(1)
led_blue.on()
led_red.off()
led_green.off()

# Initialization of the actuator
servo = pyb.Pin('D6')

# Variables
compteur = 0
MASTERKEY = [None]*4

print("\nRegistration of a first badge")

# infinie loop
while True:
	# Reading a badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)
	# Recording a badge's UID
	if stat == rdr.OK and compteur == 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			MASTERKEY = raw_uid[0:4] # Retrieves the UID values
			print("-"*36)
			print("| UID registered : %03d.%03d.%03d.%03d |" %(MASTERKEY[0], MASTERKEY[1], MASTERKEY[2], MASTERKEY[3]))
			print("-"*36)
			compteur = compteur + 1 # Increment the counter so as not to return to this loop
			led_blue.off()

	# Display of the UID in the serial monitor
	elif stat == rdr.OK and compteur != 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			print("\nUID read : %03d.%03d.%03d.%03d" %(raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))

		# Checking the badge
		if (raw_uid[0:4] == MASTERKEY[0:4]):
			print("--> Badge : valid")
			led_green.on() # Lights up the green LED
			tim_servo = pyb.Timer(1, freq=50)
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5) #Turns the actuator 90 degrees
			time.sleep(3) # 3 second delay
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)
			time.sleep_ms(500) # Time for the actuator to reset
			tim_servo.deinit() # Timer stop for the actuator
			led_green.off() # Turns off the green LED
		else:
			print("--> Badge : not valid")
			led_red.on() #  Lights up the red LED
			time.sleep(1) # 1 second delay
			led_red.off() # Turns off the red LED
