# Purpose of the script: reading an RFID badge

import pyb
import time
import mfrc522

#Initialization of the badge reader
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') # SCK, MOSI, MISO, RST, SDA

print("\n --> Please present a badge on the reader")

while True:
	(stat, tag_type) = rdr.request(rdr.REQIDL) # Detects the presence of a badge
	if stat == rdr.OK:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			# Display badge type and UID
			print("\nBadge detected !")
			print(" - type : %03d" % tag_type)
			print(" - uid : %03d.%03d.%03d.%03d" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
			# Display data in memory
			if rdr.select_tag(raw_uid) == rdr.OK:
				key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
				if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
					print(" - data : %s" % rdr.read(8))
					rdr.stop_crypto1()
				# Display in case of problems
				else:
					print("Reading error")
			else:
				print("Badge Error")

