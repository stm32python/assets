# Purpose of the script: writing on an RFID badge

import pyb
import time
import mfrc522

#Initialization of the badge reader
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10')		#SCK, MOSI, MISO, RST, SDA

print("\n--> Please present a badge on the reader")

while True:
	# Detects the presence of a badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)
	if stat == rdr.OK:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			print("\nBadge detected !")
			# Authentification
			if rdr.select_tag(raw_uid) == rdr.OK:
				key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
				# Writing
				if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
					print("--> Writing...")
					stat = rdr.write(8, b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f") #hexa values
					if stat == rdr.OK:
						print("--> Writing completed!")
					else:
						print("--> Impossible to write on the card!")
				# display
				print("Data in memory : %s" % rdr.read(8))
				# stop
				rdr.stop_crypto1()
		else:
			print("badge Error")
	time.sleep_ms(500)
