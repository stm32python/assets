# Purpose of the script: Implementation of the Nintendo Nunchuk adapter
# To start: get the data from the WiiMote controller.

from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck

i2c = I2C(1)

# Pause for 1s to give the I2C time to initialize
time.sleep_ms(1000)

wii = WiiChuck(i2c)

while True:
	direction = ''
	if wii.joy_up:
		direction = 'Haut'
	elif wii.joy_down:
		direction = 'Bas'
	elif wii.joy_right:
		direction = 'Droite'
	elif wii.joy_left:
		direction = 'Gauche'
	else:
		direction = '-----'
	if(wii.c):
		Cbouton = 'C'
	else:
		Cbouton = '-'
	if(wii.z):
		Zbouton = 'Z'
	else:
		Zbouton = '-'

	print("Joystick: (%3d, %3d) %6s \t| Accelerometre XYZ: (%3d, %3d, %3d) \t| Boutons: %s %s" %(wii.joy_x, wii.joy_y, direction, wii.accel_x, wii.accel_y, wii.accel_z, Cbouton, Zbouton))

	wii.update()
	sleep_ms(100)
