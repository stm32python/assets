# Objet du script : Mise en oeuvre de l'adaptateur Nintendo Nunchuk
# Pour aller plus loins : contrôler un servomoteur avec la manette WiiMote.

import pyb
from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck

#Initialization of servomotor
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)

#Nunchuk Initialisation
i2c = I2C(1)

# Pause for 1s to give the I2C time to initialize
sleep_ms(1000)

wii = WiiChuck(i2c)

while True:
	joystick_x = (wii.joy_x + 200)/26
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=joystick_x)
	wii.update()

#period = 1/f (usually between 10 and 20ms, here 20ms)
#pulse_width_percent = (time/period)*100
#time between 0,5 and 2,5ms
