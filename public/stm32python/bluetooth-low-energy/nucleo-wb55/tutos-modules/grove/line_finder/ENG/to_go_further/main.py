# Purpose of the script: Implement a line follower module
# using the interrupt of the digital pin to which it is
# connected.

from machine import Pin # Management of the line follower pin

# Global variable that will be modified by the interrupt service routine
line_detected = False

# Service routine of the interrupt
# This function gives the value "True" to the global variable "line_detected" if the state of
# pin changes.
@micropython.viper # Aggressively optimizes MicroPython pseudo-code
def handle_interrupt(pin):
  global line_detected
  line_detected = True
  global interrupt_pin
  interrupt_pin = pin

line_finder = Pin('D7', Pin.IN) # Line detector pin

# The interrupt is "attached" to the line detector pin
# This means that the interrupt handler contained in the STM32WB55 will "monitor
# the voltage on pin D7. If this voltage increases and goes from 0 to 3.3V (IRQ_RISING)
# then the interrupt manager will force the STM32WB55 to execute the "handle_interrupt" function.

line_finder.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
	if line_detected: # if line_detected = True then it means that the interrupt has taken place.
		print("Black line crossed !")
		line_detected = False
	# Put the microcontroller in sleep mode waiting for the next interrupt
	pyb.wfi()

