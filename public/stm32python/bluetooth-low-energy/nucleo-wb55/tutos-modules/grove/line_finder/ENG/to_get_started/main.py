# Purpose of the script: Implement a line follower module
# Source : https://github.com/DexterInd/GrovePi/blob/master/Software/Python/grove_line_finder.py

from time import sleep_ms # To time out
from machine import Pin # Management of the line follower pin

line_finder = Pin('D7', Pin.IN) # Line detector pin

while True:
	# Returns 1 when black line is detected, 1 if white is under the diodes
	if line_finder.value() == 1:
		print ("Black line detected !")
	else:
		print ("White surface detected")

	sleep_ms(100) # We delay one tenth of a second
