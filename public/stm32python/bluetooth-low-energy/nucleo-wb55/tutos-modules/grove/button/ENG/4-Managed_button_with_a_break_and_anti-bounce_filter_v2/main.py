# Purpose of the script: Turn on or off an LED with a button.
# The button is managed with an interrupt.
# A first press on the button turns on the LED, a second press turns it off.
# Hardware required in addition to the NUCLEO-WB55: a button connected to pin
# D4 pin and an LED connected to the D2 pin.
# Anti-bounce filter made with the interrupts of a timer
# Buffer allocated so that error messages from interrupt service routines
# are notified correctly (can be commented after testing the code)

import micropython
micropython.alloc_emergency_exception_buf(100)

from pyb import Pin, Timer #To manage GPIOs and timers

timer_running = 0 # Is the timer counting?
previous_state = -1 # Previous state of the button
current_state = 0 # Current status of the button 
led_state = 0 # Variable storing the LED state

# Timer 1 overflow interrupt service routine.
# It is executed every 10 seconds.
def timer_overflow_ISR(timer):
	# Global variables
	global led_state, current_state, previous_state, timer_running
	# Button status report
	current_state = bouton_in.value()
	# If the status has not changed since the previous timer interruption
	if current_state == previous_state:
		# Stop the timer
		timer.deinit()
		# Inverse the LED state (0->1 ou 1->0)
		led_state = not led_state
		led_out.value(led_state)
		# Update global variables
		timer_running = 0
		previous_state = 0
		current_state = -1
		# Re-activates the interruption of the button
		bouton_in.irq(trigger=bouton_in.IRQ_RISING, handler=button_falling_ISR)
	else:
		# Otherwise, memorizes the current state of the button
		previous_state = current_state


# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V when the button is not pressed.
bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE: the potential of D2 is not fixed.

led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # LED pin
led_out.value(led_state) # LED initially turned off

# Counts the number of times the button was interrupted
it_trigger_count = 0

# Button interrupt service routine
def button_falling_ISR(pin):
	# Starts timer 1 at 10 Hz.
	global it_trigger_count
	it_trigger_count +=1
	print("Button interrupt enabled ", it_trigger_count, " times")
	global timer_running # global Variables
	if not timer_running: # If no timer has been started
		# Start by disabling the interrupt button
		bouton_in.irq(trigger=bouton_in.IRQ_RISING, handler=None)
		# Starts a 10 Hz timer
		tim1 = pyb.Timer(1, freq=10)
		timer_running = 1
		# The "timer_overflow_ISR" routine/ISR will be executed every 1/10 seconds, at the time of
		# the timer overflow interrupt
		tim1.callback(timer_overflow_ISR)

# We "attach" the ISR to the button pin, it takes effect when the button is pressed (IRQ_FALLING)
bouton_in.irq(trigger=bouton_in.IRQ_FALLING, handler=button_falling_ISR)
