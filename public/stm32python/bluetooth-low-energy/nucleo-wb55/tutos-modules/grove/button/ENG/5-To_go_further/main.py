# Purpose of the script: Turn on or off a LED with a button by applying a 
# anti-bounce filter included in the bilblitohèque proposed here:
# https://gist.github.com/SpotlightKid/0a0aac56606286a80f860b116423c94f

from debounce import DebouncedSwitch

from pyb import Pin # manage GPIOs

# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V
# when the button is not pressed.

bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE: the potential of D2 is not fixed.
led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # LED pin

# Number of the timer used for the anti-bounce
TIMER_NUM = const(1) 

# Management of the button and the LED.
# The LED is controlled by a "callback" function contained in a
# "lambda expression" : lambda l: l.value(not l.value())
DebouncedSwitch(bouton_in, lambda l: l.value(not l.value()), led_out, delay = 50, tid = TIMER_NUM)