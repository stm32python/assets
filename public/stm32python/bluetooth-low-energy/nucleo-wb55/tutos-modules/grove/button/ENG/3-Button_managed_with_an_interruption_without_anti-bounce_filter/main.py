# Purpose of the script: Turn on or off an LED with a button.
# The button is managed with an interrupt.
# A first press on the button turns on the LED, a second press turns it off.
# Hardware required in addition to the NUCLEO-WB55: a button connected to pin
# D4 pin and an LED connected to the D2 pin.

from pyb import Pin # Class to manage GPIOs

# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V when the button is not pressed.

bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE: the potential of D2 is not fixed.

led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # LED pin
led_state = 0 # Global variable to store the state of the LED (on or off)
led_out.value(led_state) # LED initially off

# Global variable that counts calls to button_falling_ISR :
it_trigger_count = 0

# Function to manage the interruption of the button when it is pressed
def button_falling_ISR(pin):
	# Keyword "global" essential for SRI to effectively modify the variables concerned
	global it_trigger_count, led_state
	led_state = not led_state # reverses the state of the variable (0->1 or 1->0)
	led_out.value(led_state) # Reverse the status of the LED
	it_trigger_count +=1
	print("Button interrupt enabled ", it_trigger_count, " times")


# We "attach" the ISR to the button pin, it takes effect when the button is pressed (IRQ_FALLING)
bouton_in.irq(trigger=bouton_in.IRQ_FALLING, handler=button_falling_ISR)
