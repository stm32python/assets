# Purpose of the script: Turn on or off an LED by pressing a button.
# The button is managed by polling: an infinite loop
# monitors the state of the button.

from pyb import Pin # To manage GPIO

# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V
# when the button is not pressed.

bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE: the potential of D2 is not fixed.
led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # LED pin

# We start with the LED off
led_state = 0
led_out.value(led_state)

n = 0 # To count the number of state changes

# infinie loop
while True :
	# In our configuration button_in.value() is 1 when the button is pressed, 
	# which reverses the state of the LED

	if bouton_in.value() == 1:
		n += 1
		if led_state == 1:
			led_state = 0
		else:
			led_state = 1
		print("Switching ", n, " - State of LED : ", led_state)
	
	led_out.value(led_state)
