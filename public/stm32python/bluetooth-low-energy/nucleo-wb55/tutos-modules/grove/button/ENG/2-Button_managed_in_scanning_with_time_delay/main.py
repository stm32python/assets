# Purpose of the script: Turn off an LED by holding down a button.
# The button is managed in "polling" (scanning).
# Add a timer function depending on the source:
# https://docs.micropython.org/en/latest/pyboard/tutorial/debounce.html

from pyb import Pin # To manage GPIO
from time import sleep_ms

WAIT_MS = const(20)
WAIT_STEP_MS = const(1)

# Timer function
def attente_bouton_stable(pin):
	# Waits for the pin value to change
	# It must remain stable for at least WAIT_MS milliseconds
	global button_state
	button_state = pin.value()
	print("button state: ", button_state)
	temps_ecoule = 0
	while temps_ecoule < WAIT_MS:
		if pin.value() != button_state:
			temps_ecoule += WAIT_STEP_MS
		else:
			temps_ecoule = 0
		sleep_ms(WAIT_STEP_MS)

BUT_PIN = 'D4' #  button pin
LED_PIN = 'D2' #  LED pin

# The input button (IN) on pin D4 is configured as "PULL UP".

bouton_in = Pin(BUT_PIN, Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE: the potential of D2 is not fixed.
led_out = Pin(LED_PIN, Pin.OUT_PP, Pin.PULL_NONE)

# We start with the LED off
button_state = 0 # Variable containing the state of the button
led_state = 0 # Variable containing the status of the LED
led_out.value(led_state) # Turns off the LED

while True : # infinie loop

	# Waits for the state of the button pin to be stable
	attente_bouton_stable(bouton_in)

	# In our configuration button_state is 1 when the button is pressed, 
	#which reverses the state of the LED
	if button_state == 1:
		if led_state == 1:
			led_state = 0
		else:
			led_state = 1
		print(" LED state : ", led_state)
		
	led_out.value(led_state)
