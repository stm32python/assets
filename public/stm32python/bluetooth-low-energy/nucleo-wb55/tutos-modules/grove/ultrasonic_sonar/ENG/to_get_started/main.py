# Purpose of the script:
# Measuring a distance with Grove's Ultrasonic Sonar UART module

from us100 import US100 # Sonar driver
from time import sleep # To time

sonar = US100()

while True:
	print('Distance: %.1f cm ' % (sonar.distance_mm()/10), end="\r")
	sleep(0.1) # Waits 100 milliseconds