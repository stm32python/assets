from pyb import UART
from time import sleep_ms

class US100:

	def distance_mm(self):
		# Initialize communication with the sensor
		uart = UART(2)
		uart.init(9600, bits=8, parity=None, stop=1)
		sleep_ms(1)
		uart.write(b'\x55')
		
		t = 0
		buf = bytearray(2)
		
		# Wait for a character to be read on the serial link
		while not uart.any() and t < 1000:
			t = t + 1
			sleep_ms(5)
			
		#Read the character
		if t < 1000:
			uart.readinto(buf, 2)
			
		# Read and return the distance	
		dist = buf[0] * 256 + buf[1]
		if dist > 11000:
			dist = -1 # object too far or detection error
		return dist
