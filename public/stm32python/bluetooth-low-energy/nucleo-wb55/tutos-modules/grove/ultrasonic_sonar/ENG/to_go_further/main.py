# Purpose of the script: Implementing Grove's US100 ultrasonic sonar (UART)

from us100 import US100 # Sonar driver
from pyb import Pin, Timer # Classes to manage pins and timers
import time # Library for timing

sonar=US100()

BUZZER = Pin('A3') # Pin A3 (Grove shield and Arduino connector) wired to Pin PA0 (microcontroller)
tim = Timer(2, freq=440) # TIM2 channel 1 wired on A3
ch = tim.channel(1, Timer.PWM, pin=BUZZER) # PWM mode on A3

# Initialization of the interrupt flag
i = 0

# Initialization of the push button
sw=pyb.Switch()

# Push button interrupt function
def press():
	global i
	pyb.LED(1).toggle()
	if i==0:
		i = 1
	else:
		i = 0

sw.callback(press)

# Function that makes the buzzer sound
def buzz(time):
	ch.pulse_width_percent(20) #20% duty cycle
	time.sleep_ms(50)
	ch.pulse_width_percent(0)
	time.sleep_ms(time)
	

while True: #Infinite loop that turns on the radar if the button was pressed, turns it off if it is pressed again
	if i == 1:
		d = sonar.distance_mm()/10
		print(d)
		
		# No sound
		if d > 75:
			ch.pulse_width_percent(0)
			
		# Continuous sound
		elif d < 5 and d > 0:
			buzz(0)
			
		# Sound evolving according to the distance
		else:
			t = int(d*5)
			buzz(t)
	# turn off the buzzer
	ch.pulse_width_percent(0)
	time.sleep_ms(5)
