# Objet du script : Mise en oeuvre d'un sonar à ultrasons (UART) US100 de Grove

from us100 import US100 # Pilote du sonar
from pyb import Pin, Timer # Classes pour gérer les broches et les timers
import time # Bibliothèque pour temporiser

sonar=US100()

BUZZER = Pin('A3') # Pin A3 (shield Grove et connecteur Arduino) cablée sur Pin PA0 (microcontrôleur)
tim = Timer(2, freq=440) # TIM2 channel 1 cablé sur A3
ch = tim.channel(1, Timer.PWM, pin=BUZZER) #Mode PWM sur sur A3

# Initialisation du flag d'interruption
i = 0

# Initialisation du bouton poussoir
sw=pyb.Switch()

# Fonction d'interruption du bouton poussoir
def appuie():
	global i
	pyb.LED(1).toggle()
	if i==0:
		i = 1
	else:
		i = 0

sw.callback(appuie)

# Fonction qui fait sonner le buzzer
def buzz(temps):
	ch.pulse_width_percent(20) #Repport cyclique de 20%
	time.sleep_ms(50)
	ch.pulse_width_percent(0)
	time.sleep_ms(temps)
	

while True: # Boucle infinie qui enclenche le radar s'il y'a eu appui du bouton, l'éteint si appui à nouveau
	if i == 1:
		d = sonar.distance_mm()/10
		print(d)
		
		# Pas de son
		if d > 75:
			ch.pulse_width_percent(0)
			
		# Son continu
		elif d < 5 and d > 0:
			buzz(0)
			
		# Son évoluant en fonction de la distance
		else:
			t = int(d*5)
			buzz(t)
	# eteint le buzzer
	ch.pulse_width_percent(0)
	time.sleep_ms(5)
