# Purpose of the script: Calibration of the Grove MEMS-based gas sensor module MiCS-6814
# Run this script in a ventilated and unpolluted environment.
# This program performs two operations:
# 1 - It calibrates the ADCs of the three capture channels of the MiCS-6814 for the caclulation of the concentrations of the different gases
# 2 - It evaluates the maximum signal of the sensor concerning the different gases to deduce them by the following measurements (offsets)

from array import array # To use arrays
from machine import I2C # I2C bus driver
from time import sleep_ms # To delay
from mics6814 import MICS6814 # Driver of MICS6814

PRE_HEAT_ROUNDS = const(60) # Number of heating iterations
PRE_HEAT_TIME_TEMPO = const(60000) # Duration of a heating iteration (in milliseconds)

#Initialization of the I2C bus number 1 of the STM32WB55
i2c = I2C(1)

# Pause for one second  to initialize the I2C
sleep_ms(1000)

# Sensor Instantiation
sensor = MICS6814(i2c)

print("\nPreheating of the sensor before calibration : %d minutes" %((PRE_HEAT_ROUNDS * PRE_HEAT_TIME_TEMPO) // 60000))

# We start the heating resistors of the sensors
sensor.heater_on()

p = 0
for i in range(PRE_HEAT_ROUNDS):
	sleep_ms(PRE_HEAT_TIME_TEMPO)
	p += 1
	# Progress is displayed every five minutes
	if p == 5:
		print(" Advancement : %d minutes" %(i+1))
		p = 0

# 5-second delay
sleep_ms(5000)

print("\nCalibration started")
sensor.do_calibrate()
print("\nCalibration completed")

# The heating resistors of the sensors are switched off
sensor.heater_off()

# Display of the values stored in EEPROM
sensor.display_eeprom()

# Ambient measurements / offsets for measured concentrations
sensor.flush_raw()
