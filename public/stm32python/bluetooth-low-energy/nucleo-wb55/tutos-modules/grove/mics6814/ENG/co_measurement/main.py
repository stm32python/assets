# Purpose of the script:
# Measuring carbon monoxide concentrations using the Grove module based on the MiCS-6814 MEMS sensor.
# Data sheet: https://www.sgxsensortech.com/content/uploads/2015/02/1143_Datasheet-MiCS-6814-rev-8.pdf

from machine import I2C # I2C bus driver
from time import sleep # To delay

#Initialization of the I2C bus number 1 of the STM32WB55
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep(1)

from mics6814 import MICS6814

sensor = MICS6814(i2c)

# Detection thresholds for carbon monoxide in ppm
CO_LT = 1 # low threshold, according to the MICS6814 data sheet
CO_HT = 50 # high threshold, health risk threshold

# Offset (ambient) provided by the calibration script
Offset_CO = 4.0

# The heating resistors of the sensor are started
sensor.heater_on()
nb_detect = 0

# A series of "blank" measurements are made to preheat the sensor

PREHEAT_ROUNDS = const(20)

print("Preheating during %d minutes" %(PREHEAT_ROUNDS * 10 // 60) )
for i in range(PREHEAT_ROUNDS):
	co = sensor.get_co()
	sleep(10)
print("Preheating completed\n")

while True: # loop ...

	# For the moment, no measurement made, therefore potentially no CO detected
	co_detect = False

	# Estimates the CO concentration, deducting the ambient offset
	co = sensor.get_co() - Offset_CO
	
	# If the result is positive, check that it is above the low detection threshold of the sensor
	if co > CO_LT:
		co_detect = True
		nb_detect += 1
		if co < CO_HT:
			print("%1d - Concentration CO : %.1f ppm" %(nb_detect, co))
		else:
			# If the alert threshold is exceeded, report it
			print("%1d - Alerte CO : %.1f > %.1f ppm : " %(nb_detect, co, CO_HT))
	elif co < 0:
			# If the measured concentration is negative, it indicates a sensor error
			print("%1d - Carbon monoxide (CO): Measurement error!")
			# Exit the loop
			break

	if co_detect:
		print('')
		# If CO has been detected, lights the LED on the module for 10 seconds
		sensor.led_on()
		sleep(10)
		sensor.led_off()

