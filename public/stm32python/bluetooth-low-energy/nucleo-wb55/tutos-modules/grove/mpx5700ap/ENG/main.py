# Reading and digitizing the signal of an integrated Grove pressure sensor

from pyb import Pin, ADC # Analog pin and ADC management
import time # Management of time and temporizations

# Reference pressure (atmospheric) : 956 hPa
pref_hpa = const(956)

# Sum of ten analog values for 956 hPa
pref_analog = const(6883)

ratio = pref_hpa / pref_analog

# Instance of the analog-to-digital converter
adc = ADC(Pin('A0'))

while True:
	
	valeur = 0 # Analog value
	counter = 0 

	# Ten analog-to-digital conversions of the pressure are performed
	while counter < 10:
		value = value + adc.read()
		counter = counter + 1

	# Conversion of the analog value to pressure
	pressure = value * ratio

	print("Analog value = %d" %value)

	print("Pressure = %d hPa" %pressure)

	# Pause for 1s
	time.sleep(1)
