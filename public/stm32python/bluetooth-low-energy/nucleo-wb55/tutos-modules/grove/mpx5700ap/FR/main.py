# Lecture et numérisation du signal d'un capteur Grove de pression intégré

from pyb import Pin, ADC # Gestion de la broche analogique et de l'ADC
import time # Gestion du temps et des temporisations

# Pression de référence (atmosphérique) : 956 hPa
pref_hpa = const(956)

# Somme de dix valeurs analogiques pour 956 hPa
pref_analog = const(6883)

ratio = pref_hpa / pref_analog

# Instance du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

while True:
	
	valeur = 0 # Valeur analogique
	compteur = 0 

	# On effectue dix conversions analogiques-numériques de la pression
	while compteur < 10:
		valeur = valeur + adc.read()
		compteur = compteur + 1

	# Conversion de la valeur analogique en pression
	pression = valeur * ratio

	print("Valeur analogique = %d" %valeur)

	print("Pression = %d hPa" %pression)

	# Pause pendant 1 s
	time.sleep(1)
