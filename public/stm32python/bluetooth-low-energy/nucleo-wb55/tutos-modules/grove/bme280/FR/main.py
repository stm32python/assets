# Exemple adapté de https://github.com/catdog2/mpy_bme280_esp8266
# Objet du script : Mise en oeuvre du module grove I2C capteur de pression,
# température et humidité basé sur le capteur BME280

from time import sleep_ms # Pour temporiser
from machine import # I2C Pilote du contrôleur de bus I2C
import bme280 # Pilote du capteur

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
bme = bme280.BME280(i2c=i2c)

while True:

	# Temporisation d'une seconde
	sleep_ms(1000)
	
	# Lecture des valeurs mesurées
	bme280data = bme.values
	
	# Séparation et formattage (arrondis) des mesures
	temp = round(bme280data[0],1)
	press = int(bme280data[1])
	humi = int(bme280data[2])

	# Affichage des mesures
	print('=' * 40)  # Imprime une ligne de séparation
	print("Température : " + str(temp) + " °C")
	print("Pression : " + str(press) + " hPa")
	print("Humidité relative : " + str(humi) + " %")