# Example adapted from https://github.com/catdog2/mpy_bme280_esp8266
# Purpose of the script: Implementation of grove I2C pressure sensor module,
# temperature and humidity based on the BME280 sensor

from time import sleep_ms # To time
from machine import # I2C I2C bus controller driver
import bme280 # Sensor driver

# We use the I2C n°1 of the NUCLEO-WB55 board to communicate with the sensor
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# List of I2C addresses of the devices present
print("I2C addresses used : " + str(i2c.scan()))

# Instantiation of the sensor
bme = bme280.BME280(i2c=i2c)

while True:

	# One second delay
	sleep_ms(1000)
	
	# Read the measured values
	bme280data = bme.values
	
	# Separation and formatting (rounding) of measurements
	temp = round(bme280data[0],1)
	press = int(bme280data[1])
	humi = int(bme280data[2])

	# Display of measurements
	print('=' * 40) # Print a separation line
	print("Temperature : " + str(temp) + " °C")
	print("Pressure : " + str(press) + " hPa")
	print("Relative humidity : " + str(humi) + " %")