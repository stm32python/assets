# Purpose of the script: to measure and display a temperature using a thermistor with MicroPython.
# Hardware required in addition to the NUCLEO-WB55: a thermistor and a resistor.

from pyb import ADC, Pin # Management of the analog-to-digital converter and the input/output pins
from time import sleep, time # To measure the elapsed time and time out
from math import log # To calculate logarithms

# Reference voltage / range of the ADC : +3.3V
varef = 3.3

# Resolution of the ADC 12 bits = 2^12 = 4096 (min = 0, max = 4095)
RESOLUTION = const(4096)
echADC = RESOLUTION - 1

# Quantum of the ADC
quantum = varef / echADC

# Characteristics of the thermistor used
R = 10 # Resistance at 25°C : 10 kOhms
B = 3950.0 # Thermal index
T1 = 25 # Absolute temperature in K

# Pin A1 in analog input
adc_A1 = ADC(Pin('A1'))

oldtempC = 0.0
temps_s = time()

while True:
	
	# Read the A1 pin
	value = adc_A1.read()
	
	# Calculation of the voltage
	voltage = value * quantum
	
	# Calculation of the resistance value of the thermistor
	Rt = R * voltage / (Vref - voltage)
	
	# Calculation of temperatures in Kelvins and degrees Celsius
	tempK = 1/(1/(273.15 + T1) + log(Rt/R)/B)
	tempC = tempK - 273.15

	# Display of the result (if value exceeded to the nearest 0.2)
	if tempC < (oldtempC-0.2) or tempC > (oldtempC+0.2):
		oldtempC = tempC
		print("-"*21, "Time:", time() - time_s , "s", "-"*21)
		print("Temperature : %.2f°C\tTemperature : %.2f°K" %(tempC, tempK))
		
	# Pause time
	sleep(1)

