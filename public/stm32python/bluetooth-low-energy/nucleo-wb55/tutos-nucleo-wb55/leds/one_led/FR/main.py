# Objet du script :
# Exemple faisant clignoter la LED bleue de la NUCLEO-WB55 à une fréquence donnée.

import pyb # pour les accès aux périphériques (GPIO, LED, etc.)
from time import sleep # pour faire des pauses système (entre autres)

# Initialisation de la LED bleue
led_bleue = pyb.LED(3) # sérigraphiée LED1 sur le PCB

delai = 0.5 # Temps d'attente avant de changer l'état de la LED

# La boucle va se répéter dix fois (pour i de 0 à 9)
for i in range(10):

	# Affiche l'index de l'itération sur le port série de l'USB User
	# Voir https://www.geeksforgeeks.org/python-output-formatting/ pour l'explication de la ligne qui suit.

	# print("Itération {:2d}".format(i))

	# Il existe plusieurs façons d'afficher une valeur avec print, ci-dessous une alternative plus lisible

	print("Itération %d : "%i)

	led_bleue.on() # Allume la LED
	print("LED bleue allumée")
	sleep(delai) # Attends delai secondes

	led_bleue.off() # Eteint la LED
	print("LED bleue éteinte")
	sleep(delai) # Attends delai secondes