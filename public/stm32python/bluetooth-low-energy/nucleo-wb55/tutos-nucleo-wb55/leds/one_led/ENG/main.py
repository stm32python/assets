# Purpose of the script:
# Flashing the blue LED of the NUCLEO-WB55 at a given frequency.

import pyb # for access to peripherals (GPIO, LED, etc.)
from time import sleep # to make system pauses (among others)

# Initialization of the blue LED
led_blue = pyb.LED(3) # screen printed LED1 on the PCB

delai = 0.5 # time to wait before changing the state of the LED

# The loop will repeat ten times (for i from 0 to 9)
for i in range(10):

	# Displays the index of the iteration on the serial port of the USB User
	# See https://www.geeksforgeeks.org/python-output-formatting/ for an explanation of the following line.

	# print("Iteration {:2d}".format(i))

	# There are several ways to display a value with print, below is a more readable alternative

	print("Iteration %d: "%i)

	led_blue.on() # Turns on the LED
	print("Blue LED on")
	sleep(delay) # Wait for delay seconds

	led_blue.off() # Turns off the LED
	print("Blue LED off")
	sleep(delay) # Wait for delay seconds
