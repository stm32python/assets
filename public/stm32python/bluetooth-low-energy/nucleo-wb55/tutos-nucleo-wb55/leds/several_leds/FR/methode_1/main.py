# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette première approche utilise la méthode "time.ticks_ms()" qui mesure le temps écoulé 
# depuis de démarrage du script. 
# Ce script permet de comprendre le principe de base du multitâche préemptif.
# Trois tâches sont définies (ici faire clignoter différentes LED) et le microcontrôleur 
# alterne très rapidement de l'une à l'autre ce qui donne l'illusion qu'elles s'éxécutent de 
# façon simultanée.

from time import ticks_ms, ticks_diff # Bibliothèque pour gérer les temporisations

# Variable pour suivre le temps écoulé depuis le lancement du script
n = 0

# Variables globales pour les dates de clignotement des LED
blink1 = 0
blink2 = 0
blink3 = 0

while True: # Boucle infinie

	# On teste le temps écoulé pour chaque tâche.
	# Remarquez que les tests sur les durées ne portent pas sur une égalité (stricte) mais sur un dépassement.
	# Par exemple on écrit "if ticks_diff(now, blink1) > 99" et pas "if ticks_diff(now, blink1) == 99".
	# En effet, il est peu probable que la boucle infinie réalise son test exactement à l'instant qui est prévu.
	# Il y aura donc une certaine incertitude (faible) sur les fréquences de clignotement des LED.
	
	# Nombre de millisecondes écoulées depuis le lancement du script
	n = ticks_ms() 
	
	# Tâche 1 : clignotement LED rouge
	if ticks_diff(n, blink1) > 1999: # Toutes les 2000 ms depuis la dernière inversion...
		pyb.LED(1).toggle() # On inverse la LED...
		blink1 = n # Et on mémorise la date de cet évènement.
	
	# Tâche 2 : clignotement LED verte
	if ticks_diff(n, blink2) > 499: # Toutes les 500 ms depuis la dernière inversion...
		pyb.LED(2).toggle() # On inverse la LED...
		blink2 = n # Et on mémorise la date de cet évènement.
	
	# Tâche 3 : clignotement LED bleue
	if ticks_diff(n, blink3) > 99: # Toutes les 100 ms depuis la dernière inversion...
		pyb.LED(3).toggle() # on inverse la LED...
		blink3 = n # Et on mémorise la date de cet évènement.

