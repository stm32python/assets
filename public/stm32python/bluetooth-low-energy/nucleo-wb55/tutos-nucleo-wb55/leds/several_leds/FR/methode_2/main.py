# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette deuxième approche utilise un timer du STM32WB55 et exploite son interruption de dépassement de compteur 
# pour incrémenter une variable globale qui décompte le temps.
# Par comparaison avec la première solution, cette version n'est pas tellement plus compliquée et présente un 
# avantage : ce n'est plus le Cortex M4 qui compte le temps écoulé dans la boucle principale, mais l'un des 
# timers du STM32WB55.Le Cortex M4 sera donc soulagé de cette tâche et on peut imaginer que le timing des 
# diodes sera plus précis.
# Nous utilisons la fonction pyb.wfi() pour placer le microcontrôleur en mode économie d'énergie en mode 
# économie d'énergie jusqu'à la prochaine interruption (qui va le "réveiller"), ou pendant une milliseconde.

# Variable globale qui servira de rééfrence de temps
n = 0

# Variables globales pour les dates de clignotement des LED
blink1 = 0
blink2 = 0
blink3 = 0

# Routine de service de l'interruption (ISR) de dépassement de compteur du timer 1.
# Elle incrémente la variable n de 1 tous les 100-ièmes de seconde.
def tick(timer):
	global n # TRES IMPORTANT, ne pas oublier le mot-clef "global" devant la variable n
	n += 1 

# Démarre le timer 1 à la fréquence de 100 Hz.
# Assigne la fonction "tick" à l'interruption de dépassement de compteur du timer 1.
# Elle sera appelée 100 fois par seconde.
tim1 = pyb.Timer(1, freq=100, callback=tick) 

while True: # Boucle infinie

	# On teste le temps ; chaque unité de n indiquant qu'un centième de seconde s'est écoulé.
	# Remarquez que les tests sur les durées ne portent pas sur une égalité (stricte) mais sur un dépassement.
	# Par exemple on écrit "n - blink1 > 9" et pas "n - blink1 == 10".
	# En effet, le timer incrémentant n de façon idépendante, il est probable que la boucle infinie "rate" la valeur 
	# n = 10 l'essentiel de ses itérations.
	
	pyb.wfi() # Place le microcontrôleur en mode économie d'énergie
	
	if n - blink1 > 9: # Toutes les 0.1s depuis sa dernière inversion...
		pyb.LED(3).toggle() # On inverse la LED bleue...
		blink1 = n # Et on mémorise la date de cet évènement.
	if n - blink2 > 49: # Toutes les 0.5s depuis sa dernière inversion...
		pyb.LED(2).toggle() # On inverse la LED verte...
		blink2 = n # Et on mémorise la date de cet évènement.
	if n - blink3 > 199: # Toutes les deux secondes depuis sa dernière inversion...
		pyb.LED(1).toggle() # On inverse la LED rouge...
		blink3 = n # Et on mémorise la date de cet évènement.
		
		# On remet les compteurs de temps à zéro lorsque la LED avec la plus longue période a changé d'état.
		# Ceci afin d'éviter un dépassement de capacité de la variable n si le script "tourne" trop longtemps.
 		n = 0
		blink1 = 0
		blink2 = 0
		blink3 = 0
