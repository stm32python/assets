# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette variante de la troisième approche utilise trois timers intégrés au STM32WB55, les paramétrise 
# aux fréquences désirées et exploite leurs interruptions de dépassement de compteur pour inverser les LED.

# Le code ci-dessous n'utilise pas de lamda-expressions dans les callbacks des timers.
# On écrit explicitement les routines de service des trois interruptions (ISR) de dépassement des timers.

from pyb import Timer # Bibliothèque pour gérer les timers

@micropython.native # Demande à MicroPython de générer un code binaire optimisé pour cette fonction
def blink_LED_rouge(timer): # ISR du timer 1
	pyb.LED(1).toggle()

tim1 = Timer(1, freq= 0.5) # Fréquence du timer 1 fixée à 0.5 Hz
# Appelle l'ISR de l'interruption de dépassement du timer 1 : la fonction blink_LED_rouge
tim1.callback(blink_LED_rouge)

@micropython.native
def blink_LED_verte(timer): # ISR du timer 2
	pyb.LED(2).toggle()

tim2 = Timer(2, freq= 2) # Fréquence du timer 2 fixée à 2 Hz
# Appelle l'ISR de l'interruption de dépassement du timer 2 : la fonction blink_LED_verte
tim2.callback(blink_LED_verte)

@micropython.native
def blink_LED_bleue(timer): # ISR du timer 16
	pyb.LED(3).toggle()

tim16 = Timer(16, freq=10) # Fréquence du timer 16 fixée à 10 Hz
# Appelle l'ISR de l'interruption de dépassement du timer 16 : la fonction blink_LED_bleue
tim16.callback(blink_LED_bleue)

@micropython.native
def main():
	while True:
		pyb.wfi() # Place le micorocontrôleur en mode économie d'énergie

# Boucle principale	
main()