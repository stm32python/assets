# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette troisième approche utilise trois timers du STM32WB55, les paramétrise aux fréquences désirées
# et exploite leurs interruptions de dépassement de compteur pour inverser les LED.

from pyb import Timer # Bibliothèque pour gérer les timers

@micropython.native # Demande à MicroPython de générer du code optimisé pour cette fonction
def blink_LED_rouge(timer): # ISR du timer 1
    pyb.LED(1).toggle()

# Demarrer le timer 1, attacher ISR à la routine de service de l'interruption de dépassement du timer
tim1 = Timer(1, freq= 0.5, callback = blink_LED_rouge) # Timer 1 frequency set to 0.5 Hz

@micropython.native
def blink_LED_verte(timer): # ISR du timer 2
    pyb.LED(2).toggle()

# Demarrer le timer 2, attacher ISR à la routine de service de l'interruption de dépassement du timer
tim2 = Timer(2, freq= 2, callback = blink_LED_verte) # Timer 2 frequency set to 2 Hz

@micropython.native
def blink_LED_bleue(timer): # ISR du timer 16
	pyb.LED(3).toggle()

# Demarrer le timer 16, attacher ISR à la routine de service de l'interruption de dépassement du timer
tim16 = Timer(16, freq=10, callback = blink_LED_bleue) # Timer 16 frequency set to 10 Hz

@micropython.native
def main():
    while True:
        pyb.wfi() # Place le microcontroller en sauvegarde d'énergie

# fonction principale	
main()
