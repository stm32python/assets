# Objet du script : 
# Faire clignoter simultanément les trois LED de la NUCLEO-WB55 à des fréquences différentes.
# On utilise la bibliothèque "uasyncio" pour la programmation asynchrone.
# Code directement adapté de https://docs.micropython.org/en/latest/library/uasyncio.html
# Tutoriel sur uasyncio : 
#  https://github.com/peterhinch/micropython-async/blob/master/v3/docs/TUTORIAL.md

import uasyncio # On importe la bibliothèque pour l'exécution asynchrone
print("Version de uasyncio : ", uasyncio.__version__) # Version de uasyncio

# Coroutine asynchrone pour faire clignoter la LED sur la broche led
@micropython.native # Décorateur pour générer du code assembleur (plus performant)
async def blink(led, period_ms):
	while True:
		# Allume la LED
		led.on() 
		# Temporisation non blocante de period_ms millisecondes
		await uasyncio.sleep_ms(period_ms)
		# Eteint la LED
		led.off()
		# Temporisation non blocante de period_ms millisecondes
		await uasyncio.sleep_ms(period_ms)

# Crée trois taches concurrentes "blink", une par LED
@micropython.native # Décorateur pour générer du code assembleur (plus performant)
async def main(led_rouge, led_verte, led_bleue):

	task = uasyncio.create_task(blink(led_rouge, 2000)) # Tache pour la LED rouge
	uasyncio.create_task(blink(led_verte, 500)) # Tache pour la LED verte
	uasyncio.create_task(blink(led_bleue, 100)) # Tache pour la LED bleue

	# Prolonge l'exécution du programme jusqu'à ce que task ait terminé 
	# (ce qui n'arriverta jamais)
	await task

# Le code en syntaxe pyboard (valable avec la NUCLEO-WB55)
from pyb import LED
# Appel au planificateur qui lance l'exécution de la fonction main avec ses trois 
# tâches concurrentes. Aucune instruction du script située au-dessous de cette 
# ligne ne sera exécutée.
uasyncio.run(main(LED(1), LED(2), LED(3)))

# Le code en syntaxe générique (pour des LED externes par exemple, connectées
# aux broches D2, D3, D4).
# from machine import Pin
# uasyncio.run(main(Pin('D2'), Pin('D3'), Pin('D4')))