# Purpose of the script:
# Make the three LEDs of the NUCLEO-WB55 flash simultaneously at different frequencies.
# We use the "uasyncio" library for asynchronous programming.
# Code directly adapted from https://docs.micropython.org/en/latest/library/uasyncio.html
# Tutorial on uasyncio :
# https://github.com/peterhinch/micropython-async/blob/master/v3/docs/TUTORIAL.md

import uasyncio # We import the library for asynchronous execution
print("Version of uasyncio: ", uasyncio.__version__) # Version of uasyncio

# Asynchronous coroutine to flash the LED on the led pin
@micropython.native # Decorator to generate assembly code (more efficient)
async def blink(led, period_ms):
	while True:
		# turn on the LED
		led.on()
		# Non-blocking delay of period_ms milliseconds
		await uasyncio.sleep_ms(period_ms)
		# Turns off the LED
		led.off()
		# Non-blocking timeout of period_ms milliseconds
		await uasyncio.sleep_ms(period_ms)

# Creates three concurrent blink spots, one per LED
@micropython.native # Decorator to generate assembly code (more efficient)
async def main(LED_red, LED_green, LED_blue):

	task = uasyncio.create_task(blink(LED_red, 2000)) # Task for the red LED
	uasyncio.create_task(blink(LED_green, 500)) # Task for the green LED
	uasyncio.create_task(blink(LED_blue, 100)) # Stain for the blue LED

	# Extends the execution of the program until task has finished
	# (which will never happen)
	await task

# The code in pyboard syntax (valid with the NUCLEO-WB55)
from pyb import LED
# Call to the scheduler that starts the execution of the main function with its three
# concurrent tasks. No script instruction below this line will be executed.
# line will not be executed.
uasyncio.run(main(LED(1), LED(2), LED(3)))

# The code in generic syntax (for external LEDs for example, connected to
# to pins D2, D3, D4).
# from machine import Pin
# uasyncio.run(main(Pin('D2'), Pin('D3'), Pin('D4'))
