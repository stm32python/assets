# Purpose of the script: To make the three LEDs on the NUCLEO-WB55 board flash simultaneously.
# This variant of the third approach uses three timers integrated in the STM32WB55, sets them
# at the desired frequencies and exploits their overflow interrupts to invert the LEDs.

from pyb import Timer # Library to manage timers

@micropython.native # Ask MicroPython to generate an optimized binary code for this function
def blink_LED_red(timer): # ISR of timer 1
    pyb.LED(1).toggle()

# Start timer 1, attach ISR to its overflow event
tim1 = Timer(1, freq= 0.5, callback = blink_LED_red) # Timer 1 frequency set to 0.5 Hz

@micropython.native
def blink_LED_green(timer): # ISR of timer 2
    pyb.LED(2).toggle()

# Start timer 2, attach ISR to its overflow event
tim2 = Timer(2, freq= 2, callback = blink_LED_green) # Timer 2 frequency set to 2 Hz

@micropython.native
def blink_LED_blue(timer): # ISR of timer 16
	pyb.LED(3).toggle()

# Start timer 16, attach ISR to its overflow event
tim16 = Timer(16, freq=10, callback = blink_LED_blue) # Timer 16 frequency set to 10 Hz

@micropython.native
def main():
    while True:
        pyb.wfi() # Places the microcontroller in power saving mode

# Main loop	
main()
