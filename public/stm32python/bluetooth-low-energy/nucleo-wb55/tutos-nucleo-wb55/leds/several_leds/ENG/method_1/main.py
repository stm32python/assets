# Purpose of the script: To make the three LEDs on the NUCLEO-WB55 board blink simultaneously.
# This first approach uses the method "time.ticks_ms()" which measures the time elapsed
# since the script was started.
# This script allows to understand the basic principle of preemptive multitasking.
# Three independant tasks are defined (here blinking different LEDs) and the microcontroller
# alternates very quickly from one to the other which gives the illusion that they are executed simultaneously.

from time import ticks_ms, ticks_diff # Library to manage timeouts

# Variable to track the time elapsed since the script was launched
n = 0

# Global variables for LED blink dates
blink1 = 0
blink2 = 0
blink3 = 0

while True: # Infinite loop

	# We test the elapsed time for each task.
	# Note that the tests on the durations are not on a (strict) equality but on an overflow.
	# For example we write "if ticks_diff(now, blink1) > 99" and not "if ticks_diff(now, blink1) == 99".
	# Indeed, it is unlikely that the infinite loop will perform its test exactly at the time that is expected.
	# There will therefore be some (small) uncertainty about the blink frequencies of the LEDs.
	
	# Number of milliseconds that have passed since the script was launched
	n = ticks_ms()
	
	# Task 1 : red LED blinking
	if ticks_diff(n, blink1) > 1999: # Every 2000 ms since the last inversion...
		pyb.LED(1).toggle() # We invert the LED...
		blink1 = n # And we store the date of this event.
	
	# Task 2 : green LED blinking
	if ticks_diff(n, blink2) > 499: # Every 500 ms since the last inversion...
		pyb.LED(2).toggle() # We invert the LED...
		blink2 = n # And we store the date of this event.
	
	# Task 3 : blue LED blinking
	if ticks_diff(n, blink3) > 99: # Every 100 ms since the last inversion...
		pyb.LED(3).toggle() # We invert the LED...
		blink3 = n # And we store the date of this event.

