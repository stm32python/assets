# Purpose of the script: To make the three LEDs on the NUCLEO-WB55 board flash simultaneously.
# This variant of the third approach uses three timers integrated in the STM32WB55, sets them
# at the desired frequencies and exploits their overflow interrupts to invert the LEDs.

# The code below does not use lamda-expressions in the timer callbacks.
# We explicitly write the service routines for the three timer overflow interrupts (ISR).

from pyb import Timer # Library to manage timers

@micropython.native # Ask MicroPython to generate an optimized binary code for this function
def blink_LED_red(timer): # ISR of timer 1
	pyb.LED(1).toggle()

tim1 = Timer(1, freq= 0.5) # Timer 1 frequency set to 0.5 Hz
# Call the ISR of the timer 1 overflow interrupt : the blink_LED_red function
tim1.callback(blink_LED_red)

@micropython.native
def blink_LED_green(timer): # ISR of timer 2
	pyb.LED(2).toggle()

tim2 = Timer(2, freq= 2) # Timer 2 frequency set to 2 Hz
# Call the ISR of the timer 2 overflow interrupt : the blink_LED_green function
tim2.callback(blink_LED_green)

@micropython.native
def blink_LED_blue(timer): # ISR of timer 16
	pyb.LED(3).toggle()

tim16 = Timer(16, freq=10) # Timer 16 frequency set to 10 Hz
# Call the ISR of the timer 16 overflow interrupt : the blink_LED_blue function
tim16.callback(blink_LED_blue)

@micropython.native
def main():
	while True:
		pyb.wfi() # Places the micorocontroller in power saving mode

# Main loop	
main()
