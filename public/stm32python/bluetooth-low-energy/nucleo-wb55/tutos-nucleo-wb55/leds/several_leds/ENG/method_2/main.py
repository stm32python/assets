# Purpose of the script: To flash all three LEDs on the NUCLEO-WB55 board simultaneously.
# This second approach uses a timer of the STM32WB55 and exploits its overflow interrupt
# to increment a global variable that counts down the time.
# Compared to the first solution, this version is not much more complicated and has an
# advantage: it is no longer the Cortex M4 that counts the elapsed time in the main loop, but one of the
# Cortex M4 will be relieved of this task and we can imagine that the timing of the
# diodes will be more precise.
# We use the pyb.wfi() function to put the microcontroller in power saving mode
# power saving mode until the next interrupt (which will "wake it up"), or for one millisecond.

# Global variable which will be used as a time reference
n = 0

# Global variables for LED blink dates
blink1 = 0
blink2 = 0
blink3 = 0

# Interrupt service routine (ISR) for timer 1 overflow.
# It increments the n variable by 1 every 100th of a second.
def tick(timer):
	global n # VERY IMPORTANT, do not forget the keyword "global" in front of the variable n
	n += 1

# Start the timer 1 at the frequency of 100 Hz.
# Assign the "tick" function to the timer 1 overflow interrupt.
# It will be called 100 times per second.
tim1 = pyb.Timer(1, freq=100, callback=tick)

while True: # Infinite loop

	# We test the time; each unit of n indicates that one hundredth of a second has elapsed.
	# Note that the tests on the durations do not target a (strict) equality but an overflow.
	# For example we write "n - blink1 > 9" and not "n - blink1 == 10".
	# Indeed, as the timer increments n in an independent way, it is probable that the infinite loop "misses" the value
	# n = 10 for most of its iterations.
	
	pyb.wfi() # Put the microcontroller in power saving mode
	
	if n - blink1 > 9: # Every 0.1s since its last inversion...
		pyb.LED(3).toggle() # We invert the blue LED...
		blink1 = n # And we store the date of this event.
	if n - blink2 > 49: # Every 0.5s since its last inversion...
		pyb.LED(2).toggle() # Invert the green LED...
		blink2 = n # And we store the date of this event.
	if n - blink3 > 199: # Every two seconds since its last inversion...
		pyb.LED(1).toggle() # Invert the red LED...
		blink3 = n # And we memorize the date of this event.
		
		# We reset the time counters to zero when the LED with the longest period has changed state.
		# This is to avoid overflowing the variable n if the script runs too long.
		n = 0
		blink1 = 0
		blink2 = 0
		blink3 = 0
