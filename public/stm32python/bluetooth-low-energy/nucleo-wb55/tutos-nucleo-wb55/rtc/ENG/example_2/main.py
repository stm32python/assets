# Purpose of the script: implementation of the real time clock (RTC)
# of the STM32WB55.

from time import sleep # Timer library

# we declare the RTC (Real Time Clock)
rtc = pyb.RTC()

# year month day day_of_the_week hour minute second subsecond(internal counter)
# year m d wd h m s sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0 1 2 3 4 5 6 7

# initialize the date
rtc.datetime(date)

while True :
	# retrieve the updated date
	date = rtc.datetime()
	# and display it
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	# we update every second
	sleep(1)
