# Purpose of the script: Implementation of the Real Time Clock (RTC)
# of the STM32WB55.
# Creation of an LED clock with a TM1638 8x7-segment display.

import tm1638 # Driver of the display
from machine import Pin # To manage the GPIO
from time import sleep_ms # To manage the timers

# We declare the display board
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# we declare the RTC (Real Time Clock)
rtc = pyb.RTC()

# we reduce the brightness
tm.brightness(0)

# year month day day_of_the_week hour minute second subsecond(internal counter)
# year m d wd h m s sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0 1 2 3 4 5 6 7

# We initialize the date
rtc.datetime(date)

while True :
	# Retrieve the updated date ...
	date = list(rtc.datetime()) # rtc.datetime() returns a non-modifiable object, we change it into a list to modify it
	
	# and we display it by making the seconds point blink
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))
	
	# We get the information on the buttons to set the time
	buttons = tm.keys()
	
	# we compare bit by bit to identify the button
	if (buttons & 1) :
		date[4] += 1
		if (date[4] > 23):
			date[4] = 0
		rtc.datetime(date)
	if (buttons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (buttons & 1<<2) :
		date[6] += 1
		if (date[6] > 59):
			date[6] = 0
		rtc.datetime(date)
	
	# We update every 100 miliseconds (for a more sensitive reading of the buttons, can be modified)
	sleep_ms(100)
