# Objet du script : mise en oeuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.

from time import sleep # Bibliothèque de temporisation

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# on initialise la date
rtc.datetime(date)

while True :
	# on récupère la date mise a jour
	date = rtc.datetime()
	# et on l'affiche
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	#  on actualise toute les secondes
	sleep(1)
