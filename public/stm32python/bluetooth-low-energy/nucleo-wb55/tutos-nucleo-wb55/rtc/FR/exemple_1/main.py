# Objet du script : Mise en oeuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.
# Création d'une horloge LED avec un afficheur 8x7-segments TM1638.

import tm1638 # Pilote de l'afficheur
from machine import Pin # Pour gérer les GPIO
from time import sleep_ms # Pourt les temporisations

# On declare la carte de l'afficheur
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# on declare la RTC (Real Time Clock)
rtc = pyb.RTC()

# On réduit la luminosité
tm.brightness(0)

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#	   year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0  1  2  3  4  5  6  7

# On initialise la date
rtc.datetime(date)

while True :
	# On récupère la date mise a jour ...
	date = list(rtc.datetime()) # rtc.datetime() renvoie un objet non modifiable, on le change en liste pour le modifier
	
	# et on l'affiche en faisant clignoter le point des secondes
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))
	
	# On récupère l'information sur les boutons pour régler l'heure
	boutons = tm.keys()
	
	# on compare bit à bit pour identifier le bouton
	if (boutons & 1) :  
		date[4] += 1
		if (date[4] > 23) :
			date[4] = 0
		rtc.datetime(date)
	if (boutons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (boutons & 1<<2) :
		date[6] += 1
		if (date[6] > 59) :
			date[6] = 0
		rtc.datetime(date)
	
	# On actualise toute les 100 milisecondes (pour un lecture des boutons plus sensible, peut être modifié)
	sleep_ms(100)
