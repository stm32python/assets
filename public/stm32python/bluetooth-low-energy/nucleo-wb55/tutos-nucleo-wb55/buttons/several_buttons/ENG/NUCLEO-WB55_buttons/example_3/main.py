# Purpose of the script :
# Example of GPIO configuration to manage the buttons of the NUCLEO-WB55
# Inverting the logic states using the Signal class.

from machine import Pin, Signal # Pin control
from time import sleep_ms # To make system pauses

print( "GPIOs with MicroPython are easy" )

# Initialize the input pins for the buttons (SW1, SW2, SW3)
# The potential of the pins will be +3.3V when the buttons are released (Pull up)
# The potential of the pins will be at 0V when the buttons are pressed
# The parameter 'af = -1' means that we do not want to assign an alternative function to the pin.

sw1 = Pin('SW1', Pin.IN)
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)
# Reverse the logic level of the button
b1 = Signal(sw1, invert = True)

sw2 = Pin('SW2', Pin.IN)
sw2.init(Pin.IN, Pin.PULL_UP, af=-1)
b2 = Signal(sw2, invert = True)

sw3 = Pin('SW3', Pin.IN)
sw3.init(Pin.IN, Pin.PULL_UP, af=-1)
b3 = Signal(sw3, invert = True)

# Initialization of variables
old_value_sw1 = 1
old_value_sw2 = 1
old_value_sw3 = 1

while True: # Loop without exit clause ("infinite")

	# Delay for 300ms
	sleep_ms(300)
	
	# Retrieve the state of Buttons 1,2,3
	value_sw1 = b1.value()
	value_sw2 = b2.value()
	value_sw3 = b3.value()
	
	#Is the current state different from the previous state ?
	if value_sw1 != old_value_sw1:
		if value_sw1: # equivalent to value_sw1 == 1
			print( "Button 1 (SW1) is pressed" )
		else :
			print( "Button 1 (SW1) is released" )
		old_sw1_value = value_sw1
	if value_sw2 != old_value_sw2:
		if value_sw2: # equivalent to value_sw2 == 1
			print( "Button 2 (SW2) is pressed" )
		else :
			print( "Button 2 (SW2) is released" )
		old_sw2_value = value_sw2
	if value_sw3 != old_value_sw3:
		if value_sw3: # equivalent to value_sw3 == 1
			print( "Button 3 (SW3) is pressed" )
		else :
			print( "Button 3 (SW3) is released" )
		old_value_sw3 = value_sw3
