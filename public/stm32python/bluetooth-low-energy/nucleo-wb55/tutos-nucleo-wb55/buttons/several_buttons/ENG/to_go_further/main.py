# Object of the script :
# List of all alternative functions associated with pin 'A0' :

from pyb import Pin # MicroPython library allowing access to peripherals of Pyboard (GPIO, LED, etc.)

a0_pin = Pin('A0')

for val in a0_pin.af_list():
    print(val)
