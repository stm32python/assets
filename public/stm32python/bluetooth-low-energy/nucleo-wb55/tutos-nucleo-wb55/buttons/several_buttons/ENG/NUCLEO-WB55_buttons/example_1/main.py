# Purpose of the script: Turn on an LED by holding down a button.
# The button is managed by polling: an infinite loop
# monitors the status of the pin it is connected.
# Hardware required in addition to the NUCLEO-WB55: a button connected to
# D4 pin and an LED connected to D2 pin.

from pyb import Pin # To manage the GPIOs

# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V
# when the button is not pressed.

button_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE : the potential of D2 is not fixed.
led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # Pin of the LED

old_level_button = 1

while True : # Loop without exit clause ("infinite")

	# If the button is pressed, button_in.value() = 1 => the LED lights up
	# If the button is released, button_in.value() = 0 => the LED turns off
	button_level = button_in.value()
	if button_level != old_button_level:
		print("Logical level of the button:", level_button)
		old_button_level = button_level

	led_out.value(button_level)
