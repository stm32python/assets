# Purpose of the script :
# Example of GPIO configuration for a NUCLEO-WB55 button management

from machine import Pin # For access to peripherals (GPIO, LED, etc.)
from time import sleep_ms # To make system pauses

print( "GPIOs with MicroPython are easy" )

# Initialize the input pins for the buttons (SW1, SW2, SW3)
# The potential of the pins will be +3.3V when the buttons are released (Pull up)
# The potential of the pins will be at 0V when the buttons are pressed
# The parameter 'af = -1' means that we do not want to assign an alternative function to the pin.

sw1 = Pin( 'SW1' , Pin.IN)
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)

sw2 = Pin( 'SW2' , Pin.IN)
sw2.init(Pin.IN, Pin.PULL_UP, af=-1)

sw3 = Pin( 'SW3' , Pin.IN)
sw3.init(Pin.IN, Pin.PULL_UP, af=-1)

# Initialization of variables
old_value_sw1 = 1
old_value_sw2 = 1
old_value_sw3 = 1

while True: # Loop without exit clause ("infinite")

	# Delay for 500ms
	sleep_ms(500)
	
	# Retrieve the state of Buttons 1,2,3
	value_sw1 = sw1.value()
	value_sw2 = sw2.value()
	value_sw3 = sw3.value()
	
	#Is the current state different from the previous state ?
	if value_sw1 != old_value_sw1:
		if value_sw1 == 0:
			print( "Button 1 (SW1) is pressed" )
		else :
			print( "Button 1 (SW1) is released" )

	if value_sw2 != old_value_sw2:
		if value_sw2 == 0:
			print( "Button 2 (SW2) is pressed" )
		else :
			print( "Button 2 (SW2) is released" )

	if value_sw3 != old_value_sw3:
		if value_sw3 == 0:
			print( "Button 3 (SW3) is pressed" )
		else :
			print( "Button 3 (SW3) is released" )

	# Saving button state for next iteration test
	old_value_sw1 = value_sw1
	old_value_sw2 = value_sw2
	old_value_sw3 = value_sw3
