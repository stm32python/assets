# Objet du script : Allumer une LED en maintenant un bouton appuyé.
# Le bouton est géré en "polling" (scrutation) : une boucle infinie
# surveille l'état de la broche à laquelle il est connecté.
# Matériel requis en plus de la NUCLEO-WB55 : un bouton connecté à la broche
# D4 et une LED connectée à la broche D2.

from pyb import Pin # Pour gérer les GPIO

# On configure le bouton en entrée (IN) sur la broche D4.
# Le mode choisi est PULL UP : le potentiel de D4 est forcé à +3.3V
# lorsque le bouton n'est pas appuyé.

bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# On configure la LED en sortie Push-Pull (OUT_PP) sur la broche D2.
# Le mode choisi est PULL NONE : le potentiel de D2 n'est pas fixé.
led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # Broche de la LED

ancien_niveau_bouton = 1

while True : # Boucle sans clause de sortie ("infinie")

	# Si on presse le bouton, bouton_in.value() = 1 => la LED s'allume
	# Si le bouton est relâché, bouton_in.value() = 0 => la LED s'éteint
	niveau_bouton = bouton_in.value()

	if niveau_bouton != ancien_niveau_bouton:
		print("Niveau logique du bouton :", niveau_bouton)
		ancien_niveau_bouton = niveau_bouton

	led_out.value(niveau_bouton)
