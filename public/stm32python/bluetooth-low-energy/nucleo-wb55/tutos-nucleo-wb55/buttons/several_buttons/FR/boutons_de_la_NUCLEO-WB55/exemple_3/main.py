# Objet du script :
# Exemple de configuration des GPIO pour une gestion des boutons de la NUCLEO-WB55
# Inversion des états logiques àl'aide de la classe Signal.

from machine import Pin, Signal # Contrôle des broches
from time import sleep_ms # Pour faire des pauses système

print( "Les GPIO avec MicroPython c'est facile" )

# Initialisation des broches d'entrées pour les boutons (SW1, SW2, SW3)
# Le potentiel des broches sera à +3.3V lorsque les boutons seront relâchés (Pull up)
# Le potentiel des broches sera à 0V lorsque les boutons seront enfoncés
# Le paramètre 'af = -1' signifie que l'on ne souhaite pas attribuer un fonction alternative à la broche.

sw1 = Pin('SW1', Pin.IN)
sw1.init(Pin.IN, Pin.PULL_UP, af=-1) 
# Inverse le niveau logique du bouton
b1 = Signal(sw1, invert = True)

sw2 = Pin('SW2', Pin.IN)
sw2.init(Pin.IN, Pin.PULL_UP, af=-1)
b2 = Signal(sw2, invert = True)

sw3 = Pin('SW3', Pin.IN)
sw3.init(Pin.IN, Pin.PULL_UP, af=-1)
b3 = Signal(sw3, invert = True)

# Initialisation des variables
ancienne_valeur_sw1 = 1
ancienne_valeur_sw2 = 1
ancienne_valeur_sw3 = 1

while True: # Boucle sans clause de sortie ("infinie")

	# Temporisation pendant 300ms
	sleep_ms(300)
	
	#Récupération de l'état des Boutons 1,2,3
	valeur_sw1 = b1.value()
	valeur_sw2 = b2.value()
	valeur_sw3 = b3.value()
	
	#L'état courant est il différent de l'état précédent ?
	if valeur_sw1 != ancienne_valeur_sw1:
		if valeur_sw1: # équivalent à  valeur_sw1 == 1
			print( "Le bouton 1 (SW1) est appuyé" )
		else :
			print( "Le bouton 1 (SW1) est relâché" )
		ancienne_valeur_sw1 = valeur_sw1
	if valeur_sw2 != ancienne_valeur_sw2:
		if valeur_sw2:  # équivalent à  valeur_sw2 == 1
			print( "Le bouton 2 (SW2) est appuyé" )
		else :
			print( "Le bouton 2 (SW2) est relâché" )
		ancienne_valeur_sw2 = valeur_sw2
	if valeur_sw3 != ancienne_valeur_sw3:
		if valeur_sw3:  # équivalent à  valeur_sw3 == 1
			print( "Le bouton 3 (SW3) est appuyé" )
		else :
			print( "Le bouton 3 (SW3) est relâché" )
		ancienne_valeur_sw3 = valeur_sw3
