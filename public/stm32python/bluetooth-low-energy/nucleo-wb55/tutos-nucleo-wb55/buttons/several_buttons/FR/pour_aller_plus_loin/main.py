# Objet du script :
# Liste de toutes les fonctions alternatives associées à la broche 'A0' :

from pyb import Pin # Bibliothèque de MicroPython permettant les accès aux périphériques (GPIO, LED, etc.)

a0_pin = Pin('A0')

for val in a0_pin.af_list():
    print(val)
