# Purpose of the script: Create a "chase" with interrupts
# Example of GPIO configuration to manage the integrated LEDs of the NUCLEO-WB55

from machine import Pin # For pin access
from pyb import LED, ExtInt # Interrupting pins and managing LEDs
from time import sleep_ms # To make system pauses

print( "Interrupting with MicroPython is easy" )

# Initialization of the LEDs
led_blue = LED(3) # screen printed LED1 on the PCB
led_green = LED(2) # screen printed LED2 on the PCB
led_red = LED(1) # screen printed LED3 on the PCB

# Initialization of global variables
led_counter = 0

# interrupt flags
pause = 0
inv = 0

# Initialization of the buttons (SW1 and SW2)
sw1 = Pin('SW1')
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)
sw2 = Pin('SW2')
sw2.init(Pin.IN, Pin.PULL_UP, af=-1)

# Service function of the interrupt for SW1 (pauses the chase)
def Pause(line):
	global pause
	if pause == 0:
		pause = 1
		print("Pause")
	else:
		pause = 0

# Interrupt service function for SW2 (Reverse the direction of the chase)
def Invert(line):
	global inv
	if inv == 0:
		inv = 1
		print("Reverse")
	else:
		inv = 0

# We "attach" the ISR of the interrupts to the pins of the buttons
irq_1 = ExtInt(sw1, ExtInt.IRQ_FALLING, Pin.PULL_UP, Pause)
irq_2 = ExtInt(sw2, ExtInt.IRQ_FALLING, Pin.PULL_UP, Reverse)

while True: # Creation of an "infinite" loop with actions only if the system is not paused

	if pause == 0:
		if led_counter == 0:
			led_blue.on()
			led_green.off()
			led_red.off()
		elif led_counter == 1:
			led_blue.off()
			led_green.on()
			led_red.off()
		else :
			led_blue.off()
			led_green.off()
			led_red.on()
		
		# We want to turn on the next LED at the next iteration of the loop with direction management
		if inv == 0:
			led_counter = led_counter + 1
			if led_counter > 2:
				led_counter = 0
		else:
			led_counter = led_counter - 1
			if led_counter < 0:
				led_counter = 2
				
		sleep_ms(500) # Delay of 500 milliseconds
