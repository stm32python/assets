# Purpose of the script: Turn on or off an LED with a button.
# The button is managed with an interrupt.
# A first press on the button turns on the LED, a second press turns it off.
# Hardware required in addition to the NUCLEO-WB55: a button connected to pin
# D4 pin and an LED connected to the D2 pin.

from pyb import Pin # Class to manage GPIOs

# The button is configured as an input (IN) on pin D4.
# The chosen mode is PULL UP : the potential of D4 is forced to +3.3V
# when the button is not pressed.

button_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# The LED is configured as a Push-Pull output (OUT_PP) on pin D2.
# The chosen mode is PULL NONE : the potential of D2 is not fixed.

led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # Pin of the LED

status = 0 # Variable to store the state of the LED (on or off)
led_out.value(status) # LED initially off

# Function to manage the interruption of the button
def ISR(pin):
	global status
	status = not status # invert the state of the variable (0->1 or 1->0)
	led_out.value(status) # Invert the state of the LED

# We "attach" the ISR to the button pin. It will turn on when the button is
# and the voltage goes from 3.3V to 0V (IRQ_FALLING).

button_in.irq(trigger=button_in.IRQ_FALLING, handler=ISR)
