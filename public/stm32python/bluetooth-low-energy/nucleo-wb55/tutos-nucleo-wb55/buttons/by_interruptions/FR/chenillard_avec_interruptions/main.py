# Objet du script : Créer un "chenillard" avec interruptions
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la NUCLEO-WB55

from machine import Pin # Pour les accès aux broches
from pyb import LED, ExtInt # Interruption des broches et gestions des LED
from time import sleep_ms # Pour faire des pauses systèmes

print( "Les interruptions avec MicroPython c'est facile" )

# Initialisation des LED
led_bleu = LED(3) # sérigraphiée LED1 sur le PCB
led_vert = LED(2) # sérigraphiée LED2 sur le PCB
led_rouge = LED(1) # sérigraphiée LED3 sur le PCB

# Initialisation des variables globales
compteur_de_led = 0

# flags des interruptions
pause = 0
inv = 0

# Initialisation des boutons (SW1 et SW2)
sw1 = Pin('SW1')
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)
sw2 = Pin('SW2')
sw2.init(Pin.IN, Pin.PULL_UP, af=-1)

# Fonction de service de l'interruption pour SW1 (met en pause le chenillard)
def Pause(line):
	global pause
	if pause == 0:
		pause = 1
		print("Pause")
	else:
		pause = 0

# Fonction de service de l'interruption pour SW2 (Inverse le sens du chenillard)
def Inversion(line):
	global inv
	if inv == 0:
		inv = 1
		print("Inversion")
	else:
		inv = 0

# On "attache" les ISR des interruptions aux broches des boutons
irq_1 = ExtInt(sw1, ExtInt.IRQ_FALLING, Pin.PULL_UP, Pause)
irq_2 = ExtInt(sw2, ExtInt.IRQ_FALLING, Pin.PULL_UP, Inversion)

while True: # Création d'une boucle "infinie" avec des actions uniquement si le système n'est pas en pause

	if pause == 0:
		if compteur_de_led == 0:
			led_bleu.on()
			led_vert.off(
			led_rouge.off())
		elif compteur_de_led == 1:
			led_bleu.off()
			led_vert.on()
			led_rouge.off()
		else :
			led_bleu.off()
			led_vert.off()
			led_rouge.on()
		
		# On veut allumer la prochaine LED à la prochaine itération de la boucle avec gestion du sens
		if inv == 0:
			compteur_de_led = compteur_de_led + 1
			if compteur_de_led > 2:
				compteur_de_led = 0
		else:
			compteur_de_led = compteur_de_led - 1
			if compteur_de_led < 0:
				compteur_de_led = 2
				
		sleep_ms(500) # Temporisation de 500 millisecondes
