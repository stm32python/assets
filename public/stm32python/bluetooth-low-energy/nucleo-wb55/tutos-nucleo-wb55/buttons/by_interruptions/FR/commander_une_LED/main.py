# Objet du script : Allumer ou éteindre une LED avec un bouton.
# Le bouton est géré avec une interruption.
# Un premier appui sur le bouton allume la LED, un deuxième l'éteint.
# Matériel requis en plus de la NUCLEO-WB55 : un bouton connecté à la broche
# D4 et une LED connectée à la broche D2.

from pyb import Pin # Classe pour gérer les GPIO

# On configure le bouton en entrée (IN) sur la broche D4.
# Le mode choisi est PULL UP : le potentiel de D4 est forcé à +3.3V
# lorsque le bouton n'est pas appuyé.

bouton_in = Pin('D4', Pin.IN, Pin.PULL_UP)

# On configure la LED en sortie Push-Pull (OUT_PP) sur la broche D2.
# Le mode choisi est PULL NONE : le potentiel de D2 n'est pas fixé.

led_out = Pin('D2', Pin.OUT_PP, Pin.PULL_NONE) # Broche de la LED

statut = 0 # Variable pour mémoriser l'état de la LED (allumée ou pas)
led_out.value(statut) # LED initialement éteinte

# Fonction de gestion de l'interruption du bouton
def ISR(pin):
	global statut
	statut = not statut # inverse l'état de la variable (0->1 ou 1->0)
	led_out.value(statut) # Inverse l'état de la LED

# On "attache" l'ISR à la broche du bouton. Elle se déclanchera lorsque le bouton sera 
# en train de s'enfoncer et que la tension passe de 3.3V à 0V (IRQ_FALLING).

bouton_in.irq(trigger=bouton_in.IRQ_FALLING, handler=ISR)