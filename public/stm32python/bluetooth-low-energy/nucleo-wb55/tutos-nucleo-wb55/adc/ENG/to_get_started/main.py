# Object of the script :
# Example of configuration of an ADC to digitize a variable input voltage through a potentiometer.
# To increase the accuracy, the input voltage is averaged over 500 measurements (1 measurement / millisecond)
# Required material : potentiometer (Grove or other), ideally designed to work between 0 and 3.3V and connected on A0.

import pyb # to manage the GPIOs
from time import sleep_ms # to delay

print( "ADC with MicroPython is easy" )

# Reference voltage / range of the ADC : +3.3V
varef = 3.3

# Resolution of the ADC 12 bits = 2^12 = 4096 (min = 0, max = 4095)
RESOLUTION = const(4096)

# Quantum of the ADC
quantum = varef / (RESOLUTION - 1)

# Initialization of the ADC on pin A0
adc_A0 = pyb.ADC(pyb.Pin( 'A0' ))

# Initialization for averaging
Nb_Measures = 500
Inv_Nb_Measurements = 1 / Nb_Measures

while True: # "Infinite" loop (without exit clause)
	
	sum_voltage = 0
	average_voltage = 0
	
	# Calculation of the average of the voltage at the terminals of the potentiometer

	for i in range(Nb_Measures): # We do Nb_Measurements conversions of the input voltage
		
		# Reads the conversion of the ADC (a number between 0 and 4095 proportional to the input voltage)
		digital_value = adc_A0.read()
		
		# Now we calculate the voltage (analog value)
		voltage = digital_value * quantum

		# Add it to the value calculated in the previous iteration
		sum_voltage = sum_voltage + voltage

		# Delay for 1 ms
		sleep_ms(1)
	
	# Divide by Nb_Measures to calculate the average voltage of the potentiometer
	average_voltage = sum_voltage * Inv_Nb_Measurements
	
	# Display the average voltage on the serial port of the USB USER
	print( "The average voltage value is : %.2f V" %average_voltage)
