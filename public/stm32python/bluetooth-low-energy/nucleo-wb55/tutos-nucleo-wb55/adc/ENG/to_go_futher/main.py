# Object of the script :
# Example of configuration of an ADC to digitize a variable input voltage through a potentiometer.
# The measurements are driven by a timer at a rate of 10 per second.
# Hardware required: potentiometer (Grove or other), ideally designed to operate between 0 and 3.3V and connected to A0.

import pyb # to manage GPIOs
import array # to save data in an array
from pyb import Timer # to manage the iimers

# Initialization of the ADC on pin A0
adc = pyb.ADC(pyb.Pin( 'A0' ))

# Initialization of timer 1 at the frequency of 10 Hertz
tim = Timer(1, freq=10)

# Initialization of a buffer array containing 200 bytes grouped in unsigned integers ('H', two bytes).
# This array will contain 100 elements of two bytes.
buf = array.array('H', bytearray(200))

# Sampling of 100 analog values, at the frequency of 10 Hz, therefore for 100 seconds
adc.read_timed(buf, tim)

# Loop over all 100 elements of buf, and display their values
for val in buf:
	print(val)
