# Objet du script :
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Pour augmenter la précision, on calcule la moyenne de la tension d'entrée sur 500 mesures (1 mesure / milliseconde)
# Matériel requis : potentiomètre (Grove ou autre), idéalement conçu pour fonctionner entre 0 et 3.3V et connecté sur A0.

import pyb # pour gérer les GPIO
from time import sleep_ms # Pour temporiser

print( "L'ADC avec MicroPython c'est facile" )

# Tension de référence / étendue de mesure de l'ADC : +3.3V
varef = 3.3

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = varef / (RESOLUTION - 1)

# Initialisation de l'ADC sur la broche A0
adc_A0 = pyb.ADC(pyb.Pin( 'A0' ))

# Initialisations pour calcul de la moyenne
Nb_Mesures = 500
Inv_Nb_Mesures = 1 / Nb_Mesures

while True: # Boucle "infinie" (sans clause sortie)
	
	somme_tension = 0
	moyenne_tension = 0
	
	# Calcul de la moyenne de la tension aux bornes du potentiomètre

	for i in range(Nb_Mesures): # On fait Nb_Mesures conversions de la tension d'entrée
		
		# Lit la conversion de l'ADC (un nombre entre 0 et 4095 proportionnel à la tension d'entrée)
		valeur_numerique = adc_A0.read()
		
		# On calcule à présent la tension (valeur analogique) 
		tension = valeur_numerique * quantum

		# On l'ajoute à la valeur calculée à l'itération précédente
		somme_tension = somme_tension + tension

		# Temporisation pendant 1 ms
		sleep_ms(1)
	
	# On divise par Nb_Mesures pour calculer la moyenne de la tension du potentiomètre
	moyenne_tension = somme_tension * Inv_Nb_Mesures 
	
	# Affichage de la tension moyenne sur le port série de l'USB USER
	print( "La valeur moyenne de la tension est : %.2f V" %moyenne_tension)