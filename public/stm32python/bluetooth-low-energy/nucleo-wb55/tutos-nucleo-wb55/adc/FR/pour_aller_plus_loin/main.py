# Objet du script :
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Les mesures sont pilotées par un Timer à raison de 10 par seconde.
# Matériel requis : potentiomètre (Grove ou autre), idéalement conçu pour fonctionner entre 0 et 3.3V et connecté sur A0.

import pyb # pour gérer les GPIO
import array # pour enregistrer les données dans un tableau
from pyb import Timer # pour gérer les iimers

# Initialisation de l'ADC sur la broche A0
adc = pyb.ADC(pyb.Pin( 'A0' ))

# Initialisation du timer 1 à la fréquence de 10 Hertz
tim = Timer(1, freq=10)

# Initialisation d'un tableau tampon contenant 200 octets regroupés en entiers non signés ('H', deux octets).
# Ce tableau contiendra donc 100 éléments de deux octets.
buf = array.array('H', bytearray(200))  

# Echantillonnage de 100 valeurs analogiques, à la fréquence de 10 Hz, donc pendant 100 secondes
adc.read_timed(buf, tim)

# Boucle sur l'ensemble des 100 éléments de buf, et affichage de leurs valeurs
for val in buf:
	print(val)