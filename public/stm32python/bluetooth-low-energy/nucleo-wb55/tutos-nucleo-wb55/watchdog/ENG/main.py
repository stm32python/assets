# Purpose of the script :
# Demonstration of the Watchdog Timer (WDT)
# Reference: https://docs.micropython.org/en/latest/library/machine.WDT.html
# After 5 presses of the SW1 button, the script pauses for a duration
# that exceeds the restart threshold imposed by the WDT.

import pyb # for access to peripherals (GPIO, LED, etc.)
from time import sleep_ms # to make system pauses

# global variable that counts button presses
cnt = 0

# Initialization of the SW1 button
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Service function of the interrupt for SW1 (increments cnt)
def Press(line):
	global cnt
	cnt += 1

# Attach an interrupt service routine to SW1
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Press)

# Initialization of the blue LED
blue_LED = pyb.LED(3) # screen printed LED1 on the PCB

# Waiting time (ms) before changing the state of the LED
DELAY_LED = const(500)

# Wait time (ms) before the watchdog restarts the NUCLEO-WB55
WDT_TIMEOUT = const(2000)

# To manage the watchdog timer
from machine import WDT

# We start the watchdog timer
wdt = WDT(timeout = WDT_TIMEOUT)

while True:
	
	# "Reload" the watchdog for WDT_TIMEOUT milliseconds
	wdt.feed()

	if cnt == 5:
		print("Pause for %d seconds" %( WDT_TIMEOUT // 1000 ))
		cnt = 0
		sleep_ms(WDT_TIMEOUT)

	blue_LED.on() # Turn on the LED
	sleep_ms(DELAY_LED) # Wait for delay seconds

	blue_LED.off() # Turns off the LED
	sleep_ms(DELAY_LED) # Wait for delay seconds
