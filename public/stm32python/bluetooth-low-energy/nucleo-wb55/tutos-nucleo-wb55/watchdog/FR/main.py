# Objet du script :
# Démonstration du Watchdog Timer (WDT)
# Référence : https://docs.micropython.org/en/latest/library/machine.WDT.html
# Après 5 appuis sur le bouton SW1, le script sde met en pause pendant une durée
# qui dépasse le seuil de redémarrage imposé par le watchdog.

import pyb # pour les accès aux périphériques (GPIO, LED, etc.)
from time import sleep_ms # pour faire des pauses système

# Variable globale qui compte les appuis sur le bouton
cnt = 0

# Initialisation du bouton SW1
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Fonction de service de l'interruption pour SW1 (incrémente cnt)
def Press(line):
	global cnt
	cnt += 1

# On attache une routine de service d'interruptionà SW1
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Press)

# Initialisation de la LED bleue
led_bleue = pyb.LED(3) # sérigraphiée LED1 sur le PCB

# Temps d'attente (ms) avant de changer l'état de la LED
DELAY_LED = const(500)

# Temps d'attente (ms) avant que le watchdog ne rédémarre la NUCLEO-WB55
WDT_TIMEOUT = const(2000)

# Pour gérer le watchdog
from machine import WDT

# On démarre le watchdog
wdt = WDT(timeout = WDT_TIMEOUT)

while True:
	
	# On "recharge" le watchdog pour WDT_TIMEOUT millisecondes
	wdt.feed()

	if cnt == 5:
		print("Pause pendant %d secondes" %( WDT_TIMEOUT // 1000 ))
		cnt = 0
		sleep_ms(WDT_TIMEOUT)

	led_bleue.on() # Allume la LED
	sleep_ms(DELAY_LED) # Attends delai secondes

	led_bleue.off() # Eteint la LED
	sleep_ms(DELAY_LED) # Attends delai secondes
