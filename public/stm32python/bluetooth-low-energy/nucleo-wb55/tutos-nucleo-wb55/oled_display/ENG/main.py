# Purpose of the script:
# Write text to an OLED display controlled by an SSD1306
# plugged on an I2C controller

from machine import Pin, I2C
import ssd1306 # Class to drive the display
from time import sleep_ms

# Initialization of the I2C device
i2c = I2C(1)

# Pause for one second to give the I2C time to initialize
sleep_ms(1000)

# Setting the characteristics of the screen
screen_width = 128
screen_length = 32
oled = ssd1306.SSD1306_I2C(screen_width, screen_length, i2c)

# Send the text to be displayed on the OLED screen
oled.text('MicroPython OLED!', 0, 0)
oled.text(' I2C ', 0, 10)
oled.text('Too easy !!!', 0, 20)

# Trigger display
oled.show()
