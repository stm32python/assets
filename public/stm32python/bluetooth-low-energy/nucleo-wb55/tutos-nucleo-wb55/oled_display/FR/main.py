# Objet du script :
# Ecrire un texte sur un afficheur OLED contrôlé par un SSD1306.

from machine import Pin, I2C
import ssd1306 # Classe pour piloter l'afficheur
from time import sleep_ms

# Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1306.SSD1306_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

# Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show()
