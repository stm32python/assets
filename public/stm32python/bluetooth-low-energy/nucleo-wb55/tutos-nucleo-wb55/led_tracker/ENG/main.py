# Purpose of the script: Program a "chase"
# Example of GPIO configuration to manage the integrated LEDs of the NUCLEO-WB55

import pyb # For access to peripherals (GPIO, LED, etc.)
from time import sleep_ms # For system pauses

print( "LEDs with MicroPython are easy" )

# Initialization of the LEDs
led_blue = pyb.LED(3) # screen printed LED1 on the PCB
led_green = pyb.LED(2) # screen printed LED2 on the PCB
led_red = pyb.LED(1) # screen printed LED3 on the PCB

# Initialization of the LED counter
led_counter = 0

while True: # Creation of an "infinite" loop (no exit clause)
	
	if led_counter == 0:
		led_blue.on()
		led_red.off()
		led_green.off()
	elif led_counter == 1:
		led_blue.off()
		led_green.on()
		led_red.off()
	else :
		led_blue.off()
		led_green.off()
		led_red.on()
		
	# We want to turn on the next LED at the next iteration of the loop
	led_counter = led_counter + 1
	if led_counter > 2:
		led_counter = 0
	
	sleep_ms(500) # Delay of 500ms