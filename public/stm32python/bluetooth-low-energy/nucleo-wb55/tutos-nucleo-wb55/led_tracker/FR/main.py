# Objet du script : Programmer un "chenillard"
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la NUCLEO-WB55

import pyb # Pour les accès aux périphériques (GPIO, LED, etc.)
from time import sleep_ms # Pour les pauses système

print( "Les LED avec MicroPython c'est facile" )

# Initialisation des LED
led_bleu = pyb.LED(3) # sérigraphiée LED1 sur le PCB
led_vert = pyb.LED(2) # sérigraphiée LED2 sur le PCB
led_rouge = pyb.LED(1) # sérigraphiée LED3 sur le PCB

# Initialisation du compteur de LED
compteur_de_led = 0

while True: # Création d'une boucle "infinie" (pas de clause de sortie)
	
	if compteur_de_led == 0:
		led_bleu.on()
		led_rouge.off()
		led_vert.off()
	elif compteur_de_led == 1:
		led_bleu.off()
		led_vert.on()
		led_rouge.off()
	else :
		led_bleu.off()
		led_vert.off()
		led_rouge.on()
		
	# On veut allumer la prochaine LED à la prochaine itération de la boucle
	compteur_de_led = compteur_de_led + 1
	if compteur_de_led > 2:
		compteur_de_led = 0
	
	sleep_ms(500) # Temporisation de 500ms