# Purpose of the script:
# Exchange text messages between two NUCLEO-WB55s using the UART 2 connected
# on pins D0 (RX) and D1 (TX).
# Implementation: copy this script to the "PYBFLASH" folder of both boards,
# and connect the RX (respectively TX) pin of one to the TX (respectively RX) pin of the other.
# Run the two scripts and observe the message exchanges.

from time import sleep_ms, time # Classes for pauses in milliseconds and for time stamping
from machine import unique_id # Class to get a unique identifier of the NUCLEO-WB55
from machine import UART # Class to manage the UART
from ubinascii import hexlify # Class to convert a hexadecimal number into its displayable binary representation

# Gets a unique identifier of the card, gives a UTF8 encoded text representation
card_id = hexlify(unique_id()).decode("utf-8")
print("Card identifier: " + card_id)

# Delay in milliseconds for the main loop
delai_while = const(500)

# Constants related to the UART parameterization
delai_timeout = const(100) # Time (in milliseconds) that the UART waits to receive a message
baudrate = const(115200) # Baud rate of the serial communication
UART_Number = const(2) # Identifier of the UART of the NUCLEO-WB55 that will be used
RX_BUFF = const(64) # Size of the reception buffer (the messages received will be truncated at this number of characters)

# Initialization of the UART
uart = UART(UART_Number, baudrate, timeout = delai_timeout, rxbuf = RX_BUFF)

# First read to "empty" the RX reception queue of the UART
uart.read()

while True: # Loop without exit clause ("infinite")

	# Timestamp
	timestamp = time()

	# Message to send: the unique identifier of the NUCLEO-WB55
	message_to_send = card_id

	# Send the message
	# Write the bytes / characters in the transmission queue Tx.
	uart.write(message_to_send)

	# Display the message sent on the serial port of the USB USER
	print(str(timestamp) + " Message sent: " + card_id)

	# Receipt of a possible message
	# Reading of bytes / characters in the reception queue Rx.
	message_received = uart.read() # Read the received characters until the end.

	# If there was indeed a message waiting in Rx ...
	if not (message_received is None) :

		# Interprets the read bytes as a string encoded in UTF8
		message_decoded = message_received.decode("utf-8")

		# Displays the received message, preceded by the timestamp, on the serial port of the USB USER
		print(str(timestamp) + "Message received:" + message_decoded)

	sleep_ms(delai_while) # Wait delai_while milliseconds
