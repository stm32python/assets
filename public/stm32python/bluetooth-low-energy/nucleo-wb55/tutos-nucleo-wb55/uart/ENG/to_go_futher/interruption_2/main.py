# Purpose of the script: UART management by interrupts (version 2)
# Exchange text messages between two NUCLEO-WB55 boards using the UART 2 connected
# on pins D0 (RX) and D1 (TX).
# This time we use interrupts:
# - One attached to the SW1 button, to send a message
# - One attached to the receiving channel (RX) to display a received message
# Implementation: copy this script in the "PYBFLASH" folder of both boards,
# and connect the RX (respectively TX) pin of one to the TX (respectively RX) pin of the other.
# Run the two scripts and observe the message exchanges.

from pyb import Pin # To manage the GPIOs
from time import time # For the timestamp
from machine import unique_id # To get a unique identifier for the NUCLEO-WB55
from ubinascii import hexlify # To convert a hexadecimal number into its displayable binary representation

# Initialize the SW1 button
sw1 = Pin('SW1')
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)

# Card identifier
# This operation requires manipulations in memory that cannot be performed
# in an interrupt service function.

card_id = hexlify(unique_id()).decode("utf-8")

# SW1 button interrupt service function
def Send(line):
	# Retrieve the id, shared as a global variable
	global card_id
	# Write bytes/characters to the send queue Tx.
	uart.write(card_id)

# Activate the button interrupt
irq_button = ExtInt(sw1, ExtInt.IRQ_FALLING, Pin.PULL_UP, Send)

# Constants related to the UART parameterization
delai_timeout = const(100) # Time (in milliseconds) that the UART waits to receive a message
baudrate = const(115200) # Baud rate of the serial communication
UART_Number = const(2) # Identifier of the UART on the NUCLEO-WB55 board that will be used
RX_BUFF = const(64) # Size of the reception buffer (the messages received will be truncated at this number of characters)
#TX_BUFF = const(64) # Size of the transmission buffer (messages with more characters cannot be sent)

# Initialization of the UART
uart = UART(UART_Number, baudrate, timeout = delai_timeout, rxbuf = RX_BUFF)

# First read to "empty" the RX reception queue of the UART
uart.read()

# Service function of the UART reception interrupt
def Reception(uart_object):
	
	 # Read received characters
	message_received = uart_object.read()
	
	# If a message is received
	if not (message_received is None):
		
		# Timestamp
		timestamp = time()

		# Displays the received message, preceded by the timestamp, on the serial port of the USB USER
		print(str(timestamp) + "Message received: " + message_received.decode("utf-8"))

# We activate the UART interrupt (interrupt vector)
irq_uart = uart.irq(Reception, UART.IRQ_RXIDLE, False)
