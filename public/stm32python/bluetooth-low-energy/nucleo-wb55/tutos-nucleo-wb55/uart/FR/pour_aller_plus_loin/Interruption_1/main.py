# Objet du script : Gestion UART par interruptions (version 1)
# Echanger des messages textuels entre deux cartes NUCLEO-WB55 en utilisant l'UART 2 connecté
# sur les broches D0 (RX) et D1 (TX).
# On utilise cette fois-ci des interruptions :
# - Une attachée au bouton SW1, pour envoyer un message
# - Une attachée au canal de réception (RX) pour afficher un message reçu
# Mise en oeuvre : copiez ce script dans le dossier "PYBFLASH" des deux cartes,
# et reliez la broche RX (respectivement TX) de l'une à la broche TX (respectivement RX) de l'autre.
# Lancez les deux scripts et observez les échanges de messages.

from pyb import Pin # Pour gérer les GPIO
from time import time # Pour l'horodatage
from machine import unique_id, UART # Pour obtenir un identifiant unique de la NUCLEO-WB55 et pour gérer l'UART
from ubinascii import hexlify # Pour convertir un nombre hexadécimal en sa représentation binaire affichable

# Obtient un identifiant unique de la carte, en donne une représentation texte codée UTF8
id_carte = hexlify(unique_id()).decode("utf-8")
print("Identifiant de la carte : " + id_carte)

# Variables globales modifiées par les interruptions
message_recu = None
bouton_appuye = 0

# Initialisation du bouton SW1
sw1 = Pin('SW1')
sw1.init(Pin.IN, Pin.PULL_UP, af=-1)

# Fonction de service de l'interruption pour SW1
def Envoi(line):
	global bouton_appuye
	bouton_appuye = 1

# On active l'interruption du bouton
sw1.irq(trigger=sw1.IRQ_FALLING, handler=Send)

# Constantes relatives au paramétrage de l'UART
delai_timeout = const(100) # Durée (en millisecondes) pendant laquelle l'UART attend de reçevoir un message
debit = const(115200) # Débit, en bauds, de la communication série
Numero_UART = const(2) # Identifiant de l'UART de la carte NUCLEO-WB55 qui sera utilisé
RX_BUFF = const(64) # Taille du buffer de réception (les messages reçus seront tronqués à ce nombre de caractères)
#TX_BUFF = const(64) # Taille du buffer d'émission (on ne peut pas envoyer des messages comportant plus de caractères)

# Initialisation de l'UART
uart = UART(Numero_UART, debit, timeout = delai_timeout, rxbuf = RX_BUFF)

# Première lecture pour "vider" la file de réception RX de l'UART
uart.read()

# Fonction de service de l'interruption de réception de l'UART
def Reception(uart_object):
	 # Lecture des caractères reçus
	message_recu = uart_object.read()
	# Si réception d'un message
	if not (message_recu is None):
		# Horodatage
		timestamp = time()
		# Affiche le message reçu, précédé de l'horodatage, sur le port série de l'USB USER
		print(str(timestamp) + " Message reçu : " + message_recu.decode("utf-8"))

# On active l'interruption de l'UART (vecteur d'interruption)
irq_uart = uart.irq(Reception, UART.IRQ_RXIDLE, False)

while True: # Boucle sans clause de sortie ("infinie")

	# Si appui sur bouton
	if bouton_appuye == 1 :

		# Horodatage
		timestamp = time()

		# Message  à envoyer : l'identifiant unique de la carte
		message_a_expedier = id_carte

		# Expédition du message
		# Ecriture des octets / caractères dans la file d'émission Tx.
		uart.write(message_a_expedier)

		# Affiche le message expédié sur le port série de l'USB USER
		print(str(timestamp) + " Message envoyé : " + id_carte)

		bouton_appuye = 0
