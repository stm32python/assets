# Objet du script :
# Echanger des messages textuels entre deux NUCLEO-WB55 en utilisant l'UART 2 connecté
# sur les broches D0 (RX) et D1 (TX).
# Mise en oeuvre : copiez ce script dans le dossier "PYBFLASH" des deux cartes,
# et reliez la broche RX (respectivement TX) de l'une à la broche TX (respectivement RX) de l'autre.
# Lancez les deux scripts et observez les échanges de messages.

from time import sleep_ms, time # Classes pour faire des pauses en millisecondes et pour l'horodatage
from machine import unique_id # Classe pour obtenir un identifiant unique de la NUCLEO-WB55
from machine import UART # Classe pour gérer l'UART
from ubinascii import hexlify # Classe pour convertir un nombre hexadécimal en sa représentation binaire affichable

# Obtient un identifiant unique de la carte, en donne une représentation texte codée UTF8
id_carte = hexlify(unique_id()).decode("utf-8")
print("Identifiant de la carte : " + id_carte)

# Temporisation en millisecondes pour la boucle principale
delai_while = const(500)

# Constantes relatives au paramétrage de l'UART
delai_timeout = const(100) # Durée (en millisecondes) pendant laquelle l'UART attend de reçevoir un message
debit = const(115200) # Débit, en bauds, de la communication série
Numero_UART = const(2) # Identifiant de l'UART de la NUCLEO-WB55 qui sera utilisé
RX_BUFF = const(64) # Taille du buffer de réception (les messages reçus seront tronqués à ce nombre de caractères)

# Initialisation de l'UART
uart = UART(Numero_UART, debit, timeout = delai_timeout, rxbuf = RX_BUFF)

# Première lecture pour "vider" la file de réception RX de l'UART
uart.read()

while True: # Boucle sans clause de sortie ("infinie")

	# Horodatage
	timestamp = time()

	# Message à envoyer : l'identifiant unique de la NUCLEO-WB55
	message_a_expedier = id_carte

	# Expédition du message
	# Ecriture des octets / caractères dans la file d'émission Tx.
	uart.write(message_a_expedier)

	# Affiche le message expédié sur le port série de l'USB USER
	print(str(timestamp) + " Message envoyé : " + id_carte)

	# Réception d'un éventuel message
	# Lecture des octets / caractères dans la file de réception Rx.
	message_recu = uart.read() # Lis les caractères reçus jusqu'à la fin.

	# S'il y avait effectivement un message en attente dans Rx ...
	if not (message_recu is None) :

		# Interprête les octets lus comme une chaîne de caractères encodée en UTF8
		message_decode = message_recu.decode("utf-8")

		# Affiche le message reçu, précédé de l'horodatage, sur le port série de l'USB USER
		print(str(timestamp) + " Message reçu : " + message_decode)

	# Temporisation
	sleep_ms(delai_while) # Attends delai_while millisecondes
