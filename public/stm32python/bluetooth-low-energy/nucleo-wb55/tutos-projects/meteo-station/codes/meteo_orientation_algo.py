import math
import time

# Calcul IMU
x = #! Put code here: value of acceleration in axe X !#
y = #! Put code here: value of acceleration in axe Y !#
z = #! Put code here: value of acceleration in axe Z !#
if x == 0 and y == 0 and z == 0:
	pitch = 0
	roll = 0
	yaw = 0
else:
	pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
	roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	#print("[DEBUG]: Pitch: ", pitch)
	#print("[DEBUG]: Roll: ", roll)
	#print("[debug]: yaw: ", yaw)

	# display orientation
	if abs(pitch) > 50:
		''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
		if pitch > 0:
			print("#! put correct orientation detected here!#")
		else:
			print("#! put correct orientation detected here!#")
	elif abs(roll) > 50:
		'''ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN : ORIENTATION_VERTICAL_UP;'''
		if roll < 0:
			print("#! put correct orientation detected here!#")
		else:
			print("#! put correct orientation detected here!#")
	else:
		print("No move")
	time.sleep_ms(1000)
