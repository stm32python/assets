import pyb
import time

print("LEDs is easy with MicroPython")
# Init of LEDs (LED_1, LED_2, LED_3)

led_red = pyb.LED(1)
led_green = pyb.LED(2)
led_blue = pyb.LED(3)

# Init counter of LED
counter_led = 0

while 1: # infinite loop
	if counter_led == 0:
		led_red.on()
		led_green.off()
		led_blue.off()
	elif counter_led == 1:
		led_red.off()
		led_green.on()
		led_blue.off()
	else:
		led_red.off()
		led_green.off()
		led_blue.on()
	# we would like to turn on new led at next loop iteration
	counter_led = counter_led + 1
	if counter_led > 2:
		counter_led = 0
	time.sleep_ms(500)
