from machine import I2C, Pin
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22: Pressure
import LPS22
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO

import time
import math
# ---------------------------------
# Global variables
# sensors are connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	# initialisation of sensor
	temperature_sensor = hts221.HTS221(i2c1)
	pression_sensor = LPS22.LPS22(i2c1)
	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	while(1):
		# Display accelerometer information
		accel = accelerometer_sensor.get_a_raw()
		#print("[DEBUG]: Accelerometer: ", accel)
		#print("[DEBUG]:   X: ", accel[0])
		#print("[DEBUG]:   Y: ", accel[1])
		#print("[DEBUG]:   Z: ", accel[2])

		# Calcul IMU
		x = accel[0]
		y = accel[1]
		z = accel[2]
		if x == 0 and y == 0 and z == 0:
			pitch = 0
			roll = 0
			yaw = 0
		else:
			pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
			roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
			yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
		#print("[DEBUG]: Pitch: ", pitch)
		#print("[DEBUG]: Roll: ", roll)
		#print("[DEBUG]: yaw: ", yaw)

		# display orientation
		if abs(pitch) > 50:
			''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
			if pitch > 0:
				# display humidity
				humidity = temperature_sensor.humidity()
				print("Humidite: %s %s" % (str(humidity), "%") )
				time.sleep_ms(1000)
			else:
				# display temperature
				temperature = temperature_sensor.temperature()
				print("Temperature: %s C" % str(temperature) )
				time.sleep_ms(1000)
		elif abs(roll) > 50:
			'''ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN : ORIENTATION_VERTICAL_UP;'''
			if roll < 0:
				# display altitude
				altitude = pression_sensor.altitude()
				print("Altitude: %s M" % str(altitude) )
				time.sleep_ms(1000)
			else:
				# display pressure
				pression = pression_sensor.pressure()
				print("Pressure: %s hPa" % str(pression) )
				time.sleep_ms(1000)
		else:
			print("normal")
		time.sleep_ms(5000)
