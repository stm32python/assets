from micropython import const
import bluetooth
from machine import I2C, Pin
import struct
from struct import *
from micropython import const
import pyb

# sensor
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22 sensor is connected on I2C1 of arduino connector
import LPS22
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO
# display SSD1306 is connected on I2C1 of arduino connector
import framebuf
import ssd1306

import time
import math

USE_ENVIRONMENTAL = 1

# ----------------------------------------
# Screen & READ Picture
SCREEN_WIDTH = 128
SCREEN_HEIGHT = 64
def load_image(filename):
	with open(filename, 'rb') as f:
		f.readline()
		width, height = [int(v) for v in f.readline().split()]
		data = bytearray(f.read())
	return framebuf.FrameBuffer(data, width, height, framebuf.MONO_HLSB)

# ----------------------------------------
# advertising
_ADV_TYPE_FLAGS = const(0x01)
_ADV_TYPE_NAME = const(0x09)
_ADV_TYPE_UUID16_COMPLETE = const(0x3)
_ADV_TYPE_UUID32_COMPLETE = const(0x5)
_ADV_TYPE_UUID128_COMPLETE = const(0x7)
_ADV_TYPE_UUID16_MORE = const(0x2)
_ADV_TYPE_UUID32_MORE = const(0x4)
_ADV_TYPE_UUID128_MORE = const(0x6)
_ADV_TYPE_APPEARANCE = const(0x19)
_ADV_TYPE_MANUFACTURER = const(0xFF)

def advertising_payload(limited_disc=False, br_edr=False, name=None, services=None, appearance=0, manufacturer=0):
	payload = bytearray()

	def _append(adv_type, value):
		nonlocal payload
		payload += struct.pack('BB', len(value) + 1, adv_type) + value

	_append(_ADV_TYPE_FLAGS, struct.pack('B', (0x01 if limited_disc else 0x02) + (0x00 if br_edr else 0x04))) #??? 0x18

	if name:
		_append(_ADV_TYPE_NAME, name)

	if services:
		for uuid in services:
			b = bytes(uuid)
			if len(b) == 2:
				_append(_ADV_TYPE_UUID16_COMPLETE, b)
			elif len(b) == 4:
				_append(_ADV_TYPE_UUID32_COMPLETE, b)
			elif len(b) == 16:
				_append(_ADV_TYPE_UUID128_COMPLETE, b)

	if appearance:
		# See org.bluetooth.characteristic.gap.appearance.xml
		_append(_ADV_TYPE_APPEARANCE, struct.pack('<h', appearance))

	if manufacturer:
		_append(_ADV_TYPE_MANUFACTURER, manufacturer)

	return payload

# ---------------------------------------------
#            BLE sensor service
# ---------------------------------------------
_IRQ_CENTRAL_CONNECT        = const(1)
_IRQ_CENTRAL_DISCONNECT     = const(2)
_IRQ_GATTS_WRITE            = const(3)
_IRQ_GATTS_READ_REQUEST     = const(4)

_ST_APP_UUID = bluetooth.UUID('00000000-0001-11E1-AC36-0002A5D5C51B')
# Temperature char: 0x04 (TEMPERATURE) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_TEMPERATURE_UUID = (bluetooth.UUID('00040000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char
# Humidity char: 0x08 (HUMIDITY) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_HUMIDITY_UUID = (bluetooth.UUID('00080000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char
# Pressure char: 0x10 (Pressure) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_PRESSURE_UUID = (bluetooth.UUID('00100000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char

# Environment char: 1C = 0x04 (TEMPERATURE) | 0x08 (Humidity) | 0x10 (Pressure)
# 00XX0000-0001-11E1-AC36-0002A5D5C51B
_ENVIRONMENTAL_UUID = (bluetooth.UUID('001C0000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY)

_LED_UUID = (bluetooth.UUID('20000000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_WRITE|bluetooth.FLAG_NOTIFY) # LED Char

_ST_APP_THPL_SERVICE = (_ST_APP_UUID, (_TEMPERATURE_UUID, _HUMIDITY_UUID, _PRESSURE_UUID, _LED_UUID))
_ST_APP_ENVL_SERVICE = (_ST_APP_UUID, (_ENVIRONMENTAL_UUID, _LED_UUID))

_PROTOCOL_VERSION   = const(0x01)
_DEVICE_ID          = const(0x80)
_FEATURE_MASK       = const(0x201C0000)   # Temperature (2^18) and Humidity(2^19) and Pressure (2^20) and LED (2^29)
_DEVICE_MAC         = [0x10, 0xE7, 0x7A, 0x78, 0x9A, 0xBC]
_MANUFACTURER       = pack('>BBI6B', _PROTOCOL_VERSION, _DEVICE_ID, _FEATURE_MASK, *_DEVICE_MAC)

led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)

class BLESensor:
	# NOTE: The name could be changed to be more easily recognize
	def __init__(self, ble, screen, name='WB55-BLE'):
		self._ble = ble
		self._ble.active(1)
		self._ble.irq(self._irq)
		self.screen = screen
		if USE_ENVIRONMENTAL:
			((self._environment_handle,self._led_handle),) = self._ble.gatts_register_services((_ST_APP_ENVL_SERVICE, ))
		else:
			((self._temperature_handle, self._humidity_handle, self._pressure_handle, self._led_handle),) = self._ble.gatts_register_services((_ST_APP_THPL_SERVICE, ))
		self._connections = set()
		self._payload = advertising_payload(name=name, manufacturer=_MANUFACTURER)
		self._advertise()
		self._handler = None

		self.state_disconnected = 0
		self.state_connected = 1
		self.switch_connected = 1
		self.switch_disconnected = 0
		self.state = self.state_disconnected
		self.switch = self.switch_disconnected
		# Display Mac address of BLE object
		dummy, byte_mac = self._ble.config('mac')
		mac_address = ':'.join([ '{:02x}'.format(byte_mac[ele]) for ele in range(0,6)])
		print("%s: MAC Adress: " % (name), mac_address)

	def get_state(self):
		return self.state
	def get_switch(self):
		return self.switch

	def _irq(self, event, data):
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _, = data
			self._connections.add(conn_handle)
			print("BLE: Connected")
			led_bleu.on()
			self.state = self.state_connected
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _, = data
			self._connections.remove(conn_handle)
			self._advertise()
			print("BLE: Disconnected")
			self.state = self.state_disconnected
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle, = data
			if conn_handle in self._connections and value_handle == self._led_handle:
				data_received = self._ble.gatts_read(self._led_handle)
				self._ble.gatts_write(self._led_handle, struct.pack('<HB', 1000, data_received[0]))
				self._ble.gatts_notify(conn_handle, self._led_handle)
				if data_received[0] == 1:
					print("BLE: SWITCH: led ON")
					led_rouge.on()
					self.switch_logo(fill=1)
					self.switch = self.switch_connected
				else:
					print("BLE: SWITCH: led OFF")
					led_rouge.off()
					self.switch_logo()
					self.switch = self.switch_disconnected

	def switch_logo(self, fill=None):
		self.screen.fill(0)
		if fill is not None:
			logo_pbm = load_image("logo_st.pbm")
			self.screen.blit(logo_pbm, 0, 0)
		self.screen.show()

	def set_data_temperature(self, timestamp, temperature, notify=False):
		self._ble.gatts_write(self._temperature_handle, struct.pack('<hh', timestamp, temperature * 10))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._temperature_handle)
	def set_data_humidity(self, timestamp, humidity, notify=False):
		self._ble.gatts_write(self._humidity_handle, struct.pack('<hh', timestamp, humidity * 10))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._humidity_handle)
	def set_data_pressure(self, timestamp, pressure, notify=False):
		self._ble.gatts_write(self._pressure_handle, struct.pack('<hi', timestamp, pressure *100))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._pressure_handle)
	def set_data_environment(self, timestamp, temperature, pressure, humidity, notify=False):
		self._ble.gatts_write(self._environment_handle,
					struct.pack('<hihh', timestamp, pressure * 100, humidity*10, temperature * 10))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._environment_handle)

	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload)
		led_bleu.off()
class Sensors:
	def __init__(self, i2c):
		self.i2c = i2c
		self.temperature_humidity = hts221.HTS221(i2c)
		self.pressure      = LPS22.LPS22(i2c)
		self.accelerometer_sensor = LSM6DSO.LSM6DSO(i2c)

		self.ORIENTATION_LEFT_UP        = 0
		self.ORIENTATION_RIGHT_UP       = 1
		self.ORIENTATION_VERTICAL_DOWN  = 2
		self.ORIENTATION_VERTICAL_UP    = 3
		self.ORIENTATION_NORMAL         = 4

	def get_temperature(self):
		return int(self.temperature_humidity.get()[0])
	def get_humidity(self):
		return int(self.temperature_humidity.get()[1])
	def get_pressure(self):
		return int(self.pressure.pressure())
	def get_altitude(self):
		return int(self.pressure.altitude())
	def get_accel_raw(self):
		return self.accelerometer_sensor.get_a_raw()
	def isPresentOnI2C(self, i2c2_list, id_tofound):
		result = 0
		if i2c2_list is None:
			local_i2clist = self.i2c.scan()
		else:
			local_i2clist = i2c2_list
		for i in local_i2clist:
			#print("test %d %d" % (i, id_tofound))
			if i == id_tofound:
				return 1
		return result

	# Acceleromter
	def calculate_imu(self, accel):
		local_accel = accel
		if accel is None:
			local_accel = self.accelerometer_sensor.get_a_raw()
		x = local_accel[0]
		y = local_accel[1]
		z = local_accel[2]
		if x == 0 and y == 0 and z == 0:
			return [0, 0, 0]
		pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
		roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
		yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
		return [pitch, roll, yaw]

	def get_imu_orientation(self, accel):
		''' return value array:
			[ enum, string ]
		'''
		pitch, roll, yaw = self.calculate_imu(accel)
		if abs(pitch) > 50:
			''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
			if pitch > 0:
				return [self.ORIENTATION_LEFT_UP, pitch, roll, "left-up"]
			else:
				return [self.ORIENTATION_RIGHT_UP, pitch, roll, "right-up"]
		elif abs(roll) > 50:
			'''
				ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN :
					ORIENTATION_VERTICAL_UP;'''
			if roll < 0:
				return [self.ORIENTATION_VERTICAL_DOWN, pitch, roll, "bottom-down"]
			else:
				return [self.ORIENTATION_VERTICAL_UP, pitch, roll, "top-up"]
		else:
			return [self.ORIENTATION_NORMAL, pitch, roll, "normal"]

class DisplayOnScreen:
	def __init__(self, display):
		self.display = display
		self.remaing_scren_display = 0
		self.DISPLAYED_TIME = 2000

	def dec(self):
		#print("[DEBUG]: request to dec remaining = %d" % self.remaing_scren_display)
		if self.remaing_scren_display < 0:
			self.remaing_scren_display = 0
		elif self.remaing_scren_display == 0:
			self.remaing_scren_display = 0
		else:
			self.remaing_scren_display -= 500
	def inc(self, t):
		self.remaing_scren_display += t

	def reset_remaining(self):
		self.remaing_scren_display = 0

	def display_logo(self, ble_force_display=False):
		if ble_force_display:
			return
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			logo_pbm = load_image("logo_st.pbm")
			self.display.blit(logo_pbm, 0, 0)

			self.display.show()

	def display_clear(self):
		#print("[DEBUG]: remaining = %d" % self.remaing_scren_display)
		self.dec()
		if self.remaing_scren_display > 0:
			return
		if self.display is not None:
			self.display.fill(0)
			self.display.show()


	def display_temperature_humidity(self, temp, hum, ble_force_display):
		if ble_force_display:
			return
		#print("[DEBUG]: remaining = %d" % self.remaing_scren_display)
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			temperature_pbm = load_image("climate.pbm")
			self.display.blit(temperature_pbm, 0, 16)

			self.display.text('{:14s}'.format('Temperature:'), 32, 0)
			self.display.text('{:^14s}'.format(str(temp) + 'C'), 16, 16)

			self.display.text('{:14s}'.format('Humidity:'), 32, 32)
			self.display.text('{:^14s}'.format(str(hum) + '%'), 16, 48)

			self.display.show()

	def display_temperature(self, temp, ble_force_display):
		#print("[DEBUG]: remaining = %d" % self.remaing_scren_display, " Force BLE:",  ble_force_display)
		if ble_force_display:
			return
		#print("[DEBUG]: remaining = %d" % self.remaing_scren_display)
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			temperature_pbm = load_image("climate.pbm")
			self.display.blit(temperature_pbm, 0, 16)

			self.display.text('{:14s}'.format('Temperature:'), 32, 0)
			self.display.text('{:^14s}'.format(str(temp) + 'C'), 16, 16)

			self.display.show()

	def display_humidity(self, hum, ble_force_display):
		#print("[DEBUG]: remaining = %d" % self.remaing_scren_display, " Force BLE:",  ble_force_display)
		if ble_force_display:
			return
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			temperature_pbm = load_image("climate.pbm")
			self.display.blit(temperature_pbm, 0, 16)

			self.display.text('{:14s}'.format('Humidity:'), 32, 0)
			self.display.text('{:^14s}'.format(str(hum) + '%'), 16, 16)

			self.display.show()

	def display_pressure_altitude(self, press, alt, ble_force_display):
		if ble_force_display:
			return
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			pressure_pbm = load_image("pressure.pbm")
			self.display.blit(pressure_pbm, 0, 16)

			self.display.text('{:18s}'.format('Pressure:'), 36, 0)
			self.display.text('{:^14s}'.format('{:.1f}'.format(press) + 'hPa'), 18, 16)

			self.display.text('{:18s}'.format('Altitude:'), 36, 32)
			self.display.text('{:^14s}'.format('{:.1f}'.format(alt) + 'M'), 18, 48)

			self.display.show()

	def display_pressure(self, press, ble_force_display):
		if ble_force_display:
			return
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			pressure_pbm = load_image("pressure.pbm")
			self.display.blit(pressure_pbm, 0, 16)

			self.display.text('{:18s}'.format('Pressure:'), 36, 0)
			self.display.text('{:^14s}'.format('{:.1f}'.format(press) + 'hPa'), 18, 16)

			self.display.show()

	def display_altitude(self, alt, ble_force_display):
		if ble_force_display:
			return
		if self.remaing_scren_display > 0:
			return
		self.inc(self.DISPLAYED_TIME)
		if self.display is not None:
			self.display.fill(0)

			pressure_pbm = load_image("pressure.pbm")
			self.display.blit(pressure_pbm, 0, 16)

			self.display.text('{:18s}'.format('Altitude:'), 36, 0)
			self.display.text('{:^14s}'.format('{:.1f}'.format(alt) + 'M'), 18, 16)

			self.display.show()
# ---------------------------------------------
#                  main
# ---------------------------------------------
i2c1 = I2C(1)
i2c1_list = []

if __name__ == '__main__':
	# scan i2c bus to see which ip components are present
	i2c1_list = i2c1.scan()
	print(i2c1_list)
	# wait to be sure all the component are present
	time.sleep_ms(1000)
	# init sensor
	sensors = Sensors(i2c1)
	# init display
	display = None
	if sensors.isPresentOnI2C(i2c1_list, 60):
		display = ssd1306.SSD1306_I2C(SCREEN_WIDTH, SCREEN_HEIGHT, i2c1)

	screen_displayed = DisplayOnScreen(display)
	screen_displayed.display_logo()

	# init bluetooth
	ble = bluetooth.BLE()
	ble_device = BLESensor(ble, display, name='WB55-MPY-001')

	print("BLESensor initialized: ")
	while True:
		timestamp = time.time()

		humidity = sensors.get_humidity()
		temperature = sensors.get_temperature()
		altitude = sensors.get_altitude()
		pressure = sensors.get_pressure()

		# BLUETOOTH management
		if ble_device.get_state() == ble_device.state_connected:
			if USE_ENVIRONMENTAL:
				ble_device.set_data_environment(timestamp, temperature, pressure, humidity, notify=1)
			else:
				ble_device.set_data_temperature(timestamp, temperature, notify=1)
				ble_device.set_data_pressure(timestamp, pressure, notify=1)
				ble_device.set_data_humidity(timestamp, humidity, notify=1)

		ble_force_display = False
		if ble_device.get_switch() == ble_device.switch_connected:
			ble_force_display = True
		imu = sensors.get_imu_orientation(None)
		if imu[0] == sensors.ORIENTATION_LEFT_UP:
			# display humidity
			print("Humidite: %s %s" % (str(humidity), "%") )
			screen_displayed.display_humidity(humidity, ble_force_display)
			time.sleep_ms(1000)
		elif imu[0] == sensors.ORIENTATION_RIGHT_UP:
			# display temperature
			print("Temperature: %s C" % str(temperature) )
			screen_displayed.display_temperature(temperature, ble_force_display)
			time.sleep_ms(1000)
		elif imu[0] == sensors.ORIENTATION_VERTICAL_DOWN:
			# display altitude
			print("Altitude: %s M" % str(altitude) )
			screen_displayed.display_altitude(altitude, ble_force_display)
			time.sleep_ms(1000)
		elif imu[0] == sensors.ORIENTATION_VERTICAL_UP:
			# display pressure
			print("Pressure: %s hPa" % str(pressure) )
			screen_displayed.display_pressure(pressure, ble_force_display)
			time.sleep_ms(1000)
		if ble_device.get_state() == ble_device.state_connected:
			if ble_device.get_switch() == ble_device.switch_connected:
				screen_displayed.reset_remaining()
			else:
				screen_displayed.display_clear()
		else:
			screen_displayed.display_clear()

		time.sleep_ms(500)
