from micropython import const
import bluetooth
from machine import I2C, Pin
import struct
from struct import *
from micropython import const
import pyb

# sensor
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22 sensor is connected on I2C1 of arduino connector
import LPS22

import time
import math

# ----------------------------------------
# ----------------------------------------
# advertising
_ADV_TYPE_FLAGS = const(0x01)
_ADV_TYPE_NAME = const(0x09)
_ADV_TYPE_UUID16_COMPLETE = const(0x3)
_ADV_TYPE_UUID32_COMPLETE = const(0x5)
_ADV_TYPE_UUID128_COMPLETE = const(0x7)
_ADV_TYPE_UUID16_MORE = const(0x2)
_ADV_TYPE_UUID32_MORE = const(0x4)
_ADV_TYPE_UUID128_MORE = const(0x6)
_ADV_TYPE_APPEARANCE = const(0x19)
_ADV_TYPE_MANUFACTURER = const(0xFF)

def advertising_payload(limited_disc=False, br_edr=False, name=None, services=None, appearance=0, manufacturer=0):
	payload = bytearray()

	def _append(adv_type, value):
		nonlocal payload
		payload += struct.pack('BB', len(value) + 1, adv_type) + value

	_append(_ADV_TYPE_FLAGS, struct.pack('B', (0x01 if limited_disc else 0x02) + (0x00 if br_edr else 0x04))) #??? 0x18

	if name:
		_append(_ADV_TYPE_NAME, name)

	if services:
		for uuid in services:
			b = bytes(uuid)
			if len(b) == 2:
				_append(_ADV_TYPE_UUID16_COMPLETE, b)
			elif len(b) == 4:
				_append(_ADV_TYPE_UUID32_COMPLETE, b)
			elif len(b) == 16:
				_append(_ADV_TYPE_UUID128_COMPLETE, b)

	if appearance:
		_append(_ADV_TYPE_APPEARANCE, struct.pack('<h', appearance))

	if manufacturer:
		_append(_ADV_TYPE_MANUFACTURER, manufacturer)

	return payload

# ---------------------------------------------
#            BLE sensor service
# ---------------------------------------------
_IRQ_CENTRAL_CONNECT        = const(1)
_IRQ_CENTRAL_DISCONNECT     = const(2)
_IRQ_GATTS_WRITE            = const(3)
_IRQ_GATTS_READ_REQUEST     = const(4)

_ST_APP_UUID = bluetooth.UUID('00000000-0001-11E1-AC36-0002A5D5C51B')
# Temperature char: 0x04 (TEMPERATURE) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_TEMPERATURE_UUID = (bluetooth.UUID('00040000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char
# Humidity char: 0x08 (HUMIDITY) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_HUMIDITY_UUID = (bluetooth.UUID('00080000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char
# Pressure char: 0x10 (Pressure) 00XX0000-0001-11E1-AC36-0002A5D5C51B)
_PRESSURE_UUID = (bluetooth.UUID('00100000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_NOTIFY) #Temperature Char

_LED_UUID = (bluetooth.UUID('20000000-0001-11E1-AC36-0002A5D5C51B'), bluetooth.FLAG_WRITE|bluetooth.FLAG_NOTIFY) # LED Char

_ST_APP_SERVICE = (_ST_APP_UUID, (_TEMPERATURE_UUID, _HUMIDITY_UUID, _PRESSURE_UUID, _LED_UUID))

_PROTOCOL_VERSION   = const(0x01)
_DEVICE_ID          = const(0x80)
_FEATURE_MASK       = const(0x201C0000)   # Temperature (2^18) and Humidity(2^19) and Pressure (2^20) and LED (2^29)
_DEVICE_MAC         = [0x10, 0xE7, 0x7A, 0x78, 0x9A, 0xBC]
_MANUFACTURER       = pack('>BBI6B', _PROTOCOL_VERSION, _DEVICE_ID, _FEATURE_MASK, *_DEVICE_MAC)

led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)

class BLESensor:
	# NOTE: The name could be changed to be more easily recognize
	def __init__(self, ble, name='WB55-BLE'):
		self._ble = ble
		self._ble.active(1)
		self._ble.irq(self._irq)
		((self._temperature_handle, self._humidity_handle, self._pressure_handle, self._led_handle),) = self._ble.gatts_register_services((_ST_APP_SERVICE, ))
		self._connections = set()
		self._payload = advertising_payload(name=name, manufacturer=_MANUFACTURER)
		self._advertise()
		self._handler = None
		# Display Mac address of BLE object
		dummy, byte_mac = self._ble.config('mac')
		mac_address = ':'.join([ '{:02x}'.format(byte_mac[ele]) for ele in range(0,6)])
		print("%s: MAC Adress: " % (name), mac_address)

	def _irq(self, event, data):
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _, = data
			self._connections.add(conn_handle)
			print("Connected")
			led_bleu.on()
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _, = data
			self._connections.remove(conn_handle)
			self._advertise()
			print("Disconnected")
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle, = data
			if conn_handle in self._connections and value_handle == self._led_handle:
				data_received = self._ble.gatts_read(self._led_handle)
				self._ble.gatts_write(self._led_handle, struct.pack('<HB', 1000, data_received[0]))
				self._ble.gatts_notify(conn_handle, self._led_handle)
				if data_received[0] == 1:
					led_rouge.on()
				else:
					led_rouge.off()

	def set_data_temperature(self, timestamp, temperature, notify=False):
		self._ble.gatts_write(self._temperature_handle, struct.pack('<hh', timestamp, temperature * 10))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._temperature_handle)
	def set_data_humidity(self, timestamp, humidity, notify=False):
		self._ble.gatts_write(self._humidity_handle, struct.pack('<hh', timestamp, humidity * 10))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._humidity_handle)
	def set_data_pressure(self, timestamp, pressure, notify=False):
		self._ble.gatts_write(self._pressure_handle, struct.pack('<hi', timestamp, pressure *100))
		if notify:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._pressure_handle)

	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload)
		led_bleu.off()


class Sensors:
	def __init__(self, i2c):
		self.temperature_humidity = hts221.HTS221(i2c)
		self.pressure      = LPS22.LPS22(i2c)

	def get_temperature(self):
		return int(self.temperature_humidity.get()[0])
	def get_humidity(self):
		return int(self.temperature_humidity.get()[1])
	def get_pressure(self):
		return int(self.pressure.pressure())

# ---------------------------------------------
#                  main
# ---------------------------------------------
i2c1 = I2C(1)
if __name__ == '__main__':
	# scan i2c bus to see which ip components are present
	print(i2c1.scan())
	# wait to be sure all the component are present
	time.sleep_ms(1000)
	# init sensor
	sensors = Sensors(i2c1)

	ble = bluetooth.BLE()
	ble_device = BLESensor(ble, name='WB55-MPY-001')

	print("BLESensor initialized: ")
	while True:
		timestamp = time.time()
		ble_device.set_data_temperature(timestamp, sensors.get_temperature(), notify=1)
		ble_device.set_data_pressure(timestamp, sensors.get_pressure(), notify=1)
		ble_device.set_data_humidity(timestamp, sensors.get_humidity(), notify=1)
		time.sleep_ms(1000)
