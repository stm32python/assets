from machine import I2C, Pin
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22: Pressure
import LPS22
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO

import time
import math

# ---------------------------------
# Global variables
# sensors are connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# Function
def calculate_imu(accel):
	if (accel is None):
		return [ 0, 0, 0]
	x = accel[0]
	y = accel[1]
	z = accel[2]
	if x == 0 and y == 0 and z == 0:
		return [0, 0, 0]
	pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
	roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	return [pitch, roll, yaw]

ORIENTATION_LEFT_UP = 0
ORIENTATION_RIGHT_UP = 1
ORIENTATION_VERTICAL_DOWN = 2
ORIENTATION_VERTICAL_UP = 3
ORIENTATION_NORMAL = 4

def get_imu_orientation(accel):
	''' return value array:
	[ enum, string ]
	'''
	pitch, roll, yaw = calculate_imu(accel)
	if abs(pitch) > 50:
		''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
		if pitch > 0:
			return [ORIENTATION_LEFT_UP, pitch, roll, "left-up"]
		else:
			return [ORIENTATION_RIGHT_UP, pitch, roll, "right-up"]
	elif abs(roll) > 50:
		'''
		   ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN :
		   ORIENTATION_VERTICAL_UP;'''
		if roll < 0:
			return [ORIENTATION_VERTICAL_DOWN, pitch, roll, "bottom-down"]
		else:
			return [ORIENTATION_VERTICAL_UP, pitch, roll, "top-up"]
	else:
		return [ORIENTATION_NORMAL, pitch, roll, "normal"]

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	# initialisation of sensor
	temperature_sensor = hts221.HTS221(i2c1)
	pression_sensor = LPS22.LPS22(i2c1)
	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	while(1):
		# Display accelerometer information
		accel = accelerometer_sensor.get_a_raw()
		#print("[DEBUG]: Accelerometer: ", accel)
		#print("[DEBUG]:  X: ", accel[0])
		#print("[DEBUG]:  Y: ", accel[1])
		#print("[DEBUG]:  Z: ", accel[2])

		imu = get_imu_orientation(accel)
		if imu[0] == ORIENTATION_LEFT_UP:
			# display humidity
			humidity = temperature_sensor.get()[1]
			print("Humidite: %s %s" % (str(humidity),"%") )
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_RIGHT_UP:
			# display temperature
			temperature = temperature_sensor.get()[0]
			print("Temperature: %s C" % str(temperature) )
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_DOWN:
			# display altitude
			altitude = pression_sensor.altitude()
			print("Altitude: %s M" % str(altitude) )
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_UP:
			# display pressure
			pression = pression_sensor.pressure()
			print("Pressure: %s hPa" % str(pression) )
			time.sleep_ms(1000)
		else:
			time.sleep_ms(5000)
