from machine import I2C, Pin
# HTS221: Temperature / Humidity sensor
import hts221
import time

# ---------------------------------
# Global variables
# HTS221 sensor is connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# ---------------------------------
#                  Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)
	temperature_sensor = hts221.HTS221(i2c1)
	# Display temperature
	temperature = temperature_sensor.get()[0]
	print("Temperature: %s C" % str(temperature) )
