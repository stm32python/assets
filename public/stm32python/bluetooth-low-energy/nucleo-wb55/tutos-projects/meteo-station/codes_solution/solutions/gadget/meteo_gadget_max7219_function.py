from machine import I2C, SPI, Pin
import framebuf
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22: Pressure
import LPS22
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO
# Max7219: led matrice
import max7219

import time
import math

# ---------------------------------
# Global variables
# sensors are connected on I2C1 of arduino connector
i2c1 = I2C(1)
i2c1_list = []

# led matrice is connected on SPI1
spi1 = SPI(1)
led_matrice_cs = Pin('D10')

# ---------------------------------
# Function
def calculate_imu(accel):
	if (accel is None):
		return [ 0, 0, 0]
	x = accel[0]
	y = accel[1]
	z = accel[2]
	if x == 0 and y == 0 and z == 0:
		return [0, 0, 0]
	pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
	roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	return [pitch, roll, yaw]

ORIENTATION_LEFT_UP = 0
ORIENTATION_RIGHT_UP = 1
ORIENTATION_VERTICAL_DOWN = 2
ORIENTATION_VERTICAL_UP = 3
ORIENTATION_NORMAL = 4

def get_imu_orientation(accel):
	''' return value array:
	[ enum, string ]
	'''
	pitch, roll, yaw = calculate_imu(accel)
	if abs(pitch) > 50:
		''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
		if pitch > 0:
			return [ORIENTATION_LEFT_UP, pitch, roll, "left-up"]
		else:
			return [ORIENTATION_RIGHT_UP, pitch, roll, "right-up"]
	elif abs(roll) > 50:
		'''
		   ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN :
		   ORIENTATION_VERTICAL_UP;'''
		if roll < 0:
			return [ORIENTATION_VERTICAL_DOWN, pitch, roll, "bottom-down"]
		else:
			return [ORIENTATION_VERTICAL_UP, pitch, roll, "top-up"]
	else:
		return [ORIENTATION_NORMAL, pitch, roll, "normal"]

# ---------------------------------
#	    Display
def display_logo(display):
	if display is not None:
		display.fill(0)

		logo_pbm = load_image("logo_st.pbm")
		display.blit(logo_pbm, 0, 0)

		display.show()

def display_temperature(display, temp):
	if display is not None:
		display.fill(0)
		display.text('{:.1f}'.format(temp), 0, 0, 1)
		display.show()

def display_humidity(display, hum):
	if display is not None:
		display.fill(0)
		display.text('{:.1f}'.format(humidity), 0, 0, 1)
		display.show()

def display_pressure(display, press):
	if display is not None:
		display.fill(0)
		display.text('{:d}'.format(int(press)), 0, 0, 1)
		display.show()

def display_altitude(display, alt):
	if display is not None:
		display.fill(0)
		display.text('{:.1f}'.format(int(alt)), 0, 0, 1)
		display.show()

# ---------------------------------
#	    is present on I2C
def isPresentOnI2C(ic2_list, id_tofound):
	result = 0
	for i in ic2_list:
		if i == id_tofound:
			return 1

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	i2c1_list = i2c1.scan()
	print(i2c1_list)

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	# initialization of sensor
	temperature_sensor = hts221.HTS221(i2c1)
	pression_sensor = LPS22.LPS22(i2c1)
	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	# initialization of screen
	screen = max7219.Max7219(32,8, spi1, led_matrice_cs)

	while(1):
		# Display accelerometer information
		accel = accelerometer_sensor.get_a_raw()
		#print("[DEBUG]: Accelerometer: ", accel)
		#print("[DEBUG]:  X: ", accel[0])
		#print("[DEBUG]:  Y: ", accel[1])
		#print("[DEBUG]:  Z: ", accel[2])

		humidity = temperature_sensor.get()[1]
		temperature = temperature_sensor.get()[0]
		altitude = pression_sensor.altitude()
		pression = pression_sensor.pressure()

		imu = get_imu_orientation(accel)
		if imu[0] == ORIENTATION_LEFT_UP:
			# display humidity
			print("Humidite: %s %s" % (str(humidity), "%") )
			display_humidity(screen, humidity)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_RIGHT_UP:
			# display temperature
			print("Temperature: %s C" % str(temperature) )
			display_temperature(screen, temperature)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_DOWN:
			# display altitude
			print("Altitude: %s M" % str(altitude) )
			display_altitude(screen, altitude)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_UP:
			# display pressure
			print("Pressure: %s hPa" % str(pression) )
			display_pressure(screen, pression)
			time.sleep_ms(1000)
		else:
			time.sleep_ms(5000)
