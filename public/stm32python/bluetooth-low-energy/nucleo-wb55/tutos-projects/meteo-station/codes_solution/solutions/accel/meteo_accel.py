from machine import I2C, Pin
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO

import time

# ---------------------------------
# Global variables
# LSM6DSO sensor is connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	# Display accelerometer information
	accel = accelerometer_sensor.get_a_raw()
	print("Accelerometer: ", accel)
	print("  X: ", accel[0])
	print("  Y: ", accel[1])
	print("  Z: ", accel[2])
