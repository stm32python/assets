from machine import I2C, Pin
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO

import time
import math

# ---------------------------------
# Global variables
# LSM6DSO sensor is connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	while(1):
		# Display accelerometer information
		accel = accelerometer_sensor.get_a_raw()
		#print("Accelerometer: ", accel)
		#print("  X: ", accel[0])
		#print("  Y: ", accel[1])
		#print("  Z: ", accel[2])

		# Calcul IMU
		x = accel[0]
		y = accel[1]
		z = accel[2]
		if x == 0 and y == 0 and z == 0:
			pitch = 0
			roll = 0
			yaw = 0
		else:
			pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
			roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
			yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
		#print("[DEBUG]: Pitch: ", pitch)
		#print("[DEBUG]: Roll: ", roll)
		#print("[debug]: yaw: ", yaw)

		# display orientation
		if abs(pitch) > 50:
			''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
			if pitch > 0:
				print("left-up")
			else:
				print("right-up")
		elif abs(roll) > 50:
			'''ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN : ORIENTATION_VERTICAL_UP;'''
			if roll < 0:
				print("bottom-down")
			else:
				print("top-up")
		else:
			print("normal")

		time.sleep_ms(1000)
