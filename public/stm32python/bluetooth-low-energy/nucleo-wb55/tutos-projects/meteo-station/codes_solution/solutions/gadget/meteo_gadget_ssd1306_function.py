from machine import I2C, Pin
import framebuf
# HTS221: Temperature / Humidity sensor
import hts221
# LPS22: Pressure
import LPS22
# LSM6DSO: Accelerometer / Gyroscope sensor
import LSM6DSO
# SSD1306: ecran oled
import ssd1306

import time
import math

# ---------------------------------
# Global variables
# sensors are connected on I2C1 of arduino connector
i2c1 = I2C(1)
i2c1_list = []

# screen size
SCREEN_WIDTH = 128
SCREEN_HEIGHT = 64

# ---------------------------------------------
# Pictures
#
def load_image(filename):
	with open(filename, 'rb') as f:
		f.readline()
		width, height = [int(v) for v in f.readline().split()]
		data = bytearray(f.read())
	return framebuf.FrameBuffer(data, width, height, framebuf.MONO_HLSB)

# ---------------------------------
# Function
def calculate_imu(accel):
	if (accel is None):
		return [ 0, 0, 0]
	x = accel[0]
	y = accel[1]
	z = accel[2]
	if x == 0 and y == 0 and z == 0:
		return [0, 0, 0]
	pitch = round(math.atan(x / math.sqrt(y * y + z * z)) * (180.0 / math.pi))
	roll  = round(math.atan(y / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	yaw   = round(math.atan(z / math.sqrt(x * x + z * z)) * (180.0 / math.pi))
	return [pitch, roll, yaw]

ORIENTATION_LEFT_UP = 0
ORIENTATION_RIGHT_UP = 1
ORIENTATION_VERTICAL_DOWN = 2
ORIENTATION_VERTICAL_UP = 3
ORIENTATION_NORMAL = 4

def get_imu_orientation(accel):
	''' return value array:
	[ enum, string ]
	'''
	pitch, roll, yaw = calculate_imu(accel)
	if abs(pitch) > 50:
		''' (pitch > 0) ? ORIENTATION_LEFT_UP : ORIENTATION_RIGHT_UP;'''
		if pitch > 0:
			return [ORIENTATION_LEFT_UP, pitch, roll, "left-up"]
		else:
			return [ORIENTATION_RIGHT_UP, pitch, roll, "right-up"]
	elif abs(roll) > 50:
		'''
		   ret = (roll < 0) ? ORIENTATION_VERTICAL_DOWN :
		   ORIENTATION_VERTICAL_UP;'''
		if roll < 0:
			return [ORIENTATION_VERTICAL_DOWN, pitch, roll, "bottom-down"]
		else:
			return [ORIENTATION_VERTICAL_UP, pitch, roll, "top-up"]
	else:
		return [ORIENTATION_NORMAL, pitch, roll, "normal"]

# ---------------------------------
#	    Display
def display_logo(display):
	if display is not None:
		display.fill(0)

		logo_pbm = load_image("logo_st.pbm")
		display.blit(logo_pbm, 0, 0)
		display.show()

def display_temperature_humidity(display, temp, hum):
	if display is not None:
		display.fill(0)

		temperature_pbm = load_image("climate.pbm")
		display.blit(temperature_pbm, 0, 16)

		display.text('{:14s}'.format('Temperature:'), 32, 0)
		display.text('{:^14s}'.format(str(temp) + 'C'), 16, 16)

		display.text('{:14s}'.format('Humidity:'), 32, 32)
		display.text('{:^14s}'.format(str(hum) + '%'), 16, 48)
		display.show()

def display_pressure(display, press, alt):
	if display is not None:
		display.fill(0)

		pressure_pbm = load_image("pressure.pbm")
		display.blit(pressure_pbm, 0, 16)

		display.text('{:18s}'.format('Pressure:'), 36, 0)
		display.text('{:^14s}'.format('{:.1f}'.format(press) + 'hPa'), 18, 16)

		display.text('{:18s}'.format('Altitude:'), 36, 32)
		display.text('{:^14s}'.format('{:.1f}'.format(alt) + 'M'), 18, 48)
		display.show()

# ---------------------------------
#	    is present on I2C
def isPresentOnI2C(ic2_list, id_tofound):
	result = 0
	for i in ic2_list:
		if i == id_tofound:
			return 1

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	i2c1_list = i2c1.scan()
	print(i2c1_list)

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)

	# initialization of sensor
	temperature_sensor = hts221.HTS221(i2c1)
	pression_sensor = LPS22.LPS22(i2c1)
	accelerometer_sensor = LSM6DSO.LSM6DSO(i2c1)

	# initialization of screen
	screen = None
	if isPresentOnI2C(i2c1_list, 60):
		screen = ssd1306.SSD1306_I2C(SCREEN_WIDTH, SCREEN_HEIGHT, i2c1)

	# display ST logo
	display_logo(screen)
	time.sleep_ms(1000)

	while(1):
		# Display accelerometer information
		accel = accelerometer_sensor.get_a_raw()
		#print("[DEBUG]: Accelerometer: ", accel)
		#print("[DEBUG]:  X: ", accel[0])
		#print("[DEBUG]:  Y: ", accel[1])
		#print("[DEBUG]:  Z: ", accel[2])

		humidity = temperature_sensor.get()[1]
		temperature = temperature_sensor.get()[0]
		altitude = pression_sensor.altitude()
		pression = pression_sensor.pressure()

		imu = get_imu_orientation(accel)
		if imu[0] == ORIENTATION_LEFT_UP:
			# display humidity
			print("Humidite: %s %s" % (str(humidity), "%") )
			display_temperature_humidity(screen, temperature, humidity)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_RIGHT_UP:
			# display temperature
			print("Temperature: %s C" % str(temperature) )
			display_temperature_humidity(screen, temperature, humidity)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_DOWN:
			# display altitude
			print("Altitude: %s M" % str(altitude) )
			display_pressure(screen, pression, altitude)
			time.sleep_ms(1000)
		elif imu[0] == ORIENTATION_VERTICAL_UP:
			# display pressure
			print("Pressure: %s hPa" % str(pression) )
			display_pressure(screen, pression, altitude)
			time.sleep_ms(1000)
		else:
			time.sleep_ms(5000)
