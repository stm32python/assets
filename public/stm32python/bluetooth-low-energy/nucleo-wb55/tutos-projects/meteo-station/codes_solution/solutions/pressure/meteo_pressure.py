from machine import I2C, Pin
# LPS22: Pressure
import LPS22
import time
# ---------------------------------
# Global variables
# LPS22 sensor is connected on I2C1 of arduino connector
i2c1 = I2C(1)

# ---------------------------------
# ---------------------------------
#		Main
if __name__ == "__main__":
	# Scan i2c bus to see what are present
	print(i2c1.scan())

	# Workaround for main.py:
	# need to wait that i2c bus component are initialise
	time.sleep_ms(1000)
	pression_sensor = LPS22.LPS22(i2c1)

	# Display Pressure
	pression = pression_sensor.pressure()
	print("Humidite: %s hPa" % str(pression) )
