# Common assets for STM32Python linguistic sites

* [Italian site](https://stm32python.gitlab.io/it)
* [English site](https://stm32python.gitlab.io/en)
* [French site](https://stm32python.gitlab.io/fr)

